<?php

/**
 * 工具库-日志
 */
namespace extend;
use framework\Flight;

class Log {

	private $default_file_size = '1024000'; //默认日志文件大小
	private $log_dir           =  RUNTIME_PATH.'log'; //默认日志文件大小

	/**
	 * 写日志-直接写入日志文件或者邮件
	 * @param  string  $message  日志信息
	 * @param  string  $log_type 日志类型   ERROR  WARN  DEBUG  INFO
	 * @return
	 */
	public function write($message,$name = 'log',$log_type = 'DEBUG') {
		$log_path = $this->get_file_log_name($name);
		if(is_file($log_path) && ($this->default_file_size < filesize($log_path)) ) {
			rename($log_path, dirname($log_path).'/'.time().'-Bak-'.basename($log_path));
		}
		$message = $this->get_message($message, $log_type);
		error_log($message, 3, $log_path, '');
	}

	/**
	 * 写日志-获取文件日志名称
	 * @return string
	 */
	private function get_file_log_name($name = null) {
		$log_path =  RUNTIME_PATH.'log'.DS;
		if(self::createFolders($log_path)){
			return $log_path.$this->_errorLogFileName($name);
		}else{
			Flight::halt(500,'文件路径不存在或创建失败');
		}
	}

    /**
     * 创建文件夹
     *
     * @param string $path
     * @param int $mode
     */
    static function createFolders($path, $mode = 0755){
        if (!is_dir($path)) {
            return mkdir($path, $mode, true);
		}
		return true;
    }

	/**
	 * 写日志-组装message信息
	 * @param  string  $message  日志信息
	 * @param  string  $log_type 日志类型
	 * @return string
	 */
	private function get_message($message, $log_type) {
		return  date("Y-m-d H:i:s") . " [{$log_type}] : {$message}\r\n";
	}

	/**
	 *
	 * @return string
	 */
	private function _errorLogFileName($name = 'log'){
		return $name."_".date('Y-m-d').'.log';
	}
}
