<?php

/**
 * SESSION类
 */

namespace extend;


class Session{
    
    /**
     * 加解密默认key
     *
     * @var string
     */
    protected $key;

    function __construct($key = ''){
        if ($key) {
            $this->key = $key;
        }
        if (!isset($_SESSION)) {
            session_start();
        }
    }

    /**
     * 设置session的值
     *
     * @param $key
     * @param $value
     * @param int $exp
     * @return bool|mixed
     */
    function set($value, $exp = 86400){
        $_SESSION[$this->key] = $value;
        return true;
    }

    /**
     * 获取session的值
     *
     * @param $key
     * @param bool $de
     * @return mixed
     */
    function get(){
        if (false !== strpos($this->key, ':')) {
            list($v_key, $c_key) = explode(':', $this->key);
        } else {
            $v_key = $this->key;
        }

        $_result = $_SESSION[$v_key];
        if (!empty($c_key) && isset($_result[$c_key])) {
            return $_result[$c_key];
        }
        return $_result;
    }

    /**
     * 删除SESSION
     *
     * @param $key
     * @param bool $de
     * @return mixed
     */
    function del(){
        session_unset($this->key);
        unset($_SESSION[$this->key]);
        return true;
    }    
}
