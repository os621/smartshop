<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        入口文件
 */
//框架依赖的唯一常量
define('ROOT_PATH', __DIR__.DIRECTORY_SEPARATOR);
//引入框架.
require ROOT_PATH . 'framework/Flight.php';
//启动框架
\framework\Flight::run();