<?php 
return [
    //是否开启调试
    'DEBUG'  => true,
    //前台Cookie参数
    'COOKIE_PRE' => 'X2NC_',
    'COOKIE_KEY' => 'QnNydHRlY2guY29tL1NtYXJ0U2hvcA==',
    //设置时区
    'TIMEZONE' => 'PRC',
    //设置数据库
    'DB'=> [
        'DB_CACHE'   => 'DB_CACHE', //数据库缓存
        'DB_SLAVE'   => [],  //数据库主从
        //'DB_SLAVE' => [['DB_HOST' => '127.0.0.1'],['DB_HOST' => '127.0.0.2']],               
    ],
    //设置模板
    'TPL'=> [
        'TPL_CACHE' => 'TPL_CACHE', //模板缓存
        'TPL_EXTS'  => '.html', //模板类型
    ],
    'STORAGE'=>[
        'STORAGE_TYPE'=>'File',
    ],  
    'CACHE' => [
        'TPL_CACHE' => [
            'CACHE_TYPE' => 'FileCache',
            'CACHE_PATH' => RUNTIME_PATH . 'tpl_cache/',
            'GROUP'      => 'tpl',
            'HASH_DEEP'  => 0,
        ],
        
        'DB_CACHE' => [
            'CACHE_TYPE' => 'FileCache',
            'CACHE_PATH' => RUNTIME_PATH . 'db_cache/',
            'GROUP'      => 'db',
            'HASH_DEEP'  => 0,
        ],
    ], 
];