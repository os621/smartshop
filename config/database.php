<?php 
return [
    'DB' => [
        'DB_TYPE'    => 'MysqlPdo',
        'DB_HOST'    => 'localhost',
        'DB_USER'    => 'root',
        'DB_PWD'     => '123456',
        'DB_PORT'    => '3306',
        'DB_NAME'    => 'siteplus',
        'DB_CHARSET' => 'utf8',
        'DB_PREFIX'  => 'sp_',
    ]
];