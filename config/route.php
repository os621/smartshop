<?php 
return [
    'ROUTE' => [
        //默认首页
        '/'                     => 'index/index/index',
        '/list/@id:[0-9]+.html' => 'index/index/category',
        '/item/@id:[0-9]+.html' => 'index/index/item',
        '/so'                   => 'index/index/search',
        '/up_taobao'            => 'index/taobao/up_taobao',
        '/flash_taobao'         => 'index/taobao/flash_taobao',
        //移动版
        '/m'                      => 'mobile/index/index',
        '/m/cate'                 => 'mobile/index/cate',
        '/m/list/@id:[0-9]+.html' => 'mobile/index/category',
        '/m/item/@id:[0-9]+.html' => 'mobile/index/item',
        '/m/search'               => 'mobile/index/search',
        '/m/so'                   => 'mobile/index/so',
        '/m/passport'             => 'mobile/passport/index',
        '/m/passport/order'       => 'mobile/passport/order',
        '/m/passport/order_id/@id:[0-9]+' => 'mobile/passport/view',
        '/m/passport/address'             => 'mobile/passport/address',
        '/m/passport/index'               => 'mobile/passport/info',
        '/m/passport/info'                => 'mobile/passport/info',
        '/m/passport/auth'                => 'mobile/passport/auth',
        '/m/passport/message'             => 'mobile/passport/message',
        //购物车&订单支付
        '/cart'                      => 'index/cart/index',
        '/cart/order'                => 'index/order/cart',
        '/cart/order_id/@id:[0-9]+'  => 'index/order/pay',
        'POST /cart/dopay_order'     => 'index/order/dopay',
        '/dopay/notify_url/@payname' => 'index/notify/notify_url',
        '/dopay/return_url/@payname' => 'index/notify/return_url',
        '/m/cart'                      => 'mobile/cart/index',
        '/m/cart/order'                => 'mobile/order/cart',
        '/m/cart/order_id/@id:[0-9]+'  => 'mobile/order/pay',
        'POST /m/cart/dopay_order'     => 'mobile/order/dopay',
        //淘宝数据包
        '/taobao'                       => 'index/taobao/index',
        '/taobao/flash_taobao'          => 'index/taobao/flash_taobao',
        '/taobao/notify/@id:[0-9]+'     => 'index/taobao/notify',
        //用户登录
        '/user/login'                   => 'index/user/login',
        '/user/layerlogin'              => 'index/user/layerlogin',
        '/user/reg'                     => 'index/user/reg',
        '/m/user/login'                 => 'mobile/user/login',
        '/m/user/layerlogin'            => 'mobile/user/layerlogin',
        '/m/user/reg'                   => 'mobile/user/reg', 
        //文章
        '/article/@id:[0-9]+.html'      => 'index/article/article',
        '/channel/@id:[0-9]+.html'      => 'index/article/channel',
        '/service'                      => 'index/article/service',
        //用户中心
        '/passport'                     => 'passport/index/index',
        '/passport/order'               => 'passport/order/index',
        '/passport/order_id/@id:[0-9]+' => 'passport/order/view',
        '/passport/address'             => 'passport/address/index',
        '/passport/index'               => 'passport/info/index',
        '/passport/info'                => 'passport/info/info',
        '/passport/auth'                => 'passport/info/auth',
        '/passport/message'             => 'passport/message/index',
        //商家
        '/manage'                       => 'manage/index/index',
        //后台配置禁止修改
        '/admin'                        => 'admin/index/index',
        '/admin/login'                  => 'admin/index/login',
        '/admin/logout'                 => 'admin/index/logout',
    ]
];