<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2011, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */

namespace framework\util;

class Filter {

     /**
     * 安全过滤类-全局变量过滤
     * @return
     */
    public function filter() {
        if (is_array($_SERVER)) {
            foreach ($_SERVER as $k => $v) {
                if (isset($_SERVER[$k])) {
                    $_SERVER[$k] = str_replace(array('<','>','"',"'",'%3C','%3E','%22','%27','%3c','%3e'), '', $v);
                }
            }
        }
        unset($_ENV, $HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_COOKIE_VARS, $HTTP_SERVER_VARS, $HTTP_ENV_VARS);
        self::filter_slashes($_GET);
        self::filter_slashes($_POST);
        self::filter_slashes($_COOKIE);
        self::filter_slashes($_FILES);
        self::filter_slashes($_REQUEST);
    }

    /**
     * 安全过滤类-加反斜杠，防止SQL注入
     *  Controller中使用方法：$this->controller->filter_slashes(&$value)
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function filter_slashes(&$value) {
        if (get_magic_quotes_gpc()) return false; //开启魔术变量
        $value = (array) $value;
        foreach ($value as $key => $val) {
            if (is_array($val)) {
                self::filter_slashes($value[$key]);
            } else {
                $value[$key] = addslashes($val);
            }
        }
    }

    /**
     * 安全过滤类-通用数据过滤
     * @param string $value 需要过滤的变量
     * @return string|array
     */
    public function filter_escape($value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $v = self::filter_sql($v);
                $v = self::filter_script($v);
                $v = self::filter_str($v);
                $value[$k] = self::filter_html($v);
            }
        } else {
            $value = self::filter_sql($value);
            $value = self::filter_script($value);
            $value = self::filter_str($value);
            $value = self::filter_html($value);
        }
        return $value;
    }

    /**
     * 安全过滤类-过滤javascript,css,iframes,object等不安全参数 过滤级别高
     * @param  string $value 需要过滤的值
     * @return string
     */
    public function filter_script($value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = self::filter_script($v);
            }
            return $value;
        } else {
            $parten = array(
                "/(javascript:)?on(click|load|key|mouse|error|abort|move|unload|change|dblclick|move|reset|resize|submit)/i",
                "/<script(.*?)>(.*?)<\/script>/si",
                "/<iframe(.*?)>(.*?)<\/iframe>/si",
                "/<object.+<\/object>/isU"
                );
            $replace = array("\\2", "", "", "");
            $value = preg_replace($parten, $replace, $value, -1, $count);
            if ($count > 0) {
                $value = self::filter_script($value);
            }
            return $value;
        }
    }

    /**
     * 安全过滤类-过滤HTML标签
     * @param  string $value 需要过滤的值
     * @return string
     */
    public function filter_html($value) {
        if (function_exists('htmlspecialchars')) {
            return htmlspecialchars($value);
        }
        return str_replace(array("&", '"', "'", "<", ">"), array("&amp;", "&quot;", "&#039;", "&lt;", "&gt;"), $value);
    }

    /**
     * 安全过滤类-对进入的数据加下划线 防止SQL注入
     * @param  string $value 需要过滤的值
     * @return string
     */
    public function filter_sql($value) {
        $sql = array("select", 'insert', "update", "delete", "\'", "\/\*","\.\.\/", "\.\/", "union", "into", "load_file", "outfile");
        $sql_re = array("","","","","","","","","","","","");
        return str_replace($sql, $sql_re, $value);
    }


    /**
     * 安全过滤类-字符串过滤 过滤特殊有危害字符
     * @param  string $value 需要过滤的值
     * @return string
     */
    public function filter_str($value) {
        $value = str_replace(array("\0","%00","\r"), '', $value);
        $value = preg_replace(array('/[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]/','/&(?!(#[0-9]+|[a-z]+);)/is'), array('', '&amp;'), $value);
        $value = str_replace(array("%3C",'<'), '&lt;', $value);
        $value = str_replace(array("%3E",'>'), '&gt;', $value);
        $value = str_replace(array('"',"'","\t"), array('&quot;','&#39;',''), $value);
        return $value;
    }

    /**
     * 私有路径安全转化
     * @param string $fileName
     * @return string
     */
    public function filter_dir($fileName) {
        $tmpname = strtolower($fileName);
        $temp = array('://',"\0", "..");
        if (str_replace($temp, '', $tmpname) !== $tmpname) {
            return false;
        }
        return $fileName;
    }

    /**
     * 过滤目录
     * @param string $path
     * @return array
     */
    public function filter_path($path) {
        $path = str_replace(array("'",'#','=','`','$','%','&',';'), '', $path);
        return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/', '/', $path), '/');
    }

    /**
     * 过滤PHP标签
     * @param string $string
     * @return string
     */
    public function filter_phptag($string) {
        return str_replace(array('<?', '?>'), array('&lt;?', '?&gt;'), $string);
    }

    /**
     * 安全过滤类-返回函数
     * @param  string $value 需要过滤的值
     * @return string
     */
    public function str_out($value) {
        $badstr = array("&", '"', "'", "<", ">", "%3C", "%3E");
        $newstr = array("&amp;", "&quot;", "&#039;", "&lt;", "&gt;", "&lt;", "&gt;");
        $value  = str_replace($newstr, $badstr, $value);
        return stripslashes($value); //下划线
    }
}