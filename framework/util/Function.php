<?php
/**
 * Flight: An extensible micro-framework.
 * 公共函数库
 * @copyright   Copyright (c) 2011, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */
use framework\Flight;

/**
 * 访问URL
 * @param $url
 * @return url
 */
function url($diy_url = '',$params = array(),$is_host = false){ 
    $host = $is_host ? Flight::util()->getCurrentUrl().'/' : Flight::request()->base;
    $params   = array_filter($params);
    $route    = $diy_url ? $host.strtolower($diy_url) : $host.Flight::request()->url;
    $paramStr = empty($params) ? '' : '?' . http_build_query($params);
    return $route.$paramStr;
}

/**
 * 友好的调试打印方法
 * @param $var
 */
function dump($var, $exit = true){
    $output = print_r($var, true);
    $output = "<pre>" . htmlspecialchars($output,ENT_QUOTES)."</pre>";
    echo $output;
    if($exit) exit();
}

/**
 * 字符串截取
 * @param  [type]  $str    [要截取的字符串]
 * @param  [type]  $length [截取长度]
 * @param  integer $start  [开始位置]
 * @return [type]          [截取后的字符串]
 */
function len($str,$length,$start = 0){
    if(!empty($length)){
        return \extend\Util::msubstr($str,$start,$length);
    }else{
        return $str;
    }
}

/**
 * [get_client_ip 获取客户端IP]
 * @return [type] [IP地址]
 */
function get_client_ip(){
    return \extend\Util::getIp();
}

/**
 * 字符串比较函数
 * @param $haystack
 * @param $needle
 * @return bool
 */
function str_ab($haystack, $needle){
    return strncmp($haystack, $needle, strlen($needle)) === 0;
}

 /**
 * 显示友好时间格式
 * @param int $time 时间戳
 */
function ftime($time){
    return \extend\Util::ftime($time);
}

/* *
 * 获取随机值
 * @param $str
 * @param int $len 
 * @return string  
* */
function random_str($str,$len){
    return substr(md5(uniqid(rand()*strval($str))),0, (int) $len);
}

/**
 * 生成唯一的订单号 20110809111259232312
 * 2011-年日期
 * 08-月份
 * 09-日期
 * 11-小时
 * 12-分
 * 59-秒
 * 2323-微秒
 * 12-随机值
 * @return string
 */
function uuid_no(){
    list($usec, $sec) = explode(" ",microtime());
    $usec = substr(str_replace('0.','',$usec), 0 ,4);
    $str  = mt_rand(1000,9999);
    return date("YmdHis").$usec.$str;
}

