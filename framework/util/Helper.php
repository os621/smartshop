<?php

/**
 * Flight: An extensible micro-framework.
 * 系统函数库
 * @copyright   Copyright (c) 2011, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */
use framework\Flight;
/** 
 *   调用模型方法(支持跨APP调用)
 *  @var $params:string
 *  Flight::model('app/nameModel');
 */
Flight::map('model', function($params){
    $param  = explode('/', $params,2);
    $len    = count($param);
    $app    = $len == 1 ? APP_NAME : strtolower($param[0]);
    $module = $len == 1 ? ucfirst(strtolower($param[0])) : ucfirst(strtolower($param[1]));
    $ModelClass = "\\application\\{$app}\\model\\{$module}";
    if(!class_exists($ModelClass) ) {
        throw new \Exception("Model '{$ModelClass}' not found", 500);
    }
    return new $ModelClass();
});

 /** 
 *  调用API方法(支持跨APP调用)
 *  @var $params:string
 *  Flight::api('app/nameApi');
 */ 
Flight::map('api', function($params){
    $param = explode('/', $params, 2);
    $len   = count($param);
    $app   = $len == 1 ? APP_NAME : strtolower($param[0]);
    $api   = $len == 1 ? ucfirst(strtolower($param[0])) : ucfirst(strtolower($param[1]));
    $ApiClass   = "\\application\\{$app}\\api\\{$api}";
    if(!class_exists($ApiClass) ) {
        throw new \Exception("Api '{$ApiClass}' not found",500);
    }
    return new $ApiClass();
});

/** 
 *  调用指定APP的方法(支持跨APP调用)
 *  @var $app:string
 *  Flight::config('APP');
 */ 
Flight::map('config', function($app = null){
    $app = $app ? $app : APP_NAME;
    $config = APP_PATH."{$app}".DS."config.php";
    if (!file_exists($config)) {
        throw new \Exception("Config file not found:{$config}");
    }
    Flight::set(strtoupper($app),include($config));
    return Flight::get(strtoupper($app));
});

/**
 * 表单验证
 * @param  array $date  表单数组
 * @param  array $rules 验证规则
 * @return bool
 */
Flight::map('validator',function($rules = array()){
    Flight::register('validator_class', 'framework\base\Validator',array($rules));
    $result = Flight::validator_class()->rule();
    if(true !== $result){
        return array('code'=>403,'msg'=>$result);
    }else{
        return array('code'=>200,'msg'=>$result);
    }
});