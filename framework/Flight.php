<?php
/**
 * Flight: An extensible micro-framework.
 *
 * @copyright   Copyright (c) 2012, Mike Cao <mike@mikecao.com>
 * @license     MIT, http://flightphp.com/license
 */
namespace framework;

use \framework\core\Dispatcher as Dispatcher;
use \framework\core\Loader as Loader;

class Flight {

    private static $engine;

    private function __construct() {}
    private function __destruct() {}
    private function __clone() {}

    /**
     * 运行框架
     */
    public static function run() {
        //前置过滤器
        Flight::before('start', function(&$params, &$output){
            Flight::register('boots','\framework\base\App');
            Flight::boots()->run();
        });
        //开始运行框架
        Flight::start();
    }

    /**
     * 启动框架
     */
    public static function __callStatic($name, $params) {
        $app = Flight::app();
        return Dispatcher::invokeMethod(array($app,$name),$params);
    }
    
    /**
     * @return object Application instance
     */
    public static function app() {
        static $initialized = false;
        if (!$initialized) {
            require_once __DIR__.'/core/Loader.php';
            Loader::autoload(true, dirname(__DIR__));
            self::$engine = new \framework\Engine();
            $initialized = true;
        }
        return self::$engine;
    }
}
