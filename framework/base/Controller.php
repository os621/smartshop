<?php
/**
 * 公共控制器
 */
namespace framework\base;
use framework\Flight;

class Controller{
    
    /**
     * 模板赋值
     * @param  string $name 变量名
     * @param  mixed $value 变量值
     * @return void
     */
    protected function assign($name, $value = NULL) {
        return Flight::view()->set($name,$value);
    }

    /**
     * 模板输出
     * @param $tpl_path
     * @param null $value
     * @param array $is_layouts
     * @return mixed
     */
    protected function display($tpl_path = null,$value = [],$is_layouts = null) {
        $tpl_path = $tpl_path ? $tpl_path : CONTROLLER_NAME.DS.ACTION_NAME;

        if(empty($this->tplPath)){
            $tpl = APP_PATH.strtolower(APP_NAME.DS."views".DS);
        }else{
            $tpl = ROOT_PATH.$this->tplPath.DS;
        }
        if(is_null($is_layouts)){
            Flight::render($tpl.$tpl_path,$value);
        }else{
            Flight::render($tpl.$tpl_path,$value,$is_layouts);
            Flight::render($tpl.$is_layouts);
        }
        return;
    }

     /**
     * 判断post提交
     * @return boolean
     */
    protected function getVar($var = NULL){
        $request = Flight::request();
        $route   = Flight::router()->route($request)->params;
        if($var){
            return $route[$var];
        }
        return $route; 
    }
   
    /**
     * 判断post提交
     * @return boolean
     */
    protected function isPost(){
        if(Flight::request()->getMethod() == 'POST'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 判断get提交
     * @return boolean
     */
    protected function isGet(){
        if(Flight::request()->getMethod() == 'GET'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 判断ajax提交
     * @return boolean
     */
    protected function isAjax(){
        if(Flight::request()->ajax()){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 获取表单令牌
     * @return boolean
     */
    protected function enToken(){
        $Encrypter = new \extend\Encrypter();
        return $Encrypter->enFormToken();
    }

    /**
     * 表单令牌验证
     * @return boolean
     */
    protected function deToken($stamp,$token,$time = 0,$session = false){
        $Encrypter = new \extend\Encrypter();
        return $Encrypter->deFormToken($stamp,$token,$time,$session);
    }
}