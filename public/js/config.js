/**
 * 设置插件路径
 */
var js = document.scripts;
var plug_path = js[js.length-1].src.substring(0,js[js.length-1].src.lastIndexOf("/")+1);
Do.setConfig('coreLib', [plug_path + 'jquery.js']);
Do.add('base',{path :plug_path + 'function.js'});
//layer
Do.add('layer_css',{path :plug_path + 'Plug-in/layer/theme/default/layer.css',type : 'css'});
Do.add('layer',{path :plug_path + 'Plug-in/layer/layer.js',type : 'js',requires : ['layer_css']});
//form
Do.add('form_css',{path : plug_path + 'Plug-in/form/Validform.css',type : 'css'});
Do.add('form_js',{path : plug_path + 'Plug-in/form/Validform.js',type : 'js'});
Do.add('form',{path : plug_path + 'Plug-in/form/Validform_Datatype.js',requires : ['form_js', 'form_css']});
//layout
Do.add('zTree_css',{path : plug_path + 'Plug-in/zTree/zTreeStyle.css',type : 'css'});
Do.add('zTree',{path : plug_path + 'Plug-in/zTree/jquery.ztree.js',requires : ['zTree_css']});
//Tab
Do.add('tab',{path :plug_path + 'Plug-in/tab/jquery.idTabs.min.js',type : 'js'});
//JS模板引擎
Do.add('tpl',{path :plug_path + 'Plug-in/laytpl/laytpl.js',type : 'js'});
//SuperSlide
Do.add('slide',{path : plug_path + 'Plug-in/slide/SuperSlide.js'});
//uploadify
Do.add('upload_css',{path : plug_path + 'Plug-in/uploader/webuploader.css',type : 'css'});
Do.add('upload',{path : plug_path + 'Plug-in/uploader/webuploader.min.js',requires : ['upload_css']});
//tip
Do.add('tip_css',{path : plug_path + 'Plug-in/tip/style.css',type : 'css'});
Do.add('tip',{path : plug_path + 'Plug-in/tip/powerFloat.js',requires : ['tip_css']});
//nprogress
Do.add('nprogress_css',{path : plug_path + 'Plug-in/nprogress/nprogress.css',type : 'css'});
Do.add('nprogress',{path : plug_path + 'Plug-in/nprogress/nprogress.js',requires : ['nprogress_css']});
//editor
Do.add('editor_js',{path : plug_path + 'Plug-in/editor/kindeditor-min.js'});
Do.add('editor',{path : plug_path + 'Plug-in/editor/lang/zh_CN.js',requires : ['editor_js']});
//DatePicker
Do.add('datecss',{path : plug_path + 'Plug-in/date/jquery.datetimepicker.css',type : 'css'});
Do.add('date',{path : plug_path + 'Plug-in/date/jquery.datetimepicker.js',requires:['datecss']});
//color
Do.add('colorcss',{path : plug_path + 'Plug-in/color/style.css',type : 'css'});
Do.add('color',{path : plug_path + 'Plug-in/color/ColorPacker.js',requires : ['colorcss']});
//datatables
Do.add('tablescss',{path : plug_path + 'Plug-in/datatables/jquery.dataTables.min.css',type : 'css'});
Do.add('datatables',{path : plug_path + 'Plug-in/datatables/jquery.dataTables.min.js',requires : ['tablescss']});
//图片延迟加载
Do.add('lazyimg',{path : plug_path + 'Plug-in/lazyimg/lazyimg.js'});
//表单
Do.add('jform', {path: plug_path + 'Plug-in/form/jquery.form.js',type: 'js'});
//通知
Do.add('notifyCss', {path: plug_path + 'Plug-in/notify/amaran.min.css',type: 'css'});
Do.add('notify', {path: plug_path + 'Plug-in/notify/jquery.amaran.min.js',type: 'js',requires: ['notifyCss']});
//复制
Do.add('copy', {path : plug_path + 'Plug-in/copy/ZeroClipboard.min.js',type: 'js'});
//二维码
Do.add('qrcode', {path: plug_path + 'Plug-in/qrcode/qrcode.js',type: 'js'});
//分页
Do.add('laypageCss', {path: plug_path + 'Plug-in/laypage/skin/laypage.css',type: 'css'});
Do.add('laypage', {path: plug_path + 'Plug-in/laypage/laypage.js',type: 'js',requires: ['laypageCss']});
//联动菜单
Do.add('select', {path: plug_path + 'Plug-in/select/select.min.js',type: 'js'}); 
//图片动画效果
Do.add('swipercss',{path : plug_path + 'Plug-in/swiper/swiper.css',type:'css'});
Do.add('swiper',{path : plug_path + 'Plug-in/swiper/swiper.js',type : 'js',requires : ['swipercss']});
//分享
Do.add('shareCss', {path: plug_path + 'Plug-in/share/css/share.min.css',type: 'css'});
Do.add('share', {path: plug_path + 'Plug-in/share/js/share.min.js',type: 'js',requires: ['shareCss']});
//抛物线动画
Do.add('requestAnimationFrame', {path: plug_path + 'Plug-in/fly/requestAnimationFrame.js',type: 'js'}); 
Do.add('fly', {path: plug_path + 'Plug-in/fly/jquery.fly.min.js',type: 'js',requires: ['requestAnimationFrame']});