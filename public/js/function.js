//设置全局AJAX默认选项。
$.ajaxSetup({
    cache : false
});
//jQuery常用函数封装
(function ($){
//删除
$.fn.isDel = function (){
    return this.each(function() {
        $(this).click(function(){
            var url = $(this).attr("url");
            var css = this;
            var index = parent.layer.getFrameIndex(window.name); 
            parent.layer.confirm('确认要删除本资源?',{icon: 3,title:'友情提示'}, function(index){
                $.getJSON(url,function(data) {
                    parent.layer.close(index);
                    if (data.code == 200) {
                        $(css).parents("tr").remove();
                        $(css).parent().remove();
                    }else{
                        parent.layer.msg(data.msg,{icon:5})
                    }
                });
            });
        })
    });
};
//弹出窗口
$.fn.popup = function (url){
    var defaults = {input:$(this).attr('data') ? $(this).attr('data'):$(this).attr('id'),url:url}
    var options  = $.extend(defaults,options);
    return this.each(function() {
        parent.layer.open({
            type: 2,title:'信息窗口',maxmin:true,area:['60%','70%'],content:options.url +'?input=' + options.input
        });
    });
};
//表单
$.fn.isForm = function (options){
    var defaults = {types:'ajax',upload:undefined}
    var options = $.extend(defaults,options);
    return this.each(function (){
        var form = this;
        form = $(form);
        form.validatorForm({types:options.types}); //表单处理
        if ($(".ui-editor").length > 0){form.find('.ui-editor').editor(options.upload);}//编辑器
        if ($(".ui-mieditor").length > 0){form.find('.ui-mieditor').minieditor(options.upload);}//编辑器
        if ($(".ui-date").length > 0){form.find('.ui-date').times({format:'Y-m-d'});}   //时间选择器
        if ($(".ui-time").length > 0){form.find('.ui-time').times({format:'Y-m-d H:i:s'});}//时间选择器
        if ($(".ui-color").length > 0){form.find('.ui-color').color();}   //颜色选择器   
        if ($(".ui-upload").length > 0){$(".ui-upload").click(function(){$(this).popup(options.upload);});}//上传附件
    });
};
//表单验证
$.fn.validatorForm = function (options){
    var defaults = {types:'ajax'}
    var options  = $.extend(defaults,options);
    return this.each(function (){
        var form = this;
        if (options.types == 'ajax'){
            $(form).Validform({
                btnSubmit: ".submit",showAllError:false,tiptype: 4,ajaxPost: true,postonce:true,ignoreHidden:true,
                callback: function(data) {
                    var index = parent.layer.getFrameIndex(window.name); 
                    if (data.code == "200") {
                        parent.layer.alert(data.msg,{icon:1,closeBtn:0},function(index){
                            if(typeof data.data.parent != "undefined"){
                                $.isEmptyObject(data.data.url) ? window.parent.location.reload(): window.parent.location.replace(data.data.url);
                            }else{
                                $.isEmptyObject(data.data.url) ? window.location.reload(): window.location.replace(data.data.url);
                            }
                            parent.layer.close(index);
                        });
                    }else if(data.code == "302"){
                        if(typeof data.data.parent != "undefined"){
                            $.isEmptyObject(data.data.url) ? window.parent.location.reload(): window.parent.location.replace(data.data.url);
                        }else{
                            $.isEmptyObject(data.data.url) ? window.location.reload(): window.location.replace(data.data.url);
                        }
                        parent.layer.close(index);
                    } else {
                        parent.layer.alert(data.msg,{icon:5,closeBtn:0});
                    }
                }
            })
        }else{
            $(form).Validform({btnSubmit: ".submit",showAllError:false,tiptype: 4,ignoreHidden:true})
        }
    });
};

//颜色
$.fn.color = function (options){
    return this.each(function (){
        $(this).soColorPacker();
    });
};

//时间插件
$.fn.times = function (options) {
    var defaults = {lang: 'ch'}
    var options = $.extend(defaults,options);
    this.each(function () {
        $(this).datetimepicker(options);
    });
};

//编辑器调用
$.fn.editor = function (uploads){
    var defaults = {uploadUrl:uploads}
    var options = $.extend(defaults,options);
    return this.each(function (){
        var id = this;
        var editorConfig = {
            allowFileManager : false,
            uploadJson:options.uploadUrl,
            items : ['source','plainpaste','|','fontsize','forecolor','fontname','hilitecolor','bold', 'italic','underline','|', 'justifyleft','justifycenter','justifyright','insertorderedlist','insertunorderedlist','|', 'image', 'multiimage','media','insertfile','|', 'table','link','unlink','clearhtml','quickformat','|','fullscreen', 'preview'],
            afterBlur : function (){this.sync();},
            width :'100%',height:'500px'
        };
        editorConfig = $.extend(editorConfig,options.config);
        var editor = KindEditor.create(id,editorConfig);
    });
};

//编辑器调用
$.fn.minieditor = function (uploads){
    var defaults = {uploadUrl:uploads}
    var options = $.extend(defaults,options);
    return this.each(function (){
        var id = this;
        var editorConfig = {
            allowFileManager : false,
            uploadJson:options.uploadUrl,
            items : ['plainpaste','|','fontsize','forecolor','fontname','hilitecolor','bold', 'italic','underline','|', 'justifyleft','justifycenter','justifyright','insertorderedlist','insertunorderedlist','|','link','unlink'],
            afterBlur : function (){this.sync();},
            width :'100%',height:'200px'
        };
        editorConfig = $.extend(editorConfig,options.config);
        var editor = KindEditor.create(id,editorConfig);
    });
};
})(jQuery);