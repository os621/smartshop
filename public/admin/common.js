//全选
function CheckAll(event){
    var checked = event.checked;
    $(".table tbody input[type = 'checkbox']").each(function(i){
        if(!$(this).prop("disabled")){
            $(this).prop("checked",checked)
        }
    });
}

//快捷工具栏弹窗
function openwin(url){
    parent.layer.open({type: 2,title:'快捷工具',maxmin:false,area:['60%','70%'],content:url});
}

//批量操作
function tools_submit(obj){
    var ids = "";
    $("input:checkbox[name='ids[]']:checked").each(function() {
       ids += $(this).val() + ",";
    });
    var index = parent.layer.getFrameIndex(window.name); 
    parent.layer.confirm(obj.msg,{icon:3},function(index){
        $.post(obj.action,{ids:ids},function(data) {
            if (data.code == "302") {
                $.isEmptyObject(data.data.url) ? window.location.reload(): window.location.replace(data.data.url);
            }else if(data.code == "200"){
                parent.layer.alert(data.msg,{icon:1,closeBtn:0},function(index){
                    $.isEmptyObject(data.data.url) ? window.location.reload(): window.location.replace(data.data.url);
                    parent.layer.close(index);
                });
            } else {
                parent.layer.alert(data.msg,{icon:5});
            }
        },"json");
        parent.layer.close(index);
    });
}

//数组的笛卡尔积
function descartes(args){
    if(args == undefined) return null;
    var len = args.length;
    if(len == 1){
        return args[0];
    }else{
        var tem = new Array();
        tem = group(args[0],args[1]);
        for(var i=2;i<len;i++){
            tem = group(tem,args[i]);
        }
        var result = new Array();
        var tem_len = tem.length;
        num = 0;
        for(var i = 0;i < tem_len;i++){
            result[num++] = tem[i].split('*#*');
        }
        return result;
    }
}
function group (m,n){
    var tem = new Array();
    var num = 0;
    for(var i=0;i<m.length;i++){
        for(var j=0;j<n.length;j++){
            tem[num++] =m[i]+'*#*'+n[j];
        }
    }
    return tem;
}