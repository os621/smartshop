/**
 * 购物车(需要载入JS模板引擎)
 */
//Start
/*快捷购物车模板引擎*/
function shopping(url){
    $.getJSON(url+"index/cart/getCartItmes",function(data) {
        if(data.code == 200){
            var gettpl = $("#shoppingtpl").html();
            var tpl_data = {"list":data};
            laytpl(gettpl).render(tpl_data, function(html){
                $(".dropdown").html(html);
                bindDelEvent(url);
                shopping_num();
            });
        }else{
            $(".dropdown").empty().append('<div class="null-cart">购物车中还没有商品，赶紧<a href="/">选购</a>吧！</div>');
        }
    });
}
/*添加一个商品到快捷购物车*/
function updateCart(data,url){
    var card_items = '';
    for(var i in data){
        var spec = data[i]['spec'];
        var spec_str = '';
        for(var k in spec){
            spec_str +='<span class="cart-spec" title="'+spec[k]['spec_value'][2]+'">'+spec[k]['spec_value'][2]+'</span>';
        }
        card_items += '<li class="cart-item" id="'+i+'">'+
        '<div class="pic"><a class="card-pic" href="##"><img src="'+data[i]['img']+'" width="50" height="50"></a></div>'+
        '<div class="spec"><a href="##"><span class="cart-title">'+data[i]['name']+'</span></a><br />'+spec_str+'</div>'+
        '<div class="num">'+data[i]['num']+'</div>'+
        '<div class="price">'+(data[i]['amount'])+'</div><a href="javascript:;" class="close-item" productid="'+data[i]['id']+'"><i class="iconfont icon-close"></i></a></li>';
    }
    $("#cart-list").empty().append(card_items);
    shopping(url);
    changeCartInfo();
    bindDelEvent(url);
    shopping_num();
}
/*快捷购物车删除*/
function bindDelEvent(url){
    $("#shopping-cart .close-item").click("click",function(){
        var btn_close = $(this);
        $.post(url + "index/cart/cart_del",{sku:btn_close.attr("productid")},function(){
            btn_close.parent().remove();
            changeCartInfo();
            shopping_num();
        },"json");
    });
}
/*更新快捷购物车*/
function changeCartInfo(){
    $(".cart-product-num").text($(".cart-item").size());
    var total = 0.00;
    $(".cart-item .price").each(function(){
        total += parseFloat($(this).text());
    });
    $(".cart-total").text(total.toFixed(2));
    if($(".cart-item").size()==0){
        $("#cart-list").empty().append('<div class="null-cart">购物车中还没有商品，赶紧<a href="{__URL__}">选购</a>吧！</div>');
    }
}
/*统计购物车有多少物品*/
function shopping_num(){
    var item_line = $('#cart-list').children('li[class="cart-item"]').length;
    $("#shopping-num").text(item_line);
}
/*购物车抛物线*/
function fly(img){
    var offset = $("#shopping-cart").offset();  /*结束的地方的元素*/
    var flyer = $('<img id="flyer" src="'+img+'">');
    $(this).click(function(event){
        flyer.fly({
            start: {left: event.pageX,top: event.pageY},
            end: {left: offset.left+10,top:offset.top+10,width:0,height:0},
            onEnd: function(){this.destroy();}
        });
    });
}
//END
/**
 * 一键淘宝
 */
function flash_taobao(spu_id,url){
    $.post(url+"flash_taobao",{spu:spu_id},function(data){
        switch(data.code){
            case 404:
                parent.layer.msg(data['msg'],{shade:0.3});
                break;
            case 403:
                parent.layer.open({type: 2,title:'用户登录',area: ['360px', '340px'],content:[url+'user/layerlogin','no']});
                break;
            case 401:
                parent.layer.msg(data['msg'],{shade:0.3,time:1000,end:function(){window.parent.location.replace(data.data.url);parent.layer.close(index);}});
                break;
            default:
                parent.layer.open({type: 2,title:'一键淘宝',area: ['360px', '340px'],content:[data.data.url,'no']});
                break;
        }
    },"json");
}
//加入淘宝数据包
function taobao_data(spu_id,url){
    $.post(url+"index/taobao/cart_add",{spu:spu_id},function(data){
        parent.layer.msg(data['msg'],{shade:0.3,time:1000});
    },"json");
}