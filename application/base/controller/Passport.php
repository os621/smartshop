<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        前台的基础控制器
 */
namespace application\base\controller;
use framework\Flight;

class Passport extends Base{

    protected $ThemesPath    = 'themes';  //前台主题目录
    protected $website;   //当前站点信息
    protected $website_id = 'website';  //记录当前站点COOKIE名称
    protected $cookie_id  = 'user_login';  //COOKIE名称
    protected $login_time = 86400;     //有效期一周
    protected $login_user;   //登录用户信息
    protected $CodeID     = 'SmartCODE';  //验证码SESSION域

    public function __construct() {
        session_start();
        $this->isLogin();  //判断是否登录
        $this->http_host = Flight::util()->getCurrentUrl();
        if (!Flight::get('DEBUG')) {
            $this->website  = Flight::cookie()->get($this->website_id);
        }
        if(empty($this->website)){
            $this->website = Flight::model('common/website')->find_website($this->http_host);
            if(!$this->website) Flight::notFound();
            Flight::cookie()->set($this->website_id,$this->website);
        }
        $this->tplPath = $this->ThemesPath.DS.$this->website['themes'];
        Flight::api('index/tags')->extTags($this->tplPath,$this->website);     //模板引擎API
        $this->assign('website',$this->website);
    }

    /**
     * 判断是否登录
     * @return bool
     */
    protected function isLogin(){
        $noLogin = ['index'  => ['order'],'mobile'  => ['order','passport'],'manage' => [],'passport' => []];
        if(isset($noLogin[APP_NAME])){
            if(empty($noLogin[APP_NAME])){
                $this->checkUserLogin();
            }else{
                if(in_array(CONTROLLER_NAME,$noLogin[APP_NAME])){
                    $this->checkUserLogin();
                }
            }
        }
        $this->getUserLogin();
        return true;
    }

    //检查是否登录
    protected function checkUserLogin(){
        $login_user = Flight::cookie()->get($this->cookie_id);
        if(empty($login_user)){
            if(isMobile()){
                Flight::redirect(url('m/user/login',['uri'=>urldecode(Flight::request()->url)]));
            }else{
                Flight::redirect(url('user/login',['uri'=>urldecode(Flight::request()->url)]));
            }
            return false;
        }
        return true;
    }
    
    /**
     * 设置登录
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function getUserLogin(){
        $login_user = Flight::cookie()->get($this->cookie_id);
        if(empty($login_user)){
            $this->clearUserLogin();
            return false;
        }
        $this->login_user = $login_user;
        $this->assign('login_user',$login_user);
        return true;
    }

    /**
     * 设置登录
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function setUserLogin($data){
        Flight::cookie()->set($this->cookie_id,$data,$this->login_time);
        return true;
    }

    //退出登录
    protected function clearUserLogin(){
        Flight::cookie()->del($this->cookie_id);
        return true;
    }
}