<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        公用的基础控制器
 */
namespace application\base\controller;
use framework\base\Controller;
use framework\Flight;
class Base extends Controller{

    /**
     * 分页结果显示
     * @return string  字符串
     * @return action  数组,URL后面的参数
     */
     protected function getPage($pageArray,$action = array()){
        $html  = '<ul class="page">';
        $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['firstPage'],$action).'">首页</a></li>';
        $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['prevPage'],$action).'">上一页</a></li>';
            foreach ($pageArray['allPages'] as $value) {
                if($value){
                    if($value == $pageArray['page']){
                        $html .= '<li class="active">';
                    }else{
                        $html .= '<li>';
                    }
                    $html .= '<a href="'.$this->GreatePageUrl($value,$action).'">'.$value.'</a></li>';
                }        
           }
        $html .= '<li><a href="'.$this->GreatePageUrl($pageArray['nextPage'],$action).'">下一页</a></li>';
        $html .= '</ul>';
        return $html;
    }

    /**
     * 分页URL处理
     * @return page    当前页面
     * @return action  数组,URL后面的参数
     */
    protected function GreatePageUrl($page,$action = array()){
        $ary_page = array('page' => $page);
        if(!empty($action)){
            $ary_page = array_merge($ary_page,$action);
        }
        return url(APP_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME,$ary_page);
    }

    /**
     * 错误提示页面
     * @return page    当前页面
     * @return action  数组,URL后面的参数
     */
    protected function ErrorMsg($code,$msg){
        $this->tplPath = 'application'.DS.'base'.DS.'views';
        $tpl_date['code'] = $code;
        $tpl_date['msg']  = $msg;
        $this->display('notfound',$tpl_date);
        exit();
    }

    /**
     * 创建订单编号
     */
    protected function CreateOrderNo(){
        return uuid_no();
    }
}