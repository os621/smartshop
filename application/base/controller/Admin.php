<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        后台的基础控制器
 */
namespace application\base\controller;
use framework\Flight;
use extend\Session;

class Admin extends Base{

    protected $loginID = 'isAdmin';  //SESSION域
    protected $session;
  
    public function __construct() {
        $this->session = new Session($this->loginID);
        $this->isLogin();  //判断是否登录
    }

    /**
     * 判断管理员是否登录
     * @return bool
     */
    protected function isLogin(){
        //不需要登录验证的页面
        $noLogin = [
            'admin' =>['index'=>['login','logout']]
        ];
        //如果当前访问是无需登录验证，则直接返回   
        if(isset($noLogin[APP_NAME])){
            if(isset($noLogin[APP_NAME][CONTROLLER_NAME]) && in_array(ACTION_NAME,$noLogin[APP_NAME][CONTROLLER_NAME])){
                return TRUE;
            }
        }
        if(!$this->checkAdminLogin()){
            Flight::redirect(url('admin/login'));
        }
        return true;
    }


    /**
     * [isAuth 权限验证]
     * @param  [str]        $auth_code [权限识别码]
     * @param  [boolean]    $isAjax    [返回是否JSON]
     * @param  [str]        $message   [无权限提示文字]
     * @return boolean           [是否有权限]
     */
    protected function isAuth($auth_code,$isJson = FALSE,$message = '403 Forbidden'){
       $result = $this->AccessCode($auth_code,$code);
       if(!$result){
            if($isJson){
                return Flight::json(['code'=>403,'msg'=>$message]);
            }else{
                Flight::halt(203,$message);
            }
       }
       return true; 
    }

    /**
     * AccessCode 权限识别码验证
     * @param  [str]  $access_path [权限路径]
     * @param  [str]  $code [权限特征码]
     */
    private function AccessCode($access_path){
        //超级权限
        if(!$this->group_id) return true; 
        //读取权限数据
        $result = Flight::model('base/admin')->selectGroupAuth($this->group_id);
        if($result){
            foreach ($result as $value) {
                $len = strlen($value['access_path']);
                if(strncmp($value['access_path'],$access_path,$len) == 0){
                    return true;
                };
            }
        }
        return FALSE;
    }

    /**
     * 检查管理员是否登录
     * @return [bool]
     */
    protected function checkAdminLogin(){
        if(empty($_SESSION[$this->loginID])){
            $this->clearAdminLogin(); //清空已登录信息
            return false;
        }else{
            $userInfo = $this->session->get();
            $this->group_id = $userInfo['group_id'];
            $this->admin_id = $userInfo['id'];
            return true;
        }
    }

    /**
     * 管理员设置登录
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function setAdminLogin($data){
        $this->session->set($data);
    }

    /**
     * 退出登录
     * @return [bool]
     */
    protected function clearAdminLogin(){
        $this->session->del();
        return true;
    }
}