<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        权限控制
 */
namespace application\base\model;
use framework\base\Model;
use extend\Category;

class Admin extends Base{

    protected $table  = 'system_auth'; 
    protected $group_table = 'system_group'; 

    /**
     * 获取用户权限(用户判断当前用户的权限)
     * @param  array $group_id [权限组ID]
     */
    public function selectGroupAuth($group_id){
        $result = $this->selectGroupInfo($group_id,'auth_ids');
        if(!$result['auth_ids']){
            return FALSE; 
        }
        $auth_ids = implode(',',json_decode($result['auth_ids'],TRUE));
        return $this->where(["id in({$auth_ids})"])->field('access_path')->select();
    }
}