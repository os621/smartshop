<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        公用的基础接口
 */
namespace application\base\api;
use framework\base\Api;
use framework\Flight;

class Base extends Api{
 
  /**
   * [get_imgs 正则表达式读取图片]
   * @param  [string] $content 要处理的字符串
   * @return [array]          区配后的字符串
   */
    protected function get_imgs($content){
        preg_match_all('/<img.*?src=[\\\\\'| \\"](.*?(?:[\\.gif|\\.jpg]?))[\\\\\'|\\"].*?[\\/]?>/',$content,$imgs);
        return $imgs;
    }
}