<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品分类管理
 */
namespace application\index\model;
use application\base\model\Base;
use extend\Category as ctg;
use framework\Flight;

class Category extends Base{

    protected $table = 'goods_category';

    //单个
    public function select_find($id,$field = null){
        $condition['id']= (int)$id;
        return $this->where($condition)->field($field)->find();
    }

    /**
     * 读取当前站点的所有栏目(调用标签用)
     * @param  [type] $parent_id [description]
     * @param  [type] $website   [description]
     * @return [type]            [description]
     */
    public function category_lists($parent_id,$website){      
        $result = $this->where(['website_id' => $website,'parent_id'=> intval($parent_id)])->field('id,website_id,parent_id,name,title,sort,update_time,icon')->order('sort desc')->select();
        $category = [];
        if($result){
            foreach ($result as $key => $value) {
                $category[] = $value;
                $category[$key]['url'] = url('list/'.$value['id'].'.html');  
            }  
        }
        return $category;
    }

    /**
     * 读取当前站点的所有栏目(把同一级别的分到一个数组)
     * @param  [type] $parent_id [description]
     * @param  [type] $website   [description]
     * @return [type]            [description]
     */
    public function category_tree($parent_id,$website_id){ 
        $condition['website_id'] = $website_id;
        $result = $this->where($condition)->field('id,website_id,parent_id,name,title,sort,update_time,icon')->order('sort desc')->select();
        $category = [];
        if($result){
            foreach ($result as $key => $value) {
                $category[] = $value;
                $category[$key]['url'] = url('list/'.$value['id'].'.html');  
            }  
        }
        $tree =  new ctg(array('id','parent_id','name','title'));
        return $tree->getArray(intval($parent_id),$category);
    }    
    
    /**
     * 获取当前路径
     * @param type $parent_id
     * @return type
     */
    public function get_path($website_id,$parent_id = 0){
        $condition['website_id'] = $website_id;
        $result = $this->field('id,name,title,parent_id,root_id')->where($condition)->select();
        $tree =  new ctg(array('id','parent_id','name','title'));
        return $tree->getPath($result,$parent_id);
    }
}