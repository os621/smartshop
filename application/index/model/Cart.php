<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        购物车
 */
namespace application\index\model;
use application\base\model\Base;
use framework\Flight;

class Cart extends Base{

    protected $table = 'user_cart';

    //单个
    public function select_find($id,$field = null){
        $condition['user_id']= (int)$id;
        return $this->where($condition)->field($field)->find();
    }

    //增加购物车
    public function info_add($param){
        $user_id = intval($param['user_id']);
        $data['cart'] = json_encode($param['cart']);
        $rel = $this->select_find($user_id);
        if($rel){
            return $this->where(['user_id' => $user_id])->data($data)->update();
        }
        $data['user_id'] = $user_id;
        return $this->data($data)->insert();
    } 

    //读取购物车信息
    public function all_item($cart,$website_id){
        $sku = [];
        if($this->lenItems($cart) > 0){
            //读取购物车SKU
            $sku_ids = trim(implode(",", array_keys($cart)),',');
            $info =  Flight::model('goods/item')->select_sku_list(["id in($sku_ids)"]);
            //读取SKU所在SPU_ID
            $spu_id = [];
            foreach ($info as $key => $value) {
                $spu_id[] = $value['spu_id'];
            }
            //读取物品SPU
            $spu = [];
            $spu_ids = implode(',',array_unique($spu_id));
            if(empty($spu_ids)) return $sku;
            $spu_info = Flight::model('manage/item')->select_spu_list_nopage(["spu_id in($spu_ids)",'A.website_id' =>$website_id],'img,unit,B.website_id AS yun_id,A.category_id,id,name,A.price_rise,A.price_base,A.math_type,A.price_rise');
            foreach ($spu_info as $key => $value) {
                $spu[$value['id']] = $value;
                $spu[$value['id']]['spu_id'] = $value['id'];
            }
            //SPU和SPK合并
            foreach ($info as $key => $value) {
                $num = $cart[$value['id']];
                //计算销售价格(是SKU价格+站点自定义加价幅度的价格)
                $value['price_rise'] = $spu[$value['spu_id']]['price_rise'];
                $value['price_base'] = $spu[$value['spu_id']]['price_base'];
                $value['math_type']  = $spu[$value['spu_id']]['math_type'];
                $sell_price = Flight::api('manage/price')->sku_price($value);
                //最终价格(后期开发,加上商品活动的时候计算)
                $amount = sprintf("%01.2f",$sell_price*$num);
                //单个商品价格小结
                $sell_total = $sell_price * $num;
                $sku[$value['id']] = [
                    'id'           => $value['id'],
                    'spu_id'       => $value['spu_id'],
                    'num'          => $num,
                    'yun_id'       => $spu[$value['spu_id']]['yun_id'],
                    'store_nums'   => $value['store_nums'],
                    'market_price' => $value['market_price'],
                    'sell_price'   => $sell_price,
                    'price'        => $sell_price,
                    'weight'       => $value['weight'],
                    'amount'       => sprintf("%01.2f",$amount),
                    'sell_total'   => sprintf("%01.2f",$sell_total),
                    'spec'         => json_decode($value['spec_and_value_ids'],true),
                    'name'         => $spu[$value['spu_id']]['name'],
                    'img'          => $spu[$value['spu_id']]['img'],
                ];
            }
        }
        return $sku;
    }

    //判断购物车有多少
    protected function lenItems($items = array()) {
        return count($items);
    }

    //清空购物车
    public function del_item($user_id){
        $data['cart'] = json_encode([]);
        return $this->where(['user_id' => $user_id])->data($data)->update();
    }

    //读取进货车中的SPU
    public function taobao_spu($spu_id,$website_id){
        $spu = [];
        if($this->lenItems($spu_id) > 0){ 
            $spu_ids  = implode(',',array_unique($spu_id));
            $spu_info = Flight::model('manage/item')->select_spu_list_nopage(["spu_id in($spu_ids)",'A.website_id' =>$website_id],'img,unit,sell_price,market_price,A.category_id,id,name,A.price_base,A.math_type,A.price_rise');
            foreach ($spu_info as $key => $value) {
                $spu_info[$key]['sell_price'] = Flight::api('manage/price')->sku_price($value);
            }
        }
        return $spu_info;
    }
}