<?php
namespace application\index\api;
use application\base\api\Base;
use framework\Flight;
/**
 * 扩展模板库标签
 */
class Tags extends Base{

    //扩展前端标签
    public function extTags($tplPath,$website = []){
        $label = [
            /**
             * 调用模板
             * <!--#include = "tpl"-->   
             */
            '/{include\s*file\s*=\s*\"(.*)\"}/i' => "<?php self::getApi('index/tags/incfile',['id'=>\"$1\",'tplPath'=>'{$tplPath}']);?>",
            /**
             * 调用标签并循环
             * {var=>lable_name(1)} =>
             *
             * {/var}
             */
            "/\{(\S+)=>(\S+)\((.+?)\)\}/i" => "<?php \$result = self::getApi('index/tags/lables',['website'=>\$website,'types'=>'$2','id'=>$3]);\$n=1;if(is_array(\$result)){foreach(\$result as $$1) { ?>",
            '/\{\/([a-zA-Z_]+)\}/' => "<?php \$n++;}unset(\$n);unset(\$result);}?>",
            /**
             * 返回数组变量需要自己重新循环读取
             * {var=lable_name(0)}
             */
            '/\{(\S+)=(\S+)\((.+?)\)\}/i' => "<?php \$$1 = self::getApi('index/tags/lables',['website'=>\$website,'types'=>'$2','id'=>$3]);?>",

        ];
        Flight::view()->extTags($label);
    }

    //引用模板文件
    public function incfile($params){
        $this->tplPath = $params['tplPath'];
        $this->display($params['id']);
    }

    //内部标签
    /**
     * [lables description]
     * @param  [array] $params [传入的查询参数]
     * @return [array]         [数据库查询结果]
     */
    public function lables($params){
        $id      = $params['id'];
        $types   = strtolower($params['types']);
        $website = $params['website'];
        switch ($types) {
            case 'category': //商品分类
                return Flight::model('index/category')->category_lists($id,$website['id']);
                break;
            case 'category_tree':  //商品目录树
                return Flight::model('index/category')->category_tree($id,$website['id']);
                break;
            case 'item_ids':  //根据商品ID查询商品
                if(!is_array($id)) return;
                return self::item_ids($id,$website['id']);
                break;               
            case 'special':  //读取专题特性类商品
                if(!is_array($id)) return;
                return self::special($id,$website['id']);
                break;                                              
            case 'nav':  //导航
                if(!is_array($id)) return;
                return self::nav($id,$website['id']);
                break;
            case 'ads': //广告
                return Flight::model('adwords/advert')->website_select_list($id,$website['id']);
                break;
            case 'channel_news': //新闻栏目列表
                return self::channel($id,$website['id']);
                break; 
            case 'article_news': //新闻内容列表
                if(!is_array($id)) return;
                return self::article($id,$website['id']);
                break;  
            case 'special_news': //新闻内容列表(加特性)
                if(!is_array($id)) return;
                return self::special_news($id,$website['id']);
                break;
        }
    }

    /**
     * 读取专题特性类商品
     * @param  [array] $params     [查询查收]
     * @param  [int]   $website_id [当前站点ID]
     * @return [array]             [查询SQL结果]
     */
    protected function special($params,$website_id){
        $condition['special_id']   = $params['id'];
        $condition['A.website_id'] = $website_id;
        $condition['is_sale']      = 2;
        $condition['is_del']       = 0;
        $special =  Flight::model('manage/special')->select_spu_tag($condition,$params['num']);
        if($special){
            $ids_array = array_column($special,'id');  //提取商品ID
            $ids = implode(',',$ids_array);
            //读取所有专题商品ID
            $website_item = Flight::model('manage/item')->select_list(["spu_id in($ids)",'is_sale'=>2,'is_del'=>0,'website_id'=>$website_id]);
            foreach ($website_item as $key => $value) {
               $items[$value['spu_id']]['price_rise'] = $value['price_rise'];
               $items[$value['spu_id']]['price_base'] = $value['price_base'];
               $items[$value['spu_id']]['math_type']  = $value['math_type'];
            }
            //读取基础库中的商品
            foreach ($special as $key => $value) {
               $value['price_rise'] = $items[$value['id']]['price_rise'];
               $value['price_base'] = $items[$value['id']]['price_base'];
               $value['math_type']  = $items[$value['id']]['math_type'];
               $special[$key] = $value;
               $special[$key]['sell_price'] = Flight::api('manage/price')->sku_price($value);   //销售价格
            }
            return $special;
        }
        return false;
    }

    /**
     * [item_ids 根据商品ID查询商品]
     * @param  [array] $params     [商品ID数组]
     * @param  [int]   $website_id [站点ID]
     * @return [array]             [查询SQL结果]
     */
    protected function item_ids($ids_array,$website_id){
        $ids = implode(',',$ids_array);
        $condition[] = "id in($ids)";
        $condition[] = "A.website_id = ".$website['id'];
        $condition = ['A.is_sale' => 2,'A.is_del' => 0];
        $item =  Flight::model('manage/item')->select_spu_list($condition);
        if ($item) {
            foreach ($item as $key => $value) {
                $item[$key] = $value;
                $item[$key]['sell_price'] = Flight::api('manage/price')->sku_price($value);  //销售价格
                $item[$key]['url']        = url("item/{$value['id']}.html");
            }
        }
        return $item;
    }

    /**
     * [channel 查询文字栏目列表]
     * @param  [int] $ids        [栏目ID]
     * @param  [int] $website_id [站点ID]
     * @return [array]           [查询SQL结果]
     */
    protected function channel($ids,$website_id){
        $info = Flight::model('article/channel')->select_list($ids,$website['id']);
        if($info){
            foreach ($info as $key => $value) {
                if(empty($value['url'])){
                    $info[$key]['url'] = url('info/channel/'.$value['id'].'.html');
                }
            }
        }
        return $info;
    }

    /**
     * [channel 查询文字栏目列表]
     * @return [array]           [查询SQL结果]
     */
    protected function article($ids_array,$website_id){
        $channel_id = intval($ids_array['cid']);
        $issub      = intval($ids_array['issub']); //是否包含子类
        $number     = intval($ids_array['number']); //一页多少个
        $ispage     = intval($ids_array['ispage']); //是否翻页
        $condition[] = "(website_id = {$website_id} or website_id = 0)";
        if($issub){
            $ids = Flight::model('article/channel')->select_channel_sub_cid($channel_id,$condition);
            $condition[] = "channel_id in({$ids})";
        }else{
            $condition['channel_id'] = $channel_id;
        }
        if($ispage){
            $page = intval(Flight::request()->query->page); //当前第几页
            $info = Flight::model('article/index')->where($condition)->pager((int)$page,$number)->field('id,website_id,channel_id,special_ids,title,url,views,update_time,create_time')->order('neworder desc,id desc')->select(); 
        }else{
            $info = Flight::model('article/index')->where($condition)->field('id,website_id,channel_id,special_ids,title,url,views,update_time,create_time')->limit($number)->order('neworder desc,id desc')->select();
        }

        //内容链接  
        if($info){
            foreach ($info as $key => $value) {
                if(empty($value['url'])){
                    $info[$key]['url'] = url('article/'.$value['id'].'.html');
                }
            }
        }
        return $info;
    }

    /**
     * [新闻内容列表+特性]
     * @param  [int] $ids        [栏目ID]
     * @param  [int] $website_id [站点ID]
     * @return [array]           [查询SQL结果]
     */
    protected function special_news($ids_array,$website_id){
        $cid    = intval($ids_array['cid']);
        $sid    = intval($ids_array['sid']);
        $number = intval($ids_array['number']);
        if($sid == 0) return;
        $condition[] = "(website_id = {$website_id} or website_id = 0)";
        $condition[] = "FIND_IN_SET({$sid},special_ids)";
        if($cid){
            $ids = Flight::model('article/channel')->select_channel_sub_cid($channel_id,$condition);
            $condition[] = "channel_id in({$ids})";
        }

        $info = Flight::model('article/index')->where($condition)->field('id,website_id,channel_id,special_ids,title,url,views,update_time,create_time')
                    ->order('neworder desc,id desc')->limit($number)->select(); 
        //内容链接  
        if($info){
            foreach ($info as $key => $value) {
                if(empty($value['url'])){
                    $info[$key]['url'] = url('article/'.$value['id'].'.html');
                }
            }
        }     
        return $info;
    }  

    /**
     * [nav 导航]
     * @param  [int] $ids        [栏目ID]
     * @param  [int] $website_id [站点ID]
     * @return [array]           [查询SQL结果]
     */
    protected function nav($ids_array,$website_id){
        if(isset($ids_array['id'])){
            return Flight::model('article/nav')->website_select_list($ids_array['id'],$website_id);
        }else{
            if(isset($ids_array['tag'])){
                $get_id = intval($ids_array['tag']);
                $info = Flight::model('article/nav')->get_find(['get_id' => $get_id],'id');
                return Flight::model('article/nav')->website_select_list($info['id'],$website_id);
            }
        }
        return;
    }      
}