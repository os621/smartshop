<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        支付结果回调
 */
namespace application\index\controller;
use application\base\controller\Passport;
use framework\Flight;

class Notify extends Passport{

    public function __construct() {
        parent::__construct();
    }

    /**
     * 同步回调
     */
    public function notify_url(){
        $payname = Flight::filter()->filter_escape(trim($this->getVar('payname')));
        Flight::log()->write(var_export($_POST,true),'log');  
        switch ($payname) {
            case 'alipay':  //充值
                self::alipay();
                break; 
             case 'alipaydirect':  //购物
                self::alipaydirect();
                break;               
            default:
                break;
        }
    }

    /**
     * 同步回调
     */
    public function return_url(){
        $payname = Flight::filter()->filter_escape(trim($this->getVar('payname')));
        switch ($payname) {
            case 'alipay':  //充值
                Flight::redirect(url('manage/bankroll/index'),302);
                break;
            case 'alipaydirect':  //购物
                Flight::redirect(url('passport/order'),302);
                break;               
            default:
                Flight::redirect(__URL__,302);
                break;
        }   
    }

    /*
    *充值支付宝回调
     */
    protected function alipay(){
        $payment = Flight::model('common/payconfig')->info_config(['website_id' => 0,'status' => 0,0 =>'plugin_id =3']);
        //E 支付配置
        $callback = Flight::api('payment/NotifyRecharge');
        if (!is_object($callback)) {
            echo 'fail';
            return Flight::log()->write('支付宝回调错误[充值]','bug');
        }
        echo Flight::api('payment/dopay')->notify_url($payment,$callback);
    }

    /*
    *购买物品支付宝回调
     */
    protected function alipaydirect(){
        //S 支付配置
        $websiet_config  = Flight::model('common/website')->select_config_find($this->website['id']);
        if($websiet_config['is_open_pay']){
            $payment = Flight::model('common/payconfig')->info_config(['website_id' => $this->website['id'],'status' => 0,'plugin_id = 3']);
        }else{
            $payment = Flight::model('common/payconfig')->info_config(['website_id' => 0,'status' => 0,0 => 'plugin_id = 3']);
        }
        //E 支付配置
        $callback = Flight::api('payment/NotifyBuy');
        if (!is_object($callback)) {
            echo 'fail';
            return Flight::log()->write('支付宝回调错误[购物]','bug');
        }
        echo Flight::api('payment/dopay')->notify_url($payment,$callback);
    }
}

