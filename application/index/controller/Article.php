<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\index\controller;
use framework\Flight;

class Article extends Common{

    public function __construct() {
        parent::__construct();
    }

    //文章列表
    public function channel(){
        $id   = intval($this->getVar('id'));
        if (!$id)  Flight::redirect(__URL__, 302);
        $info = Flight::model('article/channel')->select_find($id);
        if (!$info)  Flight::redirect(__URL__, 302);
        if($info['website_id'] != 0 and $info['website_id'] != $this->website['id']) Flight::redirect(__URL__,302);
        $tpl_date['channel']  = $info;
        $this->display($info['tpl_lists'],$tpl_date);
    }

     //文章列表
    public function service(){
        $info = Flight::model('article/channel')->select_find(1);
        if (!$info)  Flight::redirect(__URL__, 302);
        if($info['website_id'] != 0 and $info['website_id'] != $this->website['id']) Flight::redirect(__URL__,302);
        $tpl_date['channel']  = $info;
        $this->display($info['tpl_lists'],$tpl_date);
    }   

    //内容阅读
    public function article(){
        $id   = intval($this->getVar('id'));
        if (!$id)  Flight::redirect(__URL__, 302);
        $info = Flight::model('article/index')->info_find($id);
        if (!$info)  Flight::redirect(__URL__, 302);
        if($info['website_id'] != 0 and $info['website_id'] != $this->website['id']) Flight::redirect(__URL__,302);
        $channel = Flight::model('article/channel')->select_find($info['channel_id']);
        $channel['url'] = url("channel/{$channel['id']}.html");
        $tpl_date['article']  = $info;
        $tpl_date['channel']  = $channel;
        $this->display($channel['tpl_reads'],$tpl_date);
    }
}