<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站前台基础控制器
 */
namespace application\index\controller;
use application\base\controller\Passport;
use framework\Flight;

class Common extends Passport{

    protected $httpHost;
    protected $cookie_item  = 'look_item';  //COOKIE名称
    protected $cookie_item_time = 604800;     //有效期一周

    public function __construct() {
        parent::__construct();
        //判断是否移动端
        if(isMobile()) Flight::redirect(url('m'),'302');
    }

     /**
     * 分页结果显示
     * @return string  字符串
     * @return action  数组,URL后面的参数
     */
     protected function itemPage($url,$pageArray,$action = array()){
        $html  = '<ul class="page">';
        $html .= '<li><a href="'.$this->pageUrl($url,$pageArray['firstPage'],$action).'">首页</a></li>';
        $html .= '<li><a href="'.$this->pageUrl($url,$pageArray['prevPage'],$action).'">上一页</a></li>';
            foreach ($pageArray['allPages'] as $value) {
                if($value){
                    if($value == $pageArray['page']){
                        $html .= '<li class="active">';
                    }else{
                        $html .= '<li>';
                    }
                    $html .= '<a href="'.$this->pageUrl($url,$value,$action).'">'.$value.'</a></li>';
                }        
           }
        $html .= '<li><a href="'.$this->pageUrl($url,$pageArray['nextPage'],$action).'">下一页</a></li>';
        $html .= '</ul>';
        return $html;
    }

    /**
     * 分页URL处理
     * @return page    当前页面
     * @return action  数组,URL后面的参数
     */
    protected function pageUrl($url,$page,$action = array()){
        $ary_page = array('page' => $page);
        if(!empty($action)){
            $ary_page = array_merge($ary_page,$action);
        }
        return url($url,$ary_page);
    }
}