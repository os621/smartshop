<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        购物车
 */
namespace application\index\controller;
use framework\Flight;

class Cart extends Common{

    protected $cookie_cart      = 'cart';    //购物车COOKIE名称
    protected $cookie_cart_time = 604800;   //购物车有效期一周
    private $cart;

    public function __construct() {
        parent::__construct();
        $this->cart = Flight::model('index/cart');
    }

    /**
     * 购物车信息
     * @return [type] [description]
     */
    public function index(){
        $sku_id = self::getSku_id();
        $tpl_date['sku']  = $this->cart->all_item($sku_id,$this->website['id']);
        if(empty($tpl_date['sku'])){
            $this->display('cart'.DS.'null',$tpl_date);
        }else{
            $tpl_date['fare'] = Flight::api('manage/price')->fare($tpl_date['sku']);
            $this->display(0,$tpl_date);
        }
    }

    /**
     * 批量进货车
     */
    public function wholesale(){
        $this->display(0,$tpl_date);
    }

    /**
     * 获取所有购物车信息商品列表
     * @param  array $date  用户常用数据
     * @return bool
     */
    public function getCartItmes(){
        $sku_id = self::getSku_id();
        $sku = $this->cart->all_item($sku_id,$this->website['id']);
        if(!empty($sku)){
            $i = 0;
            foreach ($sku as $key => $value) {
                $total = $total + $value['amount'];
                $data[$i] = $value;
                $j = 0;
                foreach ($value['spec'] as $spec) {
                    $data[$i]['specs'][$j] = $spec;
                    ++ $j;
                }
                ++ $i;
            }
            exit(Flight::json(['code'=>200,'msg'=>'成功','total'=>$total,'data' => $data]));
        }
        exit(Flight::json(['code'=>404,'msg'=>'失败']));
    }

    /**
     * 直接购买
     * @return [type] [description]
     */
    public function goods_add(){
        $sku = intval(Flight::request()->query->sku);
        $num = intval(Flight::request()->query->num);
        $num = $num <= 0 ? 1 : $num;
        if(!$sku) Flight::redirect(Flight::request()->referrer,302);
        $cart = self::getCart();
        //判断购物车是否登录
        if(empty($this->login_user)){
            $cart[$sku] = $num;
            self::setCart($cart);
        }else{
            $cart[$sku] = $num;
            $data['user_id'] = $this->login_user['user_id'];
            $data['cart']    = $cart;
            $this->cart->info_add($data);
            self::setCart($cart);
        }
        Flight::redirect(url('cart'),302);
    }

    /**
     * 添加商品到购物车
     * @return [type] [description]
     */
    public function cart_add(){
        if($this->isPost()){
            $sku = intval(Flight::request()->data['sku']);
            $num = intval(Flight::request()->data['num']);
            $num = $num <= 0 ? 1 : $num;
            if($sku){
                if(empty($this->login_user)){
                    $cart = self::getCart();
                    $cart[$sku] = $num;
                    self::setCart($cart);   //重写本地Cookie
                }else{
                    $info = $this->cart->select_find($this->login_user['user_id']);
                    $cart = json_decode($info['cart'],true); 
                    $cart[$sku] = $num;
                    $data['user_id'] = $this->login_user['user_id'];
                    $data['cart']    = $cart;
                    $this->cart->info_add($data);
                    self::setCart($cart);   //重写本地Cookie
                }
            }else{
                $cart = self::getSku_id();
            }
            $sku = $this->cart->all_item($cart,$this->website['id']);
            Flight::json($sku);
        }else{
            Flight::notFound();
        }
    }

    /**
     * 删除商品到购物车
     * @return [type] [description]
     */
    public function cart_del(){
        if($this->isPost()){
            $sku_id = intval(Flight::request()->data['sku']);
            if(!$sku_id){
                Flight::json(['code'=>403,'msg'=>'删除宝贝失败']);
            }
            if(empty($this->login_user)){
                $cart = self::getCart();
                unset($cart[$sku_id]);
                self::setCart($cart);   //重写本地Cookie
            }else{
                $info = $this->cart->select_find($this->login_user['user_id']);
                $cart = json_decode($info['cart'],true); 
                unset($cart[$sku_id]);
                $data['user_id'] = $this->login_user['user_id'];
                $data['cart']    = $cart;
                $this->cart->info_add($data);
                self::setCart($cart);   //重写本地Cookie
            }
            Flight::json(['code'=>200,'msg'=>'删除宝贝成功']);
        }else{
            Flight::notFound();
        }
    }

    /**
     * 修改商品到购物车数量
     * @return [type] [description]
     */
    public function cart_num(){
        if($this->isPost()){
            $sku_id = intval(Flight::request()->data['sku']);
            $num    = intval(Flight::request()->data['num']);
            if($num > 0 && $sku_id > 0){
                if(empty($this->login_user)){
                    $cart = self::getCart();
                    $cart[$sku_id] = $num;
                    self::setCart($cart);   //重写本地Cookie
                }else{
                    $info = $this->cart->select_find($this->login_user['user_id']);
                    $cart = json_decode($info['cart'],true); 
                    $cart[$sku_id] = $num;
                    $data['user_id'] = $this->login_user['user_id'];
                    $data['cart']    = $cart;
                    $this->cart->info_add($data);
                    self::setCart($cart);   //重写本地Cookie
                }
            }else{
                $cart = self::getSku_id();
            }
            $sku = $this->cart->all_item($cart,$this->website['id']);
            if($sku){
                Flight::json($sku);
            }else{
                Flight::notFound();
            }
        }else{
            Flight::notFound();
        }
    }

    /**
     * 获取购物车里的物品SKU
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function getSku_id(){
        $sku_id = [];
        if(empty($this->login_user)){
            $sku_id = empty(self::getCart()) ? [] :self::getCart();
        }else{
            $info = $this->cart->select_find($this->login_user['user_id']);
            $sku_id = $info['cart'] ? json_decode($info['cart'],true) : []; 
        }
        return array_filter($sku_id);
    }

    /**
     * 获取购物车
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function getCart(){
        return Flight::cookie()->get($this->cookie_cart);
    }

    /**
     * 设置商品
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function setCart($data){
        Flight::cookie()->set($this->cookie_cart,$data,$this->cookie_cart_time);
        return true;
    }

    //清空购物车
    protected function clearCart(){
        Flight::cookie()->del($this->cookie_cart);
        return true;
    }
}