<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        购物车
 */
namespace application\index\controller;
use framework\Flight;
use extend\Http;

class Taobao extends Common{

    protected $cookie_cart      = 'taobao';         //进货车COOKIE名称
    protected $cookie_cart_time = 1800;             //进货车有效期30分
    protected $cookie_taobao    = 'flash_taobao';   //第三方一键淘宝Cookie
    private $cart;

    public function __construct() {
        parent::__construct();
        $this->cart = Flight::model('index/cart');
    }

    /**
     * 进货车信息
     * @return [type] [description]
     */
    public function index(){
        $spu_id = self::getSpu_id();
        if(empty($spu_id)) Flight::redirect(__URL__,302);
        $tpl_date['spu']  = $this->cart->taobao_spu($spu_id,$this->website['id']);
        if($tpl_date['spu']){
            $this->display(0,$tpl_date);
        }else{
            $this->display('cart'.DS.'null',$tpl_date);
        }
    }

    /**
     * 一键淘宝HTML版
     */
    public function up_taobao(){
        $spu = intval(Flight::request()->query->spu);
        if(!$spu) Flight::halt(403,'禁止访问');
        if(empty($this->login_user)) Flight::redirect(url('user/login'),302);
        if(empty($this->login_taobao())){
            Flight::redirect(self::taobao_url($spu),302);
        }
        Flight::redirect(url('index/taobao/start_taobao',['spu' => $spu]),302);
    }

    /**
     * 一键淘宝JSON版
     */
    public function flash_taobao(){
        $spu = intval(Flight::request()->data['spu']);
        if(!$spu){
            return Flight::json(['code' => 404,'msg' => "商品未找到"]);
        }
        if(empty($this->login_user)){
            return Flight::json(['code' => 403,'msg' => "你还没有登录"]);
        }
        if(empty($this->login_taobao())){
            $url = self::taobao_url($spu);
            return Flight::json(['code' => 401,'msg' => "请进行淘宝帐号授权登录",'data' => ['url'=>$url]]);
        }
        return Flight::json(['code' => 200,'msg' => "点击开始同步",'data' => ['url'=>url('index/taobao/start_taobao',['spu' => $spu])]]);
    }

    /**
     * 开始同步
     */
    public function start_taobao(){
        if($this->isPost()){
            //判断是否登录
            if(empty($this->login_user) || empty($this->login_taobao())){
                exit(Flight::json(['code'=>403,'msg'=>'你还没有登录']));
            }
            $rules = ['itemIds' => ['empty','当前商品传入参数有误']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $spu_id = intval(Flight::request()->data['itemIds']);
            $info = Flight::model('goods/item')->select_find($spu_id,'taobao_item_id');
            if(empty($info['taobao_item_id'])){
                exit(Flight::json(['code'=>403,'msg'=>"当前商品不支持一键淘宝"]));
            }
            $itemIds = $info['taobao_item_id'];
            $nick    = $this->login_taobao()['taobao_id'];
            //POST地址
            $url    = "http://116.62.88.99:30002/copywf.do?nick={$nick}&itemIds={$itemIds}&wfkey=test";
            $result = Http::doGet($url,20);
            $info   = json_decode($result,true);
            //返回信息
            if(!empty($info)){
                switch ($info[0]['code']) {
                    case 30000:
                        Flight::json(['code'=>200,'msg'=>$info[0]['msg'],'data'=>['url'=>"http://upload.taobao.com/auction/publish/edit.htm?item_num_id={$info[0]['numiid']}&auto=false "]]);
                        break;
                    case 30001:
                        Flight::json(['code'=>30001,'msg'=>$info[0]['msg']]);
                        break;
                    case 30004:
                        Flight::json(['code'=>30004,'msg'=>$info[0]['msg'],'data'=>['url'=>$info[0]['ruleurl']]]);
                        break;
                    case 30005:
                        Flight::json(['code'=>30005,'msg'=>$info[0]['msg']]);
                    default:
                        $url =  self::taobao_url($spu);
                        Flight::json(['code'=>30002,'msg'=>$info[0]['msg'],'data'=>['url'=>$url]]);
                        break;
                }
            }
            exit(Flight::json(['code'=>403,'msg'=>"同步不成功~请重试"]));
        }else{
            $this->tplPath = null;  //初始化目录
            $spu = intval(Flight::request()->query->spu);
            if(empty($this->login_user) || empty($this->login_taobao()) || empty($spu)){
                Flight::redirect(__URL__,302);
            }
            $tpl_date['taobao_item_id'] = $spu;
            $this->display(0,$tpl_date);
        }
    }

    /**
     * 淘宝登录并跳转到商品页
     */
    public function notify(){
        $id = intval($this->getVar('id'));
        $data['taboao_id'] = Flight::request()->query->nick;
        $this->set_taobao($data);
        Flight::redirect(url("item/{$id}.html"),302);
    }

    /**
     * 在线打包CSV格式的淘宝数据包
     */
    public function taobao_data(){
        if(empty($this->login_user)){
            Flight::redirect(url("user/login"),302);
        }
        header("Content-type:text/csv"); 
        header("Content-Disposition:attachment;filename=1.csv"); 
        header('Cache-Control:must-revalidate,post-check=0,pre-check=0'); 
        header('Expires:0'); 
        header('Pragma:public'); 
        //打开PHP文件句柄,php://output 表示直接输出到浏览器
        $fp = fopen('php://output', 'a');
        //版本
        $version_data = "version 1.00";
        $version[] = iconv('utf-8', 'gbk',$version_data);
        fputcsv($fp, $version);
        //引文标题
        $header_data  = ['title','cid','seller_cids','stuff_status','location_state','location_city','item_type','price','auction_increment','num','valid_thru','freight_payer','post_fee','ems_fee','express_fee','has_invoice','has_warranty','approve_status','has_showcase','list_time','description','cateProps','postage_id','has_discount','modified','upload_fail_msg','picture_status','auction_point','picture','video','skuProps','inputPids','inputValues','outer_id','propAlias','auto_fill','num_id','local_cid','navigation_type','user_name','syncStatus','is_lighting_consigment','is_xinpin','foodparame','features','buyareatype','global_stock_type','global_stock_country','sub_stock_type','item_size','item_weight','sell_promise','custom_design_flag','wireless_desc','barcode','sku_barcode','newprepay','subtitle','cpv_memo','input_custom_cpv','qualification','add_qualification','o2o_bind_service'];
        foreach ($header_data as $key => $value) {
            $header[$key] = iconv('utf-8', 'gbk', $value);
        }
        fputcsv($fp, $header);
        //中文标题
        $title_data = ['宝贝名称','宝贝类目','店铺类目','新旧程度','省','城市','出售方式','宝贝价格','加价幅度','宝贝数量','有效期','运费承担','平邮','EMS','快递','发票','保修','放入仓库','橱窗推荐','开始时间','宝贝描述','宝贝属性','邮费模版ID','会员打折','修改时间','上传状态','图片状态','返点比例','新图片','视频','销售属性组合','用户输入ID串','用户输入名-值对','商家编码','销售属性别名','代充类型','数字ID','本地ID','宝贝分类','用户名称','宝贝状态','闪电发货','新品','食品专项','尺码库','采购地','库存类型','国家地区','库存计数','物流体积','物流重量','退换货承诺','定制工具','无线详情','商品条形码','sku 条形码','7天退货','宝贝卖点','属性值备注','自定义属性值','商品资质','增加商品资质','关联线下服务'];        
        //输出Excel列名信息
        foreach ($title_data as $key => $value) {
            $title[$key] = iconv('utf-8', 'gbk', $value);
            //$title[$key] = $value;
        }
        fputcsv($fp, $title);
        //正式数据
        $spu_id = self::getSpu_id();
        if(empty($spu_id)) Flight::redirect(__URL__,302);
        $spu_ids  = implode(',',array_unique($spu_id));
        $condition = ["spu_id in($spu_ids)",'A.website_id' =>$this->website['id']];
        $spu  = Flight::model('manage/item')->select_spu_list_nopage($condition,"B.*,A.price_rise");
        $data = [];
        if($spu){
            foreach ($spu as $key => $value) {
                $data[] = iconv('utf-8', 'gbk', $value['name']);  //宝贝名称
                $data[] = '50008900';  //宝贝类目
                $data[] = '';  //店铺类目
                $data[] = 1;  //新旧程度
                $data[] = iconv('utf-8','gbk','浙江');  //省
                $data[] = iconv('utf-8','gbk','温州');  //城市
                $data[] = 1; //出售方式
                $data[] = $value['market_price']; //宝贝价格
                $data[] = 20;  //加价幅度
                $data[] = $value['store_nums'];  //宝贝数量
                $data[] = 7;  //有效期
                $data[] = 0;  //运费承担
                $data[] = 0;  //平邮
                $data[] = 0;  //EMS
                $data[] = 0;  //快递
                $data[] = 0;  //发票
                $data[] = 1;  //保修
                $data[] = 0;  //放入仓库
                $data[] = 0;  //橱窗推荐
                $data[] = '2017/11/6 15:22:32';  //开始时间
                $data[] = iconv('utf-8', 'gbk','<p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/1.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/10.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/11.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/12.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/13.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/14.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/15.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/16.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/17.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/18.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/19.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/2.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/20.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/21.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/22.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/23.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/24.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/25.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/26.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/27.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/28.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/29.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/3.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/30.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/31.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/32.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/33.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/34.jpg" /></p><p> <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/4.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/5.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/6.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/7.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/8.jpg" /></p><p>  <img src="https://ip.freep.cn/588759/图片搬家/171015/6658/9.jpg" /></p>');  //宝贝描述
                $data[] = '20000:29534;20603:29454;20608:3267776;20663:3267192;31611:115481;1627863:105255;2917380:3226292;13328588:145656297;18073174:3267765;122216347:740150614;122216348:29444;122216507:113060;122216562:44597;122216586:29947;122216588:3235817;122216588:9142620;122216588:27328997;122216588:115481;122216588:3243112;1627207:28320;1627207:28341;1627207:3232480;20509:28314;20509:28315;20509:28316;20509:28317';  //宝贝属性
                $data[] = '';  //邮费模版ID
                $data[] = 0;   //会员打折
                $data[] = '2017/11/6 15:22:32';  //修改时间
                $data[] = 200; //上传状态
                $data[] = '';  //图片状态
                $data[] = 0;  //返点比例
                $data[] = '2c17145021c714a1e674110774ecfd63:1:0:|;4758de80d3e43b83f1b4e9eccf9b7cc9:1:1:|;c3be0a13b788df87cc100b8b31c47e72:1:2:|;d32b826eb44d59f4294d00f667bd2ef3:1:3:|;db6b56bb9aa7438c96cd5a8dfec38466:1:4:|;c892dbb49d715edcaea1bc270d06f2b5:1:5:|;';  //新图片
                $data[] = '';  //视频
                $data[] = '20509:28316;1627207:28326:3507258640089';  //销售属性组合  
                $data[] = '';  //用户输入ID串
                $data[] = iconv('utf-8', 'gbk','8819,焦糖色;颜色分类;金驼色');;  //用户输入名-值对
                $data[] = '';  //商家编码
                $data[] = '';  //销售属性别名
                $data[] = '0'; //代充类型
                $data[] = '';  //数字ID
                $data[] = '0';  //本地ID
                $data[] = '';  //宝贝分类
                $data[] = '';  //用户名称
                $data[] = '1';  //宝贝状态
                $data[] = '163'; //闪电发货
                $data[] = '';   //新品
                $data[] = '';  //食品专项
                $data[] = 'sizeGroupType:men_tops;tags:25282,52290,50370,91010,92226,104514;sizeGroupName:中国码;sizeGroupId:27013';  //尺码库
                $data[] = '0';  //采购地
                $data[] = '-1'; //库存类型
                $data[] = '';  //国家地区
                $data[] = '1'; //库存计数
                $data[] = '';  //物流体积
                $data[] = '';  //物流重量
                $data[] = '0'; //退换货承诺
                $data[] = '';  //定制工具
                $data[] = '';  //无线详情
                $data[] = '';  //商品条形码
                $data[] = '';  //sku 条形码
                $data[] = '1'; //7天退货
                $data[] = '';  //宝贝卖点
                $data[] = '';  //属性值备注
                $data[] = '';  //自定义属性值
                $data[] = '';  //商品资质
                $data[] = 1;   //增加商品资质
                $data[] = '';  //关联线下服务
            }

        }
        fputcsv($fp, $data);      //读取数据打包
    }
    /**
     * 添加商品到购物车
     * @return [type] [description]
     */
    public function cart_add(){
        if($this->isPost()){
            $spu = intval(Flight::request()->data['spu']);
            $cart = self::getSpu_id();
            if(!in_array($spu,$cart)){
                $cart[] = $spu;
                self::setCart($cart); 
            }
            Flight::json(['code' => 200,'msg' => "加入数据包成功"]);
        }else{
            Flight::notFound();
        }
    }

    /**
     * 删除商品到购物车
     * @return [type] [description]
     */
    public function cart_del(){
        if($this->isPost()){
            $spu = intval(Flight::request()->data['spu']);
            if(!$spu){
                Flight::json(['code'=>403,'msg'=>'删除宝贝失败']);
            }
            $cart = self::getCart();
            $cart = array_flip($cart);
            unset($cart[$spu]);
            $cart = array_flip($cart);
            self::setCart($cart);   //重写本地Cookie
            Flight::json(['code'=>200,'msg'=>'删除宝贝成功']);
        }else{
            Flight::notFound();
        }
    }

    /**
     * 获取购物车里的物品SKU
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function getSpu_id(){
        $sku_id = empty(self::getCart()) ? [] :self::getCart();
        return array_filter($sku_id);
    }


    /**
     * 获取购物车
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function getCart(){
        return Flight::cookie()->get($this->cookie_cart);
    }

    /**
     * 设置商品
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function setCart($data){
        Flight::cookie()->set($this->cookie_cart,$data,$this->cookie_cart_time);
        return true;
    }

    //清空购物车
    protected function clearCart(){
        Flight::cookie()->del($this->cookie_cart);
        return true;
    }

    /**
     * 判断是否登录淘宝授权
     */
    protected function login_taobao(){
        //面授权补丁
        if(!self::set_taobao()){
            return false;
        }
        return Flight::cookie()->get($this->cookie_taobao);
    }

    /**
     * 记录淘宝是否登录
     */
    protected function set_taobao(){
        if(empty($this->login_user)){
            return false;
        }
        $user = Flight::model('user/info')->info_find(['user_id' => $this->login_user['user_id']]);
        if(empty($user['taobao_name'])){
            return false;
        }
        $data['taobao_id'] = $user['taobao_name'];
        Flight::cookie()->set($this->cookie_taobao,$data,28800);  //记录登录8个小时有效期
        return true;
    } 

    /**
     * 淘宝授权登录URL
     */
    protected function taobao_url($spu){
        $url_param = "redirect_uri:{$this->website['url']}".url("taobao/notify/{$spu}").";cmd:test.com";
        return "https://oauth.taobao.com/authorize?client_id=21483280&response_type=code&redirect_uri=http%3a%2f%2f116.62.88.99%3a30002%2fuserlogin.do&state=".urlencode($url_param);
    } 
}