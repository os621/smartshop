<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        资金管理
 */
namespace application\payment\model;
use application\base\model\Base;
use framework\Flight;

class Bank extends Base{

    protected $table     = 'bank';
    protected $table_log = 'bank_log';
    protected $table_get = 'bank_get';

    //资金列表
    public function bank_list($condition,$pages = 1,$n = 20){
        return $this->where($condition)->pager((int)$pages,$n)->order('id desc')->select();
    }

    //资金列表无翻页
    public function bank_list_nopath($condition){
        return $this->where($condition)->order('id desc')->select();
    }

    //资金查询  
    public function bank_find($condition){
        return $this->where($condition)->find();
    }

    /**
     * 资金增加或减少
     */
    //资金增加
    public function money_increase($user_id,$money){
        $info = self::bank_find(['user_id' => $user_id]);
        $data['money']       = $info['money'] + $money;
        $data['update_time'] = time();
        if($info){
            return $this->data($data)->where(['user_id' => $user_id])->update();
        }
        $data['user_id'] = $user_id;
        return $this->data($data)->insert();
    }

    //资金减少
    public function money_reduce($user_id,$money){
        $info = self::bank_find(['user_id' => $user_id]);
        if($info['money'] < $money){
            return;
        }
        $data['money'] = $info['money'] - $money;
        $data['update_time'] = time();
        return $this->data($data)->where(['user_id' => $user_id])->update();
    }

    /**
     * 积分增加或减少
     * #####################################
     */
    //积分增加
    public function point_increase($user_id,$point){
        $info = self::bank_find(['user_id' => $user_id]);
        $data['point']       = $info['point'] + $point;
        $data['update_time'] = time();
        if($info){
            return $this->data($data)->where(['user_id' => $user_id])->update();
        }
        $data['user_id'] = $user_id;
        return $this->data($data)->insert();
    }

    //积分增加
    public function point_reduce($user_id,$point){
        $info = self::bank_find(['user_id' => $user_id]);
        if($info['point'] < $point){
            return;
        }
        $data['point'] = $info['point'] - $point;
        $data['update_time'] = time();
        return $this->data($data)->where(['user_id' => $user_id])->update();
    }

    /**
     * 日志
     * #####################################
     */
    //列表
    public function log_list($condition,$pages = 1,$n = 20){
        return $this->table($this->table_log)->where($condition)->pager((int)$pages,$n)->order('id desc')->select();
    }
    
    //单个
    public function log_find($condition){
        return $this->table($this->table_log)->where($condition)->find();
    }

    //单个
    public function log_edit($data,$condition){
        return $this->table($this->table_log)->data($data)->where($condition)->update();
    }
    
    /**
     * [log 增加财务日志]
     * @param  [int]     $user_id [用户ID]
     * @param  [float]   $money   [变动金额]
     * @param  [boolean] $success [是否成功] 默认不成功
     * @param  [str]     $message [日志内容]
     * @param  [boolean] $is_point[是积分还是钱] 默认是钱
     * @return [boolean]          [增加成功ID]
     */
    public function log($user_id,$money,$success = false,$message = "日常流水",$is_point = false){
        $data['user_id']     = intval($user_id);
        $data['message']     = Flight::filter()->filter_escape($message);
        $data['update_time'] = time();
        if($is_point){
            $data['point'] = intval($money);
        }else{
            $data['money'] = floatval($money);
        }
        $data['state']  = $success ? 1 : 0;
        $data['number'] = uuid_no();
        return $this->table($this->table_log)->data($data)->insert();
    }

    /**
     * 申请提现列表
     * #####################################
     */
    //列表
    public function bankget_list($condition = [],$page = 1,$n = 20){
        return $this->table($this->table_get)->where($condition)->pager($page,$n)->order('id desc')->select();
    }

    //单个
    public function bankget_find($condition){
        return $this->table($this->table_get)->where($condition)->find();
    }

    //添加或编辑
    public function bankget_edit($data,$condition = []){
        $data['update_time'] = time();
        if(empty($condition)){
            return $this->table($this->table_get)->data($data)->insert($data);
        }else{
            return $this->table($this->table_get)->where($condition)->data($data)->update();
        }
    }
}