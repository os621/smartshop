<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品订单管理
 */
namespace application\payment\model;
use application\base\model\Base;
use framework\Flight;

class Order extends Base{

	protected $table = 'order';
    protected $cache_table = 'order_cache';

    /**
     * 订单查询
     */
    public function info_list($condition,$pages = 1,$n = 20){
        return $this->where($condition)->pager((int)$pages,$n)->order('id desc')->select();
    }

     /**
     * 订单列表信息
    */
    public function order_cache_list($condition){
        return $this->table($this->cache_table)->where($condition)->order('id desc')->select();
    } 

    /**
     * 订单查询
     */
    public function find_order($condition){
        return $this->where($condition)->find();
    }
    
    /**
     * 修改订单模型
     */
    public function edit_order($data,$condition){
        return $this->data($data)->where($condition)->update();
    }
   
    /**
     * 修改订单支付状态
     */
    public function update_order($param,$condition){
        $data['pay_code']   = $param['pay_code'];   //保存支付平台订单号
        $data['pay_status'] = $param['pay_status']; //修改支付状态
        $data['pay_time']   = $param['pay_time'];   //记录支付时间
        return $this->data($data)->where($condition)->update();
    }

    /**
     * 订单首次创建
     */
    public function insert_order($data){
        $data['status']          = 0;
        $data['is_del']          = 0;
        $data['express_status']  = 0;
        $data['pay_status']      = 0;
        $data['order_starttime'] = time();
        return $this->data($data)->insert();
    }

    /**
     * ##############################################
     * 商品缓存入库
    */
    public function insert_order_cache($data){
        return $this->table($this->cache_table)->data($data)->insert();
    }   

    /**
     * 读取商品缓存数据
     */
    public function cache_list($condition){
        return $this->table($this->cache_table)->where($condition)->order('id desc')->select();
    }
}