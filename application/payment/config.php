<?php 
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        系统公共应用
 */
return [
    'APP_NAME'  => '支付应用',
    'APP_VER'   => '1.0',
    //支付接口配置
    'Alipaydirect' => [
        'use_sandbox' => true,  //是否使用沙盒模式
        'sign_type'   => 'RSA2',//RSA2
        'return_raw'  => true,  //在处理回调时是否直接返回原始数据，默认为 true
    ],
    //微信支付基础配置
];