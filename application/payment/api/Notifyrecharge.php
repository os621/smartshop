<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * 客户端需要继承该接口，并实现这个方法，在其中实现对应的业务逻辑
 */
namespace application\payment\api;
use framework\Flight;
use Payment\Notify\PayNotifyInterface;
use Payment\Config;

class Notifyrecharge implements PayNotifyInterface {

   /**
     * 客户端的业务逻辑
     *  1. 检查订单是否存在
     *  2. 检查金额是否正确
     *  3. 检查订单是否已经处理过（防止重复通知）
     *  4. 更新订单
     * @param array $data
     * @return bool  返回值一定是bool值
     * @author helei
     */
    public function notifyProcess(array $data){
        $channel = $data['channel'];
        switch ($channel) {
            case Config::ALI_CHARGE:
                $result = self::alipay($data);
                break;
            default:
                return;
                break;
        }
        if(!$result) return;
        $log_data['state']       = 1;  //修改支付状态
        $log_data['update_time'] = strtotime($data['gmt_payment']);  //记录支付时间
        Flight::model('payment/bank')->log_edit($log_data,['id' => $result['id']]);   //修改订单状态
        Flight::api('payment/bank')->money_up($result['user_id'],$result['money']);   //增加充值金额
        return true;
    }

    /*
    *阿里支付宝回调
     */
    protected function alipay(array $data){
        if ($data['trade_status'] <> 'TRADE_SUCCESS') {
            Flight::log()->write('单号['.$data['out_trade_no'].']支付状态失败','recharge','msg');
            return;
        }
        if (empty($data['trade_no'])) {
            Flight::log()->write('单号['.$data['out_trade_no'].']支付号错误','recharge','msg');
            return;
        }
        $condition['state']  = 0;  //订单未支付
        $condition['number'] = $data['out_trade_no'];
        $result  = Flight::model('payment/bank')->log_find($condition);
        if(!$result){
            Flight::log()->write('单号['.$data['out_trade_no'].']充值记录中未找到对应订单','recharge','msg');
            return;
        }
        if($result['money'] != $data['total_amount']){
            Flight::log()->write('单号['.$data['out_trade_no'].']支付金额和系统不一致','recharge','msg');
            return;
        }
        return $result;
    }
}
