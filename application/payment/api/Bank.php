<?php
namespace application\payment\api;
use application\base\api\Base;
use framework\Flight;

/**
 * 个人自己增加或减少
 */
class Bank extends Base{

    /**
     * 积分增加或减少
     */
    //积分增加
    public function point_up($user_id,$money,$msg = null){
        $result = Flight::model('payment/bank')->point_increase(intval($user_id),floatval($money));
        if($result){
            if(empty($msg)) return true;
            Flight::model('payment/bank')->log($user_id,$money,true,$msg,true);
            return true;
        }
        return;
    }

    //积分减少
    public function point_down($user_id,$money,$msg = null){
        $result = Flight::model('payment/bank')->point_reduce(intval($user_id),floatval($money));
        if($result){
            if(empty($msg)) return true;
            Flight::model('payment/bank')->log($user_id,-$money,true,$msg,true);
            return true;
        }
        return;
    }

    /**
     * 资金增加或减少
     */
    //资金增加
    public function money_up($user_id,$money,$msg = null){
        $result = Flight::model('payment/bank')->money_increase(intval($user_id),floatval($money));
        if($result){
            if(empty($msg)) return true;
            Flight::model('payment/bank')->log($user_id,$money,true,$msg);
            return true;
        }
        return;
    }

    //资金减少
    public function money_down($user_id,$money,$msg = null){
        $result = Flight::model('payment/bank')->money_reduce(intval($user_id),floatval($money));
        if($result){
            if(empty($msg)) return true;
            Flight::model('payment/bank')->log($user_id,-$money,true,$msg);
            return true;
        }
        return;
    }

    //判断支付密码是否正确
    public function is_pay($user_id,$pass){
        $info = Flight::model('user/user')->isfind(['id'=>intval($user_id)]);
        if($info['safe_password']){
            if(assword_verify(md5($pass),$info['safe_password'])) {
                return true;
            }
        }else{
            if(password_verify(md5($pass),$info['password'])) {
                return true;
            }
        }
        return FALSE;
    }
}