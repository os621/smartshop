<?php
namespace application\payment\api;
use application\base\api\Base;
use framework\Flight;
use Payment\Client\Charge;
use Payment\Common\PayException;
use Payment\Client\Notify as ApiNotify;
use Payment\Config;
/**
 * 支付支付接口
 */
class Dopay extends Base{

    private $config;

    public function __construct() {
        $this->config = Flight::config('payment');
    }

    /**支付宝接口POST接口
     * $order_info  订单信息
     * $paymeng     支付配置信息
     * 说明地址：https://docs.open.alipay.com/270/alipay.trade.page.pay/
     */
    public function Alipaydirect($order_info,$payment){
        $config    = $this->config['Alipaydirect'];  //读取支付宝配置
        $diyconfig = json_decode($payment['config'],true);
        $config['app_id']          = $diyconfig['app_id'];
        $config['ali_public_key']  = $diyconfig['public_key'];
        $config['rsa_private_key'] = $diyconfig['private_key'];
        $config['notify_url']      = $payment['notify_url'];
        $config['return_url']      = $payment['return_url'];
        // 订单信息
        $payData = [
            'body'            => $order_info['body'],
            'subject'         => $order_info['subject'],
            'order_no'        => $order_info['order_no'],
            'timeout_express' => time() + 600,    // 表示必须600s内付款
            'amount'          => $order_info['order_amount'],  // 单位为元 ,最小为0.01
            'return_param'    => $order_info['store_id'],
            'client_ip'       => get_client_ip(), // 客户地址
            'goods_type'      => 1,               // 0—虚拟类商品，1—实物类商品
            'store_id'        => $order_info['store_id'],
            'qr_mod'          => 1,
        ];
        try {
            $url = Charge::run(Config::ALI_CHANNEL_WEB,$config,$payData);
        } catch (PayException $e) {
            return ['code' => 500,'msg' => $e->errorMessage()];
        }
        return ['code' => 302,'url' => $url];
    }

    /**支付宝接口POST接口
     * $order_info  订单信息
     * $paymeng     支付配置信息
     * 说明地址：https://docs.open.alipay.com/270/alipay.trade.page.pay/
     */
    public function alipaymobile($order_info,$payment){
        $config    = $this->config['Alipaydirect'];  //读取支付宝配置
        $diyconfig = json_decode($payment['config'],true);
        $config['app_id']          = $diyconfig['app_id'];
        $config['ali_public_key']  = $diyconfig['public_key'];
        $config['rsa_private_key'] = $diyconfig['private_key'];
        $config['notify_url']      = $payment['notify_url'];
        $config['return_url']      = $payment['return_url'];
        // 订单信息
        $payData = [
            'body'            => $order_info['body'],
            'subject'         => $order_info['subject'],
            'order_no'        => $order_info['order_no'],
            'timeout_express' => time() + 600,    // 表示必须600s内付款
            'amount'          => $order_info['order_amount'],  // 单位为元 ,最小为0.01
            'return_param'    => $order_info['store_id'],
            'client_ip'       => get_client_ip(), // 客户地址
            'goods_type'      => 1,               // 0—虚拟类商品，1—实物类商品
            'store_id'        => $order_info['store_id'],
        ];
        try {
            $url = Charge::run(Config::ALI_CHANNEL_WAP,$config,$payData);
        } catch (PayException $e) {
            return ['code' => 500,'msg' => $e->errorMessage()];
        }
        return ['code' => 302,'url' => $url];
    }
    

    /**
     * 异步处理数据 
     */
    public function notify_url($payment,$class){
        $config    = $this->config['Alipaydirect'];  //读取支付宝配置
        $diyconfig = json_decode($payment['config'],true);
        $config['app_id']          = $diyconfig['app_id'];
        $config['ali_public_key']  = $diyconfig['public_key'];
        $config['rsa_private_key'] = $diyconfig['private_key'];
        try {
            return ApiNotify::run(Config::ALI_CHARGE,$config,$class);
        } catch (PayException $e) {
            exit($e->errorMessage());
        } 
    }
}