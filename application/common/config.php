<?php 
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        系统公共应用
 */
return [
    'APP_NAME'  => '系统公共应用',
    'APP_VER'   => '1.0',
    //上传目录(禁止修改)
    'UP_FOLDER' => 'upload',  //基于站点根目录
    'UP_SIZE'   => '10',
    'UP_EXTS'   => 'gif,jpg,jpeg,png,swf,doc,docx,csv,xls,xlsx,pdf,txt,zip,rar',
    //平台官网网址(禁止修改)
    'MANAGE_URL' => "http://www.shop.cn",
    //站点类型(禁止修改)
    'website_types' => ['个人商城','企事业单位','个体工商户','社团与公益组织'],
];