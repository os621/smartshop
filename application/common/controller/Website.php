<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        站点管理
 */
namespace application\common\controller;
use application\base\controller\Admin;
use framework\Flight;

class Website extends Admin{

    private $website = null;
    public function __construct() {
        parent::__construct();
        $this->assign('appconfig',Flight::config());
        $this->website = Flight::model('website');
    }

    /**
     * 站点配置
     * ############################################################
     */
    //全局配置
    public function sysconfig(){
        if($this->isPost()){
            $rules = [
                'deposit_type'        => ['empty'],
                'cloud_money_deposit' => ['empty'],
                'cloud_money_buy'     => ['empty']
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['deposit_type']        = intval($request['deposit_type']);
            $data['cloud_money_deposit'] = trim($request['cloud_money_deposit']);
            $data['cloud_money_buy']     = trim($request['cloud_money_buy']);
            $data['update_time']         = time();  
            $result =  $this->website->config_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/sysconfig')],'msg'=>'创建或编辑成功.']);
            }else{
                Flight::json(['code'=>500,'msg'=>'创建或编辑不成功.']);
            }
        }else{
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 站点全局配置','url'=>'javascript:;'];
            //菜单结束
            $tpl_date['config']   = $this->website->config_find();
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode([]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //短信配置
    public function smsconfig(){
        if($this->isPost()){
            $rules = [
                'keyid'       => ['empty','KeyID不能为空'],
                'secret'      => ['empty','secret不能为空'],
                'sign_name'   => ['empty','短信签名不能为空'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['keyid']      = Flight::filter()->filter_escape(trim($request['keyid']));
            $data['secret']     = Flight::filter()->filter_escape(trim($request['secret']));
            $data['sign_name']  = Flight::filter()->filter_escape(trim($request['sign_name']));
            $data['website_id'] = 0;
            $sms_tpl_id = [];
            if(!empty($request['sms'])){
                foreach ($request['sms'] as $key => $value) {
                    $sms_tpl_id[$key] = Flight::filter()->filter_escape(trim($value));
                }
            }
            $data['sms_id'] = json_encode($sms_tpl_id);
            $result = $this->website->sms_edit($data,0);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/smsconfig')],'msg'=>'修改成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'修改失败']);
            }
        }else{
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 站点全局配置','url'=>'javascript:;'];
            //菜单结束
            $config   = $this->website->sms_find(['website_id'=>0]);
            if($config){
                $tpl_date['config'] = $config;
                $tpl_date['sms'] = json_decode($config['sms_id'],true);
            }
            $this->display(0,$tpl_date,'layout');
        }
    }

    //列表
    public function index(){
        $page = (int)Flight::request()->query->page;
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商城管理','url'=>'javascript:;'];
        $editMenu[] = ['name'=>'<i class="iconfont icon-jiahao"></i> 添加商城','url'=>url('common/website/edit')];
        //菜单结束
        $level    = $this->website->level_list();
        $level_name = [];
        foreach ($level as $key => $value) {
            $level_name[$value['id']] = $value['name'];
        }
        $tpl_date['sitelist'] = $this->website->select_list_page($page);
        $tpl_date['pager']    = $this->getPage($this->website->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['level']    = $level_name;
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = ['url'=> ['empty'],'name'=> ['empty'],'title'=> ['empty'],'themes'=> ['empty'],'keywords'=>['empty'],'description' => ['empty']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']          = (int)$request['id'];
            $data['user_id']     = (int)$request['user_id'];
            $data['parent_id']   = (int)$request['parent_id'];
            $data['level']       = (int)$request['level'];  
            $data['url']         = $request['url'];
            $data['name']        = $request['name'];
            $data['title']       = $request['title'];
            $data['logo']        = $request['logo'];
            $data['themes']      = $request['themes'];
            $data['keywords']    = $request['keywords'];
            $data['description'] = $request['description']; 
            $data['icp']         = $request['icp'];
            $data['contacts']    = $request['contacts'];
            $data['address']     = $request['address'];
            $result =  $this->website->info_edit($data);
            if($result){
                //更新申请信息
                $webdata['title']       = $request['name'];
                $webdata['icp']         = $request['icp'];
                $webdata['url']         = $request['url'];
                $webdata['user_reg_id'] = (int)$request['user_id'];
                Flight::model('common/subshop')->info_update(['website_reg_id' => $data['id']],$webdata);
                Flight::json(['code'=>200,'data'=>['url'=>'common/website/index'],'msg'=>'成功创建或编辑站点.']);
            }else{
                Flight::json(['code'=>500,'msg'=>'创建或编辑不成功.']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('common/website/index')];
            $tpl_date['id']       = (int)Flight::request()->query->id;
            $tpl_date['info']     = $this->website->select_find(['id'=>$tpl_date['id']]);
            $tpl_date['level']    = $this->website->level_list();
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id = Flight::request()->query->id;
        Flight::json(['code'=>500,'msg'=>'禁止删除,只能禁用']);
    }

    //权限配置
    public function config(){
        if($this->isPost()){
            $rules = [
                'website_id'        => ['empty','编辑站点不存在'],
                'is_add_goods'      => ['number'],
                'is_goods_pass'     => ['number'],
                'is_open_authorize' => ['number'],
                'is_open_pay'       => ['number'],
                'types'             => ['number'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $result =  $this->website->info_config_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/index')],'msg'=>'设置成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'设置失败']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>'javascript:history.go(-1)']
            ];
            //菜单结束
            $tpl_date['id']       = (int)Flight::request()->query->id;
            $tpl_date['info']     = $this->website->select_config_find($tpl_date['id']);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }
    /**
     * 目录授权管理
     * ############################################################
     */
    //目录授权管理
    public function lists(){
        $page = (int)Flight::request()->query->page;
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 权限配置','url'=>'javascript:;'];
        $editMenu = [];
        //菜单结束
        $website_rel = $this->website->select_list(); 
        $website = [];
        if($website_rel){
            foreach ($website_rel as $key => $value) {
                $website[$value['id']]['name'] =  $value['name'];
                $website[$value['id']]['url']  =  $value['url'];
            }
        }
        $tpl_date['sitelist'] = $this->website->select_config_list($page);
        $tpl_date['pager']    = $this->getPage($this->website->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = $website;
        $this->display(0,$tpl_date,'layout');
    }

    //授权商品目录
    public function goods_category(){
        $website_id = (int)Flight::request()->query->id;
        $page       = (int)Flight::request()->query->page;
        $website_config = $this->website->select_config_find($website_id);
        if(!$website_config['is_open_authorize']){
            Flight::notFound();
        }
        $website_rel = $this->website->select_find(['id' => $website_id]);
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商城配置['.$website_rel['name'].']','url'=> url('common/website/lists')];
        $pathMaps[] = ['name'=>'<i class="iconfont icon-arrowright"></i> 授权商品目录','url'=> 'javascript:;'];
        $editMenu[] = ['name'=>'<i class="iconfont icon-edit"></i> 授权商品目录','url'=> 'javascript:openwin(\''.url('goods/category/lists',['website_id' => $website_id]).'\')'];
        //读取授权目录
        $category_model = Flight::model('goods/category');
        $rel = $this->website->select_category_find($website_id,'category_ids');
        if($rel['category_ids']){
            $condition[] = "id in ({$rel['category_ids']})";
            $condition['parent_id'] = 0;
            $category = $category_model->select_list($condition,'title,id');
            $pager    = $this->getPage($category_model->pager,['id'=>$website_id]);
        }
        $tpl_date['category'] = $category;
        $tpl_date['pager']    = $pager;
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = $website_rel;
        $this->display(0,$tpl_date,'layout');
    }
    /**
     * 站点等级增删改查
     * ############################################################
     */
    //站点等级列表
    public function level(){
        $page = (int)Flight::request()->query->page;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 站点等级','url'=>'javascript:;'];
        $editMenu[] = ['name'=>'<i class="iconfont icon-jiahao"></i> 增加等级','url'=>url('common/website/level_edit')];
        $tpl_date['level']    = $this->website->level_list_page(array(),$page);
        $tpl_date['pager']    = $this->getPage($this->website->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //站点等级（增加/删除）
    public function level_edit(){
        if($this->isPost()){
            $rules = [
                'name'  => ['empty','编辑站点不存在'],
                'point' => ['empty','开通子站需要积分必须填写'],
                'price' => ['empty','开通子站需要多少钱必须填写'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $result =  $this->website->level_config_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/level')],'msg'=>'添加/编辑成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'添加/编辑失败']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>'javascript:history.go(-1)']
            ];
            //菜单结束
            $tpl_date['id']       = (int)Flight::request()->query->id;
            $tpl_date['info']     = $this->website->level_find(['id' => $tpl_date['id']]);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除等级
    public function level_delete(){
        $id   = (int)Flight::request()->query->id;
        $info = $this->website->select_find(['level'=> $id ]);
        if($info){
            Flight::json(['code'=>500,'msg'=>'禁止删除,只能禁用']); 
        }else{
            $this->website->level_delete(['id' => $id]);
            Flight::json(['code'=>200,'msg'=>'删除成功']); 
        }        
    }

    /**
     * 站点等级增删改查
     * ############################################################
     */
    //站点等级列表
    public function skin(){
        $page = (int)Flight::request()->query->page;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 站点主题模板','url'=>'javascript:;'];
        $editMenu[] = ['name'=>'<i class="iconfont icon-jiahao"></i> 增加主题','url'=>url('common/website/skin_edit')];
        $tpl_date['themes']   = $this->website->themes_list_page(array(),$page);
        $tpl_date['pager']    = $this->getPage($this->website->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //站点等级（增加/删除）
    public function skin_edit(){
        if($this->isPost()){
            $rules = [
                'name'    => ['empty','主题名称必须填写'],
                'level'   => ['empty','使用需要站点等级必须选择'],
                'themes'  => ['empty','主题目录必须填写'],
                'preview' => ['empty','主题预览图必须填写'],
                'content' => ['empty','模板功能描述必须填写'],
                'price'   => ['empty','开通子站需要多少钱必须填写'],
                'level'   => ['type','等级编号必须输入','INT'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $result =  $this->website->themes_config_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/skin')],'msg'=>'添加/编辑成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'添加/编辑失败']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>'javascript:history.go(-1)']
            ];
            //菜单结束
            $tpl_date['id']       = (int)Flight::request()->query->id;
            $tpl_date['info']     = $this->website->themes_find(['id' => $tpl_date['id']]);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['level']    = $this->website->level_list();
            $tpl_date['website'] = $this->website->select_list();
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除主题
    public function skin_delete(){
        $id   = (int)Flight::request()->query->id;
        $rel = $this->website->themes_delete(['id' => $id]);
        if($rel){
            Flight::json(['code'=>200,'msg'=>'删除成功']); 
        }
        Flight::json(['code'=>403,'msg'=>'删除失败']); 
    }
}