<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        申请分站管理
 */
namespace application\common\controller;
use application\base\controller\Admin;
use framework\Flight;

class Subshop extends Admin{

    private $subshop = null;

    public function __construct(){
        parent::__construct();
        $this->subshop = Flight::model('common/subshop');
    }

    //弹出式模板选择窗口
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 商城审核','url'=>url('common/subshop/index')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['subshop']  = $this->subshop->info_list();
        $tpl_date['pager']    = $this->getPage($this->subshop->pager);
        $this->display(0,$tpl_date,'layout');
    }

    //通过
    public function pass(){
        if ($this->isPost()) {
            $rules = [
                'id'     => ['empty','审核站点ID丢失'],
                'level'  => ['empty','站点等级必须选择'],
                'themes' => ['empty','主题样式必须填写'],
                'state'  => ['empty','审核状态必须选择'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  = Flight::request()->data;
            $id       = intval($request['id']);
            $state    = intval($request['state']);
            $info = $this->subshop->info_find(['id' => $id,'is_pay' => 1,'state' => 0]);
            if(!$info){
                exit(Flight::json(['code'=>500,'msg'=>'审核的站点必须是已付费的']));
            }
            $is_rel = Flight::model('common/website')->select_find(['website_reg_id' => $info['id']]);
            if($is_rel){
                exit(Flight::json(['code'=>500,'msg'=>'站点列表中已经存在,请不要重复审核']));
            }
            if($state){
                //判断站点根目录
                $parent_id = Flight::model('common/website')->select_find(['id' => $info['website_id']]);
                if($parent_id['root_id']){
                    $data['root_id'] = $parent_id['root_id'];
                }else{
                    $data['root_id'] = $parent_id['id'];
                }
                $data['level']          = trim($request['level']);
                $data['themes']         = trim($request['themes']);
                $data['parent_id']      = $info['website_id'];
                $data['user_id']        = $info['user_reg_id'];
                $data['website_reg_id'] = $info['id'];
                $data['name']           = $info['title'];
                $data['title']          = $info['title'];
                $data['icp']            = $info['icp'];
                $data['url']            = $info['url'];
                $data['create_time']    = time();
                $data['update_time']    = time();
                $result = Flight::model('common/website')->info_edit($data);
                $this->subshop->info_update(['id' => $id,'is_pay' => 1],['state' => 1,'website_reg_id' => $result]);
                Flight::api('common/sys')->msg($info['user_id'],'站点审核通过');
                Flight::api('common/sys')->msg($info['user_reg_id'],'站点审核通过');
            }else{
                $result = $this->subshop->info_update(['id' => $id,'is_pay' => 1],['state' => -1]);
                Flight::api('common/sys')->msg($info['user_id'],'站点审核不通过');
                Flight::api('common/sys')->msg($info['user_reg_id'],'站点审核不通过');
            }
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/config',['id' => $result])],'msg'=>'创建或编辑成功.']);
            }else{
                Flight::json(['code'=>500,'data'=>['url'=>url('common/subshop/index')],'msg'=>'创建或编辑不成功.']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('common/subshop/index')];
            $id = (int)Flight::request()->query->id;
            $tpl_date['info']     = Flight::model('common/Subshop')->info_find(['id'=>$id]);
            $tpl_date['level']    = Flight::model('common/website')->level_list();
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }  
}