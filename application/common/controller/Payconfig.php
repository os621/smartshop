<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        支付配置
 */
namespace application\common\controller;
use application\base\controller\Admin;
use framework\Flight;

class Payconfig extends Admin{

    private $payconfig = null;

    public function __construct(){
        parent::__construct();
        $this->assign('appconfig', Flight::config());
        $this->payconfig = Flight::model('payconfig');
    }

    //列表
    public function index(){
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 支付配置','url'=>'javascript:;'];
        $editMenu   = [];
        //菜单结束
        $tpl_date['list']     = $this->payconfig->info_list(['website_id' => 0]);
        $tpl_date['config']   = $this->payconfig->config_list();
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0, $tpl_date, 'layout');
    }

    //编辑配置
    public function config(){
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 支付配置','url'=>url('common/payconfig/index')];
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;支付配置','url'=>'javascript:;'];
        //菜单结束
        $id = (int)Flight::request()->query->id;
        $config = $this->payconfig->info_find(['id' => $id]);
        if (!$config) {
            $this->ErrorMsg('403', '没有找到对应的支付接口');
        }
        if($config['plugin_id'] == 3 || $config['plugin_id'] == 4){
            $tpls = 'payconfig/alipay_edit';
        }else if($config['plugin_id'] == 5){
            $tpls = 'payconfig/weixin_edit';
        }else{
            $tpls = 'payconfig/payment_edit';
        }
        $tpl_date['config']     = $config;
        $tpl_date['plugin_id']  = $config['plugin_id'];
        $tpl_date['config_key'] = json_decode($config['config'],true) ;
        $tpl_date['pathMaps']   = json_encode($pathMaps);
        $this->display($tpls, $tpl_date, 'layout');
    }

    //支付宝配置
    public function payment_edit(){
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 支付配置','url'=>url('common/payconfig/index')];
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;支付配置','url'=>'javascript:;'];
        $editMenu = [];
        //菜单结束
        $plugin_id = (int)Flight::request()->query->plugin_id;
        //S 判断是否已经添加过当前接口
        $condition['website_id'] = 0;
        $condition['plugin_id']  = $plugin_id;
        $is_pay = $this->payconfig->info_find($condition);
        if($is_pay){
            Flight::redirect(url('common/payconfig/config',['id' => $is_pay['id']]),302);
        }
        //E 
        $config = $this->payconfig->config_find($plugin_id);
        if (!$config) {
            $this->ErrorMsg('403', '没有找到对应的支付接口');
        }
        $tpl_date['config']   = $config;
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        if($plugin_id == 3 || $plugin_id == 4){
            $tpls = 'payconfig/alipay_edit';
        }else if($plugin_id == 5){
            $tpls = 'payconfig/weixin_edit';
        }else{
            $tpls = 0;
        }
        $tpl_date['plugin_id']   = $plugin_id;
        $this->display($tpls,$tpl_date,'layout');
    }

    /**
     * 保存配置信息
     */
    public function save_data(){
        if ($this->isPost()) {
            $rules = [
                'payname'     => ['empty'],
                'app_id'      => ['empty'],
                'plugin_id'   => ['empty'],
                'public_key'  => ['empty'],
                'private_key' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if ($rel['code'] == 500) {
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request   =  Flight::request()->data;
            $plugin_id = intval($request['plugin_id']);
            $data['id']          = intval($request['id']);
            $data['plugin_id']   = $plugin_id;
            $data['pay_name']    = trim($request['payname']);
            $data['client_type'] = intval($request['client_type']);
            $data['sort']        = intval($request['sort']);
            $data['status']      = intval($request['status']);
            //保存数据规格
            if($plugin_id == 3 || $plugin_id == 4){
                $config['app_id']      = trim($request['app_id']);
                $config['public_key']  = trim($request['public_key']);
                $config['private_key'] = trim($request['private_key']);
            }else if($plugin_id == 5){
                $config['app_id']      = trim($request['app_id']);
                $config['public_key']  = trim($request['public_key']);
                $config['private_key'] = trim($request['private_key']);
            }else{
                $config['app_id']  = trim($request['app_id']);
            }
            $data['config'] = json_encode($config);
            $result =  $this->payconfig->info_edit($data);
            if ($result) {
                Flight::json(['code'=>200,'data'=>['url'=>url('common/payconfig/index')],'msg'=>'编辑成功']);
            } else {
                Flight::json(['code'=>500,'msg'=>'编辑不成功']);
            }
        }
    }  

    /**
     * 删除
     */
    public function delete(){
        $id = Flight::request()->query->id;
        $result =  $this->payconfig->info_delete(['id' => $id]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }

    /**
     * 删除
     */
    public function state(){
        $id = intval(Flight::request()->query->plugin_id);
        $result =  $this->payconfig->info_state($id);
        Flight::redirect(url('common/payconfig/index'),302);
    } 
}
