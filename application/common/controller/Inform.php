<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        通知类公共调用访问
 */
namespace application\common\controller;
use application\base\controller\Passport;
use framework\Flight;
use Flc\Dysms\Client;
use Flc\Dysms\Request\SendSms;

class Inform extends Passport{

    /**
     * 获取注册验证码
     */
    public function getregsms(){
        if($this->isPost()){
            $rules = ['phone' => ['mobile']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'message'=>$rel['msg']]));
            }
            //获取手机号
            $phone =  Flight::filter()->filter_escape(trim(Flight::request()->data['phone']));
            $user  = Flight::model('user/user')->isfind(['phone_id' => $phone]);
            if($user) Flight::json(['code'=>500,'message'=>"当前手机号已注册"]);
            self::sms($phone,'reg',$this->website['name'],$this->website['id']);
        }else{
            Flight::notFound();
        }
    } 
    
    /**
     * 获取平台验证码
    */
    public function get_sys_sms(){
        if($this->isPost()){
            $rules = ['phone' => ['mobile']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'message'=>$rel['msg']]));
            }
            //获取手机号
            $phone =  Flight::filter()->filter_escape(trim(Flight::request()->data['phone']));
            $user  = Flight::model('user/user')->isfind(['phone_id' => $phone]);
            if(!$user){
                Flight::json(['code'=>500,'message'=>"当前手机号不存在"]);
            }
            self::sms($phone);
        }else{
            Flight::notFound();
        }
    } 

    //获取发送统一接口
    protected function sms($phone,$TemplateCode = 'common',$product = "网商宝",$website_id = 0){
        //读取阿里大于配置
        $sms = Flight::model('common/website')->sms_find(['website_id' => $website_id]);
        if(empty($sms)) exit(Flight::json(['code'=>500,'message'=>'短信接口配置有误']));
        $sms['sms_id'] = json_decode($sms['sms_id'],true);
        //读取验证
        $code_number = Flight::api('common/sys')->getcode(0,9,4);
        //开启调试不发送验证码只本地显示
        if (Flight::get('DEBUG')) {
            $session = new \extend\Session($this->CodeID); //短信SESSION
            $session->set(['code' => $code_number,'phone' => $phone],600);
            exit(Flight::json(['code'=>200,'message'=>"验证码:{$code_number},10分钟中内有效"]));
        }
        //开始接入阿里短信
        $config  = ['accessKeyId'=> $sms['keyid'],'accessKeySecret' => $sms['secret']];
        $client  = new Client($config);
        $sendSms = new SendSms;
        $sendSms->setPhoneNumbers($phone);
        $sendSms->setSignName($sms['sign_name']);
        $sendSms->setTemplateCode($sms['sms_id'][$TemplateCode]);
        $sendSms->setTemplateParam(['code' => $code_number,'product' => $product]);
        $sendSms->setOutId(uuid_no());
        $result = $client->execute($sendSms);
        if($result->Code == 'OK'){
            $session = new \extend\Session($this->CodeID); //短信SESSION
            $session->set(['code' => $code_number,'phone' => $phone],600);
            Flight::json(['code'=>200,'message'=>'验证码发送成功,10分钟中输入有效']);
        }else{
            Flight::json(['code'=>500,'message'=>$result->Message]);
        }
    }
}