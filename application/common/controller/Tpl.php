<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        模板管理
 */
namespace application\common\controller;
use application\base\controller\Admin;
use framework\Flight;

class Tpl extends Admin{

    private $tpl;
    public function __construct(){
        parent::__construct();
        $this->assign('appconfig', Flight::config());
        $this->tpl = Flight::model('tpl');
    }

    //弹出式模板选择窗口
    public function popup()
    {
        $tpl_date['input'] = Flight::request()->query->input;
        $tpl_date['path']  = Flight::request()->query->path;
        $tpl_date['lists'] = $this->tpl->folder($tpl_date['path']);  
        $this->display(0, $tpl_date,'layout');
    }

    //弹出式模板选择窗口
    public function themes()
    {
        $tpl_date['input'] = Flight::request()->query->input;
        $tpl_date['path']  = Flight::request()->query->path;
        $tpl_date['lists'] = $this->tpl->themes($tpl_date['path']);
        $this->display(0, $tpl_date, 'layout');
    }
}

