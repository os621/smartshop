<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        淘宝采集接口处理
 */
namespace application\common\api;
use application\base\api\Base;
use framework\Flight;

class Sys extends Base{

    //随机数字验证码
    public function getcode($begin = 0,$end = 9,$limit = 6){
        $rand_array=range($begin,$end); 
        shuffle($rand_array);  //调用现成的数组随机排列函数 
        $str = array_slice($rand_array,0,$limit);  //截取前$limit个 
        return implode(null,$str);
    }

    //写入全局用户日志
    public function log($user_id,$text){
        return Flight::model('common/syslog')->log($user_id,$text);
    }

    //写入站内信
    public function msg($user_id,$text){
        return Flight::model('user/message')->msg($user_id,$text);
    } 
}