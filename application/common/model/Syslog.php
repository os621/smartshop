<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        分站管理
 */
namespace application\common\model;
use application\base\model\Base;
use framework\Flight;

class Syslog extends Base{
    
    protected $table = 'log';
    
    //更新数据
    public function log($user_id,$text){
        $data['user_id']     = intval($user_id);
        $data['message']     = Flight::filter()->filter_escape($text);
        $data['update_time'] = time();
        return $this->data($data)->insert();
    }
}