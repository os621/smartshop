<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        站点管理
 */
namespace application\common\model;
use application\base\model\Base;

class Website extends Base{

    protected $table                = 'website'; //站点列表
    protected $table_config         = 'website_config';  //站点配置
    protected $table_website_level  = 'website_level';  //站点等级
    protected $table_website        = 'config';  //站点配置
    protected $table_website_themes = 'website_themes';  //站点主题目录
    protected $table_website_sms    = 'website_sms';  //站点短信
    protected $table_goods_category = 'goods_category_website';  //商品授权目录

    /**
     * 站点配置
     * ############################################################
     */
    //查找单个
    public function config_find(){
        return $this->table($this->table_website)->where(['id' =>1])->find();
    } 
    
    //站点配置
    public function config_edit($data){
        return $this->table($this->table_website)->where(['id' =>1])->data($data)->update();
    } 

    /**
     * 以下是站点模型
     * ############################################################
     */
    //列表
    public function select_list_page($pages){
         return $this->pager((int)$pages,20)->order('id desc')->select();
    }

    //所有列表无翻页
    public function select_list(){
         return $this->order('id desc')->select();
    }

    //读取站点并重新排列
    public function website_name(){
        $website_rel = $this->select_list(); 
        $website = [];
        if($website_rel){
            $website[0]['name'] =  '<span class="gray">网商云平台</span>';
            $website[0]['url']  =  'javascript:;';
            foreach ($website_rel as $key => $value) {
                $website[$value['id']]['name'] =  $value['name'];
                $website[$value['id']]['url']  =  $value['url'];
            }
        }
        return $website;
    }   

    //删除
    public function info_delete($id){
         $condition['id']= (int)$id;
         return $this->where($condition)->delete();
    }

    //查找单个
    public function select_find($condition){
        return $this->where($condition)->find();
    } 

    //查询站点域名和所属用户
    public function find_website($url){
        $condition['url']     = $url;
        $condition['is_lock'] = 0;
        return $this->where($condition)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['user_id']        = (int)$param['user_id'];
        $data['parent_id']      = (int)$param['parent_id'];
        $data['level']          = (int)$param['level'];
        $data['url']            = $param['url'];
        $data['name']           = $param['name'];
        $data['title']          = $param['title'];
        $data['logo']           = $param['logo'];
        $data['themes']         = $param['themes'];
        $data['keywords']       = $param['keywords'];
        $data['description']    = $param['description'];
        $data['icp']            = $param['icp'];
        $data['contacts']       = $param['contacts'];
        $data['address']        = $param['address'];
        $data['root_id']        = (int)$param['root_id'];
        $data['website_reg_id'] = (int)$param['website_reg_id'];
        $data['update_time']    = time();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert();
        }
    } 

    /**
     * [website_config 商户管理中心站点编辑]
     * @param  [type] $param [description]
     * @return [type]        [description]
     */
    public function website_config($param){
        $data['title']        = $param['title'];
        $data['logo']         = $param['logo'];
        $data['keywords']     = $param['keywords'];
        $data['description']  = $param['description'];
        $data['contacts']     = $param['contacts'];
        $data['address']      = $param['address'];
        $data['update_time']  = time();
        $condition['id']      = $param['id'];
        $condition['user_id'] = $param['user_id'];
        return $this->where($condition)->data($data)->update();
    } 

    /**
     * 站点配置模型
     * ############################################################
     */
    //列表
    public function select_config_list($pages){
         return $this->table($this->table_config)->pager((int)$pages,20)->order('update_time desc')->select();
    }

    //查找单个
    public function select_config_find($id){
        $condition['website_id']= (int)$id;
        return $this->table($this->table_config)->where($condition)->find();
    } 
    
    //站点配置
    public function info_config_edit($param){
        $data['is_add_goods']       = intval($param['is_add_goods']);
        $data['is_goods_pass']      = intval($param['is_goods_pass']);
        $data['is_open_authorize']  = intval($param['is_open_authorize']);
        $data['is_goods_authorize'] = intval($param['is_goods_authorize']);
        $data['is_open_pay']        = intval($param['is_open_pay']);
        $data['types']              = intval($param['types']);
        $data['update_time']        = time();
        $condition['website_id']    = (int)$param['website_id'];
        $rel = $this->table($this->table_config)->where($condition)->field('website_id')->find();
        if($rel['website_id']){
            return $this->table($this->table_config)->where($condition)->data($data)->update();
        }else{
            $data['website_id']        = $param['website_id'];
            return $this->table($this->table_config)->data($data)->insert();
        }
    }

    /**
     * 授权目录模型
     * ############################################################
     */
    //查询当前收取目录
    public function select_category_find($website_id,$field = null){
        return $this->table($this->table_goods_category)->where(['website_id' => $website_id])->field($field)->find();
    }

    //编辑授权商品目录
    public function select_category_edit($param){
        $data['category_ids']    = $param['category_ids'];
        $data['update_time']     = time();
        $condition['website_id'] = (int)$param['website_id'];
        $rel = $this->table($this->table_goods_category)->where($condition)->field('website_id')->find();
        if($rel['website_id']){
            return $this->table($this->table_goods_category)->where($condition)->data($data)->update();
        }else{
            $data['website_id']  = (int)$param['website_id'];
            return $this->table($this->table_goods_category)->data($data)->insert();
        }
    }   

    /**
     * 站点等级
     * ############################################################
     */
    //列表
    public function level_list_page($condition = [],$pages = 1,$number = 20){
         return $this->table($this->table_website_level)->where($condition)->pager((int)$pages,$number)->order('id desc')->select();
    }

    //所有列表无翻页
    public function level_list($condition = []){
         return $this->table($this->table_website_level)->where($condition)->order('id desc')->select();
    }
    //查找单个
    public function level_find($condition){
        return $this->table($this->table_website_level)->where($condition)->find();
    } 
    //删除
    public function level_delete($condition){
         return $this->table($this->table_website_level)->where($condition)->delete();
    }

    //站点配置
    public function level_config_edit($param){
        $data['name']        = $param['name'];
        $data['point']       = $param['point'];
        $data['price']       = $param['price'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id'] = $param['id'];
            return $this->table($this->table_website_level)->where($condition)->data($data)->update();
        }else{
            return $this->table($this->table_website_level)->data($data)->insert();
        }
    }

    /**
     * 主题目录
     * ############################################################
     */
    //列表
    public function themes_list_page($condition = [],$pages = 1,$number = 20){
         return $this->table($this->table_website_themes)->where($condition)->pager((int)$pages,$number)->order('id desc')->select();
    }

    //所有列表无翻页
    public function themes_list($condition = []){
         return $this->table($this->table_website_themes)->where($condition)->order('id desc')->select();
    }
    //查找单个
    public function themes_find($condition){
        return $this->table($this->table_website_themes)->where($condition)->find();
    } 
    //删除
    public function themes_delete($condition){
         return $this->table($this->table_website_themes)->where($condition)->delete();
    }

    //站点配置
    public function themes_config_edit($param){
        $data['level']       = (int)$param['level'];
        $data['name']        = $param['name'];
        $data['themes']      = $param['themes'];
        $data['preview']     = $param['preview'];
        $data['content']     = $param['content'];
        $data['website_id']  = (int)$param['website_id'];
        $data['point']       = (int)$param['point'];
        $data['price']       = (int)$param['price'];
        $data['is_below']    = (int)$param['is_below'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id'] = $param['id'];
            return $this->table($this->table_website_themes)->where($condition)->data($data)->update();
        }else{
            return $this->table($this->table_website_themes)->data($data)->insert();
        }
    }

    /**
     * 短信配置
     * ############################################################
     */
    //查找单个
    public function sms_find($condition){
        return $this->table($this->table_website_sms)->where($condition)->find();
    } 
    
    //站点配置
    public function sms_edit($data,$website_id){
        $rel = $this->table($this->table_website_sms)->sms_find(['website_id' => $website_id]);
        if($rel){
            return $this->table($this->table_website_sms)->where(['website_id' => $website_id])->data($data)->update();
        }
        return $this->table($this->table_website_sms)->data($data)->insert();
    }  
}