<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        模板管理
 */
namespace application\common\model;
use application\base\model\Base;
use framework\Flight;

class Tpl extends Base
{

    protected $rootpath = ROOT_PATH.'themes'.DS;

    //处理模板路径
    private function is_root_path($path = null){
        if($path){
            $path    = str_replace('\\','/',$path);
            $path    = str_replace('../','',$path);
            $path    = str_replace('..','',$path);
            $TplPath = realpath($this->rootpath.$path);
            if(is_dir($TplPath)){
                $len        = strlen($this->rootpath);
                $diy_path   = substr($TplPath,$len);
                $folder_ary = $diy_path ? explode('\\',$diy_path) : [];
                array_pop($folder_ary);
                $folder_path['backpath'] = implode('/',$folder_ary);
                $folder_path['path']     = $TplPath.DS;
            }else{
                $folder_path['path'] = $this->rootpath;
            }
            return $folder_path;
        }
        $folder_path['path'] = $this->rootpath;
        return $folder_path;
    }

    //检测目录权限
    public function isdir($path) {
        if(!(is_dir($path) && is_writable($path))){
            $this->errorMsg = '上传根目录不存在！';
            return false;
        }
        return true;
    }  

    //查找包含目录
    public function folder($path = null){
        $folder_path = self::is_root_path($path);
        $file_info   = scandir($folder_path['path']);
        $file_list['backpath'] = [];
        $file_list['folder']   = [];
        $file_list['file']     = [];
        if(!is_null($folder_path['backpath'])){
            $file_list['backpath'][0]['name'] = '返回上级';
            $file_list['backpath'][0]['path'] = $folder_path['backpath'];
        }
        $path = $path ? $path.'/' : '';
        foreach ($file_info as $key => $value) {
            if(is_dir($folder_path['path'].$value)){
                if ($value != "." && $value != ".."){
                    $file_list['folder'][] = ['name' => $value,'path' =>$path.$value]; 
                }
            }else{
                $file_list['file'][] = $value; 
            }               
        }
        return $file_list;
    } 

    //查找包含目录
    public function themes($path = null){
        $folder_path = self::is_root_path($path);
        $file_info   = scandir($folder_path['path']);
        $folder      = [];
        foreach ($file_info as $key => $value) {
            if(is_dir($folder_path['path'].$value)){
                if ($value != "." && $value != ".."){
                    $folder[] = ['name' => $value,'path' =>__PUBLIC__.'themes/'.$value.'/preview.png']; 
                }
            }               
        }
        return $folder;
    } 

    /**
     * 查找上传目录图片
     * @param  [string] $path             [当前查找目录]
     * @param  [string] $dir_root_folder  [自定义更目录]
     * @return [array]                    [返回图片和目录名称]
     */
    public function photoshop($path = null,$dir_root_folder = null){
        $this->rootpath = ROOT_PATH.Flight::config()['UP_FOLDER'].DS;
        if($dir_root_folder) $this->rootpath = $this->rootpath.$dir_root_folder.DS;
        $folder_path = self::is_root_path($path);
        if(!self::isdir($folder_path['path'])){
           return;  
        }
        $file_info   = scandir($folder_path['path']);
        $file_list['backpath'] = [];
        $file_list['folder']   = [];
        $file_list['file']     = [];
        if(!is_null($folder_path['backpath'])){
            $file_list['backpath'][0]['name'] = '返回上级';
            $file_list['backpath'][0]['path'] = $folder_path['backpath'];
        }
        $path   = $path ? $path.'/' : '';
        $imgurl = __URL__.Flight::config()['UP_FOLDER'];
        $imgurl = $dir_root_folder ? $imgurl.'/'.$dir_root_folder.'/'.$path : $imgurl.'/'.$path;
        foreach ($file_info as $key => $value) {
            if(is_dir($folder_path['path'].$value)){
                if ($value != "." && $value != ".."){
                    $file_list['folder'][] = ['name' => $value,'path' =>$path.$value]; 
                }
            }else{
                $file_list['file'][] = $imgurl.$value; 
            }               
        }
        krsort($file_list['file']);
        krsort($file_list['folder']);
        return $file_list;
    }   

    //读取文件内容
    public function get_content($files){
        $files_path = $this->RootDir.$files;
        if(is_file($files_path)){
            return file_get_contents($files_path);
        }
        return FALSE;
    }

    //保存模板文件
    public function set_content($files,$file_contents){
        $files_path = $this->RootDir.$files;
        if(is_file($files_path)){
            if(file_put_contents($files_path,$file_contents)){
                return true;
            }
        }
        return FALSE;
    }
}