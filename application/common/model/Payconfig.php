<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        站点管理
 */
namespace application\common\model;
use application\base\model\Base;

class Payconfig extends Base{

    protected $table        = 'payment';
    protected $table_plugin = 'payment_plugin';

    /**
    * 查询支付配置
    */
    public function info_find($condition){
        return $this->where($condition)->find();
    } 

     /**
    * 查询支付配置
    */
    public function info_config($condition){
        return $this->table("$this->table AS A",TRUE)
        ->join("{pre}$this->table_plugin AS B ON A.plugin_id = B.id")
        ->field('B.apiname,A.*')
        ->where($condition)->find();
    }    
        
    /**
     * 添加配置
     */
    public function info_edit($param){
        if(isset($param['website_id'])){
            $data['website_id']  = $param['website_id'];
        }
        $data['config']      = $param['config'];
        $data['plugin_id']   = $param['plugin_id'];
        $data['pay_name']    = $param['pay_name'];
        $data['client_type'] = $param['client_type'];
        $data['sort']        = $param['sort'];
        $data['status']      = $param['status'];
        if($param['id']){
            $condition['id']= $param['id'];
            unset($data['plugin_id']);
            return $this->where($condition)->data($data)->update();
        }else{
            return $this->data($data)->insert();
        }
    }
    
    /**
     * 添加支持的支付
     */
    public function info_list($param = []){
        return $this->table("$this->table AS A",TRUE)
        ->join("{pre}$this->table_plugin AS B ON A.plugin_id = B.id")
        ->field('B.*,A.*')
        ->where($param)
        ->order('sort desc,A.id desc')->select();
    } 

     /**
     * 删除配置支持的支付
     */
    public function info_delete($param = []){
        return $this->where($param)->delete();
    } 
   
    
    //支付宝支付的接口
    /**
     * 支付配置单个调用
     */
    public function config_find($id){
        $condition['id']= $id;
        return $this->table($this->table_plugin)->where($condition)->find();
    } 
    
    /**
     * 支持的支付列表
     */
    public function config_list($param = []){
        return $this->table($this->table_plugin)->where($param)->select();
    } 

    /**
     * 支持的支付列表
     */
    public function info_state($id){
        $info = $this->config_find($id);
        $data['state'] = $info['state'] ? 0 : 1;
        return $this->table($this->table_plugin)->data($data)->where(['id' => $id])->update();
    } 
}