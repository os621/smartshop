<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        分站管理
 */
namespace application\common\model;
use application\base\model\Base;
use framework\Flight;

class Subshop extends Base{
    
    protected $table = 'website_reg';

    //列表
    public function info_list($condition = [],$page = 1,$n = 20)
    {
        return $this->where($condition)->pager($page,$n)->order("id desc")->select();
    }

    //编辑
    public function info_find($condition)
    {
        return $this->where($condition)->find();
    }

    //删除
    public function info_delete($param)
    {
         return $this->where($param)->delete();
    }

    //用户申请开通子站
    public function info_reg($param){
        $data['telphone']   = $param['phone'];
        $data['title']      = $param['title'];
        $data['url']        = $param['url'];
        $data['icp']        = $param['icp'];
        $data['website_id'] = $param['website_id'];
        $data['user_id']    = $param['user_id'];
        $data['user_reg_id']= $param['user_reg_id'];
        $data['number']     = $param['number'];
        $data['state']  = 0;
        $data['is_pay'] = 0;
        $data['update_time'] = time();
        $data['create_time'] = time();
        return $this->data($data)->insert();
    }

    //更新数据
    public function info_update($condition,$data){
        return $this->data($data)->where($condition)->update();
    }
}