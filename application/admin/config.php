<?php 
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        后台管理框架
 */
return [
  'APP_NAME'    => '网商云平台',
  'APP_VER'     => '1.0',
  'DEV_TEAM'    => 'http://www.wudutongbao.com',
  'SOFT_WARE'   => 'PHP+MYsql,FlightPHP,CanPHP',
  'SYS_VER'     => 'PHP>=5.4,Mysql>=5.6',
  'ACTION_MENU' => [
      [
          'title' => '<i class="iconfont icon-leimu"></i> 商品',
          'menu'  => [
              ['name'=>'<i class="iconfont icon-wodedingdan"></i>  商品管理',
                  'nav'=>[
                      ['name'=>"平台商品(SPU)",'url'=>"/goods/item/index"],
                      ['name'=>"用户商品(SPU)",'url'=>"/goods/item/index?type=1"]
                  ]
              ],
              ['name'=>'<i class="iconfont icon-wodedingdan"></i>  基础信息',
                  'nav'=>[
                      ['name'=>"分类管理",'url'=>"/goods/category/index"],
                      ['name'=>"规格(SKU属性)",'url'=>"/goods/spec/index"],
                      ['name'=>"品牌管理",'url'=>"/goods/brand/index"],
                      ['name'=>"仓库管理",'url'=>"/goods/warehouse/index"],
                      ['name'=>"特性与专栏",'url'=>"/goods/special/index"],
                  ]
              ],            
          ]
      ],
      [
        'title' => '<i class="iconfont icon-training"></i> 内容',
        'menu'  => [
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  内容管理',
                'nav'=>[
                  ['name'=>"内容管理",'url'=>"/article/index/index"],
                  ['name'=>"导航管理",'url'=>"/article/nav/index"],
                  ['name'=>"栏目管理",'url'=>"/article/channel/index"],
                  ['name'=>"属性管理",'url'=>"/article/special/index"],
                  ['name'=>"碎片管理",'url'=>"/article/fragment/index"],
                ]
            ],
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  广告管理',
                'nav'=>[
                    ['name'=>"广告管理",'url'=>"/adwords/position/index"]
                ]
            ],
        ]
      ],
      [
        'title' => '<i class="iconfont icon-yonghu"></i> 会员',
        'menu'  => [
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  会员管理','nav'=>
                [
                    ['name'=>"会员列表",'url'=>"/user/user/index"],
                ]
            ],
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  审核管理','nav'=>
                [
                    ['name'=>"货源商管理",'url'=>"/user/yunshop/index"],
                    ['name'=>"审核新商城",'url'=>"/common/subshop/index"],
                    ['name'=>"工单管理",'url'=>"/user/service/index"],
                ]
            ]
        ]
      ],
      [
        'title' => '<i class="iconfont icon-pingtaiguanli"></i> 站点',
        'menu'  => [
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  网站管理',
                'nav'=>[
                    ['name'=>"分站管理",'url'=>"/common/website/index"],
                    ['name'=>"目录授权",'url'=>"/common/website/lists"],
                    ['name'=>"审核管理",'url'=>"/common/subshop/index"],
                ]
            ],
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  站点配置',
                'nav'=>[
                    ['name'=>"等级管理",'url'=>"/common/website/level"],
                    ['name'=>"站点主题",'url'=>"/common/website/skin"],
                ]
            ],
        ]
      ], 
      [
        'title' => '<i class="iconfont icon-shezhi1"></i> 设置',
        'menu'  => [
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  商品设置',
                'nav'=>[
                    ['name'=>"站点主题",'url'=>"/common/website/skin"],
                    ['name'=>"运费设置",'url'=>"/goods/fare/index"],
                    ['name'=>"短信配置",'url'=>"/common/website/smsconfig"],
                    ['name'=>"支付配置",'url'=>"/common/payconfig/index"],
                    ['name'=>"站点配置",'url'=>"/common/website/sysconfig"],
                ]
            ],  
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  管理员','nav'=>
                [
                    ['name'=>"管理员",'url'=>"/admin/user/index"],
                    ['name'=>"权限组",'url'=>"/admin/group/index"],
                    ['name'=>"权限节点",'url'=>"/admin/auth/index"],
                ]
            ],
            ['name'=>'<i class="iconfont icon-wodedingdan"></i>  数据库','nav'=>
                [
                  ['name'=>"数据表",'url'=>"/admin/database/index"]
                ]
            ]
        ]
      ]
   ],
];