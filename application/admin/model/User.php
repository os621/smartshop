<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        用户管理
 */
namespace application\admin\model;
use application\base\model\Base;

class User extends Base{

    protected $table = 'system_user';
    protected $group_table = 'system_group'; 
  
    //用户登录
	public function login($param){
        $condition['username'] = $param['username'];
        $condition['locks']    = 0;
        $result = $this->where($condition)->field('username,password,id,group_id')->find();
        if($result){
            if(!password_verify(md5($param['password']),$result['password'])) {
                return FALSE;
            }
            $data['last_login_time']      = time();
            $data['last_login_ip']   = \extend\Util::getIp();
            $this->where(array('id'=>$result['id']))->data($data)->update(); 
            return $result;
        }else{
            return FALSE;
        }
	}

    //用户列表
    public function selectUserList($page = 0){
        return $this->table('system_user as A')
            ->join('{pre}system_group as B ON A.group_id = B.id','left')
            ->field('A.*,B.title as group_name')
            ->pager((int)$page,20)
            ->order('A.id desc')
            ->select();
    }

    //单个用户
    public function selectUserInfo($id){
         $condition['id'] = (int)$id;
         return $this->field('id,group_id,username,last_login_time,last_login_ip,locks,content')->where($condition)->find();
    }


    //查询当前登录用户信息
    public function FindUserInfo($id){
        $condition['A.id'] = (int)$id;
        return $this->table('system_user as A')
            ->join('{pre}system_group as B ON A.group_id = B.id','left')
            ->field('A.*,B.title as group_name')
            ->where($condition)
            ->find();
    }

    //删除用户
    public function deleteUser($id){
         $condition['id']= (int)$id;
         return $this->where($condition)->delete();
    }

    //用户添加编辑
    public function updateUserEdit($param){
        $data['group_id']        = $param['group_id'];
        $data['username']        = $param['username'];
        if($param['password']){
            $data['password']    = password_hash(md5($param['password']),PASSWORD_DEFAULT);
        }
        $data['content']         = $param['content'];
        $data['last_login_time'] = time();
        $data['last_login_ip']   = \extend\Util::getIp();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $info = $this->where(array('username' =>$param['username']))->find();
            if($info){
                return FALSE;
            }
            return $this->data($data)->insert($data);
        }
    }

    //修改密码
    public function UserPassWordEdit($param){
        $data['username']        = $param['username'];
        $data['password']        = password_hash(md5($param['password']),PASSWORD_DEFAULT);
        $data['content']         = $param['content'];
        $data['last_login_time'] = time();
        $data['last_login_ip']   = \extend\Util::getIp();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $info = $this->where(array('username' =>$param['username']))->find();
            if($info){
                return FALSE;
            }
            return $this->data($data)->insert($data);
        }
    }
    
    //权限组列表
    public function selectGroupUpdateList(){
        return $this->table($this->group_table)->field('id,title')->order('id desc')->select();
    } 
}