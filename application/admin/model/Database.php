<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        数据库管理模型
 */
namespace application\admin\model;
use application\base\model\Base;

class database extends Base{

    /**
     * 返回表结构
     * @access public
     */
    public function dbtableList(){
        return $this->query('SHOW TABLE STATUS');
    }

    /**
     * 获取数据表
     * @access public
     * @return string
     */
    public function getTable(){
        $result = $this->dbtableList();
        foreach ($result as $value) {
            $tables[] = $value['Name'];
        }
        return implode('`,`',$tables);
    }

    /**
     * 数据库优化
     * @access public
     */
    public function optimized(){
        $tables = $this->getTable();
        if($this->query("OPTIMIZE TABLE `{$tables}`")){
            return ['code' => 200,'msg' => '数据库优化成功'];
        } else {
            return ['code' => 403,'msg' => '数据库优化失败，请稍后重试！'];
        }
    }

    /**
     * 数据库修复
     * @access public
     */
    public function repair(){
        $tables = $this->getTable();
        if($this->query("REPAIR TABLE `{$tables}`")){
            return ['code' => 200,'msg' => '数据库整理成功'];
        } else {
            return ['code' => 403,'msg' => '数据库整理失败，请稍后重试！'];
        }
    } 
}