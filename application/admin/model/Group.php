<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        权限节点和群组模型
 */
namespace application\admin\model;
use application\base\model\Base;
use extend\Category;

class Group extends Base{

    protected $auth_table  = 'system_auth'; 
    protected $group_table = 'system_group'; 

    //权限组列表
    public function selectGroupList($page = 0){
        return $this->table($this->group_table)->pager((int)$page,20)->order('id desc')->select();
    } 

    //获取单个权限组
    public function selectGroupInfo($id,$field = ''){
        $condition['id'] = (int)$id;
        return $this->table($this->group_table)->field($field)->where($condition)->find();
    }

    //编辑权限组
    public function updateGroupEdit($param){
        $auth_ids = json_encode(explode(',',$param['auth_ids']));
        $data = [
            'title'       => $param['title'],
            'auth_ids'    => $auth_ids,
            'about'       => $param['about'],
            'update_time' => time(),
        ];
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->table($this->group_table)->where($condition)->data($data)->update();
        }else{
            $data['create_time']  = time();
            return $this->table($this->group_table)->data($data)->insert($data);
        }
    } 

    //删除权限组
    public function deleteGroup($id){
        $condition['id']= (int)$id;
        return $this->table($this->group_table)->where($condition)->delete();
    }
    
    //#############################################
    //节点列表
    public function selectAuthList($condition = array()){
        return $this->table($this->auth_table)->where($condition)->order('id desc')->select();
    }

    //权限组编辑--节点列表
    public function selectAuthUpdateList($condition = array()){
        return $this->table($this->auth_table)->field('id,title as name,parent_id as pId,auth_node')->where($condition)->order('id desc')->select();
    }

    //获取单个节点
    public function selectAuthInfo($id){
        $condition['id'] = (int)$id;
        return $this->table($this->auth_table)->where($condition)->find();
    }


    //编辑节点
    public function updateAuthEdit($param){
        $data['parent_id']        = (int)$param['parent_id'];
        $data['title']            = $param['title'];
        $data['last_access_path'] = strtolower($param['last_access_path']);
        $data['update_time']      = time();
        if($param['parent_id']){
            $info =  $this->table($this->auth_table)->where(['id'=>$param['parent_id']])->find();
            if($info['root_id']){
                $data['root_id'] = $info['root_id'];
            }else{
                $data['root_id'] = $info['id'];
            }
            $data['access_path'] = $info['access_path'].'.'.$param['last_access_path'];
        }else{
            $data['root_id']     = 0;
            $data['access_path'] = $param['last_access_path'];
        }
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->table($this->auth_table)->where($condition)->data($data)->update();
        }else{
            $data['create_time']  = time();
            return $this->table($this->auth_table)->data($data)->insert($data);
        }
    } 

    /**
     * 获取路径
     * @param int $parent_id
     */
    public function select_path($parent_id) {
        $pathMaps[] = [
            'name'=>'<i class="iconfont icon-shouye"></i> 权限节点','url'=>url('admin/auth/index')
        ];
        $result = $this->aute_node($parent_id);
        foreach ($result as $value) {
            $pathMaps[] = [
                'name'   =>'<i class="iconfont icon-arrowright"></i>'.$value['title'],
                'url'    => url('admin/auth/index',['parent_id'=>$value['id']]),
            ];
        }
        return $pathMaps;
    }

    /**
     * 获取当前路径
     * @param type $parent_id
     * @return type
     */
    protected function aute_node($parent_id){
        $result = $this->table($this->auth_table)->field('id,title,parent_id')->select();
        $tree =  new Category(array('id','parent_id','title','name'));
        return $tree->getPath($result,$parent_id);
    }

    //删除节点
    public function deleteAuth($id){
        $result =  $this->table($this->auth_table)->where(['parent_id'=>$id])->find();
        if($result){
            return;
        }
        $condition['id']= (int)$id;
        return $this->table($this->auth_table)->where($condition)->delete();
    }
    
    /**
     * 权限列表
     * @access public
     * @return array
     */
    public function select_Lists_ids($id){
        $auth_array = [];
        if($id){
            $info = $this->selectGroupInfo($id,'auth_ids');
            $auth_array = json_decode($info['auth_ids'],true);
        }
        $result = $this->table($this->auth_table)->field('id,title as name,parent_id as pId,access_path')->order('id desc')->select();
        if($result){
            foreach ($result as $key => $value) {
                foreach ($auth_array as $id) {
                    if($id == $value['id']){
                        $result[$key] = ['id'=>$value['id'],'pId'=>$value['pId'],'name'=>$value['name'],'checked'=>true];
                    }
                }
            }   
        }
        return $result;
    }
}