<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        数据库管理
 */
namespace application\admin\controller;
use framework\Flight;

class Database extends Common{

    private $db = null;

    public function __construct() {
        parent::__construct();
        $this->db = Flight::model('database');
    }

    //所有表
    public function index(){
        $this->isAuth('admin.db.index');
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 数据库表','url'=>'javascript:;']
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-templatedefault"></i> 优化','url'=>url('admin/database/action',['type'=>1])],
            ['name'=>'<i class="iconfont icon-survey"></i> 整理','url'=>url('admin/database/action',['type'=>2])],
        ];
        //菜单结束
        $tpl_date['pathMaps']   = json_encode($pathMaps);
        $tpl_date['editMenu']   = json_encode($editMenu);
        $tpl_date['table_list'] = $this->db->dbtableList();
        $this->display(0,$tpl_date,'layout');
    } 

    //数据库优化整理
    public function action(){
        $action = Flight::request()->query->action;
        $type   = Flight::request()->query->type;
        if($action == 'post'){
            if($type == 1){
                $this->isAuth('admin.db.optimized',true);
                $result = $this->db->optimized();  //优化
            }else{  
                $this->isAuth('admin.db.repair',true);
                $result = $this->db->repair();  //整理
            }
            Flight::json(['code'=>$result['code'],'data'=>['url'=> url('admin/database/index')],'msg'=>$result['msg']]);
        }else{
            $this->isAuth('admin.db.index');
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-jihualiebiao"></i> 数据库表','url'=>'javascript:;']
            ];
            $tpl_date['info']['posturl'] = Flight::request()->url;
            $tpl_date['info']['backurl'] = url('admin/database/index');
            $tpl_date['info']['info']    = '你确定要整理数据库吗？';
            $tpl_date['table_list']      = $this->db->DBTableList();
            $tpl_date['pathMaps']        = json_encode($pathMaps);
            $tpl_date['type']            = $type;
            $this->display(0,$tpl_date,'layout');
        }
    }
}