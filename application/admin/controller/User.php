<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        用户管理
 */
namespace application\admin\controller;
use framework\Flight;

class User extends Common{

    private $modelUser = null;

    public function __construct() {
        parent::__construct();
        $this->modelUser = Flight::model('user');
    }

    //用户列表
	public function index(){
        $this->isAuth('admin.user.index');
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 管理员','url'=>'javascript:;']
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-cjsqm"></i> 增加管理员','url'=>url('admin/user/edit')]
        ];
        //菜单结束
        $page     = Flight::request()->query->page;
        $tpl_date['userList'] = $this->modelUser->selectUserList($page);
        $tpl_date['pager']    = $this->getPage($this->modelUser->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
	}

    //用户删除
    public function delete(){
        $this->isAuth('admin.user.edit');
        $id = Flight::request()->query->id;
        if($id == 1){
            exit(Flight::json(['code'=>403,'msg'=>'系统管理员禁止删除.']));
        }
        $result = $this->modelUser->deleteUser($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }
    
    //用户编辑
    public function edit(){
        $this->isAuth('admin.user.edit');
        if($this->isPost()){
            $rules = [
                'username' => ['empty'],
                'content'  => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            $data['id']       = $request['id'];
            $data['group_id'] = $request['group_id'];
            $data['username'] = $request['username'];
            $data['password'] = $request['password'];
            $data['content']  = $request['content'];  
            $info = $this->modelUser->updateUserEdit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>'admin/user/index'],'msg'=>'成功创建或编辑用户.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'用户创建或编辑不成功,有相同的用户存在.']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('admin/user/index')]
            ];
            //菜单结束
            $id = (int)Flight::request()->query->id;
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            $tpl_date['id']        = $id;
            $tpl_date['info']      = $this->modelUser->selectUserInfo($id);
            $tpl_date['groupList'] = $this->modelUser->selectGroupUpdateList();
            $this->display(0,$tpl_date,'layout');
        }
    }

    //修改密码
    public function password(){
        if($this->isPost()){
            $rules = [
                'username' => ['empty'],
                'password' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $data['id']        = $this->admin_id;
            $data['username']  = Flight::request()->data['username'];
            $data['password']  = Flight::request()->data['password'];
            $data['content']   = Flight::request()->data['content'];  
            $info = $this->modelUser->UserPassWordEdit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>'admin/index/welcome'],'msg'=>'密码修改成功.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'密码修改失败.']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-jihualiebiaoW"></i> 我的密码','url'=>'javascript:;']
            ];
            //菜单结束
            $tpl_date['id']       = $this->admin_id;
            $tpl_date['info']     = $this->modelUser->selectUserInfo($this->admin_id);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }
}