<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        管理后台基础类
 */
namespace application\admin\controller;
use application\base\controller\Admin;
use framework\Flight;

class Common extends Admin{

    public function __construct() {
        parent::__construct();
        $this->appconfig =  Flight::config();
        $this->assign('appconfig',$this->appconfig);
    }
}