<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        权限节点管理
 */
namespace application\admin\controller;
use framework\Flight;

class Auth extends Common{

    private $modelAuth = null;

    public function __construct() {
        parent::__construct();
        $this->modelAuth = Flight::model('group');
    }

    //列表
	public function index(){
        $this->isAuth('admin.auth.index');
        $parent_id = (int)Flight::request()->query->parent_id;
        //菜单开始
        $pathMaps = $this->modelAuth->select_path($parent_id);;
        $editMenu = [
            ['name'=>'<i class="iconfont icon-cjsqm"></i> 增加节点','url'=>url('admin/auth/edit',['parent_id'=>$parent_id])]
        ];
        //菜单结束
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['authList'] = $this->modelAuth->selectAuthList(['parent_id'=>$parent_id]);
        $this->display(0,$tpl_date,'layout');
	}


    //删除
    public function delete(){
        $this->isAuth('admin.auth.edit');
        $id   = Flight::request()->query->id;
        $result  = $this->modelAuth->deleteAuth($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请检查是否包含有子节点']);
        }       
    }   
  
    //添加/编辑
    public function edit(){
        $this->isAuth('admin.auth.edit');
        if($this->isPost()){
            $rules = [
                'title'            => ['empty'],
                'last_access_path' => ['type','权限节点只能输入英文或字母','EN'],
            ];
            $result = Flight::validator($rules);
            if($result['code'] == 403){
                exit(Flight::json(['code' => 403,'msg' => $result['msg']]));
            }
            $request                  =  Flight::request()->data;
            $data['id']               = $request['id'];
            $data['parent_id']        = $request['parent_id'];
            $data['title']            = $request['title'];
            $data['last_access_path'] = $request['last_access_path'];
            $info = $this->modelAuth->updateAuthEdit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>url('admin/auth/index',['parent_id'=>$data['parent_id']])],'msg'=>'成功创建或编辑应用节点.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'创建或编辑不成功.']);
            }
        }else{
            $id        = (int)Flight::request()->query->id;
            $parent_id = (int)Flight::request()->query->parent_id;
            //菜单结束
            if($parent_id){
                $parent_info = $this->modelAuth->selectAuthInfo($parent_id);
                $tpl_date['parent_info']  = $parent_info['access_path'].'.';
            }else{
                $tpl_date['info'] = $this->modelAuth->selectAuthInfo($id);
                $len = strlen($tpl_date['info']['access_path']) - strlen($tpl_date['info']['last_access_path']);
                $tpl_date['parent_info']  = substr($info['access_path'],0,$len);
                $parent_id = $tpl_date['info']['parent_id'];
            }
            //菜单开始
            $pathMaps = $this->modelAuth->select_path($parent_id);
            $tpl_date['id']        = $id;
            $tpl_date['parent_id'] = (int)$parent_id;
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }

    /**
     * 读取权限列表返回JSON数据
     * @return [type] [description]
     */
    public function ids(){
        $this->isAuth('admin.group.edit');
        $id   = Flight::request()->query->id;
        $info = $this->modelAuth->select_Lists_ids($id);
        return Flight::json($info);
    }
}