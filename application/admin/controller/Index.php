<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        管理首页
 */
namespace application\admin\controller;
use framework\Flight;

class Index extends Common{
 
     public function __construct() {
        parent::__construct();
        $this->modelUser = Flight::model('user');
    }

    //框架
	public function index(){
        $this->assign('nav',Flight::config('admin')['ACTION_MENU']);
        $this->display();
	}

    //首页
    public function welcome(){
        //菜单
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye1"></i> 管理首页','url'=>'javascript:;']
        ];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['userinfo'] = $this->modelUser->FindUserInfo($this->admin_id);
        $this->display(0,$tpl_date,'layout');
    }

    //菜单
    public function menu(){
        $menu_key = intval(Flight::request()->query->id);
        $nav = Flight::config('admin')['ACTION_MENU'];
        Flight::json($nav[$menu_key]['menu']);
    }
    
    //提交登录
    public function login(){
        if($this->isPost()){
            $request = Flight::request()->data;
            $isToken = $this->deToken($request['stamp'],$request['token'],360,true);
            if(!$isToken){
                exit(Flight::json(['code'=>500,'msg'=>'过期的请求,请刷新重试.']));
            }
            $rules = [
                'username' => ['empty'],
                'password' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'data'=>['url'=>'/admin/login'],'msg'=>$rel['msg']]));
            }
            $data['username'] = Flight::filter()->filter_escape($request['username']);
            $data['password'] = Flight::filter()->filter_escape($request['password']);
            $info = $this->modelUser->login($data);
            if($info){
                $this->setAdminLogin($info);   
                Flight::json(['code'=>200,'data'=>['url'=>'/admin']]);
            }else{
                Flight::json(['code'=>500,'msg'=>'帐号或密码错误,请重新输入.']);
            }
        }else{
            $this->assign('token',$this->enToken());
            $this->display();
        }
    }

    //退出登录
    public function logout(){
        $this->clearAdminLogin();
        Flight::redirect('/admin/login');
    } 
}