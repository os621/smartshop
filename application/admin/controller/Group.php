<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        权限组管理
 */
namespace application\admin\controller;
use framework\Flight;

class Group extends Common{

    private $modelAuth = null;

    public function __construct() {
        parent::__construct();
        $this->modelAuth = Flight::model('group');
    }

    //列表
	public function index(){
        $this->isAuth('admin.group.index');
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 权限组','url'=>'javascript:;']
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-cjsqm"></i> 新建组','url'=>url('admin/group/edit')]
        ];
        //菜单结束
        $page = Flight::request()->query->page;
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        $tpl_date['groupList'] = $this->modelAuth->selectGroupList($page);
        $tpl_date['pager']     = $this->getPage($this->modelAuth->pager);
        $this->display(0,$tpl_date,'layout');
	}

    //删除
    public function delete(){
        $this->isAuth('admin.group.edit');
        $id = Flight::request()->query->id;
        $result =  $this->modelAuth->deleteGroup($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>500,'msg'=>'删除失败']);
        } 
    }   
  
    //添加/编辑
    public function edit(){
        $this->isAuth('admin.group.edit');
        if($this->isPost()){
            $rules = [
                'auth_ids' => ['empty'],
                'title'    => ['empty'],
                'about'    => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request       =  Flight::request()->data;
            $data['id']       = $request['id'];
            $data['title']    = $request['title'];
            $data['auth_ids'] = $request['auth_ids'];
            $data['about']    = $request['about'];
            $info = $this->modelAuth->updateGroupEdit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>'admin/group/index'],'msg'=>'成功创建或编辑权限组.']);
            }else{
                Flight::json(['code'=>500,'msg'=>'创建或编辑不成功.']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('admin/group/index')]
            ];
            //菜单结束
            $id = (int)Flight::request()->query->id;
            $info = $this->modelAuth->selectGroupInfo($id,'id,title,auth_ids,about');
            if($info['auth_ids'] ){
                $info['auth_ids'] = implode(',',json_decode($info['auth_ids'],true));
            }
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            $tpl_date['id']   = $id;
            $tpl_date['info'] = $info;
            $this->display(0,$tpl_date,'layout');
        }
    }
}