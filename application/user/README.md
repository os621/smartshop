
#Easyes CMF3.0 路由配置

----

###   配置路径 /config/route.ph
----

'ROUTE' => [
    //默认首页
   '/' => 'cms/index/index',
   //前台
   '/page/top/@id:[0-9]+' => 'cms/index/special',
   '/page/read/@id:[0-9]+' => 'cms/index/reading',
   '/page/@channel_name' => 'cms/index/channel',
]
