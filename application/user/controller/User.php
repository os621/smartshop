<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        用户管理
 */
namespace application\user\controller;
use framework\Flight;

class User extends Common{

    private $modelUser = null;

    public function __construct() {
        parent::__construct();
        $this->user = Flight::model('user/user');
    }

    //首页
    public function index(){
        $page   = (int)Flight::request()->query->page;
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 会员管理','url'=>'javascript:;']
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-jiahao"></i> 添加会员','url'=>url('user/user/edit')]
        ];
        //菜单结束    
        $tpl_date['lists']    = $this->user->select_list(array(),$page);
        $tpl_date['pager']    = $this->getPage($this->user->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //查看个人资料
    public function profile(){
        $id   = (int)Flight::request()->query->input;
        $tpl_date['user']    = Flight::model('user/user')->select_find($id);
        $tpl_date['info']    = Flight::model('user/info')->info_find(['user_id' => $id]);
        $tpl_date['website'] = Flight::model('common/website')->website_name();
        $this->display(0,$tpl_date,'layout');
    }

    //添加&编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'website'  => ['website','用户归属站点必须选择'],
                'username' => ['regname','昵称必须是2-16个字符']
            ];
            $result = Flight::validator($rules);
            if($result['code'] == 500){
                exit(Flight::json(['code' => 500,'msg' => $result['msg']]));
            }
            $request          =  Flight::request()->data;
            $data['id']       = $request['id'];
            $data['website']  = $request['website'];
            $data['username'] = $request['username'];
            $data['phone']    = $request['phone'];
            $data['email']    = $request['email'];
            $data['password'] = $request['password']; 
            $info = $this->user->edit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>'user/user/index'],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'操作失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>'javascript:history.go(-1);'];
            $tpl_date['id']       = Flight::request()->query->id;
            $tpl_date['info']     = $this->user->select_find($tpl_date['id']);
            $tpl_date['website']  = Flight::model('common/website')->select_list();
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //锁定用户
    public function islock(){
        $id = Flight::request()->query->id;
        $this->user->islock($id);
        Flight::json(['code'=>200,'msg'=>'成功']);
    }

    //检测用户名是否重置
    public function isname(){
        $types  = Flight::request()->query->types;
        $userid = (int)Flight::request()->query->userid;
        $data   = Flight::request()->data;
        if($types == 1){
            $condition['username'] = $data['param'];
            $info = "你有相同的用户名,请换一个注册。";
        }else{
            $condition['phone_id'] = $data['param'];
            $info = "手机号已注册,用手机号可找回密码。";
        }
        $condition[] = 'id != '.$userid;
        $result =  $this->user->isfind($condition);
        if($result){
            Flight::json(['status'=>'n','info'=>$info]);
        }else{
            Flight::json(['status'=>'y','info'=>'可以使用']);
        }
    } 
}