<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        货源商管理
 */
namespace application\User\controller;
use application\base\controller\Admin;
use framework\Flight;

class Yunshop extends Admin{

    private $yunshop = null;

    public function __construct(){
        parent::__construct();
        $this->yunshop = Flight::model('user/yunshop');
    }

    //货源商列表
    public function index(){
        $status  = (int)Flight::request()->query->type;
        $button[$status] = 'button-blue';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="button '.$button['0'].'"><i class="iconfont icon-templatedefault"></i> 货源商</span>','url'=>url('user/yunshop/index')],
            ['name'=>'<span class="button '.$button['1'].'"><i class="iconfont icon-caidan"></i> 待审核</span>','url'=>url('user/yunshop/index',['type'=>1])],
            ['name'=>'<span class="button '.$button['2'].'"><i class="iconfont icon-caidan"></i> 未通过</span>','url'=>url('user/yunshop/index',['type'=>2])],
            ['name'=>'<span class="button '.$button['3'].'"><i class="iconfont icon-jiesuo"></i> 已锁定</span>','url'=>url('user/yunshop/index',['type'=>3])],
            ['name'=>'<span class="button '.$button['4'].'"><i class="iconfont icon-tuikuan"></i> 已退款</span>','url'=>url('user/yunshop/index',['type'=>4])]
        ];
        switch ($status) {
            case 1:
                $condition['state'] = 0;
                break;
            case 2:
                $condition['state'] = -1;
                break;
            case 3:
                $condition['state'] = -2;
                break;
            case 4:
                $condition['state'] = -3;
                break;
            default:
                $condition['state'] = 1;
                break;
        }
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode([]);
        $tpl_date['yunshop']  = $this->yunshop->select_list($condition);
        $tpl_date['pager']    = $this->getPage($this->yunshop->pager,['type' =>$status]);
        $this->display(0,$tpl_date,'layout');
    }

    //通过
    public function ispass(){
        if ($this->isPost()) {
            $rules = [
                'types'    => ['number','企业或个人选择'],
                'name'     => ['empty','名牌/企业简称必须输入'],
                'idname'   => ['empty','个人或公司名称必须输入'],
                'idcard'   => ['empty','证件号必须输入'],
                'telphone' => ['empty','联系电话必须输入'],
                'service'  => ['empty','服务电话必须输入'],
                'about'    => ['empty','商品品牌介绍必须输入'],
                'ispass'   => ['empty','审核状态必须选择'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['types']      = $request['types'];
            $data['name']       = $request['name'];
            $data['idname']     = $request['idname'];
            $data['idcard']     = $request['idcard'];
            $data['telphone']   = $request['telphone'];
            $data['service']    = $request['service'];
            $data['about']      = $request['about'];
            $id    = intval($request['id']);
            $ispass = intval($request['ispass']);
            switch ($ispass){
                case 1:
                    $data['state'] = 1; //通过
                    break;
                case 2:
                    $data['state'] = -1;  //不通过
                    break;
                case 3:
                    $data['state'] = -2; //锁定
                    break;
                case 4:
                    $data['state'] = -3; //退款
                    break;
            }
            $info = $this->yunshop->deposit_find(['yunshop_id' => $id]);
            //查询是否付费
            if(($data['state'] == 1 || $data['state'] ==4) && $info['state'] != 1){
                exit(Flight::json(['code'=>500,'msg'=>'审核通过的供应商必须是已付费的']));
            }
            $result = $this->yunshop->where(['id' => $id])->data($data)->update();
            if($result){
                if($ispass == 4){
                    $this->yunshop->info_update(['yunshop_id' => $id],['state' => -1]);
                }
                Flight::json(['code'=>302,'data'=>['url'=>''],'msg'=>'创建或编辑成功.']);
            }else{
                Flight::json(['code'=>500,'msg'=>'创建或编辑不成功.']);
            }
        }else{
            $id = (int)Flight::request()->query->input;
            $tpl_date['info']    = Flight::model('user/yunshop')->select_find(['id'=>$id]);
            $tpl_date['deposit'] = Flight::model('user/yunshop')->deposit_find(['yunshop_id'=>$id]);
            $this->display(0,$tpl_date,'layout');
        }
    }  
}