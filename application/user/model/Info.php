<?php
namespace application\user\model;
use application\base\model\Base;
use framework\Flight;

class Info extends Base{
    
    protected $table = 'user_profile';

    //列表
    public function info_list($condition = [],$page = 1,$n = 20){
        return $this->where($condition)->pager($page,$n)->order('is_first desc,id desc')->select();
    }

    //编辑
    public function info_find($condition){
        return $this->where($condition)->find();
    }


    //添加或编辑
    public function edit(array $data,$id){
        $rel = $this->info_find(['user_id' => $id]);
        if($rel){
            return $this->where(['user_id' => $id])->data($data)->update();
        }else{
            $data['user_id'] = $id;
            return $this->data($data)->insert();
        }
    }


    //添加或编辑
    public function info_edit($param){
        $data['name']        = $param['name'];
        $data['sex']         = $param['sex'];
        $data['birthday']    = $param['birthday'];
        $data['update_time'] = time();
        $rel = $this->info_find(['user_id' => $param['user_id']]);
        if($rel){
            $condition['user_id'] = $param['user_id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['user_id'] = $param['user_id'];
            return $this->data($data)->insert();
        }
    }

    //删除
    public function info_delete($param){
         return $this->where($param)->delete();
    }

    //修改排序
    public function info_first($param){
        $rel = $this->info_find($param);
        if(!$rel['is_first']){
            $this->where(['user_id' => $param['user_id']])->data(['is_first' => 0 ])->update();
        }
        return $this->data(['is_first' => 1])->where($param)->update();
    } 
}
