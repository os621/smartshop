<?php
namespace application\user\model;
use application\base\model\Base;
use framework\Flight;

class Address extends Base{
    
    protected $table = 'user_address';

    //列表
    public function info_list($condition = [],$page = 1,$n = 20)
    {
        return $this->where($condition)->pager($page,$n)->order('is_first desc,id desc')->select();
    }

    //编辑
    public function info_find($condition)
    {
        return $this->where($condition)->find();
    }

    //添加或编辑
    public function info_edit($param)
    {
        $data['user_id']     = $param['user_id'];
        $data['is_first']    = $param['is_first'];
        $data['address']     = $param['address'];
        $data['telphone']    = $param['telphone'];
        $data['name']        = $param['name'];
        $data['website_id']  = $param['website_id'];
        $data['update_time'] = time();
        $data['create_time'] = time();
        if($data['is_first']){
            $this->where(['user_id' => $data['user_id']])->data(['is_first' => 0 ])->update();
        }
        if ($param['id']) {
            $condition['id']      = $param['id'];
            $condition['user_id'] = $param['user_id'];
            return $this->where($condition)->data($data)->update();
        } else {
            return $this->data($data)->insert($data);
        }
    }

    //查询某个条件是否有数据
    public function isfind($condition)
    {
        return $this->where($condition)->find();
    }

    //删除
    public function info_delete($param)
    {
         return $this->where($param)->delete();
    }

    //修改排序
    public function info_first($param)
    {
        $rel = $this->isfind($param);
        if(!$rel['is_first']){
            $this->where(['user_id' => $param['user_id']])->data(['is_first' => 0 ])->update();
        }
        return $this->data(['is_first' => 1])->where($param)->update();
    } 
}
