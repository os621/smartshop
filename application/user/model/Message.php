<?php
namespace application\user\model;
use application\base\model\Base;
use framework\Flight;

class Message extends Base{
    
    protected $table = 'message';


    //列表
    public function info_list($condition = [],$page = 1,$n = 20,$order = 'id desc'){
        return $this->where($condition)->pager($page,$n)->order($order)->select();
    }

    //编辑
    public function info_find($condition){
        return $this->where($condition)->find();
    }

    //添加或编辑
    public function info_edit($data,$condition = []){
        $data['update_time'] = time();
        if(empty($condition)){
            return $this->data($data)->insert($data);
        }else{
            return $this->where($condition)->data($data)->update();
        }
    }

    //删除
    public function info_delete($param){
         return $this->where($param)->delete();
    }
    
    //写入站内信
    public function msg($user_id,$text){
        $data['user_id']     = intval($user_id);
        $data['message']        = Flight::filter()->filter_escape($text);
        $data['update_time'] = time();
        $data['state']       = 0;
        return $this->data($data)->insert();
    }
}
