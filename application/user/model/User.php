<?php
namespace application\user\model;
use application\base\model\Base;
use framework\Flight;

class User extends Base{
    
    protected $table = 'user';

    //列表
    public function select_list($condition = array(),$page = 1){
        return $this->pager((int)$page,20)->order('id desc')->select();
    }

    //编辑
    public function select_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    }

    //查询某个条件是否有数据
    public function isfind($condition){
        return $this->where($condition)->find();
    }

     //修改密码
    public function passpord($param){
        $info = self::select_find($param['user_id']);
        if($info){
            if(!password_verify(md5($param['password']),$info['password'])) {
                return FALSE;
            }
            $data['password'] = password_hash(md5($param['newpassword']),PASSWORD_DEFAULT);
            return $this->where(['id' => $info['id']])->data($data)->update();
        }
        return FALSE;
    }  
    
    //修改支付密码
    public function safepassword($param){
        $info = self::select_find($param['user_id']);
        if($info){
            if(!password_verify(md5($param['password']),$info['password'])) {
                return FALSE;
            }
            if(empty($info['safe_password'])){
                $data['safe_password'] = password_hash(md5($param['newsafepassword']),PASSWORD_DEFAULT);
            }else{
                if(!password_verify(md5($param['safepassword']),$info['safe_password'])) {
                    return FALSE;
                }
                $data['safe_password'] = password_hash(md5($param['newsafepassword']),PASSWORD_DEFAULT);
            }
            return $this->where(['id' => $info['id']])->data($data)->update();
        }
        return FALSE;
    } 
    
    //添加或编辑
    public function edit($param){
        $data['website_id']  = $param['website'];
        $data['phone_id']    = $param['phone'];
        $data['email_id']    = $param['email'];
        $data['username']    = $param['username'];
        $data['password']    = password_hash(md5($param['password']),PASSWORD_DEFAULT);
        $data['login_time']  = time();
        $data['login_ip']    = \extend\Util::getIp();
        $data['update_time'] = time();
        $data['create_time'] = time();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            return $this->data($data)->insert($data);
        }
    } 

    //添加或编辑
    public function islock($id){
        $condition['id']= $id;
        $info = self::isfind($condition);
        if($info['is_lock']){
            $data['is_lock']  = 0;
        }else{
            $data['is_lock']  = 1;
        } 
        return $this->where($condition)->data($data)->update();
    } 

    /**
     * 判断登录用户
     * @access public
     * @return bool
     */
    public function login($param){
        if(preg_match('#^13[\d]{9}$|14^[0-9]\d{8}|^15[0-9]\d{8}$|^18[0-9]\d{8}$#', $param['username'])){
            $condition['phone_id'] = $param['username'];
        }elseif(preg_match('/^([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)$/i', $param['username'])){
            $condition['email_id'] = $param['username'];
        }else{
            $condition['username'] = $param['username'];
        }
        $condition['is_lock'] = 0;
        $info = self::isfind($condition);
        if($info){
            if(!password_verify(md5($param['password']),$info['password'])) {
                return FALSE;
            }
            $data = [
                'login_ip'   => \extend\Util::getIp(),
                'login_time' => time(),
            ];
            $this->where(['id' => $info['id']])->data($data)->update();
            return $info;
        }
        return FALSE;
    }

    //账户注册
    public function register($param){
        $data['website_id']  = $param['website_id'];
        $data['phone_id']    = $param['phone_id'];
        $data['username']    = $param['username'];
        $data['password']    = password_hash(md5($param['password']),PASSWORD_DEFAULT);
        $data['login_time']  = time();
        $data['login_ip']    = \extend\Util::getIp();
        $data['update_time'] = time();
        $data['create_time'] = time();
        return $this->data($data)->insert($data);
    }
}