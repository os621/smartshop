<?php
namespace application\user\model;
use application\base\model\Base;
use framework\Flight;

class Yunshop extends Base{
    
    protected $table = 'yunshop';
    protected $deposit_table = 'yunshop_deposit';

    //列表
    public function select_list($condition = array(),$page = 1){
        return $this->where($condition)->pager((int)$page,20)->order('id desc')->select();
    }

    //查询单个
    public function select_find($condition){
        return $this->where($condition)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['website_id']  = (int)$param['website_id'];
        $data['state']       = (int)$param['state'];
        $data['name']        = trim($param['name']);
        $data['idname']      = trim($param['idname']);
        $data['logo']        = trim($param['logo']);
        $data['types']       = (int)$param['types'];
        $data['telphone']    = trim($param['telphone']);
        $data['service']     = trim($param['service']);
        $data['idcard']      = trim($param['idcard']);
        $data['idimg']       = trim($param['idimg']);
        $data['about']       = trim($param['about']);
        $data['update_time'] = time();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    } 

    //添加或编辑
    public function islock($id){
        $condition['id']= $id;
        $info = self::select_find($condition);
        if($info['is_lock']){
            $data['is_lock']  = 0;
        }else{
            $data['is_lock']  = 1;
        } 
        return $this->where($condition)->data($data)->update();
    } 

    /**
     * ###################################################
     */
    //编辑是否缴纳保证金
    public function deposit_find($condition){
        return $this->table($this->deposit_table)->where($condition)->find();
    } 

    //更新数据
    public function info_update($condition,$data){
        if(self::deposit_find($condition)){
            return $this->table($this->deposit_table)->data($data)->where($condition)->update();
        }
        return $this->table($this->deposit_table)->data($data)->insert();
    }
}