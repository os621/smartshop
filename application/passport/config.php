<?php 
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        系统公共应用
 */
return [
    'APP_NAME'  => '商城会员管理中心',
    'APP_VER'   => '1.0',
    'UP_FOLDER' => 'upload',  //基于站点根目录
    'UP_SIZE'   => '10',
    'UP_EXTS'   => 'gif,jpg,jpeg,png',
];