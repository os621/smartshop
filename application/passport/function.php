<?php 
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        系统公共应用
 */
/**
 * [intEnode  PHP提供函数 crypt 加密]
 * @param  strint $str  [要加密的字符串]
 * @param  string $salt [加密的盐]
 * @return string       [加密后的字符串]
 */
function intEnode($str,$salt = 'IDSmartShop'){
    return crypt($str,$salt);
}

/**
 * 隐藏手机号中间四位
 */
function enPhone($phone){
    return substr_replace($phone,'****',3,4);  
}  