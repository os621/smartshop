<?php
namespace application\passport\api;
use application\base\api\Base;
use framework\Flight;
/**
 * 左侧菜单
 */
class Menu extends Base{
    
    /**
     * 管理中心左侧菜单
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function leftnav($key){
        $leftnav['passport'] = [
            ['name'=>'我的购物','url'=>'javascript:;','is_title' =>1],
            ['name'=>'我的购物车','url'=>url('cart'),'target' => "1"],
            ['name'=>'<i class="iconfont icon-wodedingdan"></i> 我的订单','url'=>url('passport/order')],
            ['name'=>'<i class="iconfont icon-renminbi1688"></i> 待付款','url'=>url('passport/order',['status'=>'waitPay'])],
            ['name'=>'<i class="iconfont icon-ziyouanpai"></i> 待发货','url'=>url('passport/order',['status'=>'waitSend'])],
            ['name'=>'<i class="iconfont icon-survey1"></i> 待收货','url'=>url('passport/order',['status'=>'waitConfirm'])],
            ['name'=>'服务与支持','url'=>'javascript:;','is_title' =>1],
            ['name'=>'我的工单','url'=>url('passport/service/index')],
            ['name'=>'提交工单','url'=>url('passport/service/getservice')],
        ];
        $leftnav['info'] = [
            ['name'=>'账户管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'帐号安全','url'=>url('passport/index')],
            ['name'=>'个人资料','url'=>url('passport/info')],
            ['name'=>'收货地址','url'=>url('passport/address')],
            ['name'=>'服务与支持','url'=>'javascript:;','is_title' =>1],
            ['name'=>'我的工单','url'=>url('passport/service/index')],
            ['name'=>'提交工单','url'=>url('passport/service/getservice')],
        ];
        $leftnav['user'] = [
            ['name'=>'资金管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'账户余额','url'=>url('passport/bankroll/index')],
            ['name'=>'申请提取','url'=>url('passport/bankroll/withdraw')],
            ['name'=>'账户充值','url'=>url('passport/bankroll/rechange')],
            ['name'=>'财务流水','url'=>url('passport/bankroll/log')],
        ]; 

        return $leftnav[$key];
    }
}