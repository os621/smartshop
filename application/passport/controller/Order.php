<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\passport\controller;
use framework\Flight;

class Order extends Common{

    private $order;

    public function __construct() {
        parent::__construct();
        $this->assign('leftnav',Flight::api('passport/menu')->leftnav('passport'));  //引入管理菜单  
        $this->order = Flight::model('payment/order');
    }

    //管理首页
    public function index(){
        $status  = strtolower(Flight::request()->query->status);
        $page    = (int)Flight::request()->query->page;
        switch ($status) {
            case 'waitpay':
                $i = 1;
                $condition = ['pay_status' => 0,'is_del' => 0,'status' => 0];
                break;
            case 'waitsend':
                $i = 2;
                $condition = ['pay_status' => 1,'express_status' => 0,'is_del' => 0,'status' => 0];
                break;
            case 'waitconfirm':
                $i = 3;
                $condition = ['pay_status' => 1,'express_status' => 1,'is_del' => 0,'status' => 0];
                break;
            default:
                $condition = ['is_del' => 0];
                $i = 0;
                break;
        }
        $button[$i] = 'button-blue';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="pure-button '.$button['0'].'"><i class="iconfont icon-wodedingdan"></i> 所有订单</span>&nbsp;','url'=>url('passport/order')],
            ['name'=>'<span class="pure-button '.$button['1'].'"><i class="iconfont icon-renminbi1688"></i> 待付款</span>&nbsp;','url'=>url('passport/order',['status' =>'waitPay'])],
            ['name'=>'<span class="pure-button '.$button['2'].'"><i class="iconfont icon-ziyouanpai"></i> 待发货</span>&nbsp;','url'=>url('passport/order',['status' =>'waitSend'])],
            ['name'=>'<span class="pure-button '.$button['3'].'"><i class="iconfont icon-survey1"></i> 待收货</span>&nbsp;','url'=>url('passport/order',['status' =>'waitConfirm'])],
        ];
        $tpl_date['pathMaps']    = json_encode($pathMaps);
        $condition['website_id'] = $this->website['id'];
        $condition['user_id']    = $this->login_user['user_id'];
        $orderlist = $this->order->info_list($condition,$page,10);
        $order = [];
        if($orderlist){
            $order_id = [];
            foreach ($orderlist as $key => $value) {
                $order_id[] = $value['id'];
            }
            $ids = '';
            if(!empty($order_id)){
                $ids = implode(',',$order_id);
            }
            if(!empty($ids)){
                $where[] = "order_id in($ids)";
                $order = $this->order->order_cache_list($where);
            }
        }
        //重新读取订单西信息
        foreach ($order as $key => $value) {
           $value['spec'] = json_decode($value['spec_and_value_ids'],true);
           $value['amount'] = $value['buy_price']*$value['buy_nums'];
           $order_sku[$value['order_id']][] = $value;
        }
        $tpl_date['list']      = $orderlist;
        $tpl_date['order_sku'] = $order_sku;
        $tpl_date['pager']     = $this->getPage($this->order->pager);
        $this->display(0,$tpl_date,'layout');
    } 

    //管理首页
    public function view(){
        $order_id = (int)Flight::request()->query->id;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 我的订单','url'=>url('passport/order')];  
        //读取订单信息
        $condition['id']         = $order_id;
        $condition['website_id'] = $this->website['id'];
        $condition['user_id']    = $this->login_user['user_id'];
        $condition['is_del']     = 0; 
        $order_info = Flight::model('payment/order')->find_order($condition);
        if(!$order_info){
            Flight::redirect(Flight::request()->base,302);
        }
        $tpl_date['order_info'] = $order_info;
        //读取订单物品清单
        $sku  = Flight::model('payment/order')->cache_list(['order_id' => $order_id]);
        foreach($sku as $key => $value){
            $sku[$key]['spec'] = json_decode($value['spec_and_value_ids'],true);
            $sku[$key]['amount'] = $value['buy_price']*$value['buy_nums'];
        }
        $tpl_date['sku'] = $sku;
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;订单号详情：'.$order_info['order_no'],'url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }   
}

