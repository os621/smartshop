<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理基础控制器
 */
namespace application\passport\controller;
use application\base\controller\Passport;
use framework\Flight;

class Common extends Passport{

    private $webiste_res;

    public function __construct() {
        parent::__construct();
        $this->tplPath = null;
        $this->webiste_res = 'shop/'.intEnode(md5($this->website['id']));
    }

    //图片上传窗口
    public function upload(){
        if($this->isPost()){
            if(empty($_FILES)){
                return false;
            }
            $config = Flight::config();
            $param['maxSize']   = 1024 * 1024 * intval($config['UP_SIZE']);
            $param['allowExts'] = explode(',',$config['UP_EXTS']);
            $param['rootPath']  = ROOT_PATH;
            $param['savePath']  = $config['UP_FOLDER'].'/'.$this->webiste_res.'/'.date('Ym').'/';
            $upload = new \extend\Upload($param);
            if ($upload->upload()){
                $result = $upload->getUploadFileInfo()['0'];
                $str['error'] = 0;
                $str['url']   = __URL__.$result['filepath'];
            }else{
                $str['error']   = 1;
                $str['message'] = $upload->getError();
            }
            Flight::json($str);
        }else{
            $tpl_date['input'] = Flight::request()->query->input;
            $tpl_date['path']  = Flight::request()->query->path;
            $tpl_date['tab']   = Flight::request()->query->tab;
            $tpl_date['lists'] = Flight::model('common/tpl')->photoshop($tpl_date['path'],$this->webiste_res);
            $this->display(0,$tpl_date,'meta');
        }
    }

    //图片商店
    public function photoshop(){
        $tpl_date['input'] = Flight::request()->query->input;
        $tpl_date['path']  = Flight::request()->query->path;
        $tpl_date['tab']   = Flight::request()->query->tab;
        $tpl_date['lists'] = Flight::model('common/tpl')->photoshop($tpl_date['path'],$this->webiste_res);
        $this->display(0,$tpl_date,'meta');
    }

    /**
     * [message 输出友情提示模板]
     */
    protected function message($tpl_date = []){
        $tpl_date['msg_title'] = '友情提示';       
        exit($this->display('message',$tpl_date,'layout'));
    }
}