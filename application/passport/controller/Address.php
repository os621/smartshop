<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-收获地址
 */
namespace application\passport\controller;
use framework\Flight;

class Address extends Common{

    private $address;
    public function __construct() {
        parent::__construct();
        $this->assign('leftnav',Flight::api('passport/menu')->leftnav('info'));  //引入管理菜单 
        $this->address = Flight::model('user/address'); 
    }

    //管理首页
    public function index(){
        $page        = (int)Flight::request()->query->page;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];  
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;收货地址','url'=>'javascript:;'];  
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 新增地址','url'=>url('passport/address/edit')];
        $tpl_date['list']  = $this->address->info_list(['website_id' => $this->website['id'],'user_id' => $this->login_user['user_id']],$page,20);
        $tpl_date['pager'] = $this->getPage($this->address->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    } 

    //收货地址添加编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'name'     => ['empty'],
                'telphone' => ['empty'],
                'address'  => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            $data['id']         = (int)$request['id'];
            $data['is_first']   = (int)$request['is_first'];
            $data['user_id']    = (int)$this->login_user['user_id'];
            $data['website_id'] = (int)$this->website['id'];
            $data['name']       = Flight::filter()->filter_escape(trim($request['name']));
            $data['telphone']   = Flight::filter()->filter_escape(trim($request['telphone']));
            $data['address']    = Flight::filter()->filter_escape(trim($request['address']));
            $info = $this->address->info_edit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>url('passport/address')],'msg'=>'设置成功.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'设置失败']);
            }
        }else{
            $tpl_date['id'] = (int)Flight::request()->query->id;
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];  
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;收货地址','url'=>url('passport/address/index')];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;添加/编辑','url'=>'javascript:;'];
            //菜单结束
            if($tpl_date['id']){
                $tpl_date['info'] = $this->address->info_find(['id' => $tpl_date['id'],'website_id' => $this->website['id']]);
            }else{
                $tpl_date['info'] = [];
            }
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //地址删除
    public function del(){
        $condition['id']      = (int)Flight::request()->query->id;
        $condition['user_id'] = $this->login_user['user_id'];
        $result =  $this->address->info_delete($condition);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }

    //地址删除
    public function first(){
        $id = (int)Flight::request()->data['id'];
        if(!$id) return false;
        $condition['id']      = $id;
        $condition['user_id'] = $this->login_user['user_id'];
        $result =  $this->address->info_first($condition);
        if($result){
            Flight::json(['code'=>200,'msg'=>'设置成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'设置失败']);
        } 
    } 
}