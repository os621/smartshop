<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\passport\controller;
use framework\Flight;

class Index extends Common{

    public function __construct() {
        parent::__construct();
    }

    //管理首页
    public function index(){
        Flight::redirect(url('passport/order'),302);
    } 
}