<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\passport\controller;
use framework\Flight;

class Info extends Common{

    private $info;
    public function __construct() {
        parent::__construct();
        $this->assign('leftnav',Flight::api('passport/menu')->leftnav('info'));  //引入管理菜单  
        $this->info = Flight::model('user/info'); 
    }

    //管理首页
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];  
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;安全中心','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['user']     = $this->info->info_find(['user_id' => $this->login_user['user_id']]);
        $this->display(0,$tpl_date,'layout');
    }  

    //管理首页
    public function info(){
        if($this->isPost()){
            $rules = ['name' => ['empty'],'birthday' => ['empty']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            $data['user_id']  = (int)$this->login_user['user_id'];
            $data['sex']      = (int)$request['sex'];
            $data['name']     = Flight::filter()->filter_escape(trim($request['name']));
            $data['birthday'] = strtotime(trim($request['birthday']));
            $info = $this->info->info_edit($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>url('passport/info')],'msg'=>'设置成功.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'设置失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;个人资料','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['user']     = $this->info->info_find(['user_id' => $this->login_user['user_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //绑定淘宝帐号
    public function taobao(){
        if($this->isPost()){
            $rules = ['taobao_name' => ['empty','淘宝帐号必须填写']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            $data['taobao_name'] = Flight::filter()->filter_escape(trim($request['taobao_name']));
            $user_id = (int)$this->login_user['user_id'];
            $info    = Flight::model('user/info')->edit($data,$user_id);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>url('passport/index')],'msg'=>'设置淘宝帐号成功.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'设置失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;个人资料','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['user']     = $this->info->info_find(['user_id' => $this->login_user['user_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //登录密码
    public function password(){
        if($this->isPost()){
            $rules = [
                'password'      => ['empty','登录密码必须输入'],
                'newpassword'   => ['empty','新密码必须输入'],
                'chickpassword' => ['empty','确认密码必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            if($request['newpassword'] != $request['chickpassword']){
                exit(Flight::json(['code'=>403,'msg'=>'两次密码输入不一致']));
            }
            $data['user_id']       = (int)$this->login_user['user_id'];
            $data['password']      = trim($request['password']);
            $data['newpassword']   = trim($request['newpassword']);
            $data['chickpassword'] = trim($request['chickpassword']);
            $info = Flight::model('user/User')->passpord($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>url('passport/info')],'msg'=>'设置成功.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'设置失败,原密码错误']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;个人资料','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['user']     = $this->info->info_find(['user_id' => $this->login_user['user_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //支付密码
    public function safepassword(){
        if($this->isPost()){
            $rules = [
                'password'          => ['empty','登录密码必须输入'],
                'newsafepassword'   => ['empty','新支付密码必须输入'],
                'chicksafepassword' => ['empty','确认新支付密码'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            if($request['newpassword'] != $request['chickpassword']){
                exit(Flight::json(['code'=>403,'msg'=>'两次密码输入不一致']));
            }
            $data['user_id']           = (int)$this->login_user['user_id'];
            $data['password']          = trim($request['password']);
            $data['safepassword']      = trim($request['safepassword']);
            $data['newsafepassword']   = trim($request['newsafepassword']);
            $data['chicksafepassword'] = trim($request['chicksafepassword']);
            $info = Flight::model('user/User')->safepassword($data);
            if($info){
                Flight::json(['code'=>200,'data'=>['url'=>url('passport/info')],'msg'=>'设置成功.']);
            }else{
                Flight::json(['code'=>403,'msg'=>'设置失败,原密码错误']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;个人资料','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['user']     = $this->info->info_find(['user_id' => $this->login_user['user_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    } 

    /*登录授权*/
    public function auth(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-yonghu"></i> 用户中心','url'=>'javascript:;'];  
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;第三方登录','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }      
}

