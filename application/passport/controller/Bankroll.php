<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-资金管理
 */
namespace application\passport\controller;
use framework\Flight;

class Bankroll extends Common{

    private $bank;

    public function __construct() {
        parent::__construct();
        $this->bank = Flight::model('payment/bank');
        $this->assign('leftnav',Flight::api('passport/menu')->leftnav('user'));  //引入管理菜单  
    }

    //管理首页
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 资金管理','url'=>'javascript:;'];  
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;账户余额','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['bank']  = $this->bank->bank_find(['user_id' => $this->login_user['user_id']]);
        $this->display(0,$tpl_date,'layout');
    } 

    //用户提现
    public function withdraw(){
        if($this->isPost()){
            $rules = [
                'phone' => ['mobile','支付宝绑定手机号必须输入'],
                'code'  => ['number','验证码输入格式错误'],
                'money' => ['empty','提现金额必须输入'],
                'about' => ['empty','备注说明必须输入'],
                'pass'  => ['empty','支付密码必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $pass     = Flight::filter()->filter_escape(trim($request['pass']));
            //判断支付密码
            $is_pay_pass = Flight::api('payment/bank')->is_pay($this->login_user['user_id'],$pass);
            if(!$is_pay_pass) Flight::json(['code'=>403,'msg'=>"支付密码输入错误"]);
            $telphone = Flight::filter()->filter_escape(trim($request['phone']));
            $getcode  = Flight::filter()->filter_escape(trim($request['code']));
            $money    = floatval($request['money']);
            //S 判断验证码
            $session = new \extend\Session($this->CodeID); //短信SESSION
            $code = $session->get();
            if(empty($code)) exit(Flight::json(['code'=>403,'msg'=>"验证码失效请重新获取"]));
            if($code['phone'] != $telphone || $code['code'] != $getcode){
                exit(Flight::json(['code'=>403,'msg'=>"验证码错误"]));
            }
            $session->del();
            //E 判断验证码
            //以手机号查找对应会员
            $user = Flight::model('user/user')->isfind(['id' => $this->login_user['user_id']]);
            if($user['phone_id'] != $telphone) Flight::json(['code'=>403,'msg'=>"输入支付宝帐号不是您的手机号"]);
            //判断应用余额
            $is_pay = Flight::api('payment/bank')->money_down($this->login_user['user_id'],$money,"用户《{$this->login_user['username']}》提现申请金额 ￥{$money}");
            if($is_pay){
                $data['user_id']  = $this->login_user['user_id'];
                $data['content']  = Flight::filter()->filter_escape(trim($request['about']));
                $data['telphone'] = $telphone;
                $data['money']    = $money;
                Flight::model('payment/bank')->bankget_edit($data);
                Flight::json(['code'=>200,'msg'=>"提现申请已提交,请等待审核",'data'=>['url' => url("passport/bankroll/withdraw")]]);
            }else{
                Flight::json(['code'=>403,'msg'=>"提现余额不足,禁止提现"]);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 资金管理','url'=>'javascript:;'];  
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;用户提现','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['bank']  = $this->bank->bank_find(['user_id' => $this->login_user['user_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //帐号充值
    public function rechange(){
        if($this->isPost()){
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 帐号充值','url'=>'javascript:;'];  
            $rules = [
                'money'      => ['empty','充值金额必须填写'],
                'payment_id' => ['empty','你没有选择支付方式'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                $tpl_date['pathMaps']  = json_encode($pathMaps);
                $tpl_date['msg_title'] = '友情提示';
                $tpl_date['msg_info']  = $rel['msg'];
                $this->display('message',$tpl_date,'layout');
            }
            $request    = Flight::request()->data;
            $money      = floatval($request['money']);;
            $payment_id = intval($request['payment_id']);
            $user_id    = intval($this->login_user['user_id']);
            //读取缴费接口
            //支付方式
            $payment = Flight::model('common/payconfig')->info_config(['website_id' => 0,'status' => 0,0 => 'A.id ='.$payment_id]);
            if(!$payment){
                Flight::notFound();
            }
            $payment_api           = $payment['apiname'];
            $payment['notify_url'] = Flight::config('common')['Management_URL'].url("dopay/notify_url/alipay");
            $payment['return_url'] = $this->website['url'].url("dopay/return_url/alipay");
            //创建缴费日志
            $rel = Flight::model('payment/bank')->log($user_id,$money,false,"用户充值");
            if($rel){
                $bank_logs = Flight::model('payment/bank')->log_find(['id' => $rel]);
            }
            $order_info['subject']      = '网商宝云平台充值';
            $order_info['body']         = '网商宝云平台[官方充值]';
            $order_info['store_id']     = 0;
            $order_info['order_no']     = $bank_logs['number'];
            $order_info['order_amount'] = $bank_logs['money'];
            $result = Flight::api("payment/dopay")->$payment_api($order_info,$payment);
            if($result['code'] == 302){
                Flight::redirect($result['url'],302);
            }else{
                Flight::halt(403,$result['msg']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 资金管理','url'=>'javascript:;'];  
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;帐号充值','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['payment']  = Flight::model('common/payconfig')->info_list(['website_id' => 0,'status' => 0]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //财务流水
    public function log(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 资金管理','url'=>'javascript:;'];  
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;财务流水','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['logs']  = $this->bank->log_list(['user_id' => $this->login_user['user_id']]);
        $tpl_date['pager'] = $this->getPage($this->bank->pager);
        $this->display(0,$tpl_date,'layout');
    }  
}

