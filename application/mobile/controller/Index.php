<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站首页
 */
namespace application\mobile\controller;
use framework\Flight;
use extend\Filter;

class Index extends Common
{

    private $category;
    private $website_item;

    public function __construct()
    {
        parent::__construct();
        $this->category = Flight::model('index/category');
        $this->website_item = Flight::model('manage/item');
    }

    /**
     * 网站首页
     */
    public function index(){
        $this->display(0, $tpl_date,'layout');
    }

    /**
     * 网站首页
     */
    public function cate(){
        $this->display(0, $tpl_date,'main');
    } 

    //商城栏目
    public function category(){
        $page        = (int)Flight::request()->query->page;
        $category_id = intval($this->getVar('id'));
        if (!$category_id) {
            Flight::redirect(__URL__, 302);
        }
        $category_info = $this->category->select_find($category_id, 'id,root_id,parent_id,name,title');
        if (!$category_info) {
            Flight::redirect(__URL__, 302);
        }
        $tpl_date['category_id']   =  $category_id;
        $tpl_date['category_path'] =  $this->category->get_path($this->website['id'], $category_id);
        $condition = ['A.is_sale' => 2,'A.is_del' => 0];
        $condition[] = "A.website_id = ".$this->website['id'];
        $condition[] = "FIND_IN_SET({$category_id},A.category_path_id)";
        $item = $this->website_item->select_spu_list($condition, $page);
        if ($item) {
            foreach ($item as $key => $value) {
                $item[$key] = $value;
                $item[$key]['sell_price'] = Flight::api('manage/price')->sku_price($value);
                $item[$key]['tag_ids']    = empty($value['tag_ids']) ? array():explode(',', $value['tag_ids']);
                $item[$key]['props']      = empty($value['props']) ? array():json_decode($value['props'], true);
                $item[$key]['tag_ids']    = empty($value['tag_ids']) ? array():explode(',', $value['tag_ids']);
                $item[$key]['imgs']       = empty($value['imgs']) ? array():json_decode($value['imgs'], true);
                $item[$key]['specs']      = json_decode($value['spec_and_value_ids'], true);
                $item[$key]['url']        = url("item/{$value['id']}.html");
            }
        }
        $tpl_date['item']  = $item;
        $tpl_date['pager'] = $this->itemPage("m/list/{$category_id}.html", $this->website_item->pager);
        $tpl_date['category']  = $category_info;
        $tpl_date['website']['name'] = $category_info['name'];
        $tpl_date['website']['url'] = $this->website['url'];
        $this->display(0, $tpl_date,'main');
    }

    //单个商品
    public function item()
    {
        $item_id   = (int)$this->getVar('id');
        //查询商品SPU
        $condition['A.is_sale']    = 2;
        $condition['A.is_del']     = 0;
        $condition['A.website_id'] = $this->website['id'];
        $condition['B.id']         = $item_id;
        $condition['B.is_sale']    = 2;
        $condition['B.is_del']     = 0;
        $item_info = $this->website_item->select_spu_find($condition, 'B.*,A.website_id,A.category_id,A.category_path_id,A.is_del,A.is_sale,A.price_base,A.math_type,A.price_rise');
        if (!$item_info) {
            Flight::redirect(__URL__, 302);
        }
        //计算销售价格(是SKU价格+站点自定义加价幅度的价格)
        $item_info['sell_price'] = Flight::api('manage/price')->sku_price($item_info);
        //查找商品SKU
        $skumap = array();
        $products = Flight::model('goods/item')->select_sku_list(['spu_id' => $item_id], "sell_price,cost_price,market_price,store_nums,specs_key,pro_no,id");
        if ($products) {
            $sku_price['price_rise'] = $item_info['price_rise'];
            $sku_price['price_base'] = $item_info['price_base'];
            $sku_price['math_type']  = $item_info['math_type'];
            foreach ($products as $product) {
                //计算商品SKU加价后的价格
                $sku_price['sell_price'] = $product['sell_price'];
                $sku_price['cost_price'] = $product['cost_price'];
                $product['sell_price'] = Flight::api('manage/price')->sku_price($sku_price);
                $skumap[$product['specs_key']] = $product;
            }
        }
        //商品分类信息
        $category_id   = (int)$item_info['category_id'];
        $category_info = $this->category->select_find($category_id, 'id,root_id,parent_id,title,name,icon');
        if (!$category_info) {
            Flight::notFound();
        }
        $tpl_date['category_id'] = $category_id;
        $tpl_date['category']    = $category_info;
        //设置看了又看商品数据
        $this->setLookItem($item_id);
        $tpl_date['look_item']  = $this->getLookItem();
        //当前路径
        $tpl_date['category_path'] =  $this->category->get_path($this->website['id'], $category_id);
        $tpl_date['item']    =  $item_info;
        $tpl_date['props']   =  empty($item_info['props']) ? array():json_decode($item_info['props'], true);
        $tpl_date['tag_ids'] =  empty($item_info['tag_ids']) ? array():explode(',', $item_info['tag_ids']);
        $tpl_date['imgs']    =  empty($item_info['imgs']) ? array():json_decode($item_info['imgs'], true);
        $tpl_date['specs']   =  json_decode($item_info['spec_and_value_ids'],true);
        $tpl_date['skumap']  =  json_encode($skumap);
        $this->display(0, $tpl_date,'main');
    }

    /**
     * 搜索商品
     * @货号搜索直接调整对应商品页
     * @return [type] [description]
     */
    public function search(){
        $condition = ['A.is_sale' => 2,'A.is_del' => 0];
        $condition[] = "A.website_id = ".$this->website['id'];
        $item = $this->website_item->select_spu_list($condition, $page);
        if ($item) {
            foreach ($item as $key => $value) {
                $item[$key] = $value;
                $item[$key]['sell_price'] = Flight::api('manage/price')->sku_price($value);
                $item[$key]['tag_ids']    = empty($value['tag_ids']) ? array():explode(',', $value['tag_ids']);
                $item[$key]['props']      = empty($value['props']) ? array():json_decode($value['props'], true);
                $item[$key]['tag_ids']    = empty($value['tag_ids']) ? array():explode(',', $value['tag_ids']);
                $item[$key]['imgs']       = empty($value['imgs']) ? array():json_decode($value['imgs'], true);
                $item[$key]['specs']      = json_decode($value['spec_and_value_ids'], true);
                $item[$key]['url']        = url("item/{$value['id']}.html");
            }
        }
        $tpl_date['item']  = $item;
        $tpl_date['pager'] = $this->itemPage("list/{$category_id}.html", $this->website_item->pager);
        $tpl_date['category']  = $category_info;
        $this->display(0, $tpl_date,'main');
    }

    public function so(){
        //S 关键字
        $keyword = trim(Flight::request()->query->keyword);
        $keyword = Flight::filter()->filter_escape($keyword);
        if (!filter_has_var(INPUT_GET, "keyword") || empty($keyword)) {
            Flight::redirect(__URL__, 302);
        }
        $tpl_date['keyword']  = $keyword;
        //截取关键字第一位 @
        $str_first  = mb_substr($keyword,0,1,'utf-8');
        $is_goodsno = 0;
        switch ($str_first) {
            case '@':
                $is_goodsno = 1;
                break;
            case '#':
                $is_goodsno = 2;
                break;
        }
        if($is_goodsno){
            $keyword  = mb_substr($keyword,1,null,'utf-8');
            if(empty($keyword)){
                Flight::redirect(__URL__, 302);
            }
        }
        //E 关键字
        //S 搜索条件
        $condition['A.is_sale']    = 2;
        $condition['A.is_del']     = 0;
        $condition['A.website_id'] = $this->website['id'];
        switch ($is_goodsno) {
            case 1:
                $condition['B.goods_no'] = $keyword;
                break;
            case 2:
                $condition['B.id'] = intval($keyword);
                break;
            default:
                $condition[] = "name LIKE '%$keyword%'";
                break;
        }
        //E 搜索条件 
        $page = intval(Flight::request()->query->page);
        $item = $this->website_item->select_spu_list($condition, $page, 40);

        if ($item) {
            foreach ($item as $key => $value) {
                if($is_goodsno){
                    Flight::redirect(url('item/'.$value['id'].'.html'),302);
                }else{
                    $item[$key]            = $value;
                    $item[$key]['tag_ids'] =  empty($value['tag_ids']) ? array():explode(',', $value['tag_ids']);
                    $item[$key]['props']   =  empty($value['props']) ? array():json_decode($value['props'], true);
                    $item[$key]['tag_ids'] =  empty($value['tag_ids']) ? array():explode(',', $value['tag_ids']);
                    $item[$key]['imgs']    =  empty($value['imgs']) ? array():json_decode($value['imgs'], true);
                    $item[$key]['specs']   =  json_decode($value['spec_and_value_ids'], true);
                    $item[$key]['url']     =  url("item/{$value['id']}.html");
                    $item[$key]['name']    = str_replace($keyword, '<span class="red">'.$keyword.'</span>', $value['name']);
                    $item[$key]['sell_price'] = Flight::api('manage/price')->sku_price($item[$key]);
                }
            }
        }
        $tpl_date['item']     = $item;
        $tpl_date['pager']    = $this->itemPage("so", $this->website_item->pager, ['keyword' =>$keyword]);
        $tpl_date['category'] = $category_info;
        $this->display(0, $tpl_date,'main');
    }
}
