<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站首页-会员中心
 */
namespace application\mobile\controller;
use framework\Flight;
use extend\Session;

class User extends Common
{

    protected $isCaptcha = 'isCaptcha';  //验证码ID
    private $user = null;

    public function __construct() {
        parent::__construct();
        Flight::register('session','extend\Session',[$this->isCaptcha]);
        $this->user = Flight::model('user/user');
    }

    //帐号登录
    public function login(){
        if($this->isPost()){
            $rules = [
                'loginname'     => ['empty','帐号不能为空'],
                'loginpassword' => ['empty','密码必须输入不正确'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request = Flight::request()->data;
            $data['username']   = Flight::filter()->filter_escape($request['loginname']);
            $data['password']   = Flight::filter()->filter_escape($request['loginpassword']);
            $result  = $this->user->login($data);
            if(!$result){
                Flight::json(['code'=>403,'msg'=>'帐号登录失败']); 
            }else{
                $this->setUserLogin(['user_id' => $result['id'],'username' => $result['username']]);
                $url = $request['uri'] ? $request['uri'] : __URL__;
                Flight::json(['code'=>302,'msg'=>'登录成功','data'=>['url' => urldecode($url)]]);
            }
        }else{
            $tpl_date['uri'] = urlencode(Flight::request()->query->uri);
            $this->display(0,$tpl_date,'main');
        }
    }
    
    //帐号注册
    public function reg(){
        if($this->isPost()){
            $rules = [
                'regphone'      => ['mobile','手机号必须输入'],
                'regname'       => ['regname','昵称必须是2-16个字符'],
                'phone_code'    => ['type','验证输入格式不正确','INT'],
                'password'      => ['len','密码范围在6~16位之间','min:6|max:16'],
                'checkpassword' => ['same','密码两次输入不正确','password'],
                'captcha'       => ['empty','验证码必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            } 
            $request = Flight::request()->data;
            //S 屏蔽图形验证码
            if(strtolower($request['captcha']) != strtolower(Flight::session()->get())){
                exit(Flight::json(['code'=>403,'msg'=>'验证码输入错误']));
            }
            //E 屏蔽图形验证码
            $data['website_id'] = intval($this->website['id']);
            $data['phone_id']   = Flight::filter()->filter_escape($request['regphone']);
            $data['username']   = Flight::filter()->filter_escape($request['regname']);
            $data['password']   = Flight::filter()->filter_escape($request['password']);
            //S 判断验证码
            $code_session = new \extend\Session($this->CodeID); //短信SESSION
            $code = $code_session->get();
            if(empty($code)) exit(Flight::json(['code'=>403,'msg'=>"验证码失效请重新获取"]));
            if($code['phone'] != $data['phone_id'] || $code['code'] != $request['phone_code']){
                exit(Flight::json(['code'=>403,'msg'=>"短信验证码错误"]));
            }
            //E 判断验证码
            //检测用户名或手机是否重置
            $condition[] = "'username' = '{$data['username']}' or phone_id = '{$data['phone_id']}'";
            $info =  $this->user->isfind($condition);
            if($info){
                if($data['username'] == $info['username']){
                    exit(Flight::json(['code'=>403,'msg'=>'你有相同的用户名,请换一个注册。']));
                }else{
                    exit(Flight::json(['code'=>403,'msg'=>'手机号已注册,用手机号可找回密码。']));
                } 
            }
            Flight::session()->del();
            //注册
            $result =  $this->user->register($data);
            if($result){
                $url = $request['uri'] ? $request['uri'] : url('m/passport');
                //设置登录状态
                $this->setUserLogin(['user_id' => $result,'username' => $data['username']]);
                Flight::json(['code'=>302,'data'=>['url' => urldecode($url)],'msg'=>'你的账户注册成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'商品分类创建或编辑不成功']);
            }
        }
    }
    
    //退出登录
    public function logout(){
        $this->clearUserLogin();
        Flight::redirect(__URL__);
    } 

    //获取验证码
    public function captcha(){
        $captcha = new \Minho\Captcha\CaptchaBuilder();
        $captcha->initialize(['width'  => 100,'height' =>35,'line' => false,'curve'  => false,'noise'  => 1]);
        $captcha->create();
        Flight::session()->set($captcha->getText());
        $captcha->output();
    }

    //检测用户名是否重置
    public function isname(){
        $types = intval(Flight::request()->query->types);
        $data  = Flight::request()->data;
        if($types == 1){
            $condition['username'] = Flight::filter()->filter_escape($data['param']);
            $info = "你有相同的用户名,请换一个注册。";
        }else{
            $condition['phone_id'] = Flight::filter()->filter_escape($data['param']);
            $info = "手机号已注册,用手机号可找回密码。";
        }
        $result =  $this->user->isfind($condition);
        if($result){
            Flight::json(['status'=>'n','info'=>$info]);
        }else{
            Flight::json(['status'=>'y','info'=>'可以使用']);
        }
    }
}