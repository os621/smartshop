<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        提交订单
 */
namespace application\mobile\controller;
use framework\Flight;
use extend\Log;

class Order extends Cart{

    private $cart;
    private $address;
    private $pay;

    public function __construct() {
        parent::__construct();
        $this->cart    = Flight::model('index/cart');
        $this->address = Flight::model('user/address');
        $this->pay     = Flight::model('common/payconfig');
    }

    /**
     * 购物车信息
     * @return [type] [description]
     */
    public function cart(){
        //读取购物车中的物品
        $sku_id = self::getSku_id();
        $tpl_date['sku']  = $this->cart->all_item($sku_id,$this->website['id']);
        if(!$tpl_date['sku']) Flight::redirect(url('m/cart',['uri'=>url('mobile/order/cart')]),302);
        //读取我的收货地址
        $tpl_date['address']  = $this->address->info_list(['website_id' => $this->website['id'],'user_id' => $this->login_user['user_id']],0,20);
        //读取支付方式
        $websiet_config  = Flight::model('common/website')->select_config_find($this->website['id']);
        if($websiet_config['is_open_pay']){
            $tpl_date['payment'] = $this->pay->info_list(['website_id' => $this->website['id'],'status' => 0]);
        }else{
            $tpl_date['payment'] = $this->pay->info_list(['website_id' => 0,'status' => 0]);
        }
        $tpl_date['fare'] = Flight::api('manage/price')->fare($tpl_date['sku']);
        $this->display(0,$tpl_date,'main');
    }

    /**
     * 保存提交订单 
     */
    public function order_act(){
        if($this->isPost()){
            $request    = Flight::request()->data;
            $address_id = intval($request['address_id']);
            $is_save    = 1;
            //判断是否自定义地址
            if(!$address_id){
                $rules = [
                    'name'     => ['empty','收货人姓名必须填写'],
                    'telphone' => ['mobile','手机号填写有误'],
                    'address'  => ['empty','收货地址必须填写'],
                ];
                $rel = Flight::validator($rules);
                if($rel['code'] == 403){
                    exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
                }
                $address['name']     = Flight::filter()->filter_escape(trim($request['name']));
                $address['telphone'] = Flight::filter()->filter_escape(trim($request['telphone']));
                $address['address']  = Flight::filter()->filter_escape(trim($request['address']));
                //保存地址
                if($is_save){
                    $address_data['user_id']    = $this->login_user['user_id'];
                    $address_data['is_first']   = 0;
                    $address_data['address']    = $address['address'];
                    $address_data['telphone']   = $address['telphone'];
                    $address_data['name']       = $address['name'];
                    $address_data['website_id'] = $this->website['id'];
                    $this->address->info_edit($address_data);
                }
            }else{
                $condition['website_id'] = $this->website['id'];
                $condition['user_id']    = $this->login_user['user_id'];
                $condition['id']         = $address_id;
                $address = $this->address->info_find($condition);
            }
            //支付方式
            $payment_id = intval($request['payment_id']);
            if(empty($payment_id)){
                exit(Flight::json(['code'=>403,'msg'=>'你没有选择支付方式']));
            }
            //判断是否存在当前的支付方式
            $websiet_config  = Flight::model('common/website')->select_config_find($this->website['id']);
            if($websiet_config['is_open_pay']){
                $payment = $this->pay->info_find(['website_id' => $this->website['id'],'id' => $payment_id,'status' => 0]);
            }else{
                $payment = $this->pay->info_find(['website_id' => 0,'id' => $payment_id,'status' => 0]);
            }
            if(!$payment){
                exit(Flight::json(['code'=>403,'msg'=>'支付平台不存在,请选择其它支付平台']));
            }
            //读取购物车中的商品
            $sku_id = self::getSku_id();
            $sku    = $this->cart->all_item($sku_id,$this->website['id']);
            if(!$sku) exit(Flight::json(['code'=>200,'msg'=>'订单异常','data'=>['url'=>url('m/cart')]]));
            //商品总金额,重量,积分计算
            foreach($sku as $rs){
                $real_amount += $rs['amount'];
                Flight::model('goods/Item')->update_nums($rs['id'],$rs['num']);  //减库存
            }
            $real_amount  = sprintf("%01.2f",$real_amount); //商品价格
            $real_freight = Flight::api('manage/price')->fare($sku);  //运费
            $order_amount = $real_amount+$real_freight; //商品价格+运费价格
            //订单信息
            $order['order_no']        = uuid_no();
            $order['payment_id']      = $payment_id;
            $order['website_id']      = (int)$this->website['id'];
            $order['user_id']         = (int)$this->login_user['user_id'];
            $order['order_amount']    = $order_amount;
            $order['real_amount']     = $real_amount;
            $order['real_freight']    = $real_freight;
            $order['express_name']    = $address['name'];
            $order['express_phone']   = $address['telphone'];
            $order['express_address'] = $address['address'];
            $order_id = Flight::model('payment/order')->insert_order($order);
            //写入订单商品缓存（db_table:order_cache）
            $tem_data = array();
            foreach ($sku as $key => $value) {
                $tem_data['order_id']   = $order_id;
                $tem_data['spu_id']     = $value['spu_id'];
                $tem_data['sku_id']     = $value['id'];
                $tem_data['buy_price']  = $value['sell_price'];
                $tem_data['buy_nums']   = $value['num'];
                $tem_data['name']       = $value['name'];
                $tem_data['img']        = $value['img'];
                $tem_data['spec_and_value_ids'] = json_encode($value['spec']);
                Flight::model('payment/order')->insert_order_cache($tem_data);
            }
            //清空回收站
            $this->clearCart();
            Flight::model('index/cart')->del_item($this->login_user['user_id']);
            //进入付款
            exit(Flight::json(['code'=>302,'msg'=>'提交订单成功','data'=>['url'=>url('m/cart/order_id/'.$order_id)]]));
        }
    }

    //订单支付处理页面
    public function pay(){
        $order_id = intval($this->getVar('id'));
        //读取订单信息
        $condition['id']         = $order_id;
        $condition['website_id'] = $this->website['id'];
        $condition['user_id']    = $this->login_user['user_id'];
        $condition['is_del']     = 0; 
        $condition['pay_status'] = 0; 
        $condition['status']     = 0; 
        $order_info = Flight::model('payment/order')->find_order($condition);
        if(!$order_info){
            Flight::redirect(Flight::request()->base,302);
        }
        $tpl_date['order_info'] = $order_info;
        //读取支付方式
        $websiet_config  = Flight::model('common/website')->select_config_find($this->website['id']);
        if($websiet_config['is_open_pay']){
            $tpl_date['payment'] = $this->pay->info_list(['website_id' => $this->website['id'],'status' => 0]);
        }else{
            $tpl_date['payment'] = $this->pay->info_list(['website_id' => 0,'status' => 0]);
        }
        //读取订单物品清单
        $sku  = Flight::model('payment/order')->cache_list(['order_id' => $order_id]);
        foreach($sku as $key => $value){
            $sku[$key]['spec'] = json_decode($value['spec_and_value_ids'],true);
            $sku[$key]['amount'] = $value['buy_price']*$value['buy_nums'];
        }
        $tpl_date['sku']      = $sku;
        $tpl_date['order_id'] = $order_id;
        $this->display(0,$tpl_date,'main');
    }

    //提交订单
    public function dopay(){
        if($this->isPost()){
            $request  = Flight::request()->data;
            $order_id = intval($request['order_id']);
            if(!$order_id) Flight::notFound();
            $payment_id = intval($request['payment_id']);
            //读取订单信息
            $condition['id']         = $order_id;
            $condition['website_id'] = $this->website['id'];
            $condition['user_id']    = $this->login_user['user_id'];
            $condition['is_del']     = 0; 
            $condition['pay_status'] = 0; 
            $condition['status']     = 0; 
            $order_info = Flight::model('payment/order')->find_order($condition);
            //读取支付接口
            $payment_id = $payment_id ? $payment_id : $order_info['payment_id'];
            if(!$payment_id) Flight::notFound();
            //读取订单物品清单
            $sku  = Flight::model('payment/order')->cache_list(['order_id' => $order_id]);
            foreach($sku as $key => $value){
                $spec = json_decode($value['spec_and_value_ids'],true);
                $skus .= $value['name'].'[';
                foreach($spec as $vo){
                    $skus .= $vo['name'].':'.$vo['spec_value'][1].';';
                }
                $skus .= ']';
            }
            $order_info['body']     = $skus;
            //支付特定传值
            $order_info['subject']  = $this->website['name'].'电商云平台';
            $order_info['store_id'] = $this->website['id'];
            //支付方式
            $websiet_config  = Flight::model('common/website')->select_config_find($this->website['id']);
            if($websiet_config['is_open_pay']){
                $payment = $this->pay->info_config(['website_id' => $this->website['id'],'status' => 0,'A.id ='.$payment_id]);
            }else{
                $payment = $this->pay->info_config(['website_id' => 0,'status' => 0,0 => 'A.id ='.$payment_id]);
            }
            if(!$payment){
                Flight::notFound();
            }
            $payment_api = $payment['apiname'];
            if($payment_api == 'alipaydirect'){
                $payment['notify_url'] = $this->website['url'].url("dopay/notify_url/{$payment_api}");
                $payment['return_url'] = $this->website['url'].url("dopay/return_url/{$payment_api}");
            }
            //读取支付对象            
            $result = Flight::api("payment/dopay")->$payment_api($order_info,$payment);
            if($result['code'] == 302){
                Flight::redirect($result['url'],302);
            }else{
                Flight::halt(403,$result['msg']);
            }
        }
    }

    /**
     * 检测订单状态是否支付
     * @return boolean [description]
     */
    function is_order_start(){
        if($this->isPost()){
            $condition['id']         = intval(Flight::request()->data['id']);
            $condition['website_id'] = intval($this->website['id']);
            $condition['user_id']    = intval($this->login_user['user_id']);
            $condition['pay_status'] = 1;
            $order = Flight::model('payment/order')->find_order($condition);
            if($order){
                Flight::json(['code'=>200,'url' =>url('m/passport/order_id/'.$order['id'])]);
            }
        }
    }
}