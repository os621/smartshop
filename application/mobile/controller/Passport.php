<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\mobile\controller;
use framework\Flight;

class Passport extends Common{

    public function __construct() {
        parent::__construct();
        $this->order = Flight::model('payment/order');
        $this->bank = Flight::model('payment/bank');
        $this->address = Flight::model('user/address'); 
    }

    /**
     * 会员中心
     */
    public function index(){
        $this->display(0,$tpl_date,'main');
    }

    /**
     * 网站首页
     */
    public function order(){
        $status  = strtolower(Flight::request()->query->status);
        $page    = (int)Flight::request()->query->page;
        switch ($status) {
            case 'waitpay':
                $i = 1;
                $condition = ['pay_status' => 0,'is_del' => 0,'status' => 0];
                break;
            case 'waitsend':
                $i = 2;
                $condition = ['pay_status' => 1,'express_status' => 0,'is_del' => 0,'status' => 0];
                break;
            case 'waitconfirm':
                $i = 3;
                $condition = ['pay_status' => 1,'express_status' => 1,'is_del' => 0,'status' => 0];
                break;
            default:
                $condition = ['is_del' => 0];
                $i = 0;
                break;
        }
        $button[$i] = 'button-blue';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="pure-button '.$button['0'].'"><i class="iconfont icon-wodedingdan"></i> 所有订单</span>&nbsp;','url'=>url('passport/order')],
            ['name'=>'<span class="pure-button '.$button['1'].'"><i class="iconfont icon-renminbi1688"></i> 待付款</span>&nbsp;','url'=>url('passport/order',['status' =>'waitPay'])],
            ['name'=>'<span class="pure-button '.$button['2'].'"><i class="iconfont icon-ziyouanpai"></i> 待发货</span>&nbsp;','url'=>url('passport/order',['status' =>'waitSend'])],
            ['name'=>'<span class="pure-button '.$button['3'].'"><i class="iconfont icon-survey1"></i> 待收货</span>&nbsp;','url'=>url('passport/order',['status' =>'waitConfirm'])],
        ];
        $tpl_date['pathMaps']    = json_encode($pathMaps);
        $condition['website_id'] = $this->website['id'];
        $condition['user_id']    = $this->login_user['user_id'];
        $orderlist = $this->order->info_list($condition,$page,10);
        $order = [];
        if($orderlist){
            $order_id = [];
            foreach ($orderlist as $key => $value) {
                $order_id[] = $value['id'];
            }
            $ids = '';
            if(!empty($order_id)){
                $ids = implode(',',$order_id);
            }
            if(!empty($ids)){
                $where[] = "order_id in($ids)";
                $order = $this->order->order_cache_list($where);
            }
        }
        //重新读取订单西信息
        foreach ($order as $key => $value) {
           $value['spec'] = json_decode($value['spec_and_value_ids'],true);
           $value['amount'] = $value['buy_price']*$value['buy_nums'];
           $order_sku[$value['order_id']][] = $value;
        }
        $tpl_date['list']      = $orderlist;
        $tpl_date['order_sku'] = $order_sku;
        $tpl_date['status']    = $status;
        $tpl_date['pager']     = $this->getPage($this->order->pager,['status'=>$status]);
        $this->display(0,$tpl_date,'main');
    } 

    //预览
    public function view(){
        $order_id = (int)Flight::request()->query->id;
        //读取订单信息
        $condition['id']         = $order_id;
        $condition['website_id'] = $this->website['id'];
        $condition['user_id']    = $this->login_user['user_id'];
        $condition['is_del']     = 0; 
        $order_info = Flight::model('payment/order')->find_order($condition);
        if(!$order_info){
            Flight::redirect(Flight::request()->base,302);
        }
        $tpl_date['order_info'] = $order_info;
        //读取订单物品清单
        $sku  = Flight::model('payment/order')->cache_list(['order_id' => $order_id]);
        foreach($sku as $key => $value){
            $sku[$key]['spec'] = json_decode($value['spec_and_value_ids'],true);
            $sku[$key]['amount'] = $value['buy_price']*$value['buy_nums'];
        }
        $tpl_date['sku'] = $sku;
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;订单号详情：'.$order_info['order_no'],'url'=>'javascript:;'];  
        $this->display(0,$tpl_date,'main');
    }
    
    //账户余额
    public function bankroll(){
        $tpl_date['bank']  =  Flight::model('payment/bank')->bank_find(['user_id' => $this->login_user['user_id']]);
        $this->display(0,$tpl_date,'main');
    }

    //账户充值   
    public function rechange(){
        if($this->isPost()){
            $rules = [
                'money'      => ['empty','充值金额必须填写'],
                'payment_id' => ['empty','你没有选择支付方式'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] != 403){
                $tpl_date['pathMaps']  = json_encode($pathMaps);
                $tpl_date['msg_title'] = '友情提示';
                $tpl_date['msg_info']  = $rel['msg'];
                $this->display('message',$tpl_date,'layout');
            }
            $request    = Flight::request()->data;
            $money      = floatval($request['money']);;
            $payment_id = intval($request['payment_id']);
            $user_id    = intval($this->login_user['user_id']);
            //读取缴费接口
            //支付方式
            $payment = Flight::model('common/payconfig')->info_config(['website_id' => 0,'status' => 0,0 => 'A.id ='.$payment_id]);
            if(!$payment){
                Flight::notFound();
            }
            $payment_api           = $payment['apiname'];
            $payment['notify_url'] = Flight::config('common')['Management_URL'].url("dopay/notify_url/alipay");
            $payment['return_url'] = $this->website['url'].url("dopay/return_url/alipay");
            //创建缴费日志
            $rel = Flight::model('payment/bank')->log($user_id,$money,false,"用户充值");
            if($rel){
                $bank_logs = Flight::model('payment/bank')->log_find(['id' => $rel]);
            }
            $order_info['subject']      = '网商宝云平台充值';
            $order_info['body']         = '网商宝云平台[官方充值]';
            $order_info['store_id']     = 0;
            $order_info['order_no']     = $bank_logs['number'];
            $order_info['order_amount'] = $bank_logs['money'];
            $result = Flight::api("payment/dopay")->$payment_api($order_info,$payment);
            if($result['code'] == 302){
                Flight::redirect($result['url'],302);
            }else{
                Flight::halt(403,$result['msg']);
            }
        }else{
            $tpl_date['payment']  = Flight::model('common/payconfig')->info_list(['website_id' => 0,'status' => 0]);
            $this->display(0,$tpl_date,'main');
        }
    }

    //申请提取
    public function withdraw(){
        if($this->isPost()){
            $rules = [
                'phone' => ['mobile','支付宝绑定手机号必须输入'],
                'code'  => ['number','验证码输入格式错误'],
                'money' => ['empty','提现金额必须输入'],
                'about' => ['empty','备注说明必须输入'],
                'pass'  => ['empty','支付密码必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $pass     = Flight::filter()->filter_escape(trim($request['pass']));
            //判断支付密码
            $is_pay_pass = Flight::api('payment/bank')->is_pay($this->login_user['user_id'],$pass);
            if(!$is_pay_pass) Flight::json(['code'=>403,'msg'=>"支付密码输入错误"]);
            $telphone = Flight::filter()->filter_escape(trim($request['phone']));
            $getcode  = Flight::filter()->filter_escape(trim($request['code']));
            $money    = floatval($request['money']);
            //S 判断验证码
            $session = new \extend\Session($this->CodeID); //短信SESSION
            $code = $session->get();
            if(empty($code)) exit(Flight::json(['code'=>403,'msg'=>"验证码失效请重新获取"]));
            if($code['phone'] != $telphone || $code['code'] != $getcode){
                exit(Flight::json(['code'=>403,'msg'=>"验证码错误"]));
            }
            $session->del();
            //E 判断验证码
            //以手机号查找对应会员
            $user = Flight::model('user/user')->isfind(['id' => $this->login_user['user_id']]);
            if($user['phone_id'] != $telphone) Flight::json(['code'=>403,'msg'=>"输入支付宝帐号不是您的手机号"]);
            //判断应用余额
            $is_pay = Flight::api('payment/bank')->money_down($this->login_user['user_id'],$money,"用户《{$this->login_user['username']}》提现申请金额 ￥{$money}");
            if($is_pay){
                $data['user_id']  = $this->login_user['user_id'];
                $data['content']  = Flight::filter()->filter_escape(trim($request['about']));
                $data['telphone'] = $telphone;
                $data['money']    = $money;
                Flight::model('payment/bank')->bankget_edit($data);
                Flight::json(['code'=>200,'msg'=>"提现申请已提交,请等待审核",'data'=>['url' => url("mobile/passport/withdraw")]]);
            }else{
                Flight::json(['code'=>403,'msg'=>"提现余额不足,禁止提现"]);
            }
        }else{
            $tpl_date['bank']  = $this->bank->bank_find(['user_id' => $this->login_user['user_id']]);
            $this->display(0,$tpl_date,'main');
        }
    }


    //服务支持
    public function service(){
        $this->display(0,$tpl_date,'main');
    }  
    
    //收货地址
    public function address(){
        $tpl_date['list']  = $this->address->info_list(['website_id' => $this->website['id'],'user_id' => $this->login_user['user_id']],$page,20);
        $tpl_date['pager'] = $this->getPage($this->address->pager);
        $this->display(0,$tpl_date,'main');
    }  
    
     //登录密码
    public function password(){
        $this->display(0,$tpl_date,'main');
    }     

    //安全密码
    public function safepassword(){
        $this->display(0,$tpl_date,'main');
    }       
}