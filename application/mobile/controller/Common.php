<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站前台基础控制器
 */
namespace application\mobile\controller;
use application\base\controller\Passport;
use framework\Flight;

class Common extends Passport{

    public function __construct() {
        parent::__construct();
        $this->tplPath = $this->ThemesPath.DS.'mobile';  //重置基类定义的模板目录
    }

     /**
     * 分页结果显示
     * @return string  字符串
     * @return action  数组,URL后面的参数
     */
     protected function itemPage($url,$pageArray,$action = array()){
        $html  = '<ul class="page">';
        $html .= '<li><a href="'.$this->pageUrl($url,$pageArray['prevPage'],$action).'"> < </a></li>';
            foreach ($pageArray['allPages'] as $value) {
                if($value){
                    if($value == $pageArray['page']){
                        $html .= '<li class="active">';
                    }else{
                        $html .= '<li>';
                    }
                    $html .= '<a href="'.$this->pageUrl($url,$value,$action).'">'.$value.'</a></li>';
                }        
           }
        $html .= '<li><a href="'.$this->pageUrl($url,$pageArray['nextPage'],$action).'">></a></li>';
        $html .= '</ul>';
        return $html;
    }

    /**
     * 分页URL处理
     * @return page    当前页面
     * @return action  数组,URL后面的参数
     */
    protected function pageUrl($url,$page,$action = array()){
        $ary_page = array('page' => $page);
        if(!empty($action)){
            $ary_page = array_merge($ary_page,$action);
        }
        return url($url,$ary_page);
    }

    /**
     * 设置用户流量数据
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function setLookItem($item_id){
        $data = $this->getLookItem();
        if(empty($data)){
            $data[] = $item_id;
        }else{
            $len = count($data);
            if($len <= 20){
                if(!in_array($item_id,$data)){
                    $data[] = $item_id; 
                }
            }else{
                if(!in_array($item_id,$data)){
                    $key_n = array_rand($data,1);
                    $data[$key_n] = $item_id; 
                }
            } 
        }
        $this->clearLookItem(); //清空重新设置
        return Flight::cookie()->set($this->cookie_item,$data,$this->cookie_item_time);
    }

    /**
     * 获取浏览数据
     * @param  array $date  用户常用数据
     * @return bool
     */
    protected function getLookItem(){
        return Flight::cookie()->get($this->cookie_item);
    }

    //清空浏览数据
    protected function clearLookItem(){
        return Flight::cookie()->del($this->cookie_item);
    }
}