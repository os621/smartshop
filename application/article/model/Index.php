<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        栏目管理
 */
namespace application\article\model;
use application\base\model\Base;
use framework\Flight;

class Index extends Base{

	protected $table = 'article';

    //列表
    public function select_list($channel_id,$pages = 0,$website_id = 0){
        $condition = [];
        if($channel_id){
            $ids = Flight::model('article/channel')->select_channel_sub_cid($channel_id);
            $condition[] = "channel_id in({$ids})";
        }
        if($website_id) $condition['website_id'] = $website_id;        
        return $this->where($condition)->pager((int)$pages,20)->order('neworder desc,id desc')->select();
    }

    //ID获取
    public function info_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    }

    //删除
    public function info_delete($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->delete();
    }

    //编辑
    public function info_edit($param){
        $data['channel_id']  = (int)$param['channel_id'];
        $data['special_ids'] = $param['special_ids'];
        $data['title']       = Flight::filter()->filter_escape(trim($param['title']));
        $data['content']     = Flight::filter()->filter_escape($param['content']);
        $data['url']         = Flight::filter()->filter_escape(trim($param['url']));
        $data['neworder']    = (int)$param['neworder'];
        $data['website_id']  = (int)$param['website_id'];
        $data['update_time'] = time();
        //修改还是添加
        if($param['id']){
            return $this->where(['id'=>(int)$param['id']])->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert();
        } 
    }

    /**
     * 获取路径
     * @param int $id
     */
    public function select_path($parent_id) {
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 内容管理','url'=>url('article/index/index')];
        $result = Flight::model('article/channel')->channel_path($parent_id);
        foreach ($result as $value) {
            $pathMaps[] = ['name' => '<i class="iconfont icon-arrowright"></i>'.$value['name'],'url' => url('article/index/index',['cid'=>$value['id']])];
        }
        return $pathMaps;
    }

    /**
     * 前台获取路径
     * @param int $id
     */
    public function manage_select_path($parent_id) {
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 内容管理','url'=>url('manage/article/index')];
        $result = Flight::model('article/channel')->channel_path($parent_id);
        foreach ($result as $value) {
            $pathMaps[] = ['name' => '<i class="iconfont icon-arrowright"></i>'.$value['name'],'url' => url('manage/article/index',['cid'=>$value['id']])];
        }
        return $pathMaps;
    }
}