<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        碎片管理
 */
namespace application\article\model;
use application\base\model\Base;

class Fragment extends Base{

	protected $table = 'article_fragment';

    //列表
    public function select_list($page = 0,$condition = []){
         return $this->order('id desc')->where($condition)->pager((int)$page,20)->select();
    }

    //查找单个
    public function info_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['website_id']  = $param['website_id'];
        $data['name']        = $param['name'];
        $data['content']     = $param['content'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    }  

    //删除
    public function info_delete($id){
         $condition['id']= (int)$id;
         return $this->where($condition)->delete();
    }
}