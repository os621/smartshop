<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        栏目管理
 */
namespace application\article\model;
use application\base\model\Base;
use framework\Flight;
use extend\Category;

class Channel extends Base{

	protected $table = 'article_channel';
    private $website;

    //管理列表
    public function select_list($id,$website_id = NULL){
        $condition['parent_id'] =  (int)$id;
        if($website_id){
            $condition[] = '(website_id = '.(int)$website_id.' or website_id = 0)';
        }
        return $this->where($condition)->order('neworder desc,id desc')->select();
    }

    //读取全部栏目
    public function select_list_all($condition = []){
        return $this->where($condition)->order('neworder desc,id desc')->select();
    }

    //查找单个
    public function select_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['name']        = $param['name'];
        $data['parent_id']   = (int)$param['parent_id'];
        $data['neworder']    = (int)$param['neworder'];
        $data['tpl_lists']   = $param['tpl_lists'];
        $data['tpl_reads']   = $param['tpl_reads'];
        $data['url']         = $param['url'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id']= (int)$param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            if($data['parent_id']){
                $result = $this->select_find($param['parent_id']);
                if($result['root_id']){
                    $data['root_id'] = $result['root_id'];
                }else{
                    $data['root_id'] = $result['id'];
                }
                $data['website_id']  = $result['website_id'] ? $result['website_id'] : $param['website_id'];
            }else{
                $data['website_id']  = $param['website_id'];
                $data['root_id'] = 0;
            }
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    }  

    //删除
    public function info_delete($param){
        $result = $this->where(['parent_id'=>$param['id']])->find();
        if($result){
            return;
        }
        $info = Flight::model('article/index')->where(['channel_id'=>$param['id']])->find();
        if($info){
            return;
        }
        return $this->where($param)->delete();
    }

    //当前路径
    public function select_path($parent_id,$website_id){
        $info = Flight::model('common/website')->select_find(['id' => $website_id]);
        $str = [];
        if($info){
            $str[] = [
                'name'=>'<i class="iconfont icon-arrowright"></i> '.$info['name'].'[站点]',
                'url' => url('article/channel/index',array('website_id'=>$info['id']))
            ]; 
        }
        $result = $this->channel_path($parent_id);
        foreach ($result as $key => $value) {
            $str[] = [
                'name' =>'<i class="iconfont icon-arrowright"></i>'.$value['name'],
                'url'  => url('article/channel/index',['parent_id'=>$value['id'],'website_id'=>$info['id']]),
            ];
        }
        return $str;
    }

    //前台管理路径
    public function mange_select_path($parent_id){
        $str = [];
        $result = $this->channel_path($parent_id);
        foreach ($result as $key => $value) {
            $str[] = [
                'name' =>'<i class="iconfont icon-arrowright"></i>'.$value['name'],
                'url'  => url('manage/channel/index',['parent_id'=>$value['id'],'website_id'=>$info['id']]),
            ];
        }
        return $str;
    }

    //修改排序
    public function neworder($param){
        $condition['id']= $param['id'];
        return $this->data(['neworder'=>$param['neworder']])->where($condition)->update();
    } 
    
    //栏目路径
    public function channel_path($parent_id){
        $result = $this->field('id,parent_id,name')->order('id desc')->select();
        $tree =  new Category(array('id','parent_id','name','title'));
        return $tree->getPath($result,$parent_id);
    }

    /**
     * 计算某个ID下包含的所有子栏目
     * @return [type] [description]
     */
    public function select_channel_sub_cid($channel_id = 0,$condition = []){
        if($channel_id){
            $str[] = intval($channel_id);
        }
        $result = $this->field('id,parent_id,name')->where($condition)->order('id desc')->select();
        $tree =  new Category(array('id','parent_id','name','title'));
        $channel_array = $tree->getTree($result,$channel_id);
        if($channel_array){
            foreach($channel_array as $key => $rs){
                $str[] = $rs['id'];
            }
        }
        return implode(',',$str);
    }

    /**
     * 栏目列表
     * @access public
     * @return array
     */
    public function select_lists_ids($channel_id = 0,$website_id = 0){
        $condition[] = 'website_id = '.$website_id.' or website_id = 0';
        $result = $this->field('id,name,parent_id as pId')->where($condition)->order('neworder desc,id desc')->select();
        foreach ($result as $key => $value) {
            $menu[$key] = ['id'=>$value['id'],'pId'=>$value['pId'],'name'=>$value['name']];
            if($channel_id == $value['id']){
                $menu[$key]['checked'] = true;
            }
        }   
        return $menu;
    }
}