<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站导航
 */
namespace application\article\model;
use application\base\model\Base;
use framework\Flight;
use extend\Category;

class Nav extends Base{

	protected $table = 'article_nav';
    private $website;

    //后台管理列表
    public function select_list($id,$website_id = NULL){
        $condition['parent_id'] =  (int)$id;
        if($website_id){
            $condition['website_id']= (int)$website_id;
        }
        return $this->where($condition)->order('sort desc,id desc')->select();
    }

    //前台导航管理列表
    public function website_select_list($id,$website_id = NULL){
        $condition['parent_id'] =  intval($id);
        if($website_id){
            $condition[]= "(website_id = ".intval($website_id)." or website_id = 0)";
        }
        return $this->where($condition)->order('sort desc,id desc')->select();
    } 

    //ID查找单个
    public function select_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    } 

    /*
    * 自定义条件查询
    */
    public function get_find($condition,$field = null){
        return $this->where($condition)->field($field)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['name']        = $param['name'];
        $data['parent_id']   = (int)$param['parent_id'];
        $data['sort']        = (int)$param['sort'];
        $data['url']         = $param['url'];
        $data['img']         = $param['img'];
        $data['extend']      = $param['extend'];
        $data['update_time'] = time();
        if(!empty($param['get_id'])){
            $data['get_id']  = (int)$param['get_id'];
        }
        if($param['id']){
            $condition['id']= (int)$param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            if($data['parent_id']){
                $result = $this->select_find($param['parent_id']);
                if($result['root_id']){
                    $data['root_id'] = $result['root_id'];
                }else{
                    $data['root_id'] = $result['id'];
                }
                if($result['website_id']){
                    $data['website_id']  = $result['website_id'];
                }else{
                    $data['website_id']  = $param['website_id'];
                }
            }else{
                $data['website_id']  = $param['website_id'];
                $data['root_id'] = 0;
            }
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    }

    //删除
    public function info_delete($param){
        $result = $this->where(['parent_id'=>(int)$param['id']])->find();
        if($result){
            return;
        }
        return $this->where($param)->delete();
    }

    //后台当前路径
    public function select_path($parent_id,$website_id){
        $info = Flight::model('common/website')->select_find(['id'=>$website_id]);
        $str  = [];
        if($info){
            $str[] = ['name'=>'&nbsp;>&nbsp;'.$info['name'].'[站点]','url' => url('article/nav/index',array('website_id'=>$info['id']))]; 
        }
        $result = $this->channel_path($parent_id);
        foreach ($result as $key => $value) {
            $str[] = ['name'   =>'&nbsp;>&nbsp;'.$value['name'],'url'    => url('article/nav/index',['parent_id'=>$value['id'],'website_id'=>$info['id']])];
        }
        return $str;
    }

    //前台当前路径
    public function website_select_path($parent_id){
        $result = $this->channel_path($parent_id);
        $str = [];
        foreach ($result as $key => $value) {
            $str[] = ['name'  =>'&nbsp;> '.$value['name'],'url' => url('manage/nav/index',['parent_id'=>$value['id']])];
        }
        return $str;
    }

    //修改排序
    public function sort($param){
        $condition['id']= $param['id'];
        return $this->data(['sort'=>$param['sort']])->where($condition)->update();
    } 
    
    //导航路径
    public function channel_path($parent_id){
        $result = $this->field('id,parent_id,name')->order('id desc')->select();
        $tree =  new Category(array('id','parent_id','name'));
        return $tree->getPath($result,$parent_id);
    }

    /**
     * 计算某个ID下包含的所有子导航
     * @return [type] [description]
     */
    public function select_channel_sub_cid($channel_id = 0,$condition = []){
        if($channel_id){
            $str[] = intval($channel_id);
        }
        $result = $this->field('id,parent_id,name')->where($condition)->order('id desc')->select();
        $tree =  new Category(array('id','parent_id','name'));
        $channel_array = $tree->getTree($result,$channel_id);
        if($channel_array){
            foreach($channel_array as $key => $rs){
                $str[] = $rs['id'];
            }
        }
        return implode(',',$str);
    }
}