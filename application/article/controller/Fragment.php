<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        碎片管理
 */
namespace application\article\controller;
use framework\Flight;

class Fragment extends Common{

    private $fragment;
    private $website;

    public function __construct() {
        parent::__construct();
        $this->fragment = Flight::model('fragment');
        $this->website = Flight::model('common/website');
    }

    //列表
    public function index(){
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 碎片管理','url'=>'javascript:;']
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-jiahao"></i> 添加碎片','url'=>url('article/fragment/edit')]
        ];
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        //菜单结束
        $page = Flight::request()->query->page;
        $tpl_date['fragment']  = $this->fragment->select_list($page);
        $tpl_date['pager']     = $this->getPage($this->fragment->pager);
        $tpl_date['website']   = $this->website->website_name();
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = ['name' => ['empty']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']         = $request['id'];
            $data['website_id'] = $request['website_id'];
            $data['name']       = $request['name'];
            $data['content']    = $request['content'];
            $result  =  $this->fragment->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('article/fragment/index')],'msg'=>'成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'失败']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('article/fragment/index')]
            ];
            $editMenu = [];
            //菜单结束
            $tpl_date['id']       = Flight::request()->query->id;
            $tpl_date['info']     = $this->fragment->info_find($tpl_date['id']);
            $tpl_date['website']  = $this->website->select_list();  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode($editMenu);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id     = Flight::request()->query->id;
        $result =  $this->fragment->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }
}