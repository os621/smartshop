<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站导航
 */
namespace application\article\controller;
use application\base\controller\Admin;
use framework\Flight;

class Nav extends Admin{

    private $website;
    private $nav;

    public function __construct() {
        parent::__construct();
        $this->website = Flight::model('common/website');
        $this->nav     = Flight::model('nav');
    }

    //列表
    public function index(){
        $tpl_date['parent_id']  = Flight::request()->query->parent_id;
        $website_id = Flight::request()->query->website_id;
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 导航管理','url'=>url('article/nav/index')];
        $path = $this->nav->select_path($tpl_date['parent_id'],$website_id);
        foreach ($path as $key => $value) {
            $pathMaps[] = $value;
        }
        $editMenu[] = ['name'=>'<i class="iconfont icon-tianjia"></i> 添加导航','url'=>url('article/nav/edit',['parent_id'=>intval($tpl_date['parent_id']),'website_id'=>$website_id])];
        //菜单结束
        $tpl_date['channel']   = $this->nav->select_list($tpl_date['parent_id'],$website_id);
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        $tpl_date['website']   = $this->website->website_name();
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'sort' => ['empty'],
                'name' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']         = $request['id'];
            $data['parent_id']  = $request['parent_id'];
            $data['website_id'] = $request['website_id'];
            $data['sort']       = $request['sort'];
            $data['name']       = $request['name'];
            $data['img']        = $request['img'];
            $data['extend']     = $request['extend'];
            $data['url']        = $request['url'];
            $data['get_id']     = $request['get_id'];
            $result =  $this->nav->info_edit($data);
            if($result){
                Flight::json(['code'=>302,'data'=>['url'=>url('article/nav/index',['parent_id'=>$data['parent_id'],'website_id'=>$data['website_id']])],'msg'=>'导航创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'导航创建或编辑不成功']);
            }
        }else{
            $website_id             = (int)Flight::request()->query->website_id;
            $parent_id              = (int)Flight::request()->query->parent_id;
            $tpl_date['id']         = (int)Flight::request()->query->id;
            $tpl_date['info']       = $this->nav->select_find($tpl_date['id']);
            $tpl_date['parent_id']  = $parent_id  ? $parent_id : (int)$tpl_date['info']['parent_id'];
            $tpl_date['website_id'] = $website_id  ? $website_id : (int)$tpl_date['info']['website_id'];
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-shouye"></i> 导航管理','url'=>url('article/nav/index')]
            ];
            $path = $this->nav->select_path($tpl_date['parent_id'],$tpl_date['website_id']);
            foreach ($path as $key => $value) {
                $pathMaps[] = $value;
            }
            $editMenu = [];
            //菜单结束
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode($editMenu);
            $tpl_date['website']  = $this->website->select_list();
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $param['id']  = Flight::request()->query->id;
        $result =  $this->nav->info_delete($param);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含子导航或内容']);
        } 
    }
    
    //字段排序
    public function neworder(){
        $data['sort'] = intval(Flight::request()->data['sort']);
        $data['id']   = intval(Flight::request()->data['id']);
        $result = $this->nav->sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }

    /**
     *读取导航列表返回数据
     */
    public function ids() {
        $channel_id  = (int)Flight::request()->query->channel_id;
        $website_id  = (int)Flight::request()->query->website_id;
        $info = $this->nav->select_lists_ids($channel_id,$website_id);
        Flight::json($info);
    }    
}