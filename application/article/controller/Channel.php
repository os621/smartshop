<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        栏目管理
 */
namespace application\article\controller;
use framework\Flight;

class Channel extends Common{

    private $special;
    private $website;

    public function __construct() {
        parent::__construct();
        $this->channel = Flight::model('channel');
        $this->website = Flight::model('common/website');
    }

    //列表
    public function index(){
        $tpl_date['parent_id']  = Flight::request()->query->parent_id;
        $website_id = Flight::request()->query->website_id;
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 栏目管理','url'=>url('article/channel/index')]
        ];
        $path = $this->channel->select_path($tpl_date['parent_id'],$website_id);
        foreach ($path as $key => $value) {
            $pathMaps[] = $value;
        }
        $editMenu = [
            ['name'=>'<i class="iconfont icon-tianjia"></i> 添加栏目','url'=>url('article/channel/edit',['parent_id'=>intval($tpl_date['parent_id']),'website_id'=>$website_id])]
        ];
        //菜单结束
        $tpl_date['channel']   = $this->channel->select_list($tpl_date['parent_id'],$website_id);
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        $tpl_date['website']   = $this->website->website_name();
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'neworder'  => ['empty'],
                'name'      => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']        = $request['id'];
            $data['parent_id'] = $request['parent_id'];
            $data['website_id']= $request['website_id'];
            $data['neworder']  = $request['neworder'];
            $data['name']      = $request['name'];
            $data['url']       = $request['url'];
            $data['tpl_lists'] = $request['tpl_lists'];
            $data['tpl_reads'] = $request['tpl_reads'];
            $result =  $this->channel->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('article/channel/index',['parent_id'=>$data['parent_id'],'website_id'=>$data['website_id']])],'msg'=>'栏目创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'栏目创建或编辑不成功']);
            }
        }else{
            $tpl_date['id']         = (int)Flight::request()->query->id;
            $tpl_date['website_id'] = (int)Flight::request()->query->website_id;
            $tpl_date['info']       = $this->channel->select_find($tpl_date['id']);
            $parent_id              = (int)Flight::request()->query->parent_id;
            $tpl_date['parent_id']  = $parent_id  ? $parent_id : (int)$tpl_date['info']['parent_id'];
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-shouye"></i> 栏目管理','url'=>url('article/channel/index')]
            ];
            $path = $this->channel->select_path($tpl_date['parent_id'],$tpl_date['website_id']);
            foreach ($path as $key => $value) {
                $pathMaps[] = $value;
            }
            $editMenu = [];
            //菜单结束
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode($editMenu);
            $tpl_date['website']  = $this->website->select_list();
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id = Flight::request()->query->id;
        $result =  $this->channel->info_delete(['id' => $id]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含子栏目或内容']);
        } 
    }
    
    //字段排序
    public function neworder(){
        $data['neworder'] = intval(Flight::request()->data['sorter']);
        $data['id']       = intval(Flight::request()->data['id']);
        $result = $this->channel->neworder($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }

    /**
     *读取栏目列表返回数据
     */
    public function ids() {
        $channel_id  = (int)Flight::request()->query->channel_id;
        $website_id  = (int)Flight::request()->query->website_id;
        $info = $this->channel->select_lists_ids($channel_id,$website_id);
        Flight::json($info);
    }    
}