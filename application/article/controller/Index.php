<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        文章管理
 */
namespace application\article\controller;
use framework\Flight;

class Index extends Common{

    private $index;
    private $channel;
    private $special;

    public function __construct() {
        parent::__construct();
        $this->index   = Flight::model('index');
        $this->channel = Flight::model('channel');
        $this->special = Flight::model('special');
        $this->website = Flight::model('common/website');
    }

    //列表
    public function index(){
        $cid  = (int)Flight::request()->query->cid;
        $page = (int)Flight::request()->query->page;
        $pathMaps = $this->index->select_path($cid);
        $editMenu = [
            ['name'=>'<i class="iconfont icon-tianjia"></i> 添加内容','url'=>url('article/index/add')]
        ];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $website = $this->website->website_name();
        $channel = $this->channel->select_list_all(); 
        if($channel){
            foreach ($channel as $key => $value) {
                $channel[$value['id']]['website_name'] =  $website[$value['website_id']]['name'];
                $channel[$value['id']]['website_url']  =  $website[$value['website_id']]['url'];
                $channel[$value['id']]['name']         =  $value['name'];
                $channel[$value['id']]['url']          =  $value['url'];
            }
        }
        $tpl_date['lists']   = $this->index->select_list($cid,$page);
        $tpl_date['pager']   = $this->getPage($this->index->pager,array('cid'=>$cid));
        $tpl_date['channel'] = $channel;
        $this->display(0,$tpl_date,'layout');
    }

    //添加编辑内容
    public function add(){
        if($this->isPost()){
            $rules = [
                'title'       => ['empty','标题必须填写'],
                'channel_id'  => ['empty','内容栏目必须选择'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $result =  $this->index->info_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('article/index/index',['cid'=>$request['channel_id']])],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $tpl_date['website'] = $this->website->select_list();
            $this->display(0,$tpl_date,'layout');
        }
    }

    //添加编辑内容
    public function edit(){
        if($this->isPost()){
            $rules = [
                'title'       => ['empty','标题必须填写'],
                'channel_id'  => ['empty','内容栏目必须选择'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $result =  $this->index->info_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('article/index/index',['cid'=>$request['channel_id']])],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $id   = (int)Flight::request()->query->id;
            $tpl_date['info'] = $this->index->info_find($id);
            $channel = $this->channel->select_find($tpl_date['info']['channel_id']); 
            if($channel){
                $tpl_date['website_id'] = $channel['website_id'];
            }
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id = Flight::request()->query->id;
        $result =  $this->index->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        }
    }
}