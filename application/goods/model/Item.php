<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品列表
 */
namespace application\goods\model;
use application\base\model\Base;
use framework\Flight;

class Item extends Base{

    protected $table     = 'goods_spu';  //类型
    protected $table_sku = 'goods_spu_sku';  //单品数据库
    protected $table_isv = 'goods_spu_spec_value';  //处理关联数据库

    //商品列表
    public function select_list($condition,$pages = 0){
        return $this->where($condition)->pager((int)$pages,20)->order('id desc')->select();
    }

    //商品列表-无翻页(必须是上架或未删除的商品)
    public function select_list_all($param = [],$field = null){
        $condition = $param;
        $condition['is_sale'] = 2;
        $condition['is_del']  = 0;
        return $this->where($condition)->field($field)->order('id desc')->select();
    }

    //商品单个
    public function select_find($id,$field = null){
        if(is_array($id)){
            $condition = $id;
        }else{
            $condition['id']= (int)$id;
        }
        return $this->where($condition)->field($field)->find();
    }
    
    //添加或编辑
    public function info_edit($param){
        $data['category_id']        = (int)$param['category_id'];
        $data['category_path_id']   = $param['category_path_id'];
        $data['brand_id']           = (int)$param['brand_id'];
        $data['warehouse_id']       = (int)$param['warehouse_id'];
        $data['name']               = $param['name'];
        $data['subtitle']           = $param['subtitle'];
        $data['tag_ids']            = $param['tag_ids']; 
        $data['goods_no']           = $param['goods_no'];
        $data['sell_price']         = (float)$param['sell_price'];
        $data['market_price']       = (float)$param['market_price'];
        $data['cost_price']         = (float)$param['cost_price'];
        $data['store_nums']         = (float)$param['store_nums'];
        $data['warning_line']       = (float)$param['warning_line'];
        $data['weight']             = (int)$param['weight'];
        $data['unit']               = $param['unit'];
        $data['content']            = $param['content'];
        $data['props']              = $param['props'];
        $data['spec_and_value_ids'] = $param['spec_and_value_ids'];
        $data['is_sale']            = (int)$param['is_sale'];
        $data['img']                = $param['img'];
        $data['imgs']               = $param['imgs'];
        $data['update_time']        = time();
        $data['website_id']         = $param['website_id'];
        $data['source']             = $param['source'];
        $data['note']               = $param['note'];
        $data['point']              = (int)$param['point'];
        $data['sort']               = (int)$param['sort'];
        $data['taobao_item_id']     = $param['taobao_item_id'];
        $data['goods_data']         = $param['goods_data'];
        if($param['id']){
            $condition['id'] = (int)$param['id'];
            $result = $this->where($condition)->data($data)->update();
            return $result ? $condition['id'] : '';
        }else{
            $data['create_time']  = time();
            return $this->data($data)->insert($data);
        }
    } 

    /**
     * [is_my_goods 判断是否云的商品]
     * @param  [string]  $ids      [商品ID,ID,ID]
     * @return array
     */
    protected function is_yun_goods($ids){
        $spu_id = [];
        if(!empty($ids)){     
            $spu_list =  $this->where(["id in($ids)"])->field('id,website_id')->select();
            if ($spu_list) {
                foreach ($spu_list as $key => $value) {
                    if($value['website_id'] == 0){
                        $spu_id['yun'][] = $value['id'];
                    }else{
                        $spu_id['orther'][] = $value['id'];
                    }
                }
            }
        }
        return $spu_id;
    }

    //上架,下架,从回收站恢复
    public function spu_all_action($issale,$ids,$website_id = null){ 
        switch ($issale) {
            case 1:
                $data['is_sale'] = 1; //下架
                break;
            case 2:
                $data['is_sale'] = 2;  //在售
                break;
            default:
                $data['is_sale'] = 0;  //恢复商品到未审核状态
                $data['is_del']  = 0;  //恢复商品到未软删除状态
                break;
        }
        $condition = [];
        if($website_id){
            $condition['website_id']         = $website_id;
            $website_condition['website_id'] = $website_id;
        }
        $spu_id = self::is_yun_goods($ids);
        if($issale == 1){
            Flight::model('manage/special')->spu_delete($ids);
            if(!empty($spu_id['yun'])){
                $yun_spu_ids = implode(',',$spu_id['yun']); 
                Flight::model('manage/item')->where(["spu_id in($yun_spu_ids)"])->delete();  //下架就删除所有用户已经上架的所有商品
            }
        }
        if(!empty($spu_id['orther'])){
            $orther_spu_ids = implode(',',$spu_id['orther']); 
            $website_condition[] = "spu_id in($orther_spu_ids)";
            Flight::model('manage/item')->where($website_condition)->data($data)->update();   //下架所有非云商品
        }
        $condition[] = "id in($ids)";
        return $this->where($condition)->data($data)->update(); //操作所有SPU商品
    }

    //删除SPU商品
    public function spu_delete($id,$ids,$website_id = null){
        $data['is_sale'] = 0;
        $data['is_del']  = 1;
        if($website_id){
            $condition['website_id'] = $website_id;
        }
        //单个删除
        if($id){
            $condition['id'] = $id;
            $rel = $this->select_find($id);
            if($rel['is_del']){
                $this->where($condition)->delete();
                $this->item_spec_value_del(['spu_id' => $id]);  //删除关联
                $this->sku_delete(['spu_id' => $id]);   //删除SKU
                Flight::model('manage/item')->where(["spu_id in($id)"])->delete();  //物理删除用户添加商品
                return true;
            }
            //非平台商品
            if($rel['website_id'] == 0){
                Flight::model('manage/item')->where(["spu_id in($id)"])->delete();  //物理删除用户添加商品
            }else{
                Flight::model('manage/item')->where(["spu_id in($id)"])->data($data)->update(); //软删除(跟新状态)
            }
            //软删除
            Flight::model('manage/special')->spu_delete($id);  //物理删除专题商品
            return $this->where($condition)->data($data)->update();
        }else{
            //批量删除
            if(!empty($ids)){
                $condition[] = "id in($ids)";
                $rel = $this->where($condition)->select();
                $is_del[0] = [];
                $is_del[1] = [];
                if($rel){
                    foreach ($rel as $key => $value) {
                        //筛选是物理还是还是软删除
                        if($value['is_del']){
                            $is_del[1][] = $value['id'];
                        }else{
                            $is_del[0][] = $value['id'];
                        }
                        //筛选是否云商品
                        if($rel['website_id'] == 0){
                            $yun_spu_id[1][] = $value['id'];
                        }else{
                            $order_spu_id[0][] = $value['id'];
                        }
                    }
                }
                if(!empty($is_del[0])){ //软删除
                    $up_ids = implode(',',$is_del[0]);
                    Flight::model('manage/special')->spu_delete($up_ids);  //删除所有专栏商品
                    if(empty($yun_spu_id[1])){  //物理删除
                        Flight::model('manage/item')->where(["spu_id in($up_ids)"])->data($data)->delete();
                    }
                    if(empty($order_spu_id[0])){  //软删除
                        Flight::model('manage/item')->where(["spu_id in($up_ids)"])->data($data)->update();
                    }
                    return $this->where(["id in($up_ids)"])->data($data)->update();
                }
                if(!empty($is_del[1])){ //物理删除
                    $del_ids      = implode(',',$is_del[1]);
                    $spu_id_ary[] = "spu_id in($del_ids)";
                    $this->sku_delete($spu_id_ary);   //删除SKU
                    $this->item_spec_value_del($spu_id_ary);  //删除商品规格关联信息
                    Flight::model('manage/item')->where($spu_id_ary)->delete();  //物理删除用户添加商品
                    return $this->where(["id in($del_ids)"])->delete();
                }
            }
        }
        return false;
    }

    /**
     * ##############################################
     * [select_sku_list SKU商品列表]
     * @param  [array] $condition [自定义查询条件]
     * @param  [string] $field    [限制筛选字段]
     * @return [array]            [查询结果]
     */
    public function select_sku_list($condition,$field = null){
        return $this->table($this->table_sku)->where($condition)->field($field)->order('id asc')->select();
    }

    /**
     * [select_sku_find 单个SKU产品信息]
     * @param  [array] $condition [自定义查询条件]
     * @param  [string] $field    [限制筛选字段]
     * @return [array]            [查询结果]
     */
    public function select_sku_find($condition,$field = null){
        return $this->table($this->table_sku)->where($condition)->field($field)->find();
    }

    /**
     * [update_nums 更新库存信息]
     * @param  integer  $spu_id  [商品SKU的ID]
     * @param  integer  $number  [库存变更数量]
     * @param  integer $is_type  [增加还是减少]
     * @return [Boolean]         [返回是否更新成功]
     */
    public function update_nums($spu_id,$number,$is_type = 0){
        if($is_type){
            $type = "store_nums + {$number}";
        }else{
            $type = "store_nums - {$number}";
        }
        $sql = "UPDATE {pre}{$this->table_sku} SET store_nums = {$type}  WHERE ID = {$spu_id}";
        return $this->query($sql);
    }

    //编辑SKU产品信息
    public function sku_edit($sku_spec,$data){
        $i = 0;
        if(!empty($sku_spec)){
            foreach ($sku_spec as $key => $value) {
                $sku_data = [
                    'spu_id'             => (int)$data['spu_id'],
                    'pro_no'             => Flight::filter()->filter_escape($data['sku_pro_no'][$i]),
                    'store_nums'         => (int)$data['sku_store_nums'][$i],
                    'warning_line'       => (int)$data['sku_warning_line'][$i],
                    'sell_price'         => (float)$data['sku_sell_price'][$i],
                    'market_price'       => (float)$data['sku_market_price'][$i],
                    'cost_price'         => (float)$data['sku_cost_price'][$i],
                    'weight'             => (int)$data['sku_weight'][$i],
                    'specs_key'          => $key,
                    'spec_and_value_ids' => json_encode($value),
                ];
                $condition = ["spu_id = {$sku_data['spu_id']} and specs_key = '{$sku_data['specs_key']}'"];
                $result = $this->select_sku_find($condition,'id');
                if($result){
                    $this->table($this->table_sku)->where($condition)->data($sku_data)->update();
                }else{
                    $this->table($this->table_sku)->data($sku_data)->insert();
                }
                $i++; 
            }
        }
        return $i;
    }

    //编辑产品信息（无SKU）
    public function sku_edit_spu($data){
        $sku_data = [
            'spu_id'             => (int)$data['spu_id'],
            'pro_no'             => Flight::filter()->filter_escape($data['pro_no']),
            'store_nums'         => (int)$data['store_nums'],
            'warning_line'       => (int)$data['warning_line'],
            'sell_price'         => (float)$data['sell_price'],
            'market_price'       => (float)$data['market_price'],
            'cost_price'         => (float)$data['cost_price'],
            'weight'             => (int)$data['weight'],
            'specs_key'          => '',
            'spec_and_value_ids' => json_encode([]),
        ];
        $condition['spu_id'] = $sku_data['spu_id'];
        $result = $this->select_sku_find($condition,'id');
        if($result){
            return $this->table($this->table_sku)->where($condition)->data($sku_data)->update();
        }else{
            return $this->table($this->table_sku)->data($sku_data)->insert();
        }
    }

    //清理多余的SKU商品
    public function sku_delete($condition){
        return $this->table($this->table_sku)->where($condition)->delete();
    }
    
    /**
     * 笛卡尔乘积
     * @param  [type] $spu_spec_ids   [商品规格ID]   1,2,3
     * @param  [type] $sku_spec_item  [商品规格组合] 159:银色:银色
     * @return [array]
     */
    public function info_cartesiant($spu_spec_ids,$sku_spec_item){   
        $values_dcr = [];
        $specs_new  = [];
        if(is_array($sku_spec_item)){
            $items = explode(",", $spu_spec_ids);
            /**
             * 获取包含的规格ID和值
             * [spec_value_id =>[spec_value_id,value_name,value_name]]
             */
            foreach ($sku_spec_item as $item) {
                $spec_value = explode(",", $item);
                foreach ($spec_value as $value) {
                    $value_items = explode(":", $value);
                    $values_array[$value_items[0]] = $value_items;
                }
            }
            $value_ids  = implode(",",array_keys($values_array)); //ID列表
            $specs      = Flight::model('goods/spec')->item_select_list(["id in ({$spu_spec_ids})"]);  // 规格ID查询
            $values     = Flight::model('goods/spec')->item_spec_list(["id in ({$value_ids})"]);       // 规格值ID查询
            /**
             * 规格ID下,规格值ID下的属性
             * [spec_id => [spec_value_id => [id=>167,spec_id=>29,name=>其它]]]
             * @var [type]
             */
            $values_new = [];
            foreach ($values as $k => $row) {
                $current = $values_array[$row['id']];
                if($current[1] != $current[2]) $row['name'] = $current[2];
                if($current[3] != '') $row['img'] = $current[3];
                $values_new[$row['spec_id']][$row['id']] = $row;
            }
            
            foreach ($specs as $key => $value) {
                $value['spec_value'] = isset($values_new[$value['id']])?$values_new[$value['id']]:null;
                $specs_new[$value['id']] = $value;
            }
            foreach ($sku_spec_item as $item) {
                $values = explode(",", $item);
                $key_code = ';';
                foreach ($values as $k => $value) {
                    $value_items = explode(":", $value);
                    $key         = $items[$k];
                    $tem[$key]   = $specs_new[$key];
                    $tem[$key]['spec_value'] = $values_array[$value_items[0]];
                    $key_code .= $key.':'.$values_array[$value_items[0]][0].';';
                }
                $key_code = trim($key_code,';'); 
                $values_dcr[$key_code] = $tem;
            }
        }
        return array_filter([$specs_new,$values_dcr]);
    }

    //#####################################################
    /**
     * 编辑商品的关联数据
     * @param  [array] $specs_new [商品规格信息]
     * @param  [type] $spu_id  [SPU商品ID]
     */
    public function item_spec_edit($specs_new,$spu_id){
        if(empty($specs_new)){
            $this->item_spec_value_del(['spu_id' => $spu_id]);
        }else{
            $item_spec_value = [];
            $n = 0;
            foreach ($specs_new as $key => $spec) {
                if(isset($spec['spec_value'])){
                    foreach($spec['spec_value'] as $k => $v){
                        $n++;
                        $item_spec_value[$n]['spu_id']        = $spu_id;
                        $item_spec_value[$n]['spec_id']       = $key;
                        $item_spec_value[$n]['spec_value_id'] = $k;
                    }
                }
            }
            if(!empty($item_spec_value)){
                //删除关联数据
                $this->item_spec_value_del(['spu_id' => $spu_id]);
                foreach ($item_spec_value as $key => $value) {
                    $this->item_spec_value_add($value);
                }
            }
        }
    } 

    //商品-规格-值-删除
    public function item_spec_value_del($condition){
        return $this->table($this->table_isv)->where($condition)->delete();
    }
    
    //商品-规格-值-添加
    public function item_spec_value_add($data){
        return $this->table($this->table_isv)->data($data)->insert();
    }
}