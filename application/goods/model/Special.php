<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        特性和专题
 */
namespace application\goods\model;
use application\base\model\Base;

class Special extends Base{

	protected $table = 'goods_special';

    //管理列表
    public function select_list($condition = []){
        return $this->where($condition)->order('id desc')->select();
    }

    //查找单个
    public function select_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    } 

    //删除
    public function info_delete($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->delete();
    }

    //添加或编辑
    public function info_edit($param){
        $data['name']        = $param['name'];
        $data['website_id']  = $param['website_id'];
        $data['tpl_lists']   = $param['tpl_lists'];
        $data['tpl_reads']   = $param['tpl_reads'];
        $data['content']     = $param['content'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id']= (int)$param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    } 
    
    /**
     * 属性列表
     */
    public function select_lists_ids($special_ids,$website_id = 0){
        $special = [];
        if($special_ids){
            $special = explode(',',$special_ids);
        }
        $condition[] = 'website_id = '.$website_id.' or website_id = 0';
        $result = $this->field('id,name')->where($condition)->order('id desc')->select();
        foreach ($result as $key => $value) {
            $menu[$key] = ['id'=>$value['id'],'pId'=>0,'name'=>$value['name']];
            if(in_array($value['id'], $special)){
                $menu[$key]['checked'] = true;
            }
        }  
        return $menu;
    }
}