<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品规格
 */
namespace application\goods\model;
use application\base\model\Base;

class Spec extends Base{

    protected $table      = 'goods_spec';  //规格
    protected $spec_table = 'goods_spec_value';  //商品规格值

    //商品规格-列表
    public function select_list($condition = []){
         return $this->where($condition)->order('sort desc,id desc')->select();
    }

    //商品规格-把数组重置ID => NAME
    public function select_list_id_is_name(){
        $result = $this->select_list();
        $data = [];
        if($result){
            foreach ($result as $key => $value) {
                $data[$value['id']] = $value['name'];
            }
        }
        return $data;
    }

    //商品规格-单个信息
    public function select_find($condition,$field = null){
        return $this->where($condition)->field($field)->find();
    }

    //商品规格-添加或编辑
    public function info_edit($data,$id = 0,$website_id = 0){
        if($id){
            $condition['id'] = $id;
            if($website_id){
                $condition['website_id'] = $website_id;
            }
            $this->where($condition)->data($data)->update();
            return $id;
        }else{
            return $this->data($data)->insert();
        }
    } 

    //商品规格-删除
    public function info_delete($id,$website_id = 0){
        $condition['id'] = (int)$id;
        if($website_id){
            $condition['website_id'] = $website_id;
        }
        $rel = $this->select_find($condition);
        if($rel){
            $this->spec_value_delete(['spec_id' =>$id]);
            return $this->where($condition)->delete();
        }
        return false;
    }

    //商品规格-排序
    public function info_sort($param){
        $condition['id'] = $param['id'];
        $data['sort']    = (int)$param['sort'];
        return $this->data($data)->where($condition)->update();
    } 


    /**
     * 商品规格-列表
     * 商品笛卡尔积(ItemModel.php)文件方法info_cartesiant使用
     *
     */
    public function item_select_list($where){
        return $this->where($where)->field("id,name,note")->order('sort desc,id desc')->select();
    }

    /**
     * 以下是规格值
     * #####################################################
     * 商品规格-列表
     * 商品笛卡尔积(ItemModel.php)文件方法info_cartesiant使用
     */
    public function item_spec_list($where){
        return $this->table($this->spec_table)->field("id,spec_id,name")->where($where)->order('sort asc,id desc')->select();
    }

    //商品规格参数-列表
    public function spec_list($where){
         return $this->table($this->spec_table)->where($where)->order('sort asc,id desc')->select();
    }

    //商品规格参数 - 添加编辑
    public function spec_value_edit($data,$where = []){
        if(empty($where)){
            return $this->table($this->spec_table)->data($data)->insert();
        }else{
            return $this->table($this->spec_table)->where($where)->data($data)->update();
        }
    } 

    //商品规格参数-删除
     public function spec_value_delete($param){
        return $this->table($this->spec_table)->where($param)->delete();
    }  
}