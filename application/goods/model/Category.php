<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品分类管理
 */
namespace application\goods\model;
use application\base\model\Base;
use extend\Category as ctg;
use framework\Flight;

class Category extends Base{

	protected $table = 'goods_category';

    //翻译列表
    public function select_list($condition = array(),$pages,$field = null){
         return $this->where($condition)->field($field)->pager((int)$pages,20)->order('sort desc,id desc')->select();
    }

    //无翻页列表
    public function select_list_all($condition = array(),$field = null){
         return $this->where($condition)->field($field)->order('sort desc,id desc')->select();
    }

    //删除
    public function select_del($id){
         $condition['id']= (int)$id;
         return $this->where($condition)->delete();
    }

    //单个
    public function select_find($id,$field = null){
        $condition['id']= (int)$id;
        return $this->where($condition)->field($field)->find();
    }

    //添加或编辑
    public function edit($param){
        $data['parent_id']         = $param['parent_id'];
        $data['title']             = $param['title'];
        $data['name']              = $param['name'];
        $data['sort']              = $param['sort'];
        $data['icon']              = $param['icon'];
        $data['get_root_cate_id']  = 0;
        $data['is_open_authorize'] = 1;  //后台添加默认授权
        $data['update_time'] = time();
        if($param['parent_id']){
            $info =  $this->where(['id'=>$param['parent_id']])->field('id,root_id')->find();
            if($info['root_id']){
                $data['root_id'] = $info['root_id'];
            }else{
                $data['root_id'] = $info['id'];
            }
        }else{
            $data['root_id']     = 0;
        }
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            return $this->data($data)->insert($data);
        }
    } 

    /**
     * 获取路径
     * @param int $parent_id
     */
    public function select_path($parent_id,$status = 0) {
        $pathMaps = [];
        $result = $this->getPath($parent_id);
        foreach ($result as $value) {
            $pathMaps[] = [
                'name' =>'&nbsp>&nbsp'.$value['title'],
                'url'  => url('goods/category/index',['parent_id'=>$value['id'],'status'=>$status]),
            ];
        }
        return $pathMaps;
    } 

    /**
     * 获取当前路径
     * @param int $parent_id
     * @return array
     */
    public function getPath($parent_id){
        $result = $this->field('id,title,parent_id')->select();
        $tree =  new ctg(array('id','parent_id','title','name'));
        return $tree->getPath($result,$parent_id);
    }

      /**
     * 获取某个ID下所有子栏目
     * @param  array $condition
     * @param  array|int $ids_array
     * @return string
     */
    public function getIds($condition,$ids_array){
        $category_rel = $this->select_list_all($condition,'id,title,parent_id');
        $category_ids = [];
        $tree =  new ctg(array('id','parent_id','title','name'));
        if(is_array($ids_array)){
            foreach ($ids_array as $key => $id) {
                $ids_all = $tree->getTree($category_rel,$id);
                if(!empty($ids_all)){
                    foreach ($ids_all as $key => $value) {
                        $category_ids[] = $value['id'];
                    }
                }
            }
        }else{
            $ids_all = $tree->getTree($category_rel,$ids_array);
            $category_ids[] = $ids_array;
            foreach ($ids_all as $key => $value) {
                $category_ids[] = $value['id'];
            }
        }
        $ids = implode(',',$category_ids);
        return $ids;
    }  

    //修改排序
    public function info_sort($param){
        $condition['id'] = $param['id'];
        $data['sort']    = (int)$param['sort'];
        return $this->data($data)->where($condition)->update();
    }
}