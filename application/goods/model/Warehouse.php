<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        仓库管理
 */
namespace application\goods\model;
use application\base\model\Base;

class Warehouse extends Base{

    protected $table  = 'goods_warehouse';  //类型

    //列表
    public function select_list($condition = [],$pages = 0){
         return $this->where($condition)->pager((int)$pages,20)->order('id desc')->select();
    }

    //单个
    public function info_find($condition){
        return $this->where($condition)->find();
    }

    //添加或编辑
    public function info_edit($param){
        $data['name']        = $param['name'];
        $data['address']     = $param['address'];
        $data['update_time'] = time();
        if($param['id']){
            if($param['website_id']){
                $condition['website_id'] = intval($param['website_id']);
            };
            $condition['id'] = intval($param['id']);
            return $this->where($condition)->data($data)->update();
        }else{
            $data['website_id']  = $param['website_id'];
            return $this->data($data)->insert($data);
        }
    } 

    //删除
    public function info_delete($condition){
        return $this->where($condition)->delete();
    }
}