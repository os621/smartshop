<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品品牌
 */
namespace application\goods\model;
use application\base\model\Base;

class Brand extends Base{

    protected $table  = 'goods_brand';  //类型

    //列表
    public function select_list($condition = []){
         return $this->where($condition)->order('sort desc,id desc')->select();
    }

    //单个
    public function select_find($condition){
        return $this->where($condition)->find();
    }

    //添加或编辑
    public function info_edit($param){
        $data['name']        = trim($param['name']);
        $data['en_name']     = trim($param['en_name']);
        $data['logo']        = $param['logo'];
        $data['first_abc']   = trim($param['first_abc']);
        $data['sort']        = $param['sort'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id'] = (int)$param['id'];
            if($param['website_id']){
                $condition['website_id'] = $param['website_id'];
            }
            return $this->where($condition)->data($data)->update();
        }else{
            $data['website_id'] = $param['website_id'];
            return $this->data($data)->insert($data);
        }
    } 

    //删除
    public function info_delete($id){
         $condition['id'] = (int)$id;
         return $this->where($condition)->delete();
    }

    //修改排序
    public function info_sort($param){
        $condition['id'] = $param['id'];
        $data['sort']    = (int)$param['sort'];
        return $this->data($data)->where($condition)->update();
    } 
}