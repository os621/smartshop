<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        运费设置
 */
namespace application\goods\model;
use application\base\model\Base;

class Fare extends Base{

    protected $table = 'fare';

    //查找单个
    public function info_find($condition){
        return $this->where($condition)->find();
    } 

    //查找多个
    public function info_select($condition){
        return $this->where($condition)->select();
    } 

    //站点配置
    public function info_edit($data,$website_id){
        $rel = $this->info_find(['website_id' => $website_id]);
        if($rel){
            return $this->where(['website_id' => $website_id])->data($data)->update();
        }
        return $this->data($data)->insert();
    }  
}