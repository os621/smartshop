<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        特性和专题
 */
namespace application\goods\controller;
use framework\Flight;

class Special extends Common{

    private $special;
    private $website;

    public function __construct() {
        parent::__construct();
        $this->special = Flight::model('special');
        $this->website = Flight::model('common/website');
    }

    //列表
    public function index(){
        //菜单开始
        $pathMaps = [
            ['name'=>'<i class="iconfont icon-shouye"></i> 特性与专栏','url'=>'javascript:;']
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-jiahao"></i> 添加特性','url'=>url('goods/special/edit')]
        ];
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        //菜单结束
        $tpl_date['special']  = $this->special->select_list();
        $tpl_date['website']  = Flight::model('common/website')->website_name();
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'name' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']         = $request['id'];
            $data['website_id'] = $request['website_id'];
            $data['name']       = $request['name'];
            $data['tpl_lists']  = $request['tpl_lists'];
            $data['tpl_reads']  = $request['tpl_reads'];
            $data['content']    = $request['content'];
            $result  =  $this->special->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('goods/special/index')],'msg'=>'成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'失败']);
            }
        }else{
            //菜单开始
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('goods/special/index')]
            ];
            $editMenu = [];
            //菜单结束
            $tpl_date['id']       = Flight::request()->query->id;
            $tpl_date['info']     =  $this->special->select_find($tpl_date['id']);
            $tpl_date['website']  = $this->website->select_list();  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode($editMenu);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id     = Flight::request()->query->id;
        $result =  $this->special->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }

    /**
     * 读取属性列表
     * @access public
     */
    public function ids() {
        $special_ids = Flight::request()->query->special_ids;
        $website_id  = (int)Flight::request()->query->website_id;
        $info = $this->special->select_lists_ids($special_ids,$website_id);
        Flight::json($info);
    }
}