<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        采集蜘蛛
 */
namespace application\goods\controller;
use framework\Flight;

class Taobao extends Common{

    private $spec;
    private $category;
    private $brand;

    public function __construct() {
        parent::__construct();
        $this->isAuth('goods.spider');
        $this->spec     = Flight::model('spec');
        $this->category = Flight::model('category');
        $this->brand    = Flight::model('brand');
        $this->warehouse    = Flight::model('warehouse');
    }
    /**
     * 规格列表
     */
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 淘宝数据包','url'=>'javascript:;'];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }


    /**
     * 解析网站
     * @return [type] [description]
     */
    public function upload(){
        $tpl_date['input'] = Flight::request()->query->input;
        $this->display(0, $tpl_date, 'layout');
    }
    
    /**
     * 处理采集数据入库
     */
    public function getdata(){
        if($this->isPost()){
            $rules = [
                'taobao_data'      => ['empty','数据来源未知,请重新选择数据来源'],
                'category_id'      => ['empty','请选择商品分类'],
                'category_path_id' => ['empty','请选择商品分类'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                $this->ErrorMsg('友情提示',$rel['msg']);
            }
            $request  = Flight::request()->data;
            $category_id = (int)$request['category_id'];       
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 淘宝数据包','url'=>url('goods/taobao/index')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            //获取路由信息
            $data = self::taobao($request['taobao_data']);
            //栏目路径
            $category = $this->category->getPath($category_id);
            $category_path = '';
            foreach ($category as $key => $value) {
                 $category_path .= $value['title'].' <i class="iconfont icon-arrowright"></i> ';
            }
            $tpl_date['category_path'] = $category_path;
            $spu['is_spec'] = count(json_decode($spu['spec_and_value_ids'],true));
            $tpl_date['spu'] = $data;
            $tpl_date['spu']['category_id']      = $category_id;  //栏目ID
            $tpl_date['spu']['category_path_id'] = $request['category_path_id'];  //栏目路径
            $tpl_date['products_info'] = "new Array()";
            $tpl_date['brand']         = $this->brand->select_list(['website_id' => 0]);
            $tpl_date['warehouse']     = $this->warehouse->select_list(['website_id' =>0]);
            $this->display('item/edit',$tpl_date,'layout');
        }
    }

    /**
     * [taobao 淘宝采集]
     * @param  [string] $url [淘宝商品URL]
     * @return [array]      [采集数据源]
     */
    protected function taobao($url){
        $cellName = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ', 'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ', 'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ', 'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ');
        //引入PHPExcel类
        require ROOT_PATH."vendor/phpoffice/phpexcel/Classes/PHPExcel.php";
        $objReader = new \PHPExcel_Reader_Excel2007();
        $objReader->setReadDataOnly(true);
        $objWorksheet = $objReader->load(ROOT_PATH.$url,'utf-8')->getActiveSheet(); 
        $allRow    = $objWorksheet->getHighestRow();    //获取总行数  
        $allColumn = $objWorksheet->getHighestColumn();  //获取总列数  
        $highestColumnCount = \PHPExcel_Cell::columnIndexFromString($allColumn);
        $columnCnt = array_search($allColumn,$cellName);  
        $excel_data = array();
        //读取内容
        for($_row=4; $_row<=$allRow; $_row++){
            for($_column=0; $_column<=$columnCnt; $_column++){  
                $cellId = $cellName[$_column].$_row;  
                $cellValue = $objWorksheet->getCell($cellId)->getValue();  
                //$cellValue = $objWorksheet->getCell($cellId)->getCalculatedValue();  #获取公式计算的值  
                if($cellValue instanceof PHPExcel_RichText){   //富文本转换字符串  
                    $cellValue = $cellValue->__toString();  
                }
                $excel_data[$cellName[$_column]] = $cellValue;  
            }  
        }  
        $img = self::format_img($excel_data['AC']);
        if(!empty($excel_data)){
            $data['name']             = $excel_data['A'];
            $data['tag_ids']          = $excel_data['E'].','.$excel_data['F']; //商品关键字
            $data['goods_no']         = $excel_data['AK']; //商品编号
            $data['sell_price']       = (float)$excel_data['H'];  //销售价
            $data['market_price']     = (float)$excel_data['H'];  //市场价
            $data['store_nums']       = (int)$excel_data['J'];  //库存
            $data['weight']           = $excel_data['AY'] ? $excel_data['AY'] : 0;  //库存
            $data['warning_line']     = 20;  //默认20个预警线
            $data['imgs']             = $img;
            $data['img']              = $img[0];
            $data['content']          = $excel_data['U'];
            $data['source']           = $url;
        }
        return $data;
    }  

    /**
     * 格式化图片
     */
    protected function format_img($imgs){
        $ary_img = explode(';',$imgs);
        foreach ($ary_img as $key => $value) {
            $img = explode('|',$value);
            if(!empty($img[0])){
                $array_img[] = $img[1];
            }
        }
        return $array_img;
    }
}