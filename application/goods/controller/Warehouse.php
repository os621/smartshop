<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        仓库管理
 */
namespace application\goods\controller;
use framework\Flight;

class Warehouse extends Common{
    private $warehouse;

    public function __construct() {
        parent::__construct();
        $this->warehouse = Flight::model('goods/warehouse');
    }

    /**
     * 列表
     * @access public
     */
    public function index(){
        $page    = intval(Flight::request()->query->page);
        $status  = intval(Flight::request()->query->status);
        $button[$status] = 'button-blue';
        $condition[] = $status ? 'website_id > 0' : 'website_id = 0';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="button '.$button['0'].'"><i class="iconfont icon-pingtaiguanli"></i> 平台仓库</span>','url'=>url('goods/warehouse/index')],
            ['name'=>'<span class="button '.$button['2'].'"><i class="iconfont icon-kehu"></i> 用户仓库</span>','url'=>url('goods/warehouse/index',['status' =>2])],
        ];
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 添加仓库','url'=>url('goods/warehouse/edit')];
        $tpl_date['warehouse'] = $this->warehouse->select_list($condition,$page);
        $tpl_date['pager']    = $this->getPage($this->warehouse->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = Flight::model('common/website')->website_name();
        $this->display(0,$tpl_date,'layout');
    }
    
    /**
     * 添加权限节点
     * @access public
     */
    public function edit() {
        if($this->isPost()){
            $rules = [
                'name'    => ['empty'],
                'address' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['id']         = $request['id'];
            $data['name']       = trim($request['name']);
            $data['address']    = trim($request['address']);
            $data['website_id'] = 0;
            $result  = $this->warehouse->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('goods/warehouse/index')],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $id = (int)Flight::request()->query->id;
            $pathMaps = [
                ['name'=>'<i class="iconfont icon-home"></i> 仓库管理','url'=>url('goods/warehouse/index')],
                ['name' =>'&nbsp;>&nbsp;添加/编辑','url'=>'javascript:;']
            ];
            $tpl_date['info'] = $this->warehouse->info_find(['id' => $id]);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    } 

    //删除
    public function delete(){
        $id         = (int)Flight::request()->query->id;
        $info       = Flight::model('goods/item')->where(['warehouse_id' => $id])->find(); 
        if($info){
            exit(Flight::json(['code'=>403,'msg'=>'删除失败,当前仓库中还有商品']));
        }
        $result =  $this->warehouse->info_delete(['id' => $id]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }
}