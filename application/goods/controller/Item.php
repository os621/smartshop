<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品列表
 */
namespace application\goods\controller;
use framework\Flight;

class Item extends Common
{

    private $brand;
    private $category;
    private $spec;
    private $item;
    private $warehouse;

    public function __construct() {
        parent::__construct();
        $this->isAuth('goods.item');
        $this->brand     = Flight::model('brand');
        $this->category  = Flight::model('category');
        $this->spec      = Flight::model('spec');
        $this->item      = Flight::model('item');
        $this->warehouse = Flight::model('warehouse');
    }

    //列表
    public function index(){
        $status  = Flight::request()->query->status;
        $type    = (int)Flight::request()->query->type;
        $keyword = Flight::request()->query->keyword;
        $page    = (int)Flight::request()->query->page;
        $condition = [];
        switch ($status) {
            case 'is_sale':
                $i = 1;
                $condition = ['is_sale' => 2,'is_del' => 0];
                break;
            case 'on_sale':
                $i = 2;
                $condition = ['is_sale' => 1,'is_del' => 0];
                break;
            case 'review':
                $i = 3;
                $condition = ['is_sale' => 0,'is_del' => 0];
                break;
            case 'trash':
                $i = 4;
                $condition = ['is_del' => 1];
                break;
            default:
                $condition = ['is_del' => 0];
                $i = 0;
                break;
        }
        $button[$i] = 'button-blue';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="button '.$button['0'].'"><i class="iconfont icon-shouye"></i> 全部商品(SPU)</span>','url'=>url('goods/item/index',['type'=>$type])],
            ['name'=>'<span class="button '.$button['1'].'"><i class="iconfont icon-xiangshang3"></i> 在售商品</span>','url'=>url('goods/item/index',['status' =>'is_sale','type'=>$type])],
            ['name'=>'<span class="button '.$button['2'].'"><i class="iconfont icon-xiangxia5"></i> 下架商品</span>','url'=>url('goods/item/index',['status' =>'on_sale','type'=>$type])],
            ['name'=>'<span class="button '.$button['3'].'"><i class="iconfont icon-tishi"></i> 商品审核</span>','url'=>url('goods/item/index',['status' =>'review','type'=>$type])],
            ['name'=>'<span class="button '.$button['4'].'"><i class="iconfont icon-shanchu"></i> 商品回收站</span>','url'=>url('goods/item/index',['status' =>'trash','type'=>$type])],
        ];
        $editMenu = [];
        if($type){
            $condition[] = "website_id > 0";  
        }else{
            $condition[] = "website_id = 0";  
            $editMenu = [
                ['name'=>'<i class="iconfont icon-jiahao"></i> 添加商品','url'=>url('goods/item/edit')],
                ['name'=>'<i class="iconfont icon-lingcunwei"></i> 淘宝数据包','url'=>url('goods/taobao/index')],
            ];
        }
        //菜单结束 
        if($keyword) $condition[] = "name LIKE '%$keyword%'";  
        $item = $this->item->select_list($condition,$page);
        if($keyword){
            foreach ($item as $key => $value) {
                $item[$key]['name'] = str_replace($keyword,'<span class="red">'.$keyword.'</span>',$value['name']);
            }
        }
        $tpl_date['item']     = $item;
        $tpl_date['pager']    = $this->getPage($this->item->pager,['status'=>$status,'type'=>$type,'keyword'=>$keyword]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['status']   = $status;
        $tpl_date['keyword']  = $keyword;
        $tpl_date['type']     = $type;
        $tpl_date['website']  = Flight::model('common/website')->website_name();
        $this->display(0,$tpl_date,'layout');
    }
    //添加商品
    public function edit(){
        if($this->isPost()){
            $this->save_data();
            Flight::json(['code'=>200,'data'=>['url'=>url('goods/item/index')],'msg'=>' 商品添加或编辑成功']);
        }else{
            $id  = (int)Flight::request()->query->id;
            $spu = $this->item->select_find($id);
            $products_sku = [];
            if($spu){
                $tpl_date['spec_and_value_ids'] = json_decode($spu['spec_and_value_ids'],true);  //使用的规格
                foreach ($tpl_date['spec_and_value_ids'] as $key => $item) {
                   $spec_ids .= $item['id'].',';
                }
                $tpl_date['spec_ids'] = rtrim($spec_ids,",");  //使用的规格ID
                $sku = $this->item->select_sku_list(['spu_id' => $spu['id']]);
                if($sku){
                    $spec_str = '';
                    foreach ($sku as $key => $item) {
                        $spec_and_value_ids = json_decode($item['spec_and_value_ids'],true);
                        foreach ($spec_and_value_ids as $value) {
                            $sku[$key]['spec_name'][] = $value['spec_value'][2]; //SKU信息
                            $spec_str .= implode(":",$value['spec_value']).",";  //规格组合KEY
                        }
                        $spec_item_key = trim($spec_str,',');
                        $products_sku[$spec_item_key] = [
                            'store_nums'   => $item['store_nums'],
                            'warning_line' => $item['warning_line'],
                            'sell_price'   => $item['sell_price'],
                            'market_price' => $item['market_price'],
                            'cost_price'   => $item['cost_price'],
                            'weight'       => $item['weight'],
                        ];
                        $sku[$key]['spec_str'] = $spec_item_key;
                        $spec_str = '';  //参数重置
                    }
                }
                //当前商品目录
                $category = $this->category->getPath($spu['category_id']);
                foreach ($category as $key => $value) {
                     $category_path .= $value['title'].' <i class="iconfont icon-arrowright"></i> ';
                }
                $tpl_date['category_path'] = $category_path;
                if($spu['imgs'])  $spu['imgs']  = json_decode($spu['imgs'],true); //商品小图
                if($spu['props']) $spu['props'] = json_decode($spu['props'],true); //商品属性
                $spu['is_spec'] = count(json_decode($spu['spec_and_value_ids'],true)); //统计有多少个SKU规格
            }
            $tpl_date['id']            = $id;
            $tpl_date['spu']           = $spu;
            $tpl_date['sku']           = $sku;
            $tpl_date['brand']         = $this->brand->select_list(['website_id' => 0]);
            $tpl_date['warehouse']     = $this->warehouse->select_list(['website_id' => 0]);
            $tpl_date['products_info'] = empty($products_sku)?'"new Array()"':json_encode($products_sku);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除进入回收站
    public function delete(){
        $id      = (int)Flight::request()->query->id;
        $type    = (int)Flight::request()->query->type;
        $status  = Flight::request()->query->status;
        $ids_str = Flight::request()->data['ids'];
        $ids     = null;
        if($ids_str){
            $id_array = [];
            $ids_ary = explode(',',trim($ids_str,','));
            foreach ($ids_ary as $key => $value) {
                $id_array[] = (int)$value;
            }
            $ids = implode(',',$id_array);
        }
        $result = $this->item->spu_delete($id,$ids);
        if($result){
   
            Flight::json(['code'=>200,'msg'=>'删除成功','data'=>['url'=>url('goods/item/index',['status' => $status,'type'=>$type])]]);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,批量操作不排除有成功的。']);
        } 
    }

    //上架,下架,从回收站恢复
    public function ids_action(){
        $issale  = (int)Flight::request()->query->issale;
        $status  = (int)Flight::request()->query->status;
        $type    = (int)Flight::request()->query->type;
        $ids_str = Flight::request()->data['ids'];
        if($ids_str){
            $id_array = [];
            $ids_ary = explode(',',trim($ids_str,','));
            foreach ($ids_ary as $key => $value) {
                $id_array[] = (int)$value;
            }
            $ids = implode(',',$id_array);
            $this->item->spu_all_action($issale,$ids);
            Flight::json(['code'=>200,'msg'=>'操作成功','data'=>['url'=>url('goods/item/index',['status' => $status,'type'=>$type])]]);
        }else{
            Flight::json(['code'=>403,'msg'=>'为选择任何要操作商品']);
        }
    } 

    /**
     * ######################################################################
     * 提交保存数据
     * @return [type] [description]
     */
    protected function save_data(){
      //表单验证
        $rules = [
            'category_id'      => ['type','商品分类必须输入','INT'],
            'category_path_id' => ['empty','商品分类必须输入'],
            'brand_id'         => ['type','品牌必须选择','INT'],
            'warehouse_id'     => ['type','商品所在仓库必须选择','INT'],
            'name'             => ['empty','商品名称必须输入'],
            'goods_no'         => ['empty','商品编号必须输入'],
            'sell_price'       => ['empty','销售价必须输入'],
            'market_price'     => ['empty','市场价必须输入'],
            'cost_price'       => ['empty','成本价必须输入'],
            'store_nums'       => ['empty','库存必须输入'],
            'warning_line'     => ['empty','预警线必须输入'],
            'img'              => ['empty','没有设置默认图片'],
            'content'          => ['empty','商品描述必须填写'],
            'props_name'       => ['array','商品属性名称必须填写'],
            'props_value'      => ['array','商品属性必须填写'],
            'point'            => ['type','积分必须填写','INT'],
            'sort'             => ['type','排序必须填写','INT'],
        ];
        $rel = Flight::validator($rules);
        if($rel['code'] == 403){
            exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
        }
        //开始处理数据逻辑
        $request  =  Flight::request()->data;
        $imgs = is_array($request['imgs']) ? $request['imgs'] : [];
        $data['id']               = (int)$request['id'];
        $data['category_id']      = (int)$request['category_id'];
        $data['category_path_id'] = $request['category_path_id'];
        $data['brand_id']         = (int)$request['brand_id'];
        $data['warehouse_id']     = (int)$request['warehouse_id'];
        $data['name']             = trim($request['name']);
        $data['subtitle']         = trim($request['subtitle']);
        $data['tag_ids']          = trim($request['tag_ids']);
        $data['goods_no']         = trim($request['goods_no']);
        $data['sell_price']       = (float)$request['sell_price'];
        $data['market_price']     = (float)$request['market_price'];
        $data['cost_price']       = (float)$request['cost_price'];
        $data['store_nums']       = (float)$request['store_nums'];
        $data['warning_line']     = (float)$request['warning_line'];
        $data['weight']           = (int)$request['weight'];
        $data['unit']             = trim($request['unit']);
        $data['imgs']             = json_encode($imgs);
        $data['img']              = trim($request['img']);
        $data['content']          = $request['content'];
        $data['website_id']       = 0;
        $data['source']           = trim($request['source']);
        $data['note']             = trim($request['note']);
        $data['point']            = (int)$request['point'];
        $data['sort']             = (int)$request['sort'];
        $data['is_sale']          = (int)$request['is_sale'] ? 2 : 1;;
        $data['taobao_item_id']   = trim($request['taobao_item_id']);
        if(is_array($request['props_name']) && is_array($request['props_value'])){
            foreach ($request['props_name'] as $key => $value) {
                $ary[$key]['name'] = $value;
            }
          foreach ($request['props_value'] as $key => $value) {
                $ary[$key]['value'] = $value;
            }
            $data['props'] = json_encode($ary);
        }
        //开始处理商品SKU
        $sku_spec_item = is_array($request['sku_spec_item']) ? array_filter($request['sku_spec_item']) : [];
        $specs_new     = [];
        $values_dcr    = [];
        if(!empty($sku_spec_item)){
            $spu_spec_ids  = $request['spec_items'];   //获取使用了的商品规格主ID
            $sku_spec   = $this->item->info_cartesiant($spu_spec_ids,$sku_spec_item);  //笛卡尔乘积
            $specs_new  = $sku_spec[0];
            $values_dcr = $sku_spec[1];
            $data['sku_spec_item']    = $sku_spec_item;
            $data['sku_pro_no']       = $request['sku_pro_no'];
            $data['sku_sell_price']   = $request['sku_sell_price'];
            $data['sku_market_price'] = $request['sku_market_price'];
            $data['sku_cost_price']   = $request['sku_cost_price'];
            $data['sku_store_nums']   = $request['sku_store_nums'];
            $data['sku_warning_line'] = $request['sku_warning_line'];
            $data['sku_weight']       = $request['sku_weight'];
        }
        $data['spec_and_value_ids']  = empty($specs_new) ? json_encode([]) : json_encode($specs_new);
        $spu_id = $this->item->info_edit($data);
        //清理多余的商品
        $keys  = empty($values_dcr) ? '' : implode("','",array_keys($values_dcr));
        if($keys){
            $this->item->sku_delete(["spu_id = ".$spu_id." and specs_key not in('$keys')"]);
        }else{
            $this->item->sku_delete(["spu_id = ".$spu_id]);
        }
        //编辑SKU商品
        $data['spu_id'] = $spu_id;
        $i = $this->item->sku_edit($values_dcr,$data);
        if(!$i){
            $this->item->sku_edit_spu($data);
        }
        //商品规格,SPU,SKU关联
        $this->item->item_spec_edit($specs_new,$spu_id);
        return true;
    }
}