<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品品牌
 */
namespace application\goods\controller;
use framework\Flight;

class Brand extends Common{

    private $brand;

    public function __construct() {
        parent::__construct();
        $this->isAuth('goods.brand');
        $this->brand = Flight::model('brand');
    }

    //列表
    public function index(){
        $status  = intval(Flight::request()->query->status);
        $button[$status] = 'button-blue';
        $condition[] = $status ? 'website_id > 0' : 'website_id = 0';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="button '.$button['0'].'"><i class="iconfont icon-pingtaiguanli"></i> 平台品牌</span>','url'=>url('goods/brand/index')],
            ['name'=>'<span class="button '.$button['2'].'"><i class="iconfont icon-kehu"></i> 用户品牌</span>','url'=>url('goods/brand/index',['status' =>1])],
        ];
        $editMenu = [
            ['name'=>'<i class="iconfont icon-jiahao"></i> 添加品牌','url'=>url('goods/brand/edit')]
        ];
        //菜单结束    
        $tpl_date['lists']    = $this->brand->select_list($condition);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = Flight::model('common/website')->website_name();
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'name'      => ['empty','品牌名称必须填写'],
                'en_name'   => ['empty','品牌英文名称必须填写'],
                'first_abc' => ['len','品牌英文名称必须填写且只能填写一个字母','min:1,max:1'],
                'sort'      => ['type','品牌排序必须输入','INT'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request      =  Flight::request()->data;
            $data['id']         = (int)$request['id'];
            $data['name']       = $request['name'];
            $data['en_name']    = $request['en_name'];
            $data['first_abc']  = strtoupper($request['first_abc']);
            $data['logo']       = $request['logo'];
            $data['sort']       = (int)$request['sort'];
            $data['website_id'] = (int)$request['website_id'];
            $result =  $this->brand->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('goods/brand/index')],'msg'=>' 品牌创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>' 品牌创建或编辑不成功']);
            }
        }else{
            $id = intval(Flight::request()->query->id);
            $pathMaps[] = ['name'=>'<i class="iconfont icon-arrowleft"></i> 返回','url'=>url('goods/brand/index')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['info']     = $this->brand->select_find(['id' => $id]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id = Flight::request()->query->id;
        $result =  $this->brand->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含类型属性']);
        } 
    }

    //字段排序
    public function sort(){
        $data['sort'] = intval(Flight::request()->data['sort']);
        $data['id']   = intval(Flight::request()->data['id']);
        $result = $this->brand->info_sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }
}