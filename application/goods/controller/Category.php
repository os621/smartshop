<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品分类
 */
namespace application\goods\controller;
use framework\Flight;

class Category extends Common{

    private $category;
    private $spec;

    public function __construct() {
        parent::__construct();
        $this->category = Flight::model('Category');
        $this->spec = Flight::model('spec');
    }

    //列表
    public function index(){
        $this->isAuth('goods.category.index');
        $status    = intval(Flight::request()->query->status);
        $page      = (int)Flight::request()->query->page;
        $parent_id = (int)Flight::request()->query->parent_id; 
        //菜单开始
        $button[$status] = 'button-blue';
        $pathButton = [
            ['name'=>'<span class="button '.$button['0'].'"><i class="iconfont icon-pingtaiguanli"></i> 平台分类</span>','url'=>url('goods/category/index')],
            ['name'=>'<span class="button '.$button['1'].'"><i class="iconfont icon-kehu"></i> 用户分类</span>','url'=>url('goods/category/index',['status' =>1])],
        ];
        $path = $this->category->select_path($parent_id,$status);
        $pathMaps =  array_merge($pathButton,$path);

        $editMenu[] = ['name'=>'<i class="iconfont icon-jiahao"></i> 添加分类','url'=>url('goods/category/edit',['status'=>$status,'parent_id'=>$parent_id])];
        //菜单结束
        $condition[] = $status ? 'website_id > 0' : 'website_id = 0';
        $condition['parent_id'] = $parent_id;
        $tpl_date['lists'] = $this->category->select_list($condition,$page);
        $tpl_date['pager'] = $this->getPage($this->category->pager,['parent_id'=>$$parent_id]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = Flight::model('common/website')->website_name();
        $tpl_date['status']   = $status;
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        $this->isAuth('goods.category.edit');
        if($this->isPost()){
            $rules = [
                'title'   => ['empty'],
                'name'    => ['empty'],
                'sort'    => ['type','自定义排序必须填写','INT']
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']        = $request['id'];
            $data['parent_id'] = $request['parent_id'];
            $data['title']     = $request['title'];
            $data['name']      = $request['name'];
            $data['sort']      = $request['sort'];
            $result =  $this->category->edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('goods/category/index',['parent_id'=>$data['parent_id']])],'msg'=>'商品分类创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'商品分类创建或编辑不成功']);
            }
        }else{
            $status    = intval(Flight::request()->query->status);
            //菜单开始
            $button[$status] = 'button-blue';
            $pathButton = [
                ['name'=>'<span class="button '.$button['0'].'"><i class="iconfont icon-pingtaiguanli"></i> 平台分类</span>','url'=>url('goods/category/index')],
                ['name'=>'<span class="button '.$button['1'].'"><i class="iconfont icon-kehu"></i> 用户分类</span>','url'=>url('goods/category/index',['status' =>1])],
            ];
            //菜单开始
            $tpl_date['id']        = (int)Flight::request()->query->id;
            $tpl_date['parent_id'] = (int)Flight::request()->query->parent_id;
            $tpl_date['info']      = $this->category->select_find($tpl_date['id']);
            if($tpl_date['id']){
                $tpl_date['parent_id'] = $tpl_date['info']['parent_id'];
            }
            $path = $this->category->select_path($tpl_date['parent_id'],$status);
            $pathMaps =  array_merge($pathButton,$path);
            $tpl_date['pathMaps']    = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $this->isAuth('goods.category.delete');
        $id = (int)Flight::request()->query->id;
        $info =  $this->category->where(['parent_id' => $id])->find();
        if($info){
            exit(Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含子栏目']));
        }
        $goods =  Flight::model('item')->where(['category_id' => $id])->find();
        if($goods){
            exit(Flight::json(['code'=>403,'msg'=>'删除失败,栏目中还包含商品']));
        }
        $result =  $this->category->where(['id' => $id])->delete();
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含子栏目']);
        } 
    }

    //字段排序
    public function sort(){
        $this->isAuth('goods.category');
        $data['sort'] = (int)Flight::request()->data['sort'];
        $data['id']   = (int)Flight::request()->data['id'];
        $result = $this->category->info_sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }

    //弹出式模板选择窗口
    public function popup(){
        $this->isAuth('goods.item'); //商品栏目选择
        $tpl_date['input'] = Flight::request()->query->input;
        $tpl_date['path']  = Flight::request()->query->path;
        $this->display(0, $tpl_date, 'layout');
    }

    //联动菜单
    public function selected(){
        $this->isAuth('goods.item'); //商品栏目选择
        $parent_id = Flight::request()->query->parent_id;
        $info = $this->category->select_list_all(['parent_id' => $parent_id,'website_id' => 0],'id,title');
        if($info){
            Flight::json(['code'=>200,'msg'=>'成功','data'=>$info]);
        }
        Flight::json(['code'=>403,'msg'=>'读取商品分类列表失败']);
    }

    //当前路径
    public function selected_path(){
        $this->isAuth('goods.item');  //商品栏目选择
        $cid  = (int)Flight::request()->query->cid;
        $info = $this->category->getPath($cid);
        if($info){
            $len = count($info);
            $category = [];
            foreach ($info as $key => $value) {
                $category[] = $value['id'];
            }
            $category_id = implode(',',$category);
            Flight::json(['code'=>200,'msg'=>'成功','data'=>$info,'path' => $len,'category_id' => $category_id]);
        }
        Flight::json(['code'=>403,'msg'=>'读取商品分类路径失败']);
    }

    //商品授权列表
    public function lists(){
        $this->isAuth('common.website');
        if($this->isPost()){
            $rules = [
                'website_id'  => ['type','站点ID丢失,请关闭重新选择','INT']
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['website_id'] = $request['website_id'];
            $ids = $request['ids'];
            if(empty($ids)){
                exit(Flight::json(['code'=>403,'msg'=>'请选择授权商品目录']));
            }
            $data['category_ids'] = implode(',',$ids);
            $result = Flight::model('common/website')->select_category_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('common/website/goods_category',['id'=>$data['website_id']])],'msg'=>'商品目录授权操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'商品目录授权操作失败']);
            }
        }else{
            //获取所有站点
            $website_rel = Flight::model('common/website')->select_list(); 
            $website = [];
            if($website_rel){
                foreach ($website_rel as $key => $value) {
                    $website[$value['id']]['name'] =  $value['name'];
                    $website[$value['id']]['url']  =  $value['url'];
                }
            }
            $tpl_date['website']   = $website;
            //获取授权分类
            $page  = (int)Flight::request()->query->page;
            $tpl_date['website_id']   = (int)Flight::request()->query->website_id;
            $category_ids = Flight::model('common/website')->select_category_find($tpl_date['website_id']);
            $tpl_date['category_ids']  = [];
            if($category_ids){
                $tpl_date['category_ids'] = explode(',',$category_ids['category_ids']);
            }
            $condition['parent_id'] = 0;  //只授权根目录
            $condition['is_open_authorize'] = 1;  //开放的授权目录
            $condition[] = "website_id <> {$tpl_date['website_id']}";  //排除自己给自己授权
            $tpl_date['lists'] = $this->category->select_list($condition,$page);
            $tpl_date['pager'] = $this->getPage($this->category->pager,['website_id'=>$tpl_date['website_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }
}