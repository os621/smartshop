<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        运费设置
 */
namespace application\goods\controller;
use framework\Flight;

class Fare extends Common{

    private $fare = null;

    public function __construct() {
        parent::__construct();
        $this->fare = Flight::model('fare');
    }
    //列表
    public function index(){
        if($this->isPost()){
            $rules = ['first_weight'  => ['empty'],'first_price'   => ['empty'],'second_weight' => ['empty'],'second_price'  => ['empty'],];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['name']          = trim($request['name']);
            $data['website_id']    = 0;
            $data['first_weight']  = trim($request['first_weight']);
            $data['first_price']   = trim($request['first_price']);
            $data['second_weight'] = trim($request['second_weight']);
            $data['second_price']  = trim($request['second_price']);
            $data['update_time']   = time();
            $result =  $this->fare->info_edit($data,0);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('goods/fare/index')],'msg'=>'编辑成功.']);
            }else{
                Flight::json(['code'=>500,'msg'=>'编辑失败.']);
            }
        }else{
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 运费设置','url'=>'javascript:;'];
            $editMenu = [];
            //菜单结束
            $tpl_date['config'] = $this->fare->info_find(['website_id' => 0]);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode($editMenu);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //计算商品运费
    public function calculate(){
        
    }
    
}