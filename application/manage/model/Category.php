<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品分类管理
 */
namespace application\manage\model;
use application\base\model\Base;
use framework\Flight;

class Category extends Base{

     /**
     * 保存
     * @param int $parent_id
     */
    public function edit($param) {
        $data['parent_id']         = $param['parent_id'];
        $data['get_root_cate_id']  = $param['get_root_cate_id'];
        $data['is_open_authorize'] = $param['is_open_authorize'];
        $data['title']             = $param['title'];
        $data['name']              = $param['name'];
        $data['icon']              = $param['icon'];
        $data['website_id']        = $param['website_id'];
        $data['user_id']           = $param['user_id'];
        $data['update_time']       = time();
        if($param['parent_id']){
            $info =  Flight::model('goods/category')->where(['id'=>$param['parent_id']])->field('id,root_id')->find();
            if($info['root_id']){
                $data['root_id'] = $info['root_id'];
            }else{
                $data['root_id'] = $info['id'];
            }
        }else{
            $data['root_id']     = 0;
        }
        if($param['id']){
            $condition['id']= $param['id'];
            return Flight::model('goods/category')->where($condition)->data($data)->update();
        }else{
            return Flight::model('goods/category')->data($data)->insert($data);
        }
    } 

     /**
     * 获取路径
     * @param int $parent_id
     */
    public function select_path($parent_id) {
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 根分类','url'=>url('manage/category/index')];
        $result = Flight::model('goods/category')->getPath($parent_id);
        foreach ($result as $value) {
            $pathMaps[] = [
                'name' =>'&nbsp>&nbsp'.$value['title'],
                'url'  => url('manage/category/index',['parent_id'=>$value['id']]),
            ];
        }
        return $pathMaps;
    }  
}