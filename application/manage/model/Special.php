<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name         特性与专栏
 */
namespace application\manage\model;
use application\base\model\Base;
use extend\Category as ctg;
use framework\Flight;

class Special extends Base{

    protected $table             = 'goods_special';
    protected $goods_special_spu = 'goods_special_spu';
    protected $goods_table       = 'goods_spu';

    private $goods_item;
    public function __construct() {
        parent::__construct();
        $this->goods_item = Flight::model('goods/item');
    }

    //列表
    public function select_list($condition = [],$field = null){
        return $this->where($condition)->field($field)->order('id desc')->select();
    }

    //goods_special_spu 列表
    public function select_special_spu($condition = [],$field = null){
        return $this->table($this->goods_special_spu)->where($condition)->field($field)->order('id desc')->select();
    }  

    //和goods_spu表联表查询SPU
    public function select_spu_list($condition = [],$pages = 0){
        return $this->table("$this->goods_special_spu AS A")
              ->join("{pre}$this->goods_table AS B ON A.spu_id = B.id")
              ->field('B.*,A.website_id,A.special_id,A.spu_id,A.update_time,A.id as special_id')
              ->where($condition)
              ->pager((int)$pages,20)->order('B.id desc')->select();
    }

    //前台调用商品专栏
    public function select_spu_tag($condition = [],$n = 1){
        $result =  $this->table("$this->goods_special_spu AS A")
              ->join("{pre}$this->goods_table AS B ON A.spu_id = B.id")
              ->field('B.*,A.website_id,A.special_id,A.spu_id,A.update_time,A.id as special_id')
              ->where($condition)
              ->order('B.id desc')->limit($n)->select();
        $item = [];
        if($result){
            foreach ($result as $key => $value) {
                $item[] = $value;
                $item[$key]['url'] = url('item/'.$value['id'].'.html');  
            }  
        }
        return $item;
    }    

    //单个我的商品单个
    public function select_find($condition,$field = null){
        return $this->where($condition)->field($field)->find();
    }

    //确认专栏商品
    public function spu_all_action($param){
        if(empty($param['ids'])){
            return false;
        }
        $data['website_id']  = $param['website_id'];
        $data['special_id']  = $param['special_id'];
        $data['update_time'] = time();
        $spu_ids = implode(',',$param['ids']);
        $web_condition[] = "spu_id in($spu_ids)";
        $web_condition['special_id'] = $param['special_id'];
        $rel = $this->select_special_spu($web_condition);
        $spu_id = [];
        foreach ($rel as $key => $value) {
            $spu_id[] = $value['id'];
        }
        foreach ($param['ids'] as $key => $id) {
            $data['spu_id'] = $id;
            if(!in_array($id,$spu_id)){
                $this->table($this->goods_special_spu)->data($data)->insert();
            } 
        }
        return true;
    }

    //删除SPU商品
    public function spu_delete($ids,$special_id = null,$website_id = null){
        if($website_id){
            $condition['website_id'] = $website_id;
        }
        if($special_id){
            $condition['special_id'] = $special_id;
        }
        $condition[] = "spu_id in($ids)";
        return $this->table($this->goods_special_spu)->where($condition)->delete();
    }
}