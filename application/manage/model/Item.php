<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品分类管理
 */
namespace application\manage\model;
use application\base\model\Base;
use extend\Category as ctg;
use framework\Flight;

class Item extends Base{

	protected $table = 'goods_spu_website';
    protected $goods_table = 'goods_spu';

    private $goods_item;
    public function __construct() {
        parent::__construct();
        $this->goods_item = Flight::model('goods/item');
    }

    //列表(云平台在用)application\manage\controller\Cloud;
    public function select_list($condition = [],$field = null){
        return $this->where($condition)->field($field)->order('spu_id desc')->select();
    }

    //和goods_spu表联表查询SPU
    public function select_spu_list($condition = array(),$pages = 0,$n = 24){
        return $this->table("$this->table AS A",TRUE)
              ->join("{pre}$this->goods_table AS B ON A.spu_id = B.id")
              ->field('B.*,B.website_id as web_id,A.website_id,A.category_id,A.category_path_id,A.is_del,A.is_sale,A.price_base,A.math_type,A.price_rise')
              ->where($condition)
              ->pager((int)$pages,$n)->order('B.id desc')->select();
    }

    //无翻页链表查询
    public function select_spu_list_nopage($condition = array(),$field = null){
        return $this->table("$this->table AS A",TRUE)->join("{pre}$this->goods_table AS B ON A.spu_id = B.id")
              ->field($field)->where($condition)->order('B.id desc')->select();
    } 

    //单个我的商品单个
    public function select_find($condition,$field = null){
        return $this->where($condition)->field($field)->find();
    }

    //单个我的商品单个(前提首页查询)
    public function select_spu_find($condition,$field = null){
        return  $this->table("$this->table AS A",TRUE)->join("{pre}$this->goods_table AS B ON A.spu_id = B.id")->field($field)->where($condition)->find();
    }

    //上架,下架,从回收站恢复
    public function spu_all_action($issale,$ids,$website_id){
        switch ($issale) {
            case 1:
                $data['is_sale'] = 1; //下架
                break;
            case 2:
                $data['is_sale'] = 2;  //在售
                break;
            default:
                $data['is_del']  = 0;  //恢复商品到未软删除状态
                break;
        }
        $spu_id = self::is_my_goods($ids,$website_id);
        //我的商品
        if(!empty($spu_id['my'])){   
            $my_spu_ids = implode(',',$spu_id['my']);
            $web_condition[] = "spu_id in($my_spu_ids)";
            if($issale == 2){
                $web_condition['website_id'] = $website_id;
            }else{
                Flight::model('manage/special')->spu_delete($my_spu_ids);  //删除专栏商品
            }
            $this->where($web_condition)->data($data)->update();
            $this->goods_item->where(["id in($ids)"])->data($data)->update();
        }
        //其他人商品
        if(!empty($spu_id['orther'])){
            $orther_spu_ids = implode(',',$spu_id['orther']);
            if($issale != 2){
                Flight::model('manage/special')->spu_delete($ids,null,$website_id);  //删除专栏商品
            }
            //软删除
            $orther_condition[] = "spu_id in($orther_spu_ids)";
            $orther_condition['website_id'] = $website_id;
            $this->where($orther_condition)->data($data)->update();
        }
        return true;
    }

    //删除SPU商品
    public function spu_delete($ids,$website_id){
        //读取商品判断当前删除状态
        $condition[] = "spu_id in($ids)";
        $rel = $this->where($condition)->select();
        $is_del[0] = [];
        $is_del[1] = [];
        if($rel){
            foreach ($rel as $key => $value) {
                if($value['is_del']){
                    $is_del[1][] = $value['spu_id'];
                }else{
                    $is_del[0][] = $value['spu_id'];
                }
            }
        }
        //区分我的or云商
        $spu_id = self::is_my_goods($ids,$website_id);
        $data['is_del']  = 1;
        //我的商品
        if(!empty($spu_id['my'])){
            $data['is_sale'] = 0;
            if(!empty($is_del[0])){
                $up_ids = implode(',',$is_del[0]);
                Flight::model('manage/special')->spu_delete($up_ids);  //删除专栏商品
                $this->where(["spu_id in($up_ids)"])->data($data)->update();
                $this->goods_item->where(["id in($up_ids)"])->data($data)->update();
                return true;
            }
            if(!empty($is_del[1])){
                $del_ids = implode(',',$is_del[1]);
                $this->goods_item->item_spec_value_del(["spu_id in($del_ids)"]);  //删除关联
                $this->goods_item->sku_delete(["spu_id in($del_ids)"]);   //删除SKU
                $this->goods_item->where(["id in($del_ids)"])->delete();
                $this->where(["spu_id in($del_ids)"])->delete();
                return true;
            }
        }
        //其他人商品
        if(!empty($spu_id['orther'])){
            $data['is_sale'] = 1;
            //软删除
            if(!empty($is_del[0])){
                $up_ids = implode(',',$is_del[0]);
                Flight::model('manage/special')->spu_delete($up_ids,null,$website_id);  //删除专栏商品
                return $this->where(["spu_id in($up_ids)"])->data($data)->update();
            }
            //物理删除
            if(!empty($is_del[1])){
                $del_ids = implode(',',$is_del[1]);
                return $this->where(["spu_id in($del_ids)"])->delete();
            }
        }
    }

    //删除站点SPU商品
    public function info_delete($condition){
        return $this->where($condition)->delete();
    }  

    //修改编辑
    public function item_edit($data,$condition){
        return $this->data($data)->where($condition)->update();
    }

    /**
     * [all_action 批量添加商品到我的网站]
     * @param  [array] $param [添加的书u]
     * @param  [array] $ids   [添加的商品ID]
     * @return [boolean]      [执行结果]
     */
    public function all_action($param,$ids){
        if(empty($ids)) return;
        $data['website_id']       = $param['website_id']; 
        $data['category_id']      = $param['category_id'];
        $data['category_path_id'] = $param['category_path_id'];
        $data['is_sale']          = (int)$param['is_sale'];
        $data['update_time']      = time();
        $info = [];
        if(is_array($ids)){
            foreach ($ids as $key => $id) {
                $rel = $this->select_find(['website_id' => $data['website_id'],'spu_id' =>$id]);
                if(!$rel){
                    $data['spu_id']  = $id;
                    $info[] = $this->data($data)->insert();
                }
            }
        }else{
            $rel = $this->select_find(['website_id' => $data['website_id'],'spu_id' =>$ids]);
            if($rel){
                $info = $this->where(['spu_id' =>$ids])->data($data)->update(); 
            }else{
                $data['spu_id']  = $ids;
                $info = $this->data($data)->insert(); 
            }
        }
        return $info;
    }


    /**
     * [is_my_goods 判断是否是自己的商品]
     * @param  [string]  $ids      [商品ID,ID,ID]
     * @param  [int]  $website_id  [我的网站ID]
     * @return array
     */
    protected function is_my_goods($ids,$website_id){
        $spu_id = [];
        if(!empty($ids)){     
            $spu_list =  $this->goods_item->where(["id in($ids)"])->field('id,website_id')->select();
            if ($spu_list) {
                foreach ($spu_list as $key => $value) {
                    if($value['website_id'] == $website_id){
                        $spu_id['my'][] = $value['id'];
                    }else{
                        $spu_id['orther'][] = $value['id'];
                    }
                }
            }
        }
        return $spu_id;
    }
}