<?php
namespace application\manage\api;
use application\base\api\Base;
use framework\Flight;
/**
 * 信息提示
 */
class Infomessage extends Base{
    
    //判断是否有添加商品权限
    public function is_add_goods($website_id){
        $website_config  = Flight::model('common/website')->select_config_find($website_id);
        if(!$website_config['is_add_goods']){
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 友情提示','url'=>'javascript:;'];
            $tpl_date['msg_title'] = '没有权限';
            $tpl_date['msg_info']  = '您没有自助添加商品的权限,可以联系服务商请求开通'; 
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            exit($this->display('message',$tpl_date,'layout'));
        }
    }

    //判断是否有云服务权限
    public function is_open_authorize($website_id){
        $website_config  = Flight::model('common/website')->select_config_find($website_id);
        if(!$website_config['is_open_authorize']){
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 友情提示','url'=>'javascript:;'];
            $tpl_date['msg_title'] = '没有权限';
            $tpl_date['msg_info']  = '您没有使用云货源的权限,可以联系服务商请求开通'; 
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            exit($this->display('message',$tpl_date,'layout'));
        }
    }

    //判断是否有允许配置自己的支付
    public function is_open_pay($website_id){
        $website_config  = Flight::model('common/website')->select_config_find($website_id);
        if(!$website_config['is_open_pay']){
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 友情提示','url'=>'javascript:;'];
            $tpl_date['msg_title'] = '没有权限';
            $tpl_date['msg_info']  = '您没有使用自助收款的权限,可以联系服务商请求开通'; 
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            exit($this->display('message',$tpl_date,'layout'));
        }
    }

    /**
     * [message 输出友情提示模板]
     */
    public function message($tpl_date = []){
        $tpl_date['msg_title'] = '友情提示';       
        exit($this->display('message',$tpl_date,'layout'));
    }
}