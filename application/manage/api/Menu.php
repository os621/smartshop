<?php
namespace application\manage\api;
use application\base\api\Base;
use framework\Flight;
/**
 * 左侧菜单
 */
class Menu extends Base{
    
    /**
     * 管理中心左侧菜单
     * @param  [type] $key [description]
     * @return [type]      [description]
     */
    public function leftnav($key){
        $leftnav['goods'] = [
            ['name'=>'商品管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'商品管理(SPU)','url'=>url('manage/goods/index')],
            ['name'=>'添加商品','url'=>url('manage/goods/edit')],
            ['name'=>'商品采集','url'=>url('manage/spider/index')],
            ['name'=>'货源管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'九州云平台','url'=>url('manage/cloud/index')],
            ['name'=>'商品属性','url'=>'javascript:;','is_title' =>1],
            ['name'=>'商品分类','url'=>url('manage/category/index')],
            ['name'=>'品牌管理','url'=>url('manage/brand/index')],
            ['name'=>'规格管理','url'=>url('manage/spec/index')],
            ['name'=>'仓库管理','url'=>url('manage/warehouse/index')],
            ['name'=>'站点管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'特性与专栏','url'=>url('manage/goodsspecial/index')],
            ['name'=>'网站导航','url'=>url('manage/nav/index')],
        ];
        $leftnav['article'] = [
            ['name'=>'网站管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'内容管理','url'=>url('manage/article/index')],
            ['name'=>'广告管理','url'=>url('manage/adwords/index')],
            ['name'=>'栏目管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'内容栏目','url'=>url('manage/channel/index')],
            ['name'=>'网站导航','url'=>url('manage/nav/index')]
        ];
        $leftnav['user'] = [
            ['name'=>'信息管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'我的收件箱','url'=>url('manage/user/message')],
            ['name'=>'资金管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'账户余额','url'=>url('manage/bankroll/index')],
            ['name'=>'申请提取','url'=>url('manage/bankroll/withdraw')],
            ['name'=>'账户充值','url'=>url('manage/bankroll/rechange')],
            ['name'=>'财务流水','url'=>url('manage/bankroll/log')],
            ['name'=>'分站管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'我的下级','url'=>url('manage/account/subshop')],
            ['name'=>'新增下级','url'=>url('manage/account/subshop_reg')],
            ['name'=>'权限开通','url'=>'javascript:;','is_title' =>1],
            ['name'=>'供货商','url'=>url('manage/account/yunshop')],
            ['name'=>'服务与支持','url'=>'javascript:;','is_title' =>1],
            ['name'=>'我的工单','url'=>url('manage/service/index')],
            ['name'=>'提交工单','url'=>url('manage/service/getservice')],
        ];
        $leftnav['account'] = [
            ['name'=>'商城配置','url'=>'javascript:;','is_title' =>1],
            ['name'=>'站点设置','url'=>url('manage/website/config')],
            ['name'=>'支付通道','url'=>url('manage/website/payconfig')],
            ['name'=>'运费模板','url'=>url('manage/website/fareconfig')],
            ['name'=>'短信接口','url'=>url('manage/website/smsconfig')],
            ['name'=>'主题管理','url'=>'javascript:;','is_title' =>1],
            ['name'=>'模板商店','url'=>url('manage/website/skin')],
            ['name'=>'我的主题','url'=>url('manage/website/skin?status=1')],
        ];
        return $leftnav[$key];
    }
}