<?php
namespace application\manage\api;
use application\base\api\Base;
use framework\Flight;
/**
 * 商品价格计算接口
 */
class Price extends Base{
    
    /**
     * 单个商品价格
     * @param  [array] $key [计算参数]
     * @return [float]      [单个商品价格]
     * $data['price_rise'] = 商品调价幅度 0是不调价
     * $data['math_type']  = 计算方式 0：乘，1：加
     * $data['price_base'] = 计算方式 0：成本价，1：销售价
     * $data['sell_price'] = 销售价格
     * $data['cost_price'] = 成本价格
     */
    public function sku_price(array $data){
        if($data['price_rise'] > 0){
            $price_base = $data['price_base'] >= 1 ? $data['sell_price'] : $data['cost_price'];
            $price = $data['math_type'] >= 1 ? $price_base + $data['price_rise'] : $price_base * $data['price_rise'];
        }else{
            $price = $data['sell_price'];
        }
        return sprintf("%01.2f",$price);
    }

    /**
     * 计算运费多少钱
     * @param  array   $data       [计算参数]
     * @param  integer $website_id [当前站点ID]
     * @return [type]              [运费价格]
     */
    public function fare(array $data){
        //读取运费模板
        $array_yun_id = array_unique(array_column($data,'yun_id','id'));
        $yun_id       = implode(',',$array_yun_id);
        $condition[]  = "website_id in('{$yun_id}')";
        $fare = Flight::model('goods/fare')->info_select($condition);
        $website = [];
        if($fare){
            foreach ($fare as $key => $value) {
                $website[$value['website_id']] = $value;
            }
        }
        //开始计算运费
        $fare        = 0;
        foreach ($data as $key => $value) {
            $yun_id = $website[$value['yun_id']]['website_id'];
            if($value['yun_id'] == $yun_id){
                $weight        = $value['weight'] * $value['num'];
                $first_weight  = $website[$value['yun_id']]['first_weight'];
                $first_price   = $website[$value['yun_id']]['first_price'];
                $second_weight = $website[$value['yun_id']]['second_weight'];
                $second_price  = $website[$value['yun_id']]['second_price'];
                if($weight <= $first_weight){
                    $total  = $first_price;
                }else{
                    $weight = $weight - $second_weight;
                    $total  = $first_price + ceil($weight/$second_weight) * $second_price;
                }
                $fare += $total;
            }
        }
        return sprintf("%01.2f",$fare);
    }
}