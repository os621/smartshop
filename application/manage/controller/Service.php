<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-工单服务
 */
namespace application\manage\controller;
use framework\Flight;

class Service extends Common{

    private $service;

    public function __construct() {
        parent::__construct();
        $this->service = Flight::model('user/service');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('user'));  //引入管理菜单  

    }

    //管理首页
    public function index(){
        $page = (int)Flight::request()->query->page;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 服务与支持','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['service']  = $this->service->info_list(['user_id' =>$this->login_user['user_id'],'root_id' => 0],$page);
        $tpl_date['pager']    = $this->getPage($this->service->pager);
        $this->display(0,$tpl_date,'layout');
    }

    //查看
     public function reader(){
        if($this->isPost()){
            $rules = [
                'title'   => ['empty','标题没有填写'],
                'content' => ['empty','内容没有填写'],
                'id'      => ['empty','恢复主题丢失'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['title']   = Flight::filter()->filter_escape(trim($request['title']));
            $data['content'] = Flight::filter()->filter_escape(trim($request['content']));
            $data['user_id'] = $this->login_user['user_id'];
            $data['root_id'] = intval($request['id']);
            $data['state']   = 0;
            $result =  $this->service->info_edit($data);
            if($result){
                $this->service->info_edit(['state' => 0],['id' => $data['root_id']]);
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/service/reader',['id'=> $data['root_id']])],'msg'=>'工单提交成功,我们会及时反馈处理。']);
            }else{
                Flight::json(['code'=>403,'msg'=>'工单提交失败']);
            }
        }else{
            $id = (int)Flight::request()->query->id;
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 服务与支持','url'=>url("manage/service/index")];  
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;继续服务申请','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $condition['user_id'] = $this->login_user['user_id'];
            $condition[]          = "id = $id or root_id = $id";
            $tpl_date['service']  = $this->service->info_list($condition,$page,20,'id asc');
            $tpl_date['id']       = $id;
            $this->display(0,$tpl_date,'layout');
        }
    }   

    //新增服务
    public function getservice(){
        if($this->isPost()){
            $rules = [
                'title'    => ['empty','标题没有填写'],
                'content'  => ['empty','内容没有填写'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['title']      = Flight::filter()->filter_escape(trim($request['title']));
            $data['content']    = Flight::filter()->filter_escape(trim($request['content']));
            $data['user_id']    = $this->login_user['user_id'];
            $data['state']      = 0;
            $data['website_id'] = $this->website['id'];
            $result =  $this->service->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/service/index')],'msg'=>'工单提交成功,我们会及时反馈处理。']);
            }else{
                Flight::json(['code'=>403,'msg'=>'工单提交失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 服务与支持','url'=>'javascript:;'];  
            $pathMaps[] = ['name'=>'&nbsp;> 提交工单','url'=>'javascript:;'];  
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }    
}

