<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        采集蜘蛛
 */
namespace application\manage\controller;
use framework\Flight;

class Spider extends Common{

    private $spec;
    private $category;
    private $brand;

    public function __construct() {
        parent::__construct();
        $this->spec     = Flight::model('goods/spec');
        $this->category = Flight::model('goods/category');
        $this->brand    = Flight::model('goods/brand');
        $this->warehouse    = Flight::model('goods/warehouse');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
        Flight::api('manage/infomessage')->is_add_goods($this->website['id']);
    }

    /**
     * 规格列表
     */
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商品采集','url'=>'javascript:;'];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }

    /**
     * 采集规则
     */
    public function geturl(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商品采集','url'=>url('manage/spider/index')];
        $pathMaps[] = ['name'=>' > 开始采集','url'=>'javascript:;'];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['geturl']   = Flight::request()->query->geturl;;
        $this->display(0,$tpl_date,'layout');
    }

    /**
     * 解析网站
     * @return [type] [description]
     */
    public function trueurl(){
        $geturl = Flight::request()->query->geturl;
        $url    = Flight::request()->query->url;
        switch ($geturl) {
            case 'taobao':
                $rel = Flight::api('goods/taobao')->get_url($url);
                break;
        }
        return Flight::json($rel);
    }
    
    /**
     * 处理采集数据入库
     */
    public function getdata(){
        $rules = [
            'geturl'           => ['empty','数据来源未知,请重新选择数据来源'],
            'category_id'      => ['empty','请选择商品分类'],
            'category_path_id' => ['empty','请选择商品分类'],
        ];
        $rel = Flight::validator($rules);
        if($rel['code'] == 403){
            $this->ErrorMsg('友情提示',$rel['msg']);
        }
        $request     = Flight::request()->data;
        $url         = Flight::filter()->filter_escape(trim($request['url']));
        $content     = $request['content'];
        if(empty($url) && empty($content)){
            $this->ErrorMsg('友情提示','采集数据源,网址和解析内容最少输入一项');
        }
        $geturl      = Flight::filter()->filter_escape(trim($request['geturl']));
        $category_id = (int)$request['category_id'];
        if($this->isPost()){
            switch ($geturl) {
                case 'taobao':
                    $wdetail[0] = $content;
                    $wdetail[1] = $request['wdetail'];
                    $data = self::taobao($url,$wdetail);
                    break;
            }
            if($result['code'] == 404){
                $this->ErrorMsg('友情提示',$result['msg']);
            }
        }
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商品采集','url'=>url('goods/spider/index')];
        $pathMaps[] = ['name'=>' > 开始采集','url'=> url('goods/spider/getur',['geturl' => $geturl])];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        //栏目路径
        $category = $this->category->getPath($category_id);
        $category_path = '';
        foreach ($category as $key => $value) {
             $category_path .= $value['title'].' <i class="iconfont icon-arrowright"></i> ';
        }
        $tpl_date['category_path'] = $category_path;
        $spu['is_spec'] = count(json_decode($spu['spec_and_value_ids'],true));
        $tpl_date['spu'] = $data;
        $tpl_date['spu']['category_id']      = $category_id;;//栏目ID
        $tpl_date['spu']['category_path_id'] = $request['category_path_id'];//栏目路径
        $tpl_date['products_info'] = "new Array()";
        $tpl_date['brand']         = $this->brand->select_list(['website_id' => $this->website['id']]);
        $tpl_date['warehouse']     = $this->warehouse->select_list(['website_id' => $this->website['id']]);
        $this->display('goods/edit',$tpl_date,'layout');
    }

    /**
     * [taobao 淘宝采集]
     * @param  [string] $url [淘宝商品URL]
     * @return [array]      [采集数据源]
     */
    protected function taobao($url,$content){
        $result = Flight::api('goods/taobao')->index($url,$content);
        $data = [];
        if($result['code'] == 200){
            $data['name']             = $result['data']['itemInfoModel']['title'];
            $data['tag_ids']          = $result['data']['itemInfoModel']['stuffStatus'].','.$result['data']['itemInfoModel']['location']; //商品关键字
            $data['goods_no']         = $result['data']['itemInfoModel']['itemId']; //商品编号
            $data['sell_price']       = (float)$result['data']['marketprice'];  //销售价
            $data['market_price']     = (float)$result['data']['productprice'];  //市场价
            $data['cost_price']       = (float)$result['data']['costprice']; //成本价
            $data['store_nums']       = (float)$result['data']['total'];  //库存
            $data['warning_line']     = 20;  //默认20个预警线
            $data['imgs']             = $result['data']['itemInfoModel']['picsPath'];
            $data['img']              = $result['data']['itemInfoModel']['picsPath'][0];
            $data['content']          = $result['data']['content'];
            $data['props']            = $result['data']['props'];
            $data['taobao_item_id']   = $result['data']['itemInfoModel']['itemId'];
        }
        return $data;
    }  
}