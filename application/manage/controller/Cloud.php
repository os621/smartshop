<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-云货源
 */
namespace application\manage\controller;
use framework\Flight;
use extend\Category as ctg;

class Cloud extends Common{

    private $category;
    private $item;

    public function __construct() {
        parent::__construct();
        $this->category     = Flight::model('goods/category');
        $this->item         = Flight::model('goods/item');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
        Flight::api('manage/infomessage')->is_open_authorize($this->website['id']);
    }

    //云平台
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 九州云平台','url'=>url('manage/cloud/index')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }

    //列表
    public function item(){
        $cloud_id = (int)Flight::request()->query->cloud_id;
        $list_id  = (int)Flight::request()->query->list_id;
        $page     = (int)Flight::request()->query->page;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 九州云平台','url'=>url('manage/cloud/index')]; 
        $editMenu[] = ['name'=>'<i class="iconfont icon-leimu"></i> 分类筛选','url'=>"javascript:select_category('".url('manage/cloud/category',['cloud_id' => $cloud_id])."')"];
        $condition['is_sale'] = 2;
        $condition['is_del']  = 0;
        //官方商品目录
        $item = [];
        if(!$cloud_id){
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;官方服务','url'=>'javascript:;'];    
            $rel  = Flight::model('common/website')->select_category_find($this->website['id'],'category_ids');
            if(!$rel['category_ids']){
                $tpl_date['msg_info']  = '您没有授权目录,请联系客服开通授权商品目录。'; 
                $tpl_date['pathMaps'] = json_encode($pathMaps);
                Flight::api('manage/infomessage')->message($tpl_date);
            }
            $ids_array = explode(',',$rel['category_ids']);
            if($list_id){
                //当访问目录不再授权目录时候，计算下级目录根目录是否在授权目录
                if(!in_array($list_id,$ids_array)){
                    $category_info = $this->category->where(['id' => $list_id])->field('id,root_id')->find();
                    if(!in_array($category_info['root_id'],$ids_array)){
                        $tpl_date['msg_info']  = '您没有授权目录,请联系客服开通授权商品目录。'; 
                        $tpl_date['pathMaps'] = json_encode($pathMaps);           
                        $Flight::api('manage/infomessage')->message($tpl_date);  
                    }
                }
                $ids = $this->category->getIds(['website_id' => 0],$list_id);
            }else{
                $ids = $this->category->getIds(['website_id' => 0],$ids_array);
            }
            $condition[] = "category_id in ({$ids})";
            //S 筛选已上架的
            $website_spu = [];
            $where['website_id'] = intval($this->website['id']);
            $info = Flight::model('manage/item')->select_list($where,'spu_id');
            if($info){
                foreach ($info as $key => $value) {
                    $website_spu[] = $value['spu_id'];
                }
                $spu_ids = implode(',',$website_spu);
                $condition[] = "id not in ({$spu_ids})";
            }
            //E 筛选已上架的
            $item = $this->item->select_list($condition,$page);
        }
        //E 筛选已上架的
        $tpl_date['item']     = $item;
        $tpl_date['pager']    = $this->getPage($this->item->pager,['list_id'=>$list_id]);
        $tpl_date['website_spu']  = $website_spu;
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //商品预览
    public function preview(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 网商云平台','url'=>url('manage/cloud/index')]; 
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;云商品预览','url'=>'javascript:;'];  
        $item_id   = (int)Flight::request()->query->item;
        $condition['id']      = $item_id;
        $item_info = Flight::model('goods/item')->select_find($condition);
        $not_item = false;
        if(!$item_info) $not_item = true;
        if($item_info['website_id'] != $this->website['id']){
            if($item_info['is_sale'] !=2  && $item_info['is_del'] == 0){
                $not_item = true;
            }
        }
        if($not_item == true){
            $this->ErrorMsg('403 Forbidden','当前商品以下架');
        }
        $skumap = array();
        $products = Flight::model('goods/item')->select_sku_list(['spu_id' => $item_id],"sell_price,market_price,store_nums,specs_key,pro_no,id");
        if($products){
            foreach ($products as $product) {
                $skumap[$product['specs_key']] = $product;
            }
        }
        //商品分类信息
        $category_id   = (int)$item_info['category_id'];
        $category_info = $this->category->select_find($category_id,'id,root_id,parent_id,title');
        if(!$category_info) Flight::notFound();
        $tpl_date['category_id']   =  $category_id;
        //当前路径
        $tpl_date['item']    =  $item_info;
        $tpl_date['props']   =  empty($item_info['props']) ? array():json_decode($item_info['props'],true);
        $tpl_date['tag_ids'] =  empty($item_info['tag_ids']) ? array():explode(',',$item_info['tag_ids']);
        $tpl_date['imgs']    =  empty($item_info['imgs']) ? array():json_decode($item_info['imgs'],true);
        $tpl_date['specs']   =  json_decode($item_info['spec_and_value_ids'],true);
        $tpl_date['skumap']  =  json_encode($skumap);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }

    //上传商品到自己的商城中
    public function ids_action(){
        $ids_str          = Flight::request()->data['ids'];
        $category_id      = (int)Flight::request()->data['category_id'];
        $category_path_id = Flight::request()->data['category_path_id'];
        if(!$ids_str){
            Flight::json(['code'=>403,'msg'=>'没有选择任何要操作商品']);
        }
        if(!$category_id || !$category_path_id){
            Flight::json(['code'=>403,'msg'=>'请选择要加入的商品目录']);
        }
        $ids = [];
        $ids_ary = explode(',',trim($ids_str,','));
        foreach ($ids_ary as $key => $value) {
            $ids[] = (int)$value;
        }
        $data['website_id']       = intval($this->website['id']);
        $data['category_id']      = $category_id;
        $data['category_path_id'] = trim($category_path_id);
        $data['is_sale']          = 1;  //下架
        Flight::model('manage/item')->all_action($data,$ids);
        Flight::json(['code'=>302,'msg'=>'商品成功上架到我的商城']);
    }

    //读取授权目录
    public function category(){
        $tpl_date['input']    = Flight::request()->query->input;
        $tpl_date['cloud_id'] = intval(Flight::request()->query->cloud_id);
        $this->display(0,$tpl_date, 'meta');
    }

    //联动菜单
    public function selected(){
        $parent_id = intval(Flight::request()->query->parent_id);
        $cloud_id  = intval(Flight::request()->query->cloud_id);
        //官方商品目录
        if(!$cloud_id){
            $rel  = Flight::model('common/website')->select_category_find($this->website['id'],'category_ids');
            if(!$rel['category_ids']){
                Flight::json(['code'=>403,'msg'=>'您没有授权目录,请联系客服开通授权商品目录。']);
            }
            if(!$parent_id){
                $condition[] = "id in ({$rel['category_ids']})";
                $condition['parent_id'] = 0;
                $info = $this->category->select_list_all($condition,'title,id');
            }else{
                $category_ids = explode(',',$rel['category_ids']);
                //当访问目录不再授权目录时候，计算下级目录根目录是否在授权目录
                if(!in_array($parent_id,$category_ids)){
                    $category_info = $this->category->where(['id' => $parent_id])->field('id,root_id')->find();
                    if(!in_array($category_info['root_id'],$category_ids)){
                        Flight::json(['code'=>403,'msg'=>'您没有授权目录,请联系客服开通授权商品目录。']);
                    }
                }
                $condition['parent_id'] = $parent_id;
                $info = $this->category->select_list_all($condition,'title,id');
            }
        }
        if($info){
            Flight::json(['code'=>200,'msg'=>'成功','data'=>$info]);
        }
        Flight::json(['code'=>403,'msg'=>'读取商品分类列表失败']);
    }

    //当前路径
    public function selected_path(){
        $cid  = (int)Flight::request()->query->cid;
        $info = $this->category->getPath($cid);
        if($info){
            $len = count($info);
            $category = [];
            foreach ($info as $key => $value) {
                $category[] = $value['id'];
            }
            $category_id = implode(',',$category);
            Flight::json(['code'=>200,'msg'=>'成功','data'=>$info,'path' => $len,'category_id' => $category_id]);
        }
        Flight::json(['code'=>403,'msg'=>'读取商品分类路径失败']);
    } 
}