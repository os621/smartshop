<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        栏目管理
 */
namespace application\manage\controller;
use framework\Flight;

class Channel extends Common{

    private $special;

    public function __construct() {
        parent::__construct();
        $this->channel = Flight::model('article/channel');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('article'));  //引入管理菜单
    }

    //列表
    public function index(){
        $tpl_date['parent_id']  = (int)Flight::request()->query->parent_id;
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 栏目管理','url'=>url('manage/channel/index')];
        $path = $this->channel->mange_select_path($tpl_date['parent_id']);
        foreach ($path as $key => $value) {
            $pathMaps[] = $value;
        }
        $editMenu = [];
        if($tpl_date['parent_id']){
            $editMenu[] = [
                'name'=> '<i class="iconfont icon-tianjia"></i> 添加栏目',
                'url' => url('manage/channel/edit',['parent_id'=>intval($tpl_date['parent_id'])])
            ];
        }
        //菜单结束
        $tpl_date['channel']   = $this->channel->select_list($tpl_date['parent_id'],$this->website['id']);
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = ['neworder' => ['empty'],'name'=> ['empty'],'parent_id'=> ['empty','你没有修改当前栏目的权限']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']        = (int)$request['id'];
            $data['parent_id'] = (int)$request['parent_id'];
            $data['website_id']= (int)$this->website['id'];
            $data['neworder']  = Flight::filter()->filter_escape(trim($request['neworder']));
            $data['name']      = Flight::filter()->filter_escape(trim($request['name']));
            $data['url']       = Flight::filter()->filter_escape(trim($request['url']));
            if($data['id']){
                $info = $this->channel->select_find($data['id']);
                if($info['website_id'] != $data['website_id']) Flight::json(['code'=>403,'msg'=>'你没有操作当前导航的权限']);
            }
            $result =  $this->channel->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/channel/index',['parent_id'=>$data['parent_id']])],'msg'=>'栏目创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'栏目创建或编辑不成功']);
            }
        }else{
            $id        = (int)Flight::request()->query->id;
            $parent_id = (int)Flight::request()->query->parent_id;
            $tpl_date['info']       = $this->channel->select_find($id);
            $tpl_date['parent_id']  = $parent_id  ? $parent_id : (int)$tpl_date['info']['parent_id'];
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 栏目管理','url'=>url('manage/channel/index')];
            $path = $this->channel->mange_select_path($tpl_date['parent_id']);
            foreach ($path as $key => $value) {
                $pathMaps[] = $value;
            }
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            //菜单结束
            $tpl_date['id']  = $id;
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id         = (int)Flight::request()->query->id;
        $website_id = $this->website['id'];
        $result =  $this->channel->info_delete(['id' => $id,'website_id' => $website_id]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请先删除栏目的子栏目和内容']);
        } 
    }
    
    //字段排序
    public function neworder(){
        $data['neworder'] = intval(Flight::request()->data['sorter']);
        $data['id']       = intval(Flight::request()->data['id']);
        $result = $this->channel->neworder($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }

    /**
     *读取栏目列表返回数据
     */
    public function ids() {
        $channel_id  = (int)Flight::request()->query->channel_id;
        $website_id  = (int)Flight::request()->query->website_id;
        $info = $this->channel->select_lists_ids($channel_id,$website_id);
        Flight::json($info);
    }    
}