<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\manage\controller;
use framework\Flight;

class User extends Common{

    private $message;

    public function __construct() {
        parent::__construct();
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('user'));  //引入管理菜单 
        $this->message = Flight::model('user/message'); 
    }

    //管理首页
    public function index(){
        Flight::redirect(url('manage/bankroll/index'));
    } 

    //我的站内信
    public function message(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 我的收件箱','url'=>url('manage/user/message')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['message']  = $this->message->info_list(['user_id' => $this->login_user['user_id']]);
        $tpl_date['pager']    = $this->getPage($this->message->pager);
        $this->display(0,$tpl_date,'layout');
    }

    //查看
     public function reader(){
        $id = (int)Flight::request()->query->id;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 我的收件箱','url'=>url("manage/user/message")];  
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;阅读内容','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $condition['user_id'] = $this->login_user['user_id'];
        $condition['id']      = $id;
        $tpl_date['info']     = $this->message->info_find($condition);
        if(empty($tpl_date['info'])){
            Flight::redirect(url('passport/message'),302);
        }
        $this->message->info_edit(['state' => 1],$condition);
        $this->display(0,$tpl_date,'layout');
    }  

    //删除
    public function message_delete(){
        $condition['user_id'] = $this->login_user['user_id'];
        $condition['id']      = (int)Flight::request()->query->id;
        $result =  $this->message->info_delete($condition);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        }
    }
}

