<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        仓库管理
 */
namespace application\manage\controller;
use framework\Flight;

class Brand extends Common{

    private $brand;

    public function __construct() {
        parent::__construct();
        $this->brand = Flight::model('goods/brand');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
        Flight::api('manage/infomessage')->is_add_goods($this->website['id']);
    }

    //列表
    public function index(){
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商品品牌','url'=>'javascript:;'];
        $editMenu[] = ['name'=>'<i class="iconfont icon-jiahao"></i> 添加品牌','url'=>url('manage/brand/edit')];
        //菜单结束    
        $tpl_date['lists']    = $this->brand->select_list(['website_id' => $this->website['id']]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'name'      => ['empty','品牌名称必须填写'],
                'en_name'   => ['empty','品牌英文名称必须填写'],
                'first_abc' => ['len','品牌英文名称必须填写且只能填写一个字母','min:1,max:1'],
                'sort'      => ['type','品牌排序必须输入','INT'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request      =  Flight::request()->data;
            $data['id']         = (int)$request['id'];
            $data['name']       = Flight::filter()->filter_escape(trim($request['name']));
            $data['en_name']    = Flight::filter()->filter_escape(trim($request['en_name']));
            $data['first_abc']  = Flight::filter()->filter_escape(trim(strtoupper($request['first_abc'])));
            $data['logo']       = Flight::filter()->filter_escape(trim($request['logo']));
            $data['sort']       = (int)$request['sort'];
            $data['website_id'] = (int)$this->website['id'];
            $result =  $this->brand->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/brand/index')],'msg'=>' 品牌创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>' 品牌创建或编辑不成功']);
            }
        }else{
            $id = intval(Flight::request()->query->id);
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商品品牌','url'=>url('manage/brand/index')];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp;添加/编辑','url'=>'javascript:;'];
            //菜单结束
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['info']     = $this->brand->select_find(['id' =>$id,'website_id'=>$this->website['id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $id = Flight::request()->query->id;
        $result =  $this->brand->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含类型属性']);
        } 
    }

    //字段排序
    public function sort(){
        $data['sort'] = intval(Flight::request()->data['sort']);
        $data['id']   = intval(Flight::request()->data['id']);
        $result = $this->brand->info_sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }
}