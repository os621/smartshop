<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-商品管理
 */
namespace application\manage\controller;
use framework\Flight;

class Goods extends Common{

    private $brand;
    private $category;
    private $spec;
    private $item;
    private $warehouse;

    public function __construct() {
        parent::__construct();
        $this->brand        = Flight::model('goods/brand');
        $this->category     = Flight::model('goods/category');
        $this->spec         = Flight::model('goods/spec');
        $this->item         = Flight::model('goods/item');
        $this->warehouse    = Flight::model('goods/warehouse');
        $this->website_item = Flight::model('manage/item');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
    }

    //列表
    public function index(){
        $status  = Flight::request()->query->status;
        $page    = (int)Flight::request()->query->page;
        $keyword = Flight::request()->query->keyword;
        $list_id = (int)Flight::request()->query->list_id;
        switch ($status) {
            case 'is_sale':
                $i = 1;
                $condition = ['A.is_sale' => 2,'A.is_del' => 0];
                break;
            case 'on_sale':
                $i = 2;
                $condition = ['A.is_sale' => 1,'A.is_del' => 0];
                break;
            case 'review':
                $i = 3;
                $condition = ['A.is_sale' => 0,'A.is_del' => 0];
                break;
            case 'trash':
                $i = 4;
                $condition = ['A.is_del' => 1];
                break;
            default:
                $condition = ['A.is_del' => 0];
                $i = 0;
                break;
        }
        if($list_id){
            $ids = $this->category->getIds(['website_id' => $this->website['id']],$list_id);
            $condition[] = "A.category_id in ({$ids})";
        }
        $button[$i] = 'button-blue';
        //菜单开始
        $pathMaps = [
            ['name'=>'<span class="pure-button '.$button['0'].'"><i class="iconfont icon-shouye"></i> 全部商品</span>&nbsp;','url'=>url('manage/goods/index')],
            ['name'=>'<span class="pure-button '.$button['1'].'"><i class="iconfont icon-xiangshang3"></i> 在售商品</span>&nbsp;','url'=>url('manage/goods/index',['status' =>'is_sale'])],
            ['name'=>'<span class="pure-button '.$button['3'].'"><i class="iconfont icon-yanjing"></i> 待审商品</span>&nbsp;','url'=>url('manage/goods/index',['status' =>'review'])],
            ['name'=>'<span class="pure-button '.$button['2'].'"><i class="iconfont icon-xiangxia5"></i> 下架商品</span>&nbsp;','url'=>url('manage/goods/index',['status' =>'on_sale'])],
            ['name'=>'<span class="pure-button '.$button['4'].'"><i class="iconfont icon-shanchu"></i> 商品回收站</span>&nbsp;','url'=>url('manage/goods/index',['status' =>'trash'])],
        ];
        //菜单结束 
        $editMenu[]  = ['name'=>'<i class="iconfont icon-leimu"></i> 分类筛选','url'=>"javascript:select_category('".url('manage/category/popup',['selected' =>1])."')"];
        $condition[] = "A.website_id = ".$this->website['id'];
        $item     = $this->website_item->select_spu_list($condition,$page,25);
        if(!$item){
            $tpl_date['msg_title'] = '没有任何商品';
            $tpl_date['msg_info']  = '没有任何商品'; 
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            Flight::api('manage/infomessage')->message($tpl_date);
        }
        //计算调价后的价格
        foreach ($item as $key => $value) {  
            $item[$key]['price'] = Flight::api('manage/price')->sku_price($value);
        }
        $tpl_date['item']     = $item;
        $tpl_date['pager']    = $this->getPage($this->website_item->pager,['list_id' =>$list_id,'status' => $status]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['status']   = $status;
        $this->display(0,$tpl_date,'layout');
    }

    //添加商品
    public function edit(){
        //读取当前站点配置
        Flight::api('manage/infomessage')->is_add_goods($this->website['id']);
        if($this->isPost()){
            $this->save_data();
            Flight::json(['code'=>200,'data'=>['url'=>url('manage/goods/index')],'msg'=>' 商品添加或编辑成功']);
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i>&nbsp;商品管理&nbsp;','url'=>url('manage/goods/index')];
            $pathMaps[] = ['name'=>' >&nbsp;添加商品','url'=> 'javascript:;'];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $id  = (int)Flight::request()->query->id;
            $spu = $this->item->select_find($id);
            $products_sku = [];
            if($spu){
                if($spu['website_id'] != $this->website['id']){
                    $tpl_date['msg_info'] = '当前商品是云商品,禁止编辑'; 
                    $tpl_date['pathMaps'] = json_encode($pathMaps);           
                    Flight::api('manage/infomessage')->message($tpl_date);   
                }
                $tpl_date['spec_and_value_ids'] = json_decode($spu['spec_and_value_ids'],true);  //使用的规格
                foreach ($tpl_date['spec_and_value_ids'] as $key => $item) {
                   $spec_ids .= $item['id'].',';
                }
                $tpl_date['spec_ids'] = rtrim($spec_ids,",");  //使用的规格ID
                $sku = $this->item->select_sku_list(['spu_id' => $spu['id']]);
                if($sku){
                    $spec_str = '';
                    foreach ($sku as $key => $item) {
                        $spec_and_value_ids = json_decode($item['spec_and_value_ids'],true);
                        foreach ($spec_and_value_ids as $value) {
                            $sku[$key]['spec_name'][] = $value['spec_value'][2]; //SKU信息
                            $spec_str .= implode(":",$value['spec_value']).",";  //规格组合KEY
                        }
                        $spec_item_key = trim($spec_str,',');
                        $products_sku[$spec_item_key] = [
                            'store_nums'   => $item['store_nums'],
                            'warning_line' => $item['warning_line'],
                            'sell_price'   => $item['sell_price'],
                            'market_price' => $item['market_price'],
                            'cost_price'   => $item['cost_price'],
                        ];
                        $sku[$key]['spec_str'] = $spec_item_key;
                        $spec_str = '';  //参数重置
                    }
                }
                //当前商品目录
                $category = $this->category->getPath($spu['category_id']);
                foreach ($category as $key => $value) {
                     $category_path .= $value['title'].' <i class="iconfont icon-arrowright"></i> ';
                }
                $tpl_date['category_path'] = $category_path;
                //商品小图
                if($spu['imgs']){
                    $spu['imgs'] = json_decode($spu['imgs'],true);
                }
                //商品属性
                if($spu['props']){
                    $spu['props'] = json_decode($spu['props'],true);
                }
                //统计有多少个SKU规格
                $spu['is_spec'] = count(json_decode($spu['spec_and_value_ids'],true)); 
            }
            $tpl_date['id']  = $id;
            $tpl_date['spu'] = $spu;
            $tpl_date['sku'] = $sku;
            $tpl_date['warehouse']     = $this->warehouse->select_list(['website_id' => $this->website['id']]);
            $tpl_date['products_info'] = empty($products_sku)?'"new Array()"':json_encode($products_sku);
            $tpl_date['brand'] = $this->brand->select_list(['website_id' => $this->website['id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除进入回收站
    public function delete(){
        if($this->isPost()){
            $ids_str = Flight::request()->data['ids'];
            $status  = (int)Flight::request()->query->status;
            if($ids_str){
                $ids = [];
                $ids_ary = explode(',',trim($ids_str,','));
                foreach ($ids_ary as $key => $value) {
                    $id[] = (int)$value;
                }
                $ids = implode(',',$id);
            }else{
                Flight::json(['code'=>403,'msg'=>'未选择任何要操作商品']);
            }
            $result = $this->website_item->spu_delete($ids,$this->website['id']);
            if($result){
                Flight::json(['code'=>200,'msg'=>'删除成功','data'=>['url'=>url('manage/goods/index',['status' => $status])]]);
            }else{
                Flight::json(['code'=>403,'msg'=>'删除失败,批量操作不排除有成功的。']);
            } 
        }
        Flight::notFound();
    }

    //上架,下架,从回收站恢复
    public function ids_action(){
        if($this->isPost()){
            $issale  = (int)Flight::request()->query->issale;
            $status  = (int)Flight::request()->query->status;
            $ids_str = Flight::request()->data['ids'];
            if($ids_str){
                $ids = [];
                $ids_ary = explode(',',trim($ids_str,','));
                foreach ($ids_ary as $key => $value) {
                    $id[] = (int)$value;
                }
                $ids = implode(',',$id);
                $this->website_item->spu_all_action($issale,$ids,$this->website['id']);
                Flight::json(['code'=>200,'msg'=>'操作成功','data'=>['url'=>'']]);
            }else{
                Flight::json(['code'=>403,'msg'=>'未选择任何要操作商品']);
            }
        }
        Flight::notFound();
    } 

    //单个修改商品价格幅度
    public function price(){
        if($this->isPost()){
            $rules = [
                'id'         => ['type','商品编辑ID错误','INT'],
                'price_base' => ['type','调价幅度必须填写','INT'],
                'math_type'  => ['type','调价幅度必须填写','INT'],
                'price'      => ['require','调价幅度必须填写']
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            //开始处理数据逻辑
            $request =  Flight::request()->data;
            $data['price_base'] = (int)$request['price_base'];
            $data['math_type']  = (int)$request['math_type'];
            $data['price_rise'] = floatval($request['price']);
            $rel = $this->website_item->item_edit($data,['website_id' => $this->website['id'],'spu_id' => (int)$request['id']]);
            if($rel){
                Flight::json(['code'=>302,'msg'=>'操作成功','data'=>['url'=>'']]);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $id  = (int)Flight::request()->query->id;
            if(!$id)Flight::halt('403','非法访问');
            $info = $this->website_item->select_find(['website_id'=>$this->website['id'],'spu_id' => $id]);
            if(!$info)Flight::halt('403','查询商品不存在');
            $tpl_date['info'] = $info;
            $this->display(0,$tpl_date,'meta');
        }
    }

    //批量增加商品价格幅度
    public function allprice(){
        if($this->isPost()){
            $rules = [
                'ids'        => ['empty','商品编辑ID不能为空'],
                'price_base' => ['number','调价幅度必须填写'],
                'math_type'  => ['number','调价幅度必须填写'],
                'price'      => ['require','调价幅度必须填写']
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            //开始处理数据逻辑
            $request =  Flight::request()->data;
            $ids_str = Flight::request()->data->ids;
            if($ids_str){
                $ids_ary = explode(',',$ids_str);
                $id_ary  = [];
                foreach ($ids_ary as $key => $value) {
                    $id_ary[] = (int)$value;
                }
                $ids = implode(',',$id_ary);
                //条件
                $condition[] = "spu_id in($ids)";
                $condition['website_id'] = $this->website['id'];
                //值
                $data['price_base'] = (int)$request['price_base'];
                $data['math_type']  = (int)$request['math_type'];
                $data['price_rise'] = floatval($request['price']);
                $rel = $this->website_item->item_edit($data,$condition);
                if($rel){
                    Flight::json(['code'=>302,'msg'=>'操作成功','data'=>['url'=>'']]);
                }else{
                    Flight::json(['code'=>403,'msg'=>'操作失败']);
                }
            }else{
                Flight::json(['code'=>403,'msg'=>'未选择任何要操作商品']);
            }
        }else{
            $ids_str = Flight::request()->query->ids;
            if($ids_str){
                $ids_str = trim($ids_str,',');
            }
            $tpl_date['ids'] = $ids_str;
            $this->display(0,$tpl_date,'meta');
        }
    }

    /**
     * ######################################################################
     * 提交保存数据
     * @return [type] [description]
     */
    protected function save_data(){
      //表单验证
        $rules = [
            'category_id'      => ['type','商品分类必须输入','INT'],
            'category_path_id' => ['empty','商品分类必须输入'],
            'brand_id'         => ['type','品牌排序必须输入','INT'],
            'warehouse_id'     => ['type','商品所在仓库必须输入','INT'],
            'name'             => ['empty','商品名称必须输入'],
            'goods_no'         => ['empty','商品编号必须输入'],
            'sell_price'       => ['empty','销售价必须输入'],
            'market_price'     => ['empty','市场价必须输入'],
            'cost_price'       => ['empty','成本价必须输入'],
            'store_nums'       => ['empty','库存必须输入'],
            'warning_line'     => ['empty','预警线必须输入'],
            'img'              => ['empty','没有设置默认图片'],
            'content'          => ['empty','商品描述必须填写'],
            'props_name'       => ['array','商品属性名称必须填写'],
            'props_value'      => ['array','商品属性必须填写'],
            'point'            => ['type','积分必须填写','INT'],
            'sort'             => ['type','排序必须填写','INT'],
        ];
        $rel = Flight::validator($rules);
        if($rel['code'] == 403){
            exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
        }
        $website_config  = Flight::model('common/website')->select_config_find($this->website['id']);
        $is_sale = $website_config['is_goods_pass'] ? 0 : 1;
        //开始处理数据逻辑
        $request  =  Flight::request()->data;
        $imgs = is_array($request['imgs']) ? $request['imgs'] : [];
        $data['id']               = (int)$request['id'];
        $data['category_id']      = (int)$request['category_id'];
        $data['category_path_id'] = Flight::filter()->filter_escape(trim($request['category_path_id']));
        $data['brand_id']         = (int)$request['brand_id'];
        $data['warehouse_id']     = (int)$request['warehouse_id'];
        $data['name']             = Flight::filter()->filter_escape(trim($request['name']));
        $data['subtitle']         = Flight::filter()->filter_escape(trim($request['subtitle']));
        $data['tag_ids']          = Flight::filter()->filter_escape(trim($request['tag_ids']));
        $data['goods_no']         = Flight::filter()->filter_escape(trim($request['goods_no']));
        $data['pro_no']           = Flight::filter()->filter_escape(trim($request['pro_no']));
        $data['sell_price']       = (float)$request['sell_price'];
        $data['market_price']     = (float)$request['market_price'];
        $data['cost_price']       = (float)$request['cost_price'];
        $data['store_nums']       = (float)$request['store_nums'];
        $data['warning_line']     = (float)$request['warning_line'];
        $data['unit']             = Flight::filter()->filter_escape(trim($request['unit']));
        $data['imgs']             = json_encode(Flight::filter()->filter_escape($imgs));
        $data['img']              = Flight::filter()->filter_escape(trim($request['img']));
        $data['content']          = Flight::filter()->filter_escape($request['content']);
        $data['website_id']       = (int)$this->website['id'];
        $data['source']           = Flight::filter()->filter_escape(trim($request['source']));
        $data['note']             = Flight::filter()->filter_escape(trim($request['note']));
        $data['point']            = (int)$request['point'];
        $data['sort']             = (int)$request['sort'];
        $data['is_sale']          = $is_sale;
        $data['taobao_item_id']   = Flight::filter()->filter_escape(trim($request['taobao_item_id']));
        $data['goods_data']       = Flight::filter()->filter_escape(trim($request['goods_data']));
        if(is_array($request['props_name']) && is_array($request['props_value'])){
            foreach ($request['props_name'] as $key => $value) {
                $ary[$key]['name'] = $value;
            }
          foreach ($request['props_value'] as $key => $value) {
                $ary[$key]['value'] = $value;
            }
            $data['props'] = json_encode($ary);
        }
        //开始处理商品SKU
        $sku_spec_item = is_array($request['sku_spec_item']) ? array_filter($request['sku_spec_item']) : [];
        $specs_new     = [];
        $values_dcr    = [];
        if(!empty($sku_spec_item)){
            $spu_spec_ids  = $request['spec_items'];   //获取使用了的商品规格主ID
            $sku_spec   = $this->item->info_cartesiant($spu_spec_ids,$sku_spec_item);  //笛卡尔乘积
            $specs_new  = $sku_spec[0];
            $values_dcr = $sku_spec[1];
            $data['sku_spec_item']    = $sku_spec_item;
            $data['sku_pro_no']       = $request['sku_pro_no'];
            $data['sku_sell_price']   = $request['sku_sell_price'];
            $data['sku_market_price'] = $request['sku_market_price'];
            $data['sku_cost_price']   = $request['sku_cost_price'];
            $data['sku_store_nums']   = $request['sku_store_nums'];
            $data['sku_warning_line'] = $request['sku_warning_line'];
            $data['sku_weight']       = $request['sku_weight'];
        }
        $data['spec_and_value_ids']  = empty($specs_new) ? json_encode([]) : json_encode($specs_new);
        $spu_id = $this->item->info_edit($data);
        //清理多余的商品
        $keys  = empty($values_dcr) ? '' : implode("','",array_keys($values_dcr));
        if($keys){
            $this->item->sku_delete(["spu_id = ".$spu_id." and specs_key not in('$keys')"]);
        }else{
            $this->item->sku_delete(["spu_id = ".$spu_id]);
        }
        //编辑SKU商品
        $data['spu_id'] = $spu_id;
        $i = $this->item->sku_edit($values_dcr,$data);
        if(!$i){
            $this->item->sku_edit_spu($data);
        }
        //商品规格,SPU,SKU关联
        $this->item->item_spec_edit($specs_new,$spu_id);
        //添加商品到站点关联库
        $website_spu_data['website_id']       = $data['website_id'];
        $website_spu_data['category_id']      = $data['category_id'];
        $website_spu_data['category_path_id'] = $data['category_path_id'];
        $website_spu_data['is_sale']          = $is_sale;  //审核
        $this->website_item->all_action($website_spu_data,$spu_id);
        return true;
    }

    //获取商品规格
    public function spec_popup(){
        $list = $this->spec->select_list(['website_id' => $this->website['id']]);
        if($list){
            foreach ($list as $key => $value) {
                $list[$key]['spec_value'] = json_decode($value['spec_value'],true);
            }
        }
        $tpl_date['spec'] = $list;
        $this->display(0,$tpl_date,'meta');
    }

    //商品特性与专栏
    public function special(){
        $special_id  = intval(Flight::request()->query->special_id);
        $category_id = intval(Flight::request()->query->category_id);
        $input      = Flight::request()->query->input;
        $keyword    = Flight::request()->query->keyword;
        $page       = (int)Flight::request()->query->page;
        $condition['A.is_sale']    = 2;
        $condition['A.is_del']     = 0;
        $condition['A.website_id'] = $this->website['id'];
        if($category_id) $condition[] = "FIND_IN_SET('{$category_id}',A.category_path_id)";
        if($keyword) $condition[] = "name LIKE '%$keyword%'"; 
        $item   = $this->website_item->select_spu_list($condition,$page,25);
        $spu_id = [];
        foreach ($item as $key => $value) {
            $spu_id[] = $value['id'];
        }
        $spu_ids = implode(',',$spu_id);
        $special_spu = [];
        if($spu_ids){
            $where['website_id'] = intval($this->website['id']);
            $where['special_id'] = $special_id;
            $where[] = "spu_id in({$spu_ids})";
            $info = Flight::model('manage/special')->select_special_spu($where,'spu_id');
            foreach ($info as $key => $value) {
                $special_spu[] = $value['spu_id'];
            }
        } 
        $tpl_date['item']        = $item;
        $tpl_date['pager']       = $this->getPage($this->website_item->pager,['special_id'=>$special_id,'input'=>$input,'keyword'=>$keyword]);
        $tpl_date['special_id']  = $special_id;
        $tpl_date['input']       = $input;
        $tpl_date['keyword']     = $keyword;
        $tpl_date['special_spu'] = $special_spu;
        $this->display(0,$tpl_date,'meta');
    }    
}