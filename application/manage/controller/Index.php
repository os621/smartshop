<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-会员中心
 */
namespace application\manage\controller;
use framework\Flight;

class Index extends Common{

    public function __construct() {
        parent::__construct();
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单  
    }

    //管理首页
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商家首页','url'=>'javascript:;'];  
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        Flight::redirect(url('manage/goods/index'),302);
        //$this->display(0,$tpl_date,'layout');
    } 
}

