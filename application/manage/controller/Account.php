<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-云货源
 */
namespace application\manage\controller;
use framework\Flight;

class Account extends Common{

    public function __construct() {
        parent::__construct();
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('user'));  //引入管理菜单
    }

    /**
     * 开通子站功能
     * ########################################################################
     */
    //分站管理
    public function subshop(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 我的下级商城','url'=>url('manage/account/subshop')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 新增下级商城','url'=>url('manage/account/subshop_reg')];
        $tpl_date['editMenu'] = json_encode($editMenu);
        $subshop              = Flight::model('common/subshop');
        $tpl_date['subshop']  = $subshop->info_list(['website_id' => $this->website['id']]);
        $tpl_date['pager']    = $this->getPage($subshop->pager);
        $this->display(0,$tpl_date,'layout');
    }

    //开通分站
    public function subshop_reg(){
        if($this->isPost()){
            $rules = [
                'phone' => ['mobile','手机号必须输入'],
                'code'  => ['number','验证码输入格式错误'],
                'title' => ['empty','站点名称必须输入'],
                'url'   => ['empty','网址格式不正确'],
                'icp'   => ['empty','ICP备案号必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['phone'] = Flight::filter()->filter_escape(trim($request['phone']));
            $data['code']  = Flight::filter()->filter_escape(trim($request['code']));
            $data['title'] = Flight::filter()->filter_escape(trim($request['title']));
            $data['url']   = Flight::filter()->filter_escape(trim($request['url']));
            $data['icp']   = Flight::filter()->filter_escape(trim($request['icp']));
            $data['website_id']  = $this->website['id'];
            $data['user_id'] = $this->login_user['user_id'];
            $data['number'] = uuid_no();
            //S 判断验证码
            $captcha = new \extend\Session($this->CodeID); //短信SESSION
            $code = $captcha->get();
            if(empty($code)) exit(Flight::json(['code'=>403,'msg'=>"验证码失效请重新获取"]));
            if($code['phone'] != $data['phone'] || $code['code'] != $data['code']){
                exit(Flight::json(['code'=>403,'msg'=>"验证码错误"]));
            }
            $captcha->del();
            //E 判断验证码
            //以手机号查找对应会员
            $user = Flight::model('user/user')->isfind(['phone_id' => $data['phone']]);
            if(!$user) Flight::json(['code'=>403,'msg'=>"系统不存在对应手机注册用户用户"]);
            $data['user_reg_id'] = $user['id'];
            $rel = Flight::model('common/subshop')->info_reg($data);
            if($rel){
                Flight::json(['code'=>200,'msg'=>"申请开通成功,请支付开通费用",'data'=>['url' => url("manage/account/subshop_order",['id' => $rel])]]);
            }else{
                Flight::json(['code'=>403,'msg'=>"申请不成功"]);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 我的下级商城','url'=>url('manage/account/subshop')];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp开通下级商城','url'=>url('manage/account/subshop_reg')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }   

    //子站付费
    public function subshop_order(){
        if($this->isPost()){
            $rules = [
                'pay_type' => ['number','支付类型必须选择'],
                'id'       => ['number','支付站点出错'],
                'pass'     => ['empty','支付密码必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  = Flight::request()->data;
            $pay_type = intval($request['pay_type']);
            $id       = intval($request['id']);
            $pass     = Flight::filter()->filter_escape(trim($request['pass']));
            //判断支付密码
            $is_pay_pass = Flight::api('payment/bank')->is_pay($this->login_user['user_id'],$pass);
            if(!$is_pay_pass) Flight::json(['code'=>403,'msg'=>"支付密码输入错误"]);
            //读取应该扣多少钱
            $level  = Flight::model('common/website')->level_find(['id' => $this->website['level']]);
            if($level['price'] <= 0 || $level['point'] <= 0 ){  //免费
                $pay_result = Flight::model('common/subshop')->info_update(['website_id' => $this->website['id'],'id' => $id],['is_pay' => 1]);
                $msg = "免费";
            }else{
                if($pay_type){
                    $msg = "站点ID:{$this->website['id']}《{$this->website['name']}》支付积分:{$level['point']},申请开通商城分站";
                    $is_pay = Flight::api('payment/bank')->point_down($this->login_user['user_id'],$level['point'],$msg);
                }else{
                    $msg = "站点ID:{$this->website['id']}《{$this->website['name']}》支付人民币: ￥{$level['price']},申请开通商城分站";
                    $is_pay = Flight::api('payment/bank')->money_down($this->login_user['user_id'],$level['price'],$msg);
                    if($is_pay){ //增加积分
                        $price = intval($level['price']);
                        Flight::api('payment/bank')->point_up($this->login_user['user_id'],$price,"消费人民币: ￥{$level['price']},增加积分:{$price}");
                    }
                }
                if($is_pay){
                    $pay_result = Flight::model('common/subshop')->info_update(['website_id' => $this->website['id'],'id' => $id],['is_pay' => 1]);
                }else{
                    Flight::json(['code'=>403,'msg'=>"扣款失败,余额不足请重置.",'data'=>['url'=>url('manage/bankroll/recharge')]]);
                }
            }
            if($pay_result){
                Flight::api('common/sys')->log($this->login_user['user_id'],$msg);
                Flight::json(['code'=>200,'msg'=>"申请开通成功,请等待审核通过.",'data'=>['url'=>url("manage/account/subshop_order",['id'=>$id])]]);
            }else{
                Flight::json(['code'=>403,'msg'=>"开通不成功,请联系管理员."]);
            }
        }else{
            $id = (int)Flight::request()->query->id;
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 网商云平台','url'=>url('manage/account/cloud')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['level']    = Flight::model('common/website')->level_find(['id' => $this->website['level']]);
            $tpl_date['bank']     = Flight::model('payment/bank')->bank_find(['user_id' => $this->login_user['user_id']]);
            $tpl_date['subshop']  = Flight::model('common/subshop')->info_find(['website_id' => $this->website['id'],'id' => $id]);
            if(empty($tpl_date['subshop'])){
                Flight::redirect(url('manage/account/subshop_reg'),302);
            }
            if($tpl_date['subshop']['state'] == 1){
                Flight::redirect(url('manage/account/subshop_info',['id' => $id]),302);
            }
            $this->display(0,$tpl_date,'layout');
        }
    } 

    //子站付费
    public function subshop_info(){
        $id = (int)Flight::request()->query->id;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 网商云平台','url'=>url('manage/account/cloud')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $subshop  = Flight::model('common/subshop')->info_find(['website_id' => $this->website['id'],'id' => $id,'state'=>1]);
        if(empty($subshop)){
            Flight::redirect(url('manage/account/subshop_order',['id' => $id]),302);
        }
        $tpl_date['info'] = Flight::model('common/website')->select_find(['website_reg_id'=>$id]);
        $this->display(0,$tpl_date,'layout');
    } 

    /**
     * 开通子站功能
     * ########################################################################
     */
    //云平台
    public function yunshop(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 网商云平台','url'=>url('manage/account/cloud')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $yunshop = Flight::model('user/yunshop')->select_find(['website_id' => $this->website['id']]);
        $deposit = [];
        if($yunshop){
            $deposit = Flight::model('user/yunshop')->deposit_find(['website_id' => $this->website['id'],'yunshop_id' =>$yunshop['id']]);
        }
        $tpl_date['yunshop'] = $yunshop;
        $tpl_date['deposit'] = $deposit;
        $this->display(0,$tpl_date,'layout');
    }

    //申请开通云平台供货商
    public function yunshop_reg(){
        if($this->isPost()){
            $rules = [
                'logo'     => ['empty','品牌(公司)LOGO必须输入'],
                'idimg'    => ['empty','证件图片必须输入'],
                'types'    => ['number','企业或个人选择'],
                'name'     => ['empty','名牌/企业简称必须输入'],
                'idname'   => ['empty','个人或公司名称必须输入'],
                'idcard'   => ['empty','证件号必须输入'],
                'telphone' => ['empty','联系电话必须输入'],
                'service'  => ['empty','服务电话必须输入'],
                'about'    => ['empty','商品品牌介绍必须输入'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['website_id'] = intval($this->website['id']);
            $data['logo']       = $request['logo'];
            $data['idimg']      = $request['idimg'];
            $data['types']      = $request['types'];
            $data['name']       = $request['name'];
            $data['idname']     = $request['idname'];
            $data['idcard']     = $request['idcard'];
            $data['telphone']   = $request['telphone'];
            $data['service']    = $request['service'];
            $data['about']      = $request['about'];
            $data['state']      = 0;
            //判断是否申请过
            $info  = Flight::model('user/yunshop')->select_find(['website_id' => $this->website['id']]);
            if($info){
                Flight::json(['code'=>403,'msg'=>'禁止重复申请']);
            }
            $result  = Flight::model('user/yunshop')->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/account/yunshop_reg')],'msg'=>'申请提交成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'申请提交失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 网商云平台','url'=>url('manage/account/cloud')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['sysconfig'] = Flight::model('common/website')->config_find();
            $result = Flight::model('user/yunshop')->select_find(['website_id' => $this->website['id']]);
            if($result){
                $deposit = Flight::model('user/yunshop')->deposit_find(['website_id' => $this->website['id'],'yunshop_id' =>$result['id']]);
                if($deposit['state'] == 1){
                    Flight::redirect(url('manage/account/yunshop'),302);
                }
                $tpl_date['bank']    = Flight::model('payment/bank')->bank_find(['user_id' => $this->login_user['user_id']]);
                $this->display('account'.DS.'yunshop_order',$tpl_date,'layout');
            }else{
                $this->display(0,$tpl_date,'layout');
            }
        }
    }

    //子站付费
    public function yunshop_order(){
        if($this->isPost()){
            //读取参数并过滤
            $rules = ['pay_type' => ['number','支付类型必须选择'],'pass' => ['empty','支付密码必须输入']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  = Flight::request()->data;
            $pay_type = intval($request['pay_type']);
            $pass     = Flight::filter()->filter_escape(trim($request['pass']));
            //读取申请资料
            $yunshop = Flight::model('user/yunshop')->select_find(['website_id' => (int)$this->website['id']]);
            if(empty($yunshop))  Flight::json(['code'=>403,'msg'=>"您还没有提交供应商资料"]);
            //判断支付密码
            $is_pay_pass = Flight::api('payment/bank')->is_pay($this->login_user['user_id'],$pass);
            if(!$is_pay_pass) Flight::json(['code'=>403,'msg'=>"支付密码输入错误"]);
            //读取应该扣多少钱
            $sysconfig = Flight::model('common/website')->config_find();
            $pay_data['website_id']  = (int)$this->website['id'];
            $pay_data['yunshop_id']  = $yunshop['id'];
            $pay_data['create_time'] = time();
            $pay_data['update_time'] = time();
            $pay_data['state']        = 1;
            if($sysconfig['cloud_money_deposit'] <= 0 || $sysconfig['cloud_money_buy'] <= 0 ){  //免费
                $pay_data['deposit_type'] = 1;
                $pay_data['money']        = 0;
                $pay_result = Flight::model('user/yunshop')->info_update(['yunshop_id' => $yunshop['id']],$pay_data);
            }else{
                if($pay_type){ //购买模式
                    $price = $sysconfig['cloud_money_buy'];
                    $msg = "支付开户费人民币: ￥{$price}";
                    $deposit_type = 1;
                }else{ //保证金模式
                    $price = $sysconfig['cloud_money_deposit'];
                    $msg = "支付保证金人民币: ￥{$price}";
                    $deposit_type = 0;
                }
                $msg = "合作伙伴站点ID:{$this->website['id']}《{$this->website['name']}》,{$msg},申请开通供应商";
                //扣除用户余额费用
                $is_pay = Flight::api('payment/bank')->money_down($this->login_user['user_id'],$price,$msg);
                if($is_pay){
                    $pay_data['deposit_type'] = $deposit_type;
                    $pay_data['money']        = $price;
                    $pay_result = Flight::model('user/yunshop')->info_update(['yunshop_id' => $yunshop['id']],$pay_data);
                }else{
                    Flight::json(['code'=>403,'msg'=>"扣款失败,余额不足请重置.",'data'=>['url'=>url('manage/bankroll/recharge')]]);
                }
            }
            if($pay_result){
                Flight::api('common/sys')->log($this->login_user['user_id'],$msg);  //日志
                Flight::json(['code'=>200,'msg'=>"申请成功,请等待审核通过.",'data'=>['url'=>url("manage/account/yunshop")]]);
            }else{
                Flight::json(['code'=>403,'msg'=>"申请不成功,请联系管理员."]);
            }
        }
    } 
}