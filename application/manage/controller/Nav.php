<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        网站管理-导航管理
 */
namespace application\manage\controller;
use framework\Flight;

class Nav extends Common{

    private $special;
    private $nav;

    public function __construct() {
        parent::__construct();
        $this->web = Flight::model('common/website');
        $this->nav = Flight::model('article/nav');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('article'));  //引入管理菜单
    }

    //列表
    public function index(){
        $tpl_date['parent_id']  = (int)Flight::request()->query->parent_id;
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 导航管理','url'=>url('manage/nav/index')];
        $path = $this->nav->website_select_path($tpl_date['parent_id']);
        foreach ($path as $key => $value) {
            $pathMaps[] = $value;
        }
        $editMenu = [];
        if($tpl_date['parent_id']){ 
            $editMenu[] = [
                'name' =>'<i class="iconfont icon-jiahao"></i> 添加导航',
                'url'  => url('manage/nav/edit',['parent_id' => $tpl_date['parent_id']])
            ];
        }
        //菜单结束
        $tpl_date['channel']   = $this->nav->website_select_list($tpl_date['parent_id'],$this->website['id']);
        $tpl_date['pathMaps']  = json_encode($pathMaps);
        $tpl_date['editMenu']  = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'sort' => ['empty'],
                'name' => ['empty'],
                'url'  => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']         = intval($request['id']);
            $data['parent_id']  = intval($request['parent_id']);
            $data['website_id'] = (int)$this->website['id'];
            $data['sort']       = Flight::filter()->filter_escape(trim($request['sort']));
            $data['name']       = Flight::filter()->filter_escape(trim($request['name']));
            $data['img']        = Flight::filter()->filter_escape(trim($request['img']));
            $data['url']        = Flight::filter()->filter_escape(trim($request['url']));
            $data['extend']     = Flight::filter()->filter_escape(trim($request['extend']));
            if($data['id']){
                $info = $this->nav->select_find($data['id']);
                if($info['website_id'] != $data['website_id']) Flight::json(['code'=>403,'msg'=>'你没有操作当前导航的权限']);
            }
            $result =  $this->nav->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/nav/index',['parent_id'=>$data['parent_id']])],'msg'=>'导航操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'导航操作失败']);
            }
        }else{
            $parent_id      = (int)Flight::request()->query->parent_id;
            $tpl_date['id'] = (int)Flight::request()->query->id;
            $info           = $this->nav->select_find($tpl_date['id']);
            if($info){
                if(!$info['website_id'] || ($info['website_id'] <> $this->website['id'])){
                    $this->ErrorMsg('友情提示','你没有操作当前导航的权限');
                }
            }
            $tpl_date['parent_id']  = $parent_id  ? $parent_id : (int)$info['parent_id'];
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 导航管理','url'=>'javascript:;'];
            $path = $this->nav->website_select_path($tpl_date['parent_id']);
            foreach ($path as $key => $value) {
                $pathMaps[] = $value;
            }
            $editMenu = [];
            //菜单结束
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['info']     = $info;
            $this->display(0,$tpl_date,'layout');
        }
    }

    //删除
    public function delete(){
        $param['id']         = (int)Flight::request()->query->id;
        $param['website_id'] = $this->website['id'];
        $result =  $this->nav->info_delete($param);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含子导航或内容']);
        } 
    }
    
    //字段排序
    public function neworder(){
        $data['sort'] = (int)Flight::request()->data['sort'];
        $data['id']   = (int)Flight::request()->data['id'];
        $result = $this->nav->sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }

    /**
     *读取导航列表返回数据
     */
    public function ids() {
        $channel_id  = (int)Flight::request()->query->channel_id;
        $website_id  = (int)Flight::request()->query->website_id;
        $info = $this->nav->select_lists_ids($channel_id,$website_id);
        Flight::json($info);
    }    
}