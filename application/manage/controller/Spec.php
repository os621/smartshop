<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品规格
 */
namespace application\manage\controller;
use framework\Flight;

class Spec extends Common{

    private $spec;

    public function __construct() {
        parent::__construct();
        $this->spec = Flight::model('goods/spec');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
        Flight::api('manage/infomessage')->is_add_goods($this->website['id']);
    }

    //商品规格
    public function index(){
        //菜单开始
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 商品规格','url'=>'javascript:;'];
        $editMenu[] = ['name'=>'<i class="iconfont icon-jiahao"></i> 添加规格','url'=>url('manage/spec/edit')];
        //菜单结束    
        $tpl_date['lists']    = $this->spec->select_list(['website_id' => $this->website['id']]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }

    //规格编辑
    public function edit(){
        if($this->isPost()){
            $rules = [
                'name' => ['empty','商品规格名称必须填写'],
                'sort' => ['type','排序必须填写','INT'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request    =  Flight::request()->data;
            $id         = (int)$request['id'];
            $website_id = intval($this->website['id']);
            //写入规格名称
            $spec_data['name'] = Flight::filter()->filter_escape(trim($request['name']));
            $spec_data['sort'] = Flight::filter()->filter_escape(trim($request['sort']));
            $spec_data['note'] = Flight::filter()->filter_escape(trim($request['note']));
            $spec_data['website_id'] = $website_id;
            $lastid = $this->spec->info_edit($spec_data,$id,$website_id);
            if(!$lastid) exit(Flight::json(['code'=>403,'msg'=>' 商品规格创建或编辑失败']));
            $lastid = $id ? $id : $lastid;
            //处理规格参数
            $values    = $request['values'];
            $values_id = $request['values_id'];
            if(is_array($values_id)){
                //删除不存在的ID
                $values_ids = implode(',',array_filter($values_id));
                if($values_ids != ''){
                    $this->spec->spec_value_delete(['spec_id = '.$lastid.' and id not in('.$values_ids.')']);
                }
                //判断新增还是修改
                $spec_value_data['spec_id'] = $lastid;
                $spec_value_data['sort'] = 0;
                foreach ($values_id as $key => $value) {
                    $spec_value_data['name'] = $values[$key];
                    if(empty($value) || !$value){ //新增
                        $this->spec->spec_value_edit($spec_value_data);
                    }else{ //修改
                        $this->spec->spec_value_edit($spec_value_data,['id' =>$value]);
                    }
                }
                $spec_values = $this->spec->spec_list(['spec_id' =>$lastid]);
                $spec_len = count($spec_values);
                $this->spec->info_edit(['spec_value'=>json_encode($spec_values),'counts' => $spec_len],$lastid);
            }else{
                $this->spec->spec_value_delete(['spec_id' =>$lastid]);
            }
            Flight::json(['code'=>200,'data'=>['url'=>url('manage/spec/index')],'msg'=>' 商品规格创建或编辑成功']);
        }else{
            $id          = (int)Flight::request()->query->id;
            //菜单开始
            $pathMaps = [
                ['name' =>'<i class="iconfont icon-shouye"></i> 商品规格','url'=>url('manage/spec/index')],
                ['name' =>'&nbsp;>&nbsp;添加/编辑','url'=>'javascript:;']
            ];
            //菜单结束
            $tpl_date['pathMaps']    = json_encode($pathMaps);
            $tpl_date['info']        = $this->spec->select_find(['id'=>$id,'website_id' => $this->website['id']]);
            if($tpl_date['info']) $tpl_date['spec_values'] = $this->spec->spec_list(['spec_id' =>$id]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    //规格删除
    public function delete(){
        $id = intval(Flight::request()->query->id);
        $result =  $this->spec->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含规格属性']);
        } 
    }

    //属性排序
    public function sort(){
        $data['sort'] = intval(Flight::request()->data['sort']);
        $data['id']   = intval(Flight::request()->data['id']);
        $result = $this->spec->info_sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }
}