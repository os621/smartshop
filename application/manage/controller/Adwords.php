<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * 广告位
 */
namespace application\manage\controller;
use framework\Flight;

class Adwords extends Common{

    private $position;
    private $web;
    private $advert;

    public function __construct() {
        parent::__construct();
        $this->position = Flight::model('adwords/position');
        $this->advert   = Flight::model('adwords/advert');
        $this->web      = Flight::model('common/website');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('article'));  //引入管理菜单
    }

    /**
     * 列表
     * @access public
     */
    public function index(){
        $website_id = (int)Flight::request()->query->website_id;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 广告位','url'=>url('manage/adwords/index')];
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 添加广告位','url'=>"javascript:openwin('".url('manage/adwords/edit')."')"];
        $page = (int)Flight::request()->query->page;
        $tpl_date['position'] = $this->position->select_list($website_id,$page);
        $tpl_date['pager']    = $this->getPage($this->position->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0,$tpl_date,'layout');
    }
    
    /**
     * 添加权限节点
     * @access public
     */
    public function edit() {
        if($this->isPost()){
            $rules = [
                'title'    => ['empty'],
                'contents' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['id']         = (int)$request['id'];
            $data['title']      = Flight::filter()->filter_escape(trim($request['title']));
            $data['website_id'] = (int)$this->website['id'];
            $data['contents']   = Flight::filter()->filter_escape($request['contents']);
            if($data['id']){
                $info = $this->position->info_find($data['id']);
                if($info['website_id'] != $data['website_id']) Flight::json(['code'=>403,'msg'=>'你没有操作当前广告位的权限']);
            }
            $result  = $this->position->info_edit($data);
            if($result){
                Flight::json(['code'=>302,'data'=>['url'=>url('manage/adwords/index')],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $id = (int)Flight::request()->query->id;
            $info  = $this->position->info_find($id);
            if($info){
                if(!$info['website_id'] || $info['website_id'] <> $this->website['id']){
                    $this->ErrorMsg('友情提示','你没有操作当前广告位的权限');
                }
            }
            $tpl_date['info'] = $info;
            $this->display(0,$tpl_date,'meta');
        }
    } 

    //删除
    public function delete(){
        $id         = (int)Flight::request()->query->id;
        $website_id = (int)$this->website['id'];
        $info       = $this->advert->where(['position_id' => $id])->find(); 
        if($info){
            exit(Flight::json(['code'=>403,'msg'=>'删除失败,请先删除包含的广告资源']));
        }
        $result =  $this->position->info_delete(['id' => $id,'website_id' => $website_id]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }

    /**
     * 广告列表
     * @access public
     */
    public function advert() {
        $position_id = (int)Flight::request()->query->position_id;
        $info  = $this->position->info_find($position_id);
        $pathMaps[] = ['name' => '<i class="iconfont icon-home"></i> 广告位', 'url' => url('manage/adwords/index')];
        $pathMaps[] = ['name' => '&nbsp;>&nbsp;'.$info['title'],'url' => url('manage/adwords/advert',['position_id' =>$position_id])];
        $editMenu[] = [
            'name' => '<i class="iconfont icon-cjsqm"></i> 添加广告',
            'url'  => url('manage/adwords/advert_edit',['position_id'=>$position_id])
        ];
        $tpl_date['advert']   = $this->advert->select_list(['position_id' => $position_id,'website_id'=>$this->website['id']]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $this->display(0, $tpl_date, 'layout');
    }

    /**
     * 添加广告列表
     * @access public
     */
    public function advert_edit() {
        if($this->isPost()){
            $rules = [
                'position_id' => ['empty'],
                'name'        => ['empty'],
                'picture'     => ['empty','请上传你的广告位图片'],
                'link'        => ['empty'],
                'contents'    => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            
            $request =  Flight::request()->data;
            $data['id']          = (int)$request['id'];
            $data['position_id'] = (int)$request['position_id'];
            $data['name']        = Flight::filter()->filter_escape(trim($request['name']));
            $data['website_id']  = (int)$this->website['id'];
            $data['picture']     = Flight::filter()->filter_escape(trim($request['picture']));
            $data['link']        = Flight::filter()->filter_escape(trim($request['link']));
            $data['contents']    = Flight::filter()->filter_escape(trim($request['contents']));

            $result  = $this->advert->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/adwords/advert',['position_id' => $request['position_id']])],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $id = (int)Flight::request()->query->id;
            if($id){
                $tpl_date['info'] = $this->advert->info_find($id);
                $position_id = $tpl_date['info']['position_id'];
            }else{
                $position_id = (int)Flight::request()->query->position_id;
            }
            if(!$position_id){
                $this->ErrorMsg('友情提示','广告位不存在');
            }
            $position = $this->position->info_find($position_id);
            if(!$position){
                $this->ErrorMsg('友情提示','广告位不存在');
            }
            //判断是不是自己的广告位
            if($position['website_id']){
                if($position['website_id'] != $this->website['id']){
                    $this->ErrorMsg('友情提示','广告位不存在');
                }
            }
            $pathMaps[] = ['name' => '<i class="iconfont icon-home"></i> 广告位', 'url' => url('manage/adwords/index')];
            $pathMaps[] = ['name' => '&nbsp;>&nbsp;'.$position['title'], 'url' => url('manage/adwords/index',['position_id' =>$position_id])];
            $tpl_date['pathMaps']    = json_encode($pathMaps);
            $tpl_date['position_id'] = $position_id;
            $this->display(0, $tpl_date,'layout');
        }
    } 
    
    /**
     * 删除广告列表
     * @access public
     */
    public function advert_delete() {
        $param['id']         = (int)Flight::request()->query->id;
        $param['website_id'] = (int)$this->website['id'];
        $result =  $this->advert->info_delete($param);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }
}