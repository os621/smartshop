<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-云货源
 */
namespace application\manage\controller;
use framework\Flight;

class Website extends Common{

    private $web;

    public function __construct() {
        parent::__construct();
        $this->web = Flight::model('common/website');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('account'));  //引入管理菜单
    }

    //站点配置
    public function config(){
        if($this->isPost()){
            $rules = [
                'title'       => ['empty'],
                'logo'        => ['empty'],
                'themes'      => ['empty'],
                'keywords'    => ['empty'],
                'description' => ['empty'],
                'contacts'    => ['empty'],
                'address'     => ['empty']
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['id']          = (int)$this->website['id'];
            $data['user_id']     = (int)$this->login_user['user_id'];
            $data['title']       = Flight::filter()->filter_escape(trim($request['title']));
            $data['logo']        = Flight::filter()->filter_escape(trim($request['logo']));
            $data['themes']      = Flight::filter()->filter_escape(trim($request['themes']));
            $data['keywords']    = Flight::filter()->filter_escape(trim($request['keywords']));
            $data['description'] = Flight::filter()->filter_escape(trim($request['description'])); 
            $data['contacts']    = Flight::filter()->filter_escape(trim($request['contacts']));
            $data['address']     = Flight::filter()->filter_escape(trim($request['address'])); 
            $result =  $this->web->website_config($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/website/config')],'msg'=>'修改成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'修改失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 站点配置','url'=>url('manage/account/cloud')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['info']     = $this->web->select_find(['id'=>$this->website['id'],'user_id'=>$this->login_user['user_id']]);
            $this->display(0,$tpl_date,'layout');
        }
    }

    /**
     * 支付设置
     */
    public function payconfig(){
        Flight::api('manage/infomessage')->is_open_pay($this->website['id']); 
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 支付配置','url'=>url('manage/website/payconfig')];
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 增加支付方式','url'=>url('manage/website/payconfig_add')];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['list']     = Flight::model('common/payconfig')->info_list(['website_id' => $this->website['id']]);
        $this->display(0,$tpl_date,'layout');
    }

    /**
     * 支付的支付方式
     */
    public function payconfig_add(){
        Flight::api('manage/infomessage')->is_open_pay($this->website['id']); 
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 支付配置','url'=>url('manage/website/payconfig')];
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;支付的支付方式','url'=>'javascript:;'];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['config']   = Flight::model('common/payconfig')->config_list(['state' => 1]);
        $this->display(0,$tpl_date,'layout');
    }

    /**
     * 支付方式配置与增加
     */
    public function payconfig_edit(){
        Flight::api('manage/infomessage')->is_open_pay($this->website['id']); 
        //S 菜单
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 支付配置','url'=>url('manage/website/payconfig')];
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;支付的支付方式','url'=>url('manage/website/payconfig_add')];
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;配置支付接口信息','url'=>'javascript:;'];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        //E 菜单
        $plugin_id = (int)Flight::request()->query->plugin_id;
        $id        = (int)Flight::request()->query->id;
        //增加
        if($plugin_id){
            $condition['website_id'] = $this->website['id'];
            $condition['plugin_id']  = $plugin_id;
            $is_pay = Flight::model('common/payconfig')->info_find($condition);
            if($is_pay){
                $tpl_date['config'] = $is_pay;
                $tpl_date['config_key'] = json_decode($is_pay['config'],true) ;
                $plugin_id = $is_pay['plugin_id'];
            }else{
                $config = Flight::model('common/payconfig')->config_find($plugin_id);
                if(!$config || !$config['state']) {
                    $tpl_date['msg_info']  = '没有找到支付接口'; 
                    Flight::api('manage/infomessage')->message($tpl_date);
                }
                $tpl_date['config']   = $config;
            }
        }else{
            $condition['website_id'] = $this->website['id'];
            $condition['id']  = $id;
            $config = Flight::model('common/payconfig')->info_find($condition);
            if (!$config) {
                $tpl_date['msg_info']  = '没有找到支付接口'; 
                Flight::api('manage/infomessage')->message($tpl_date);
            }
            $tpl_date['config'] = $config;
            $tpl_date['config_key'] = json_decode($config['config'],true) ;
            $plugin_id = $config['plugin_id'];
        }
        if($plugin_id == 3 || $plugin_id == 4){
            $tpl = 'website/payconfig_edit_alipay';
        }else if($plugin_id == 5){
            $tpl = 'website/payconfig_edit_weixin';
        }else{
            $tpl = 0;
        }
        $tpl_date['plugin_id']  = $plugin_id;
        $this->display($tpl,$tpl_date,'layout');
    }
    /**
     * 保存支付信息
     */
    public function payconfig_save(){
        Flight::api('manage/infomessage')->is_open_pay($this->website['id']); 
        if ($this->isPost()) {
            $rules = [
                'payname'     => ['empty'],
                'app_id'      => ['empty'],
                'plugin_id'   => ['empty'],
                'public_key'  => ['empty'],
                'private_key' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if ($rel['code'] == 500) {
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request   =  Flight::request()->data;
            $plugin_id = intval($request['plugin_id']);
            $is_pay = Flight::model('common/payconfig')->config_find($plugin_id);
            if(!$is_pay || !$is_pay['state']) {
                Flight::json(['code'=>500,'msg'=>'没有找到允许配置的支付接口']);
            }
            $data['id']          = intval($request['id']);
            $data['website_id']  = intval($this->website['id']);
            $data['plugin_id']   = $plugin_id;
            $data['pay_name']    = Flight::filter()->filter_escape(trim($request['payname']));
            $data['client_type'] = intval($request['client_type']);
            $data['sort']        = intval($request['sort']);
            $data['status']      = intval($request['status']);
            //保存数据规格
            if($plugin_id == 3 || $plugin_id == 4){ //支付宝
                $config['app_id']      = Flight::filter()->filter_escape(trim($request['app_id']));
                $config['public_key']  = Flight::filter()->filter_escape(trim($request['public_key']));
                $config['private_key'] = Flight::filter()->filter_escape(trim($request['private_key']));
            }else if($plugin_id == 5){ //微信
                $config['app_id']      = Flight::filter()->filter_escape(trim($request['app_id']));
                $config['public_key']  = Flight::filter()->filter_escape(trim($request['public_key']));
                $config['private_key'] = Flight::filter()->filter_escape(trim($request['private_key']));
            }else{
                $config['app_id']  = trim($request['app_id']);
            }
            $data['config'] = json_encode($config);
            $result =  Flight::model('common/payconfig')->info_edit($data);
            if ($result) {
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/website/payconfig')],'msg'=>'编辑成功']);
            } else {
                Flight::json(['code'=>500,'msg'=>'编辑不成功']);
            }
        }
    }

    /**
     * 删除支付接口
     */
    public function payconfig_delete(){
        $id = Flight::request()->query->id;
        $result =  Flight::model('common/payconfig')->info_delete(['id' => $id,'website_id' => $this->website['id']]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }

    /**
     * 运费模板
     */
    public function fareconfig(){
        if($this->isPost()){
            $rules = ['first_weight'  => ['empty'],'first_price'   => ['empty'],'second_weight' => ['empty'],'second_price'  => ['empty'],];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['name']          = trim($request['name']);
            $data['website_id']    = $this->website['id'];
            $data['first_weight']  = trim($request['first_weight']);
            $data['first_price']   = trim($request['first_price']);
            $data['second_weight'] = trim($request['second_weight']);
            $data['second_price']  = trim($request['second_price']);
            $data['update_time']   = time();
            $result = Flight::model('goods/fare')->info_edit($data,$this->website['id']);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/website/fareconfig')],'msg'=>'编辑成功.']);
            }else{
                Flight::json(['code'=>500,'msg'=>'编辑失败.']);
            }
        }else{
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i> 运费设置','url'=>'javascript:;'];
            $editMenu = [];
            //菜单结束
            $tpl_date['config'] = Flight::model('goods/fare')->info_find(['website_id' => $this->website['id']]);
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $tpl_date['editMenu'] = json_encode($editMenu);
            $this->display(0,$tpl_date,'layout');
        }
    }

    /**
     * 短信接口设置
     */
    public function smsconfig(){
        if($this->isPost()){
            $rules = [
                'keyid'       => ['empty','KeyID不能为空'],
                'secret'      => ['empty','secret不能为空'],
                'sign_name'   => ['empty','短信签名不能为空'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 500){
                exit(Flight::json(['code'=>500,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $data['keyid']      = Flight::filter()->filter_escape(trim($request['keyid']));
            $data['secret']     = Flight::filter()->filter_escape(trim($request['secret']));
            $data['sign_name']  = Flight::filter()->filter_escape(trim($request['sign_name']));
            $data['website_id'] = $this->website['id'];
            $sms_tpl_id = [];
            if(!empty($request['sms'])){
                foreach ($request['sms'] as $key => $value) {
                    $sms_tpl_id[$key] = Flight::filter()->filter_escape(trim($value));
                }
            }
            $data['sms_id'] = json_encode($sms_tpl_id);
            $result = Flight::model('common/website')->sms_edit($data,$this->website['id']);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/website/smsconfig')],'msg'=>'修改成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'修改失败']);
            }
        }else{
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 阿里大于短信接口配置:','url'=>url('manage/website/smsconfig')];
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $config   = Flight::model('common/website')->sms_find(['website_id'=>$this->website['id']]);
            if($config){
                $tpl_date['config'] = $config;
                $tpl_date['sms'] = json_decode($config['sms_id'],true);
            }
            $this->display(0,$tpl_date,'layout');
        }
    }

    /**
     * 主题选择
     */
    public function skin(){
        if($this->isPost()){
            $id  =  Flight::request()->data['id'];
            $themes = Flight::model('common/website')->themes_find(['id' => $id]);
            if(empty($themes['themes'])){
                Flight::json(['code'=>500,'msg'=>'主题目录异常~请联系管理员']);
            }
            $data['themes'] = $themes['themes'];
            $result =  Flight::model('common/website')->data($data)->where(['id'=>(int)$this->website['id']])->update();
            if($result){
                Flight::cookie()->del($this->website_id); //清空当前站点Cookie
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/website/skin',['status'=>1])],'msg'=>'修改成功']);
            }else{
                Flight::json(['code'=>500,'msg'=>'修改主题失败']);
            }
        }else{
            $status = (int)Flight::request()->query->status;
            $page   = (int)Flight::request()->query->page;
            $button[$status] = 'button-blue';
            //菜单开始
            $pathMaps = [
                ['name'=>'<span class="pure-button '.$button['0'].'"><i class="iconfont icon-shouye"></i> 所有主题</span>&nbsp;','url'=>url('manage/website/skin')],
                ['name'=>'<span class="pure-button '.$button['1'].'"><i class="iconfont icon-xiangshang3"></i> 我的主题</span>&nbsp;','url'=>url('manage/website/skin',['status' =>1])],
            ];
            if($status){
                $condition['themes'] = $this->website['themes'];
                $tpl_date['themes']  = Flight::model('common/website')->themes_list($condition);
            }else{
                $level = (int)$this->website['level'];
                $condition[] = "(level <= {$level} and is_below = 0 and website_id != {$this->website['id']}) or (level = {$level} and is_below = 1 and website_id != {$this->website['id']}) or website_id = {$this->website['id']}";  //
                $tpl_date['themes'] = Flight::model('common/website')->themes_list_page($condition,$page);
            }
            $tpl_date['pathMaps'] = json_encode($pathMaps);
            $this->display(0,$tpl_date,'layout');
        }
    }
}