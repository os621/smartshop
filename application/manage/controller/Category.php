<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商家管理-栏目管理
 */
namespace application\manage\controller;
use framework\Flight;

class Category extends Common{

    private $websiteMode;
    private $categoryMode;
    private $manage_category;

    public function __construct() {
        parent::__construct();
        $this->websiteMode     = Flight::model('common/website');
        $this->categoryMode    = Flight::model('goods/category');
        $this->manage_category = Flight::model('category');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
        Flight::api('manage/infomessage')->is_add_goods($this->website['id']);
    }

    //管理首页
    public function index(){
        $parent_id = (int)Flight::request()->query->parent_id;
        $page      = (int)Flight::request()->query->page;
        //菜单开始
        $pathMaps = $this->manage_category->select_path($parent_id);
        $tpl_date['pathLen']  = count($pathMaps);
        $editMenu[] = [
            'name' =>'<i class="iconfont icon-copy"></i> 同步平台分类',
            'url'  =>url('manage/category/getcate',['parent_id' => $parent_id])
        ];
        if($tpl_date['pathLen'] <= 3){
            $editMenu[] = [
                'name' =>'<i class="iconfont icon-jiahao"></i> 添加商品分类',
                'url'  =>url('manage/category/edit',['parent_id' => $parent_id])
            ];
        }
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        //菜单结束 
        $condition['parent_id']  = $parent_id;
        $condition['website_id'] = $this->website['id'];
        $condition['user_id']    = $this->website['user_id'];
        $tpl_date['category']    = $this->categoryMode->select_list($condition,$page);
        $tpl_date['pager']       = $this->getPage($this->categoryMode->pager,['parent_id' => $parent_id]);
        $tpl_date['parent_id']   = $parent_id;
        $this->display(0,$tpl_date,'layout');
    }
    //添加编辑
    public function edit(){
        if($this->isPost()){
            $rules = ['title' => ['empty'],'name' => ['empty']];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            Flight::filter()->filter();
            $request  =  Flight::request()->data;
            $data['id']                = (int)$request['id'];
            $data['parent_id']         = (int)$request['parent_id'];
            $data['is_open_authorize'] = (int)$request['is_open_authorize'];
            $data['title']             = Flight::filter()->filter_escape($request['title']);
            $data['name']              = Flight::filter()->filter_escape($request['name']);
            $data['icon']              = Flight::filter()->filter_escape($request['icon']);
            $data['website_id']        = (int)$this->website['id'];
            $data['user_id']           = (int)$this->website['user_id'];
            $data['get_root_cate_id']  = 0;
            $result =  $this->manage_category->edit($data);
            if($result){
                Flight::json(['code'=>302,'data'=>['url'=>url('manage/category/index',['parent_id'=>$data['parent_id']])],'msg'=>'商品分类创建或编辑成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'商品分类创建或编辑不成功']);
            }
        }else{
            $tpl_date['id']        = (int)Flight::request()->query->id;
            $tpl_date['parent_id'] = (int)Flight::request()->query->parent_id;
            $tpl_date['info']      = $this->categoryMode->select_find($tpl_date['id']);
            if($tpl_date['id']) $tpl_date['parent_id'] = $tpl_date['info']['parent_id'];
            $pathMaps       = $this->manage_category->select_path($tpl_date['parent_id']);
            $websiet_config = $this->websiteMode->select_config_find($this->website['id']);
            $tpl_date['pathMaps']           = json_encode($pathMaps);
            $tpl_date['is_goods_authorize'] = $websiet_config['is_goods_authorize'];
            $this->display(0,$tpl_date,'layout');
        }
    }   

    //删除我的分类
    public function delete(){
        $id = (int)Flight::request()->query->id;
        $info =  $this->categoryMode->where(['parent_id' => $id])->find();
        if($info){
            exit(Flight::json(['code'=>403,'msg'=>'删除失败,请查看是否包含子栏目']));
        }
        $goods =  Flight::model('goods/item')->where(['category_id' => $id])->find();
        if($goods){
            exit(Flight::json(['code'=>403,'msg'=>'删除失败,栏目中还包含商品']));
        }
        $condition['website_id'] = (int)$this->website['id'];
        $condition['user_id']    = (int)$this->website['user_id'];
        $condition['id']         = $id;
        $result =  $this->categoryMode->where($condition)->delete();
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }

    //弹出式模板选择窗口
    public function popup(){
        $selected  = Flight::request()->query->selected;
        $tpl_date['input'] = Flight::request()->query->input;
        $tpl_date['special_id'] = Flight::request()->query->special_id;  //解决前台关联添加特性商品筛选问题
        if($selected == 1){
            $this->display('category/selected', $tpl_date, 'meta');
        }elseif($selected == 2){
            $this->display('category/goodsspecial', $tpl_date, 'meta');
        }else{
            $this->display(0, $tpl_date, 'meta');
        }
    }


    //联动菜单
    public function selected(){
        $parent_id  = Flight::request()->query->parent_id;
        $website_id = (int)$this->website['id'];
        $info = $this->categoryMode->select_list_all(['parent_id' => $parent_id,'website_id' => $website_id],'id,title');
        if($info){
            Flight::json(['code'=>200,'msg'=>'成功','data'=>$info]);
        }
        Flight::json(['code'=>403,'msg'=>'读取商品分类列表失败']);
    }

    //当前路径
    public function selected_path(){
        $cid  = (int)Flight::request()->query->cid;
        $info = $this->categoryMode->getPath($cid);
        if($info){
            $len = count($info);
            $category = [];
            foreach ($info as $key => $value) {
                $category[] = $value['id'];
            }
            $category_id = implode(',',$category);
            Flight::json(['code'=>200,'msg'=>'成功','data'=>$info,'path' => $len,'category_id' => $category_id]);
        }
        Flight::json(['code'=>403,'msg'=>'读取商品分类路径失败']);
    } 

    //字段排序
    public function sort(){
        $data['sort'] = (int)Flight::request()->data['sort'];
        $data['id']   = (int)Flight::request()->data['id'];
        $result = $this->categoryMode->info_sort($data);
        if($result){
            Flight::json(['code'=>200,'msg'=>'自定义排序成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'自定义排序失败']);
        }
    }

    //同步主站分类
    public function getcate(){
        $parent_id = (int)Flight::request()->query->parent_id;
        $page      = (int)Flight::request()->query->page;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-shouye"></i>&nbsp;商品分类&nbsp;','url'=>url('manage/category/index')];
        $pathMaps[] = ['name'=>' >&nbsp;同步平台分类','url'=> 'javascript:;'];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        //菜单结束
        if(!$this->website['root_id']){
            $tpl_date['msg_info']  = '主站不需要同步目录,请自行添加。';         
            Flight::api('manage/infomessage')->message($tpl_date); 
        }
        $condition['parent_id']  = $parent_id;
        $condition['website_id'] = $this->website['root_id'];
        $tpl_date['category']    = $this->categoryMode->select_list($condition,$page);
        $tpl_date['pager']       = $this->getPage($this->categoryMode->pager,['parent_id' => $parent_id]);
        $tpl_date['parent_id']   = $parent_id;
        $this->display(0,$tpl_date,'layout');
    }

    //立即同步
    public function putcate(){
        if($this->isPost()){
            if(!$this->website['root_id']){       
                Flight::json(['code'=>403,'msg'=>'主站不需要同步目录,请自行添加.']);
            }
            $request  =  Flight::request()->data;
            $ids_str = Flight::request()->data['ids'];
            if(!$ids_str){
                Flight::json(['code'=>403,'msg'=>'没有选择任何要操作商品']);
            }
            //查主站所有分类
            $condition['website_id'] = $this->website['root_id']; 
            $category = self::getcate_ids(0,$condition);
            if(empty($category)) Flight::json(['code'=>403,'msg'=>'同步失败~查找主站分类失败']);
            //查我站的所有分类
            $result = $this->categoryMode->select_list_all(['website_id' => $this->website['id'],'get_root_cate_id != 0'],'get_root_cate_id');
            $root_cate_id = [];
            if(!empty($result)){
                foreach ($result as $key => $value) {
                    $root_cate_id[] = $value['get_root_cate_id'];
                }
            }
            $ids_ary = explode(',',trim($ids_str,','));
            foreach ($ids_ary as $key => $root_id) {
                self::cate($category,$root_id);
            }
            Flight::json(['code'=>200,'msg'=>'商品分类同步成功','data'=>['url' => url('manage/category/index')]]);
        }
    }

    /**
     * [cate 同步分类]
     * @param  array   $category [要同步的所有分类]
     * @param  [type]  $root_id  [当前同步的根分类ID]
     * @param  integer $cate_id  [查找子分类的数组键位]
     * @param  integer $rid      [最后一次添加的分类ID]
     * @return [boolen]          [成功]
     */
    protected function cate(array $category,$root_id,$cate_id = 0,$rid = 0){
        if(!empty($category[$cate_id])){
            foreach ($category[$cate_id] as $key => $value) {
                if($cate_id == 0){
                    if($value['id'] == $root_id){
                        $rel = self::savacate($value,$rid);
                    }else{
                        $rel = false;
                    }
                }else{
                    $rel = self::savacate($value,$rid);
                }
                if($rel) self::cate($category,$root_id,$value['id'],$rel);
            }
        }
        return true;
    }

    /**
     * [savacate 保存通知分类]
     * @param  [array] $value [保存的数据]
     * @param  [intar] $rid   [最后一次添加的ID]
     * @return [array]        [添加是否成功]
     */
    protected function savacate($value,$rid){
        $data['get_root_cate_id']  = $value['id'];
        $data['parent_id']         = $rid;
        $data['is_open_authorize'] = 0;
        $data['title']             = $value['title'];
        $data['name']              = $value['name'];
        $data['icon']              = $value['icon'];
        $data['website_id']        = $this->website['id'];
        $data['user_id']           = $this->website['user_id'];
        return $this->manage_category->edit($data);  
    }

    /**
     * [getcate_ids 获取商品ID分组]
     * @param  integer $parent_id [从第几层开始查找]
     * @param  [array] $condition [查询条件]
     * @return [array]            [返回查询数组]
     */
    protected function getcate_ids($parent_id = 0,$condition = []){
        $result = $this->categoryMode->select_list_all($condition);
        $tree =  new \extend\Category(array('id','parent_id','name','title'));
        return $tree->getArray($parent_id,$result);
    }
}