<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        商品专栏管理
 */
namespace application\manage\controller;
use framework\Flight;
use extend\Category as ctg;

class Goodsspecial extends Common{

    private $special;

    public function __construct() {
        parent::__construct();
        $this->special = Flight::model('manage/special');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('goods'));  //引入管理菜单
        Flight::api('manage/infomessage')->is_open_authorize($this->website['id']);
    }

    //特性与专栏
    public function index(){
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 特性与专栏','url'=>url('manage/cloud/index')];
        $condition[] = "(website_id = ".intval($this->website['id'])." or website_id = 0)";
        $tpl_date['special']  = $this->special->select_list($condition);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }

    //商品列表
    public function item(){
        $id   = intval(Flight::request()->query->id);
        $page = intval(Flight::request()->query->page);
        $info = $this->special->select_find(['id' => $id]);
        //S 菜单
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 特性与专栏','url'=>url('manage/goodsspecial/index')];
        if(!$info){
            $tpl_date['msg_info']  = '当前特性与专栏不存在。'; 
            $tpl_date['pathMaps'] = json_encode($pathMaps);           
            Flight::api('manage/infomessage')->message($tpl_date);   
        }
        $pathMaps[] = ['name'=>'&nbsp;>&nbsp;'.$info['name'],'url'=>'javascript:;']; 
        $editMenu[] = ['name'=>'<i class="iconfont icon-xiangshang3"></i> 添加特性与专栏商品','url'=>"javascript:select_category('".url('manage/goods/special',['special_id' => $info['id']])."')"];
        //E 菜单
        $condition[] = "A.website_id = ".$this->website['id'];
        $condition['special_id'] = $id;
        $tpl_date['item']     = $this->special->select_spu_list($condition,$page);
        $tpl_date['pager']    = $this->getPage($this->special->pager,['id'=>$id]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['info']     = $info;
        $this->display(0,$tpl_date,'layout');
    }

    //专栏商品确认
    public function ids_special(){
        if($this->isPost()){
            $special_id  =  intval(Flight::request()->data['special_id']);
            if(!$special_id){
                Flight::json(['code'=>403,'msg'=>'没有选择商品的特性与专栏']);
            }
            $ids_str = Flight::request()->data['ids'];
            if($ids_str){
                $ids_ary = explode(',',trim($ids_str,','));
                $id_array = [];
                foreach ($ids_ary as $key => $value) {
                    $id_array[] = (int)$value;
                }
                $data['ids']        = $id_array;
                $data['special_id'] = $special_id;
                $data['website_id'] = $this->website['id'];
                $this->special->spu_all_action($data);
                Flight::json(['code'=>200,'msg'=>'添加专栏商品成功,请可以继续添加。']);
            }else{
                Flight::json(['code'=>403,'msg'=>'为选择任何要操作商品']);
            }
        }
        Flight::notFound();
    }

    //删除进入回收站
    public function delete(){
        if($this->isPost()){
            $ids_str = Flight::request()->data['ids'];
            $special_id  = intval(Flight::request()->query->id);
            if(!$special_id){
                Flight::json(['code'=>403,'msg'=>'商品专栏无找到,无法删除']);
            }
            $ids = null;
            if($ids_str){
                $ids_ary  = explode(',',trim($ids_str,','));
                $id_array =  [];
                foreach ($ids_ary as $key => $value) {
                    $id_array[] = (int)$value;
                }
                $ids = implode(',',$id_array);
            }
            if(empty($ids)){
                Flight::json(['code'=>403,'msg'=>'没有选择任何商品']);
            }
            $result = $this->special->spu_delete($ids,$special_id,$this->website['id']);
            if($result){
                Flight::json(['code'=>200,'msg'=>'删除成功','data'=>'']);
            }else{
                Flight::json(['code'=>403,'msg'=>'删除失败,批量操作不排除有成功的。']);
            } 
        }
        Flight::notFound();
    }
}