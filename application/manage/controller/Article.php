<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        文章管理
 */
namespace application\manage\controller;
use framework\Flight;

class Article extends Common{

    private $index;
    private $channel;
    private $special;

    public function __construct() {
        parent::__construct();
        $this->index   = Flight::model('article/index');
        $this->channel = Flight::model('article/Channel');
        $this->special = Flight::model('article/special');
        $this->assign('leftnav',Flight::api('manage/menu')->leftnav('article'));  //引入管理菜单
    }

    //列表
    public function index(){
        $cid  = (int)Flight::request()->query->cid;
        $page = (int)Flight::request()->query->page;
        $pathMaps = $this->index->manage_select_path($cid);
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 添加内容','url'=>url('manage/article/add',['cid' => $cid])];
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $channel = $this->channel->select_list_all(["website_id = {$this->website['id']} or website_id = 0"]); 
        if($channel){
            foreach ($channel as $key => $value) {
                $channel[$value['id']]['name'] =  $value['name'];
                $channel[$value['id']]['url']  =  $value['url'];
            }
        }
        $tpl_date['lists']   = $this->index->select_list($cid,$page,$this->website['id']);
        $tpl_date['pager']   = $this->getPage($this->index->pager,array('cid'=>$cid));
        $tpl_date['channel'] = $channel;
        $this->display(0,$tpl_date,'layout');
    }

    //添加编辑内容
    public function add(){
        $cid      = (int)Flight::request()->query->cid;
        $pathMaps = $this->index->manage_select_path($cid);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['cid']      = $cid;
        $this->display(0,$tpl_date,'layout');
    }

    //添加编辑内容
    public function edit(){
        $id      = (int)Flight::request()->query->id;
        $info    = $this->index->info_find($id);
        $pathMaps = $this->index->manage_select_path($info['channel_id']);
        $tpl_date['info']     = $info;
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $this->display(0,$tpl_date,'layout');
    }

    /**
     * 保存提交数据
     * @return [type] [description]
     */
    public function savedata(){
        if($this->isPost()){
            $rules = [
                'title'       => ['empty','标题必须填写'],
                'channel_id'  => ['empty','内容栏目必须选择'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request  =  Flight::request()->data;
            $request['website_id'] = (int)$this->website['id'];
            $result =  $this->index->info_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('manage/article/index',['cid'=>$request['channel_id']])],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }
    }

    //删除
    public function delete(){
        $id = Flight::request()->query->id;
        $result =  $this->index->info_delete($id);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        }
    }

    /**
     *读取栏目列表返回数据
     */
    public function channelids() {
        $channel_id  = (int)Flight::request()->query->channel_id;
        $website_id  = (int)$this->website['id'];
        $info = $this->channel->select_lists_ids($channel_id,$website_id);
        Flight::json($info);
    } 

    /**
     * 读取属性列表
     * @access public
     */
    public function specialids() {
        $special_ids = Flight::request()->query->special_ids;
        $website_id  = (int)$this->website['id'];
        $info = $this->special->select_lists_ids($special_ids,$website_id);
        Flight::json($info);
    }
}