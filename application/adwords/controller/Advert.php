<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        广告
 */
namespace application\adwords\controller;
use framework\Flight;

class Advert extends Common {

    private $advert;
    private $website;

    public function __construct() {
        parent::__construct();
        $this->advert  = Flight::model('advert');
        $this->website = Flight::model('common/website');
    }

    /**
     * 列表
     * @access public
     */
    public function index() {
        $position_id = (int)Flight::request()->query->position_id;
        $info   = Flight::model('position')->info_find($position_id);
        $pathMaps[] = ['name' => '<i class="iconfont icon-home"></i> 广告位', 'url' => url('adwords/position/index')];
        if($info['website_id']){
            $website = $this->website->select_find($info['website_id']);
            $pathMaps[] = [
                'name' => '&nbsp;>&nbsp;'.$website['name'].'[站点]', 
                'url' => url('adwords/position/index',['website_id' =>$website['id']])
            ];
        }
        $pathMaps[] = ['name' => '&nbsp;>&nbsp;'.$info['title'],'url' => url('adwords/advert/index',['position_id' =>$position_id])];
        $editMenu = [
            ['name' => '<i class="iconfont icon-cjsqm"></i> 添加广告', 'url' => url('adwords/advert/edit',['position_id'=>$position_id])]
        ];
        $tpl_date['advert']   = $this->advert->select_list(['position_id' => $position_id]);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = Flight::model('common/website')->website_name();
        $this->display(0, $tpl_date, 'layout');
    }

    /**
     * 添加
     * @access public
     */
    public function edit() {
        if($this->isPost()){
            $rules = [
                'position_id' => ['empty'],
                'website_id'  => ['Require'],
                'name'        => ['empty'],
                'picture'     => ['empty'],
                'link'        => ['empty'],
                'contents'    => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $result  = $this->advert->info_edit($request);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('adwords/advert/index',['position_id' =>$request['position_id']])],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            $id = (int)Flight::request()->query->id;
            if($id){
                $tpl_date['info'] = $this->advert->info_find($id);
                $position_id = $tpl_date['info']['position_id'];
            }else{
                $position_id = (int)Flight::request()->query->position_id;
            }
            $position = Flight::model('position')->info_find($position_id);
            $pathMaps[] = ['name' => '<i class="iconfont icon-home"></i> 广告位', 'url' => url('adwords/position/index')];
            if($position['website_id']){
                $website = $this->website->select_find($position['website_id']);
                $pathMaps[] = [
                    'name' => '&nbsp;>&nbsp;'.$website['name'].'[站点]', 
                    'url' => url('adwords/position/index',['website_id' =>$website['id']])
                ];
                $tpl_date['website_id']  = $position['website_id'];
            }else{
                $tpl_date['website'] = $this->website->select_list();
            }
            $pathMaps[] = ['name' => '&nbsp;>&nbsp;'.$position['title'], 'url' => url('adwords/advert/index',['position_id' =>$position_id])];
            $tpl_date['pathMaps']    = json_encode($pathMaps);
            $tpl_date['position_id'] = $position_id;
            $this->display(0, $tpl_date, 'layout');
        }
    } 
    
    /**
     * 删除
     * @access public
     */
    public function delete() {
        $param['id'] = Flight::request()->query->id;
        $result =  $this->advert->info_delete($param);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }
}