<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        广告位
 */
namespace application\adwords\controller;
use framework\Flight;

class Position extends Common{

    private $position;
    private $website;

    public function __construct() {
        parent::__construct();
        $this->position = Flight::model('position');
        $this->website  = Flight::model('common/website');
    }

    /**
     * 列表
     * @access public
     */
    public function index(){
        $website_id = (int)Flight::request()->query->website_id;
        $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 广告位','url'=>url('adwords/position/index')];
        $editMenu[] = ['name'=>'<i class="iconfont icon-cjsqm"></i> 添加广告位','url'=>url('adwords/position/edit')];
        $website = Flight::model('common/website')->website_name();
        if($website_id){
            $pathMaps[] = [
                'name' => '&nbsp;>&nbsp;'.$website[$website_id]['name'].'[站点]', 
                'url' => url('adwords/position/index',['website_id' =>$website['id']])
            ];
        }
        $page = (int)Flight::request()->query->page;
        $tpl_date['position'] = $this->position->select_list($website_id,$page);
        $tpl_date['pager']    = $this->getPage($this->position->pager);
        $tpl_date['pathMaps'] = json_encode($pathMaps);
        $tpl_date['editMenu'] = json_encode($editMenu);
        $tpl_date['website']  = $website;
        $this->display(0,$tpl_date,'layout');
    }
    
    /**
     * 添加权限节点
     * @access public
     */
    public function edit() {
        if($this->isPost()){
            $rules = [
                'title'    => ['empty'],
                'contents' => ['empty'],
            ];
            $rel = Flight::validator($rules);
            if($rel['code'] == 403){
                exit(Flight::json(['code'=>403,'msg'=>$rel['msg']]));
            }
            $request =  Flight::request()->data;
            $data['id']         = $request['id'];
            $data['title']      = $request['title'];
            $data['website_id'] = $request['website_id'];
            $data['contents']   = $request['contents'];
            $result  = $this->position->info_edit($data);
            if($result){
                Flight::json(['code'=>200,'data'=>['url'=>url('adwords/position/index')],'msg'=>'操作成功']);
            }else{
                Flight::json(['code'=>403,'msg'=>'操作失败']);
            }
        }else{
            //菜单开始
            $pathMaps[] = ['name'=>'<i class="iconfont icon-home"></i> 广告位','url'=>url('adwords/position/index')];
            $pathMaps[] = ['name'=>'&nbsp;>&nbsp; 添加/编辑','url'=>'javascript:;'];
            $tpl_date['pathMaps']  = json_encode($pathMaps);
            //菜单结束
            $tpl_date['website']   = $this->website->select_list();
            $tpl_date['parent_id'] = (int)Flight::request()->query->id;
            $tpl_date['info']      = $this->position->info_find($tpl_date['parent_id']);
            $this->display(0,$tpl_date,'layout');
        }
    } 


    //删除
    public function delete(){
        $id   = Flight::request()->query->id;
        $info = Flight::model('advert')->where(['position_id'=>$id])->find();
        if($info){
            Flight::json(['code'=>403,'msg'=>'删除失败,请先删除包含的广告资源']);
            return;
        }
        $result =  $this->position->info_delete(['id' => $id]);
        if($result){
            Flight::json(['code'=>200,'msg'=>'删除成功']);
        }else{
            Flight::json(['code'=>403,'msg'=>'删除失败']);
        } 
    }
}