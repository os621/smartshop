<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        广告位
 */
namespace application\adwords\model;
use application\base\model\Base;

class Advert extends Base{

    protected $table = 'ads_advert';
  
    //列表
    public function select_list($condition){
         return $this->where($condition)->order('id desc')->select();
    }

     //标签调用列表
    public function website_select_list($id,$website_id){
        $condition['position_id'] = intval($id);
        $condition[] = '(website_id = '.$website_id.' or website_id = 0)';
        return $this->where($condition)->order('id desc')->select();
    }  

    //查找单个
    public function info_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['position_id'] = $param['position_id'];
        $data['website_id']  = $param['website_id'];
        $data['name']        = $param['name'];
        $data['picture']     = $param['picture'];
        $data['link']        = $param['link'];
        $data['contents']    = $param['contents'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    }  

    //删除
    public function info_delete($param){
        return $this->where($param)->delete();
    }
}