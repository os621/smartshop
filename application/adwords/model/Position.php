<?php
/**
 * Smart-B2B2C: An Extensible Content Management System.
 * @copyright   Copyright (c) 2017 http://www.brsttech.com All rights reserved.
 * @license     Licensed (http://www.apache.org/licenses/LICENSE-2.0).
 * @name        广告位
 */
namespace application\adwords\model;
use application\base\model\Base;

class Position extends Base{

    protected $table  = 'ads_position';

    //列表
    public function select_list($website_id,$page = 0){
        $condition = [];
        if($website_id){
            $condition[] = 'website_id = '.$website_id.' or website_id = 0';
        };
        return $this->where($condition)->pager((int)$page,25)->order('id desc')->select();
    }

    //查找单个
    public function info_find($id){
        $condition['id']= (int)$id;
        return $this->where($condition)->find();
    } 

    //添加或编辑
    public function info_edit($param){
        $data['title']       = $param['title'];
        $data['contents']    = $param['contents'];
        $data['website_id']  = $param['website_id'];
        $data['update_time'] = time();
        if($param['id']){
            $condition['id']= $param['id'];
            return $this->where($condition)->data($data)->update();
        }else{
            $data['create_time'] = time();
            return $this->data($data)->insert($data);
        }
    }  

    //删除
    public function info_delete($param){
        return $this->where($param)->delete();
    }
}